 START MY SQL
		mysql.sever start

DATABASE mysql
	NB : db.sqlite3 is not used

# HHMOTiF
* Author (1st scientifically published version): Roman Prytuliak
* Current maintainer/coauthor (preparing 1st release) : Bianca Habermann 
* Previous version uploaded and maintained by Edlira Nano


HHMOTiF is a software performing de novo detection of short linear motifs in proteins by
Hidden Markov Model comparisons.

HHMOTiF has been published and can be cited as : 
HH-MOTiF, R. Prytuliak, M. Volkmer, M. Meier, B. Habermann, in 
`Nucleic Acids Research, Volume 45, Issue W1, 3 July 2017, Pages W470–W477', [DOI](https://doi.org/10.1093/nar/gkx341)

More info to come here soon ...
