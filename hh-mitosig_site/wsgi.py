"""
WSGI config for hhmitosig_site project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""
import sys
sys.path.append('/usr/local/lib.python3.6/dist-packages/django')
import os
import django
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hhmitosig_site.settings")

application = get_wsgi_application()
