Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:23 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_65A71_65Q71_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P23004_14_IM_ref_sig5_130        0.9      27   0.063    6.2   0.0    1    5-5      11-11  (19)
  2 P16451_30_MM_ref_sig5_130        0.8      29   0.066    7.1   0.0    3    3-5       7-9   (35)
  3 P09440_34_Mito_ref_sig5_130      0.8      29   0.068    7.2   0.0    3    3-5      25-27  (39)
  4 Q04728_8_MM_ref_sig5_130         0.8      30   0.069    5.7   0.0    3    3-5       2-4   (13)
  5 Q0QF01_42_IM_ref_sig5_130        0.8      32   0.073    7.2   0.0    1    5-5       8-8   (47)
  6 P07756_38_Mito_Nu_ref_sig5_130   0.7      33   0.076    4.8   0.0    1    2-2      43-43  (43)
  7 P10817_9_IM_ref_sig5_130         0.7      34   0.077    5.6   0.0    2    5-6       6-7   (14)
  8 P11325_9_MM_ref_sig5_130         0.7      36   0.082    5.6   0.0    1    5-5       3-3   (14)
  9 P00842_66_MitoM_ref_sig5_130     0.7      36   0.083    7.7   0.0    1    4-4      21-21  (71)
 10 P29147_46_MM_ref_sig5_130        0.7      37   0.085    4.3   0.0    1    1-1       1-1   (51)
 11 P16276_27_Mito_ref_sig5_130      0.7      38   0.087    6.6   0.0    1    4-4      22-22  (32)
 12 P83484_51_Mito_IM_ref_sig5_130   0.7      38   0.087    5.8   0.0    1    2-2      56-56  (56)
 13 P07471_12_IM_ref_sig5_130        0.6      40   0.092    5.7   0.0    2    5-6       9-10  (17)
 14 P25846_21_Mito_ref_sig5_130      0.6      44     0.1    6.2   0.0    1    5-5      15-15  (26)
 15 P10818_26_IM_ref_sig5_130        0.6      46     0.1    6.3   0.0    1    4-4      11-11  (31)
 16 P01098_23_Mito_ref_sig5_130      0.5      48    0.11    6.2   0.0    1    5-5       7-7   (28)
 17 P42028_36_Mito_ref_sig5_130      0.5      48    0.11    6.6   0.0    2    2-3      38-39  (41)
 18 Q40089_21_Mito_IM_ref_sig5_130   0.5      48    0.11    6.0   0.0    1    5-5      10-10  (26)
 19 Q96E29_68_Mito_ref_sig5_130      0.5      49    0.11    4.0   0.0    1    1-1       1-1   (73)
 20 P07257_16_IM_ref_sig5_130        0.5      49    0.11    5.6   0.0    1    4-4      19-19  (21)

No 1  
>P23004_14_IM_ref_sig5_130
Probab=0.88  E-value=27  Score=6.23  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S.    5 (6)
Q Consensus         5 s.    5 (6)
                      | 
T Consensus        11 s.   11 (19)
T signal           11 S.   11 (19)
T 83               11 S.   11 (19)
T 38                8 K.    8 (16)
T 44                8 S.    8 (16)
T 45                9 K.    9 (17)
T 46                8 A.    8 (16)
T 39                9 Sk   10 (18)
T 47                9 S.    9 (17)
T 51                9 Sk   10 (18)
T 40               11 Sr   12 (20)
Confidence            2 


No 2  
>P16451_30_MM_ref_sig5_130
Probab=0.84  E-value=29  Score=7.09  Aligned_cols=3  Identities=100%  Similarity=0.977  Sum_probs=1.1

Q 5_Mustela         3 KVS    5 (6)
Q Consensus         3 kvs    5 (6)
                      |||
T Consensus         7 kvs    9 (35)
T signal            7 KVS    9 (35)
Confidence            343


No 3  
>P09440_34_Mito_ref_sig5_130
Probab=0.82  E-value=29  Score=7.20  Aligned_cols=3  Identities=100%  Similarity=0.977  Sum_probs=1.2

Q 5_Mustela         3 KVS    5 (6)
Q Consensus         3 kvs    5 (6)
                      |||
T Consensus        25 kvs   27 (39)
T signal           25 KVS   27 (39)
Confidence            343


No 4  
>Q04728_8_MM_ref_sig5_130
Probab=0.81  E-value=30  Score=5.67  Aligned_cols=3  Identities=33%  Similarity=0.888  Sum_probs=1.4

Q 5_Mustela         3 KVS    5 (6)
Q Consensus         3 kvs    5 (6)
                      |+|
T Consensus         2 kis    4 (13)
T signal            2 RIS    4 (13)
T 90                2 KIS    4 (13)
T 88                2 KIS    4 (13)
Confidence            444


No 5  
>Q0QF01_42_IM_ref_sig5_130
Probab=0.77  E-value=32  Score=7.21  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.4

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      |
T Consensus         8 S    8 (47)
T signal            8 S    8 (47)
T 124               8 S    8 (47)
T 126               8 S    8 (43)
T 136               9 S    9 (50)
T 137               8 S    8 (46)
T 144               8 S    8 (48)
T 145               5 A    5 (47)
T 171               1 -    0 (44)
T 154               1 -    0 (30)
Confidence            3


No 6  
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.74  E-value=33  Score=4.81  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      -
T Consensus        43 ~   43 (43)
T signal           43 A   43 (43)
T 79_Emiliania     43 -   42 (42)
T 77_Thalassiosi   43 -   42 (42)
T 60_Anolis        43 -   42 (42)
T 48_Alligator     43 -   42 (42)
T 44_Papio         43 -   42 (42)
T 28_Ovis          43 -   42 (42)
T 18_Pongo         43 -   42 (42)
T cl|HABBABABA|1   53 -   52 (52)
T 72_Branchiosto   42 -   41 (41)
Confidence            0


No 7  
>P10817_9_IM_ref_sig5_130
Probab=0.73  E-value=34  Score=5.64  Aligned_cols=2  Identities=100%  Similarity=1.149  Sum_probs=0.9

Q 5_Mustela         5 SR    6 (6)
Q Consensus         5 sr    6 (6)
                      ||
T Consensus         6 sR    7 (14)
T signal            6 SR    7 (14)
T 14                6 NR    7 (14)
T 18                6 SR    7 (14)
T 22                6 SR    7 (14)
T 23                6 GR    7 (14)
T 46                6 NR    7 (14)
T 4                 6 SR    7 (14)
Confidence            44


No 8  
>P11325_9_MM_ref_sig5_130
Probab=0.69  E-value=36  Score=5.61  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      |
T Consensus         3 s    3 (14)
T signal            3 S    3 (14)
Confidence            3


No 9  
>P00842_66_MitoM_ref_sig5_130
Probab=0.68  E-value=36  Score=7.71  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         4 V    4 (6)
Q Consensus         4 v    4 (6)
                      |
T Consensus        21 v   21 (71)
T signal           21 V   21 (71)
T 99               19 V   19 (68)
T 98               19 A   19 (70)
T 91               21 V   21 (81)
T 93               19 A   19 (69)
T 94               22 A   22 (68)
T 88               21 V   21 (74)
T 86               13 V   13 (66)
T 89               16 V   16 (74)
T 85               18 V   18 (75)
Confidence            2


No 10 
>P29147_46_MM_ref_sig5_130
Probab=0.67  E-value=37  Score=4.29  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 77_Aplysia        1 M    1 (50)
T 61_Papio          1 V    1 (50)
T 19_Tupaia         1 M    1 (50)
T 80_Tribolium      1 M    1 (50)
T 79_Apis           1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 54_Canis          1 M    1 (50)
T 39_Nomascus       1 M    1 (50)
Confidence            0


No 11 
>P16276_27_Mito_ref_sig5_130
Probab=0.66  E-value=38  Score=6.59  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         4 V    4 (6)
Q Consensus         4 v    4 (6)
                      |
T Consensus        22 V   22 (32)
T signal           22 V   22 (32)
T 50               24 L   24 (34)
T 97               24 I   24 (34)
T 111              24 V   24 (34)
T 112              22 V   22 (32)
T 113              26 V   26 (36)
T 122              28 V   28 (38)
T 115              23 V   23 (33)
T 105              24 V   24 (34)
T 108              24 V   24 (34)
Confidence            3


No 12 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.65  E-value=38  Score=5.84  Aligned_cols=1  Identities=0%  Similarity=-1.028  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      -
T Consensus        56 ~   56 (56)
T signal           56 P   56 (56)
T 34_Volvox        39 -   38 (38)
T 134_Guillardia   39 -   38 (38)
T 152_Trichinell   39 -   38 (38)
T 159_Ixodes       39 -   38 (38)
T 41_Galdieria     39 -   38 (38)
T 112_Tribolium    39 -   38 (38)
T 117_Ceratitis    39 -   38 (38)
T 111_Musca        39 -   38 (38)
T 39_Selaginella   39 -   38 (38)
Confidence            0


No 13 
>P07471_12_IM_ref_sig5_130
Probab=0.63  E-value=40  Score=5.73  Aligned_cols=2  Identities=100%  Similarity=1.149  Sum_probs=0.8

Q 5_Mustela         5 SR    6 (6)
Q Consensus         5 sr    6 (6)
                      ||
T Consensus         9 sR   10 (17)
T signal            9 SR   10 (17)
T 8                 9 NR   10 (17)
T 15                9 SR   10 (17)
T 17                9 GR   10 (17)
T 18                9 SR   10 (17)
T 42                9 NR   10 (17)
T 3                 9 SR   10 (17)
Confidence            43


No 14 
>P25846_21_Mito_ref_sig5_130
Probab=0.58  E-value=44  Score=6.20  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      |
T Consensus        15 s   15 (26)
T signal           15 S   15 (26)
Confidence            2


No 15 
>P10818_26_IM_ref_sig5_130
Probab=0.56  E-value=46  Score=6.32  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         4 V    4 (6)
Q Consensus         4 v    4 (6)
                      |
T Consensus        11 v   11 (31)
T signal           11 V   11 (31)
T 53                8 V    8 (28)
T 56                8 V    8 (28)
T 64                8 L    8 (28)
T 36               10 L   10 (30)
T 44                8 V    8 (28)
T 55                8 L    8 (28)
T 6                10 V   10 (30)
T 7                 9 L    9 (29)
T 43                6 L    6 (25)
Confidence            2


No 16 
>P01098_23_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.18  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      |
T Consensus         7 s    7 (28)
T signal            7 S    7 (28)
Confidence            3


No 17 
>P42028_36_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.57  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=1.0

Q 5_Mustela         2 YK    3 (6)
Q Consensus         2 yk    3 (6)
                      ||
T Consensus        38 YK   39 (41)
T signal           38 YK   39 (41)
T 142              34 YK   35 (37)
T 152              36 YK   37 (39)
T 172              36 YK   37 (39)
T 180              36 YK   37 (39)
T 162              36 YK   37 (39)
T 113              32 YK   33 (35)
T 147              33 YK   34 (36)
T 140              32 YK   33 (35)
T 144              33 YK   34 (36)
Confidence            44


No 18 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=0.53  E-value=48  Score=6.01  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.3

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      +
T Consensus        10 a   10 (26)
T signal           10 A   10 (26)
T 44               10 A   10 (26)
T 43               10 A   10 (26)
T 40               11 S   11 (23)
T 41               11 S   11 (23)
T 3                10 A   10 (24)
T 5                10 S   10 (24)
T 10               10 A   10 (24)
T 11               10 A   10 (24)
Confidence            2


No 19 
>Q96E29_68_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=3.98  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (73)
T signal            1 M    1 (73)
T 93_Musca          1 M    1 (72)
T 107_Ciona         1 M    1 (72)
T 58_Taeniopygia    1 M    1 (72)
T 112_Loa           1 M    1 (72)
T 77_Takifugu       1 M    1 (72)
T 85_Acyrthosiph    1 M    1 (72)
Confidence            0


No 20 
>P07257_16_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=5.64  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         4 V    4 (6)
Q Consensus         4 v    4 (6)
                      |
T Consensus        19 I   19 (21)
T signal           19 V   19 (21)
T 20               16 V   16 (18)
T 34               16 I   16 (18)
T 14               17 I   17 (19)
T 15               18 I   18 (20)
T 23               20 I   20 (22)
T 19               18 V   18 (20)
T 40               20 I   20 (22)
T 42               18 I   18 (20)
Confidence            3


Done!
