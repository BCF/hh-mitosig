Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:12 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_97A103_97Q103_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9BYV1_41_Mito_ref_sig5_130      2.5     8.1   0.019    4.8   0.0    1    4-4       1-1   (46)
  2 P34899_31_Mito_ref_sig5_130      2.4     8.5    0.02    8.5   0.0    1    4-4       3-3   (36)
  3 P21953_50_MM_ref_sig5_130        2.4     8.6    0.02    4.8   0.0    1    1-1       1-1   (55)
  4 P41367_25_MM_ref_sig5_130        2.3       9   0.021    4.7   0.0    1    4-4       1-1   (30)
  5 P08503_25_MM_ref_sig5_130        2.2     9.3   0.021    4.7   0.0    1    4-4       1-1   (30)
  6 P11913_28_MM_ref_sig5_130        2.2     9.4   0.022    5.8   0.0    1    4-4       1-1   (33)
  7 Q0QF01_42_IM_ref_sig5_130        2.1     9.7   0.022    8.6   0.0    2    4-5       1-2   (47)
  8 P11024_43_IM_ref_sig5_130        2.0      10   0.024    4.5   0.0    1    4-4       1-1   (48)
  9 P11310_25_MM_ref_sig5_130        1.9      11   0.025    4.6   0.0    1    4-4       1-1   (30)
 10 Q0VC21_22_Mito_ref_sig5_130      1.8      12   0.027    7.6   0.0    2    4-5       1-2   (27)
 11 P30048_62_Mito_ref_sig5_130      1.8      12   0.027    6.1   0.0    1    4-4       1-1   (67)
 12 P23709_38_IM_ref_sig5_130        1.8      12   0.028    8.4   0.0    1    4-4       4-4   (43)
 13 P30404_30_MM_ref_sig5_130        1.7      13   0.029    5.1   0.0    1    4-4       2-2   (35)
 14 Q02380_46_IM_ref_sig5_130        1.6      13    0.03    8.5   0.0    1    4-4       4-4   (51)
 15 P31023_31_MM_ref_sig5_130        1.6      14   0.031    8.0   0.0    3    3-5       2-4   (36)
 16 Q99K67_32_Mito_ref_sig5_130      1.5      14   0.033    7.7   0.0    2    4-5      26-27  (37)
 17 Q9NR28_55_Mito_ref_sig5_130      1.5      15   0.034    5.5   0.0    1    4-4       1-1   (60)
 18 Q9Z0J5_36_Mito_ref_sig5_130      1.5      15   0.034    5.1   0.0    1    1-1       1-1   (41)
 19 P22570_32_MM_ref_sig5_130        1.5      15   0.035    4.6   0.0    1    4-4       1-1   (37)
 20 Q6UB35_31_Mito_ref_sig5_130      1.5      15   0.035    4.5   0.0    1    4-4       1-1   (36)

No 1  
>Q9BYV1_41_Mito_ref_sig5_130
Probab=2.50  E-value=8.1  Score=4.83  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (46)
T signal            1 M    1 (46)
T 122_Ixodes        1 M    1 (43)
T 118_Zea           1 M    1 (43)
T 94_Bombus         1 M    1 (43)
T 91_Nasonia        1 M    1 (43)
T 78_Musca          1 M    1 (43)
T 74_Strongyloce    1 M    1 (43)
T 64_Meleagris      1 M    1 (43)
T 58_Ficedula       1 M    1 (43)
Confidence            1


No 2  
>P34899_31_Mito_ref_sig5_130
Probab=2.39  E-value=8.5  Score=8.53  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         3 M    3 (36)
T signal            3 M    3 (36)
T 145               3 M    3 (36)
T 135               3 M    3 (35)
T 141               3 M    3 (36)
T 140               3 M    3 (35)
T 134               1 M    1 (34)
T 138               1 M    1 (35)
T 139               1 M    1 (33)
T 143               1 M    1 (34)
T 144               1 M    1 (34)
Confidence            3


No 3  
>P21953_50_MM_ref_sig5_130
Probab=2.36  E-value=8.6  Score=4.79  Aligned_cols=1  Identities=0%  Similarity=-1.161  Sum_probs=0.0

Q 5_Mustela         1 G    1 (6)
Q Consensus         1 g    1 (6)
                      =
T Consensus         1 M    1 (55)
T signal            1 M    1 (55)
T 145_Nectria       1 M    1 (53)
T 134_Cyanidiosc    1 M    1 (53)
T 123_Trichinell    1 K    1 (53)
T 111_Phytophtho    1 M    1 (53)
T 110_Toxoplasma    1 M    1 (53)
T 105_Glycine       1 M    1 (53)
T 104_Plasmodium    1 M    1 (53)
T 78_Loa            1 M    1 (53)
T 76_Trichoplax     1 G    1 (53)
Confidence            0


No 4  
>P41367_25_MM_ref_sig5_130
Probab=2.28  E-value=9  Score=4.66  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 102_Strongyloc    1 M    1 (28)
T 100_Leishmania    1 M    1 (28)
T 89_Caenorhabdi    1 M    1 (28)
T 88_Haplochromi    1 M    1 (28)
T 79_Cavia          1 M    1 (28)
T 76_Tribolium      1 M    1 (28)
T 71_Pediculus      1 M    1 (28)
T 64_Sarcophilus    1 T    1 (28)
T 41_Ficedula       1 M    1 (28)
Confidence            2


No 5  
>P08503_25_MM_ref_sig5_130
Probab=2.23  E-value=9.3  Score=4.69  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 96_Nematostell    1 M    1 (28)
T 51_Xenopus        1 M    1 (28)
T 47_Ornithorhyn    1 M    1 (28)
T 46_Geospiza       1 M    1 (28)
Confidence            2


No 6  
>P11913_28_MM_ref_sig5_130
Probab=2.20  E-value=9.4  Score=5.81  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (33)
T signal            1 M    1 (33)
T 156_Myotis        1 M    1 (28)
T 154_Vanderwalt    1 T    1 (28)
T 150_Anopheles     1 M    1 (28)
T 116_Zygosaccha    1 M    1 (28)
T 95_Cryptococcu    1 M    1 (28)
T 181_Tursiops      1 P    1 (28)
T 83_Coprinopsis    1 M    1 (28)
Confidence            2


No 7  
>Q0QF01_42_IM_ref_sig5_130
Probab=2.15  E-value=9.7  Score=8.65  Aligned_cols=2  Identities=50%  Similarity=0.899  Sum_probs=0.8

Q 5_Mustela         4 MA    5 (6)
Q Consensus         4 ma    5 (6)
                      ||
T Consensus         1 Ma    2 (47)
T signal            1 MS    2 (47)
T 124               1 MA    2 (47)
T 126               1 MA    2 (43)
T 136               1 MA    2 (50)
T 137               1 MA    2 (46)
T 144               1 MA    2 (48)
T 145               1 MA    2 (47)
T 171               1 --    0 (44)
T 154               1 --    0 (30)
Confidence            33


No 8  
>P11024_43_IM_ref_sig5_130
Probab=2.04  E-value=10  Score=4.50  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (48)
T signal            1 M    1 (48)
T 144_Moniliopht    1 M    1 (47)
T 141_Cryptospor    1 M    1 (47)
T 108_Ichthyopht    1 M    1 (47)
T 99_Aspergillus    1 M    1 (47)
T 97_Volvox         1 M    1 (47)
T 96_Schizophyll    1 M    1 (47)
Confidence            1


No 9  
>P11310_25_MM_ref_sig5_130
Probab=1.95  E-value=11  Score=4.63  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 81_Nematostell    1 M    1 (28)
T 80_Nasonia        1 M    1 (28)
T 70_Bombyx         1 M    1 (28)
T 64_Branchiosto    1 M    1 (28)
T 63_Aplysia        1 M    1 (28)
T 61_Saccoglossu    1 M    1 (28)
T 50_Columba        1 M    1 (28)
Confidence            2


No 10 
>Q0VC21_22_Mito_ref_sig5_130
Probab=1.84  E-value=12  Score=7.60  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.7

Q 5_Mustela         4 MA    5 (6)
Q Consensus         4 ma    5 (6)
                      ||
T Consensus         1 Ma    2 (27)
T signal            1 MA    2 (27)
T 137               1 MS    2 (23)
T 148               1 MG    2 (26)
T 170               1 MA    2 (26)
T 131               1 MA    2 (29)
T 120               1 MS    2 (25)
T 123               1 MS    2 (25)
T 126               1 MA    2 (25)
T 116               1 MA    2 (25)
T 96                1 MS    2 (28)
Confidence            33


No 11 
>P30048_62_Mito_ref_sig5_130
Probab=1.82  E-value=12  Score=6.09  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (67)
T signal            1 M    1 (67)
T 79_Drosophila     1 M    1 (67)
T 76_Meleagris      1 M    1 (67)
T 70_Strongyloce    1 M    1 (67)
T 69_Metaseiulus    1 M    1 (67)
T 63_Columba        1 Q    1 (67)
T 57_Falco          1 P    1 (67)
T 44_Camelus        1 M    1 (67)
Confidence            2


No 12 
>P23709_38_IM_ref_sig5_130
Probab=1.76  E-value=12  Score=8.40  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.3

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         4 m    4 (43)
T signal            4 A    4 (43)
T 202               1 M    1 (40)
T 205               1 M    1 (40)
T 217               1 M    1 (40)
T 220               1 M    1 (40)
T 192               1 M    1 (40)
T 196               1 M    1 (40)
T 197               1 M    1 (39)
T 225               1 M    1 (40)
T 191               2 M    2 (41)
Confidence            3


No 13 
>P30404_30_MM_ref_sig5_130
Probab=1.70  E-value=13  Score=5.10  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         2 M    2 (35)
T signal            2 M    2 (35)
T 81_Thalassiosi    1 S    1 (33)
T 79_Aedes          1 M    1 (33)
T 77_Acyrthosiph    1 M    1 (33)
T 66_Columba        1 M    1 (33)
T 44_Ornithorhyn    1 M    1 (33)
T 40_Equus          1 S    1 (33)
T 33_Pantholops     1 M    1 (33)
Confidence            2


No 14 
>Q02380_46_IM_ref_sig5_130
Probab=1.64  E-value=13  Score=8.52  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         4 M    4 (51)
T signal            4 M    4 (51)
T 55                4 M    4 (51)
T 59                4 M    4 (51)
T 51                4 M    4 (57)
T 94                4 M    4 (42)
T 49                4 M    4 (52)
T 46                4 M    4 (51)
T 42                4 M    4 (60)
T 24                4 M    4 (44)
T 40                4 M    4 (48)
Confidence            3


No 15 
>P31023_31_MM_ref_sig5_130
Probab=1.61  E-value=14  Score=7.99  Aligned_cols=3  Identities=100%  Similarity=1.010  Sum_probs=1.3

Q 5_Mustela         3 AMA    5 (6)
Q Consensus         3 ama    5 (6)
                      |||
T Consensus         2 ama    4 (36)
T signal            2 AMA    4 (36)
T 194               2 GLA    4 (38)
T 185               2 AMA    4 (37)
T 186               2 AMA    4 (46)
T 187               2 AIG    4 (35)
T 188               2 AMA    4 (45)
T 190               2 AMA    4 (45)
T 193               2 AMA    4 (35)
T 189               2 AMA    4 (42)
T 191               2 AMA    4 (41)
Confidence            444


No 16 
>Q99K67_32_Mito_ref_sig5_130
Probab=1.54  E-value=14  Score=7.74  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.8

Q 5_Mustela         4 MA    5 (6)
Q Consensus         4 ma    5 (6)
                      ||
T Consensus        26 mA   27 (37)
T signal           26 MA   27 (37)
T 74               28 LA   29 (39)
T 70               26 LA   27 (37)
T 111              24 LA   25 (35)
T 99               33 IA   34 (44)
T 103              30 MA   31 (41)
T 127              29 MA   30 (40)
T 57               32 IA   33 (43)
T 85               18 VA   19 (29)
Confidence            43


No 17 
>Q9NR28_55_Mito_ref_sig5_130
Probab=1.51  E-value=15  Score=5.53  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (60)
T signal            1 M    1 (60)
T 71_Takifugu       1 F    1 (57)
T 67_Haplochromi    1 M    1 (57)
T 65_Xiphophorus    1 M    1 (57)
T 58_Meleagris      1 M    1 (57)
T 57_Falco          1 M    1 (57)
T 38_Cricetulus     1 M    1 (57)
T cl|DABBABABA|1    1 M    1 (57)
T cl|KABBABABA|1    1 M    1 (57)
T cl|BEBBABABA|1    1 M    1 (57)
Confidence            2


No 18 
>Q9Z0J5_36_Mito_ref_sig5_130
Probab=1.48  E-value=15  Score=5.07  Aligned_cols=1  Identities=0%  Similarity=-1.161  Sum_probs=0.0

Q 5_Mustela         1 G    1 (6)
Q Consensus         1 g    1 (6)
                      =
T Consensus         1 M    1 (41)
T signal            1 M    1 (41)
T 85_Bombus         1 M    1 (34)
T 58_Orcinus        1 M    1 (34)
T 44_Chrysemys      1 M    1 (34)
T 30_Bos            1 M    1 (34)
Confidence            0


No 19 
>P22570_32_MM_ref_sig5_130
Probab=1.46  E-value=15  Score=4.63  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 90_Drosophila     1 M    1 (29)
T 84_Megachile      1 M    1 (29)
T 73_Nematostell    1 P    1 (29)
T 49_Columba        1 M    1 (29)
T 38_Myotis         1 M    1 (29)
Confidence            2


No 20 
>Q6UB35_31_Mito_ref_sig5_130
Probab=1.46  E-value=15  Score=4.50  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (6)
Q Consensus         4 m    4 (6)
                      |
T Consensus         1 M    1 (36)
T signal            1 M    1 (36)
T 44_Alligator      1 M    1 (35)
T 38_Ictidomys      1 M    1 (35)
T 25_Myotis         1 M    1 (35)
T 19_Tupaia         1 M    1 (35)
T 39_Condylura      1 M    1 (35)
Confidence            1


Done!
