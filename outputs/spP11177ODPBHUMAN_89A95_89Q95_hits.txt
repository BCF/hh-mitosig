Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:59 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_89A95_89Q95_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9ZP06_22_MM_ref_sig5_130        1.4      16   0.036    7.3   0.0    3    2-4      21-23  (27)
  2 Q6SZW1_27_Cy_Mito_ref_sig5_130   1.4      16   0.037    4.3   0.0    1    2-2      32-32  (32)
  3 P07471_12_IM_ref_sig5_130        1.4      16   0.038    6.7   0.0    3    2-4      11-13  (17)
  4 P30099_34_MitoM_ref_sig5_130     1.4      17   0.038    6.0   0.0    1    2-2      39-39  (39)
  5 P22570_32_MM_ref_sig5_130        1.4      17   0.038    4.6   0.0    1    2-2      37-37  (37)
  6 Q9UGC7_26_Mito_ref_sig5_130      1.3      17   0.038    3.5   0.0    1    2-2      31-31  (31)
  7 P11913_28_MM_ref_sig5_130        1.3      18    0.04    5.2   0.0    1    3-3      29-29  (33)
  8 A4FUC0_29_Mito_ref_sig5_130      1.1      21   0.048    3.6   0.0    1    2-2      34-34  (34)
  9 P07919_13_IM_ref_sig5_130        1.1      21   0.048    6.4   0.0    2    1-2       1-2   (18)
 10 Q96CB9_25_Mito_ref_sig5_130      1.1      21   0.049    3.6   0.0    1    2-2      30-30  (30)
 11 P86924_17_Mito_ref_sig5_130      1.1      22    0.05    6.4   0.0    1    1-1       1-1   (22)
 12 P37841_53_IM_ref_sig5_130        1.1      22    0.05    8.0   0.0    3    2-4      52-54  (58)
 13 Q3ZBF3_26_Mito_ref_sig5_130      1.1      22    0.05    4.0   0.0    1    2-2      31-31  (31)
 14 P11024_43_IM_ref_sig5_130        1.0      23   0.054    3.8   0.0    1    2-2      48-48  (48)
 15 P17783_27_MM_ref_sig5_130        1.0      25   0.057    7.1   0.0    3    2-4      26-28  (32)
 16 P01096_25_Mito_ref_sig5_130      1.0      25   0.057    7.0   0.0    2    2-3      24-25  (30)
 17 P41367_25_MM_ref_sig5_130        0.9      25   0.058    3.7   0.0    1    2-2      30-30  (30)
 18 P35571_42_Mito_ref_sig5_130      0.9      26   0.059    5.3   0.0    1    1-1       1-1   (47)
 19 P08165_32_MM_ref_sig5_130        0.9      26    0.06    4.2   0.0    1    2-2      37-37  (37)
 20 P09622_35_MM_ref_sig5_130        0.9      27   0.061    3.6   0.0    1    2-2      40-40  (40)

No 1  
>Q9ZP06_22_MM_ref_sig5_130
Probab=1.44  E-value=16  Score=7.32  Aligned_cols=3  Identities=33%  Similarity=0.944  Sum_probs=1.5

Q 5_Mustela         2 GFA    4 (6)
Q Consensus         2 gfa    4 (6)
                      |||
T Consensus        21 ~~a   23 (27)
T signal           21 SFS   23 (27)
T 93               21 GFA   23 (27)
T 95               21 GYA   23 (27)
T 97               22 TFA   24 (28)
T 91               25 SYC   27 (31)
T 94               20 GYA   22 (26)
T 98               20 GYA   22 (26)
Confidence            454


No 2  
>Q6SZW1_27_Cy_Mito_ref_sig5_130
Probab=1.40  E-value=16  Score=4.30  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      +
T Consensus        32 ~   32 (32)
T signal           32 G   32 (32)
T 93_Amphimedon    32 I   32 (32)
T 87_Branchiosto   32 V   32 (32)
T 83_Aplysia       32 N   32 (32)
T 81_Aedes         32 G   32 (32)
T 61_Chrysemys     32 L   32 (32)
T 57_Ficedula      32 I   32 (32)
T 52_Falco         32 V   32 (32)
Confidence            0


No 3  
>P07471_12_IM_ref_sig5_130
Probab=1.37  E-value=16  Score=6.70  Aligned_cols=3  Identities=67%  Similarity=1.221  Sum_probs=1.4

Q 5_Mustela         2 GFA    4 (6)
Q Consensus         2 gfa    4 (6)
                      |+|
T Consensus        11 glA   13 (17)
T signal           11 GLA   13 (17)
T 8                11 GLA   13 (17)
T 15               11 SMA   13 (17)
T 17               11 GLA   13 (17)
T 18               11 GLA   13 (17)
T 42               11 GFA   13 (17)
T 3                11 GLA   13 (17)
Confidence            454


No 4  
>P30099_34_MitoM_ref_sig5_130
Probab=1.36  E-value=17  Score=5.99  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      .
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 46_Amphimedon    28 S   28 (28)
T 44_Anolis        28 V   28 (28)
T 42_Monodelphis   28 G   28 (28)
T 39_Sarcophilus   28 A   28 (28)
T 35_Mustela       28 V   28 (28)
T 7_Heterocephal   28 R   28 (28)
T cl|DABBABABA|1   29 -   28 (28)
T cl|RABBABABA|1   29 -   28 (28)
T cl|VABBABABA|2   29 -   28 (28)
Confidence            0


No 5  
>P22570_32_MM_ref_sig5_130
Probab=1.36  E-value=17  Score=4.55  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        37 ~   37 (37)
T signal           37 K   37 (37)
T 90_Drosophila    30 -   29 (29)
T 84_Megachile     30 -   29 (29)
T 73_Nematostell   30 -   29 (29)
T 49_Columba       30 -   29 (29)
T 38_Myotis        30 -   29 (29)
Confidence            0


No 6  
>Q9UGC7_26_Mito_ref_sig5_130
Probab=1.35  E-value=17  Score=3.47  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 119_Populus      31 -   30 (30)
T 107_Phytophtho   31 -   30 (30)
T 101_Setaria      31 -   30 (30)
T 82_Anopheles     31 -   30 (30)
T 69_Chrysemys     31 -   30 (30)
T 55_Geospiza      31 -   30 (30)
Confidence            0


No 7  
>P11913_28_MM_ref_sig5_130
Probab=1.29  E-value=18  Score=5.20  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 F    3 (6)
Q Consensus         3 f    3 (6)
                      |
T Consensus        29 ~   29 (33)
T signal           29 L   29 (33)
T 156_Myotis       29 -   28 (28)
T 154_Vanderwalt   29 -   28 (28)
T 150_Anopheles    29 -   28 (28)
T 116_Zygosaccha   29 -   28 (28)
T 95_Cryptococcu   29 -   28 (28)
T 181_Tursiops     29 -   28 (28)
T 83_Coprinopsis   29 -   28 (28)
Confidence            2


No 8  
>A4FUC0_29_Mito_ref_sig5_130
Probab=1.11  E-value=21  Score=3.62  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      +
T Consensus        34 ~   34 (34)
T signal           34 G   34 (34)
T 83_Ceratitis     34 A   34 (34)
T 70_Xiphophorus   34 N   34 (34)
T 62_Anolis        34 G   34 (34)
T 61_Geospiza      34 E   34 (34)
T 60_Anas          34 V   34 (34)
T 59_Zonotrichia   34 K   34 (34)
T 57_Columba       34 E   34 (34)
T 55_Falco         34 A   34 (34)
T 43_Dasypus       34 P   34 (34)
Confidence            0


No 9  
>P07919_13_IM_ref_sig5_130
Probab=1.11  E-value=21  Score=6.41  Aligned_cols=2  Identities=100%  Similarity=1.813  Sum_probs=0.8

Q 5_Mustela         1 MG    2 (6)
Q Consensus         1 mg    2 (6)
                      ||
T Consensus         1 Mg    2 (18)
T signal            1 MG    2 (18)
T 41                1 MG    2 (18)
T 13                1 MG    2 (18)
T 23                1 MG    2 (18)
T 58                1 MV    2 (17)
T 61                1 MV    2 (18)
T 18                1 MG    2 (18)
T 22                1 MG    2 (18)
T 59                1 MG    2 (18)
T 79                1 MG    2 (15)
Confidence            34


No 10 
>Q96CB9_25_Mito_ref_sig5_130
Probab=1.09  E-value=21  Score=3.57  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        30 ~   30 (30)
T signal           30 K   30 (30)
T 100_Pediculus    30 F   30 (30)
T 96_Nematostell   30 V   30 (30)
T 80_Bombus        30 L   30 (30)
T 78_Aedes         30 T   30 (30)
T 63_Columba       30 H   30 (30)
Confidence            0


No 11 
>P86924_17_Mito_ref_sig5_130
Probab=1.08  E-value=22  Score=6.35  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         1 M    1 (6)
Q Consensus         1 m    1 (6)
                      |
T Consensus         1 M    1 (22)
T signal            1 M    1 (22)
T 4_Naegleria       1 M    1 (22)
T 2_Leishmania      1 M    1 (22)
T cl|CABBABABA|1    1 M    1 (22)
T cl|FABBABABA|1    1 M    1 (22)
Confidence            2


No 12 
>P37841_53_IM_ref_sig5_130
Probab=1.07  E-value=22  Score=7.98  Aligned_cols=3  Identities=67%  Similarity=1.630  Sum_probs=1.4

Q 5_Mustela         2 GFA    4 (6)
Q Consensus         2 gfa    4 (6)
                      ||+
T Consensus        52 GFs   54 (58)
T signal           52 GFS   54 (58)
T 193              59 GFS   61 (65)
T 194              59 GFS   61 (64)
T 190              66 GFS   68 (72)
T 181              59 GFS   61 (63)
T 188              61 GFS   63 (65)
T 180              39 ---   38 (38)
T 184              60 GFA   62 (66)
T 182              61 GFA   63 (67)
Confidence            443


No 13 
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=1.07  E-value=22  Score=4.00  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 14 
>P11024_43_IM_ref_sig5_130
Probab=1.00  E-value=23  Score=3.77  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      .
T Consensus        48 ~   48 (48)
T signal           48 V   48 (48)
T 144_Moniliopht   48 -   47 (47)
T 141_Cryptospor   48 -   47 (47)
T 108_Ichthyopht   48 -   47 (47)
T 99_Aspergillus   48 -   47 (47)
T 97_Volvox        48 -   47 (47)
T 96_Schizophyll   48 -   47 (47)
Confidence            0


No 15 
>P17783_27_MM_ref_sig5_130
Probab=0.96  E-value=25  Score=7.09  Aligned_cols=3  Identities=67%  Similarity=1.088  Sum_probs=1.3

Q 5_Mustela         2 GFA    4 (6)
Q Consensus         2 gfa    4 (6)
                      |||
T Consensus        26 ~fa   28 (32)
T signal           26 SFA   28 (32)
T 120              29 SYC   31 (35)
T 121              21 GYA   23 (27)
T 123              21 SFS   23 (27)
T 118              21 GFA   23 (27)
T 122              24 GYA   26 (30)
T 125              25 GYA   27 (31)
Confidence            444


No 16 
>P01096_25_Mito_ref_sig5_130
Probab=0.96  E-value=25  Score=6.98  Aligned_cols=2  Identities=100%  Similarity=2.261  Sum_probs=0.9

Q 5_Mustela         2 GF    3 (6)
Q Consensus         2 gf    3 (6)
                      ||
T Consensus        24 GF   25 (30)
T signal           24 GF   25 (30)
T 26               24 GF   25 (30)
T 48               24 GF   25 (30)
T 60               24 GF   25 (30)
T 69               24 GF   25 (30)
T 38               24 GF   25 (30)
T 42               24 GF   25 (30)
T 40               24 GF   25 (30)
T 49               24 GF   25 (30)
T 23               24 GF   25 (29)
Confidence            44


No 17 
>P41367_25_MM_ref_sig5_130
Probab=0.94  E-value=25  Score=3.73  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        30 ~   30 (30)
T signal           30 Q   30 (30)
T 102_Strongyloc   29 -   28 (28)
T 100_Leishmania   29 -   28 (28)
T 89_Caenorhabdi   29 -   28 (28)
T 88_Haplochromi   29 -   28 (28)
T 79_Cavia         29 -   28 (28)
T 76_Tribolium     29 -   28 (28)
T 71_Pediculus     29 -   28 (28)
T 64_Sarcophilus   29 -   28 (28)
T 41_Ficedula      29 -   28 (28)
Confidence            0


No 18 
>P35571_42_Mito_ref_sig5_130
Probab=0.93  E-value=26  Score=5.28  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.1

Q 5_Mustela         1 M    1 (6)
Q Consensus         1 m    1 (6)
                      |
T Consensus         1 M    1 (47)
T signal            1 M    1 (47)
T 85_Nematostell    1 M    1 (46)
T 97_Amphimedon     1 M    1 (46)
T 64_Oreochromis    1 M    1 (46)
T 74_Pongo          1 M    1 (46)
T 158_Laccaria      1 M    1 (46)
T 73_Ciona          1 M    1 (46)
T 81_Drosophila     1 M    1 (46)
T 112_Aplysia       1 M    1 (46)
T 58_Columba        1 S    1 (46)
Confidence            0


No 19 
>P08165_32_MM_ref_sig5_130
Probab=0.92  E-value=26  Score=4.17  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 117_Pyrenophor   30 -   29 (29)
T 89_Acyrthosiph   30 -   29 (29)
T 86_Musca         30 -   29 (29)
T 72_Nematostell   30 -   29 (29)
T 37_Myotis        30 -   29 (29)
T 34_Pongo         30 -   29 (29)
Confidence            0


No 20 
>P09622_35_MM_ref_sig5_130
Probab=0.90  E-value=27  Score=3.58  Aligned_cols=1  Identities=0%  Similarity=-1.493  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


Done!
