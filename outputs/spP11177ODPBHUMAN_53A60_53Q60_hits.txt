Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:06 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_53A60_53Q60_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q99K67_32_Mito_ref_sig5_130      0.3      79    0.18    5.9   0.0    1    1-1       1-1   (37)
  2 P00889_27_MM_ref_sig5_130        0.3      80    0.18    5.9   0.0    1    4-4      12-12  (32)
  3 P11325_9_MM_ref_sig5_130         0.3      80    0.18    4.9   0.0    2    1-2       9-10  (14)
  4 P07271_45_Nu_IM_ref_sig5_130     0.3      80    0.18    6.5   0.0    1    2-2      26-26  (50)
  5 P56522_34_MM_ref_sig5_130        0.3      81    0.19    2.5   0.0    1    3-3      39-39  (39)
  6 Q94IN5_37_Mito_ref_sig5_130      0.3      83    0.19    6.2   0.0    1    3-3      42-42  (42)
  7 P86924_17_Mito_ref_sig5_130      0.3      84    0.19    5.0   0.0    1    4-4      20-20  (22)
  8 P38646_46_Mito_Nu_ref_sig5_130   0.3      84    0.19    3.3   0.0    1    3-3      51-51  (51)
  9 P08461_77_MM_ref_sig5_130        0.3      85    0.19    7.0   0.0    1    4-4      70-70  (82)
 10 P22027_25_Mito_IM_ref_sig5_130   0.3      85    0.19    5.5   0.0    1    4-4       5-5   (30)
 11 P41563_27_Mito_ref_sig5_130      0.3      87     0.2    5.6   0.0    1    4-4      15-15  (32)
 12 P14063_12_Mito_ref_sig5_130      0.3      87     0.2    4.9   0.0    1    4-4      13-13  (17)
 13 P14519_29_Mito_MM_Nu_IM_ref_si   0.3      88     0.2    3.1   0.0    1    3-3      34-34  (34)
 14 Q05046_32_Mito_ref_sig5_130      0.3      89     0.2    3.5   0.0    1    3-3      37-37  (37)
 15 P23934_28_IM_ref_sig5_130        0.3      89     0.2    5.7   0.0    1    3-3      11-11  (33)
 16 Q9NUB1_37_MM_ref_sig5_130        0.3      90    0.21    6.1   0.0    1    3-3      14-14  (42)
 17 P36519_19_Mito_ref_sig5_130      0.3      91    0.21    5.3   0.0    1    2-2       5-5   (24)
 18 P24918_33_IM_ref_sig5_130        0.3      94    0.22    5.8   0.0    1    1-1       1-1   (38)
 19 P53590_38_Mito_ref_sig5_130      0.3      95    0.22    6.0   0.0    1    3-3      14-14  (43)
 20 Q6UPE0_34_IM_ref_sig5_130        0.3      97    0.22    2.6   0.0    1    3-3      39-39  (39)

No 1  
>Q99K67_32_Mito_ref_sig5_130
Probab=0.33  E-value=79  Score=5.90  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.0

Q 5_Mustela         1 F    1 (7)
Q Consensus         1 f    1 (7)
                      .
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 74                1 M    1 (39)
T 70                1 M    1 (37)
T 111               1 Q    1 (35)
T 99                1 M    1 (44)
T 103               1 M    1 (41)
T 127               1 M    1 (40)
T 57                1 M    1 (43)
T 85                1 -    0 (29)
Confidence            0


No 2  
>P00889_27_MM_ref_sig5_130
Probab=0.33  E-value=80  Score=5.87  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G......    4 (7)
Q Consensus         4 g......    4 (7)
                      |      
T Consensus        12 ~......   12 (32)
T signal           12 G......   12 (32)
T 114              15 N......   15 (35)
T 127              15 K......   15 (34)
T 136              16 G......   16 (35)
T 141              15 G......   15 (34)
T 152               9 -pv....   10 (30)
T 153              10 Sgafevt   16 (36)
T 115               7 G......    7 (27)
T 133              11 R......   11 (30)
T 128               5 S......    5 (25)
Confidence            3      


No 3  
>P11325_9_MM_ref_sig5_130
Probab=0.33  E-value=80  Score=4.90  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.8

Q 5_Mustela         1 FL    2 (7)
Q Consensus         1 fl    2 (7)
                      ||
T Consensus         9 fl   10 (14)
T signal            9 FL   10 (14)
Confidence            33


No 4  
>P07271_45_Nu_IM_ref_sig5_130
Probab=0.33  E-value=80  Score=6.46  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         2 L    2 (7)
Q Consensus         2 l    2 (7)
                      |
T Consensus        26 l   26 (50)
T signal           26 L   26 (50)
Confidence            2


No 5  
>P56522_34_MM_ref_sig5_130
Probab=0.32  E-value=81  Score=2.45  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      .
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 138_Tuber        38 -   37 (37)
T 98_Oryza         38 -   37 (37)
T 88_Apis          38 -   37 (37)
T 81_Amphimedon    38 -   37 (37)
T 79_Trichoplax    38 -   37 (37)
T 61_Meleagris     38 -   37 (37)
Confidence            0


No 6  
>Q94IN5_37_Mito_ref_sig5_130
Probab=0.32  E-value=83  Score=6.20  Aligned_cols=1  Identities=0%  Similarity=-0.695  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      -
T Consensus        42 k   42 (42)
T signal           42 K   42 (42)
Confidence            0


No 7  
>P86924_17_Mito_ref_sig5_130
Probab=0.31  E-value=84  Score=5.01  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        20 G   20 (22)
T signal           20 G   20 (22)
T 4_Naegleria      20 G   20 (22)
T 2_Leishmania     20 G   20 (22)
T cl|CABBABABA|1   20 P   20 (22)
T cl|FABBABABA|1   20 G   20 (22)
Confidence            2


No 8  
>P38646_46_Mito_Nu_ref_sig5_130
Probab=0.31  E-value=84  Score=3.32  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      .
T Consensus        51 ~   51 (51)
T signal           51 I   51 (51)
T 80_Apis          51 -   50 (50)
T 179_Acyrthosip   51 -   50 (50)
T 105_Ornithorhy   51 -   50 (50)
T 90_Saccoglossu   51 -   50 (50)
T 98_Trichinella   51 -   50 (50)
T 77_Ixodes        51 -   50 (50)
T 82_Aplysia       51 -   50 (50)
T 46_Pelodiscus    50 -   49 (49)
Confidence            0


No 9  
>P08461_77_MM_ref_sig5_130
Probab=0.31  E-value=85  Score=7.03  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        70 G   70 (82)
T signal           70 G   70 (82)
T 153              79 G   79 (91)
T 154              80 G   80 (92)
T 156              79 G   79 (91)
T 161              79 G   79 (91)
T 166              79 G   79 (91)
T 145              75 G   75 (87)
Confidence            3


No 10 
>P22027_25_Mito_IM_ref_sig5_130
Probab=0.31  E-value=85  Score=5.46  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus         5 ~    5 (30)
T signal            5 G    5 (30)
T 24                1 -    0 (24)
T 25                1 -    0 (25)
T 26                1 -    0 (25)
T 32                1 -    0 (25)
T 35                1 -    0 (25)
T 39                1 -    0 (25)
T 40                1 -    0 (25)
T 41                1 -    0 (25)
Confidence            3


No 11 
>P41563_27_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=5.61  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        15 G   15 (32)
T signal           15 G   15 (32)
T 20               15 G   15 (30)
T 42               15 G   15 (32)
T 45               15 G   15 (31)
T 46               14 G   14 (30)
T 61               15 G   15 (31)
T 76               15 G   15 (30)
T 84               15 G   15 (31)
T 94               15 G   15 (31)
T 114              15 G   15 (31)
Confidence            2


No 12 
>P14063_12_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=4.94  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        13 G   13 (17)
T signal           13 G   13 (17)
T 2                13 G   13 (17)
T 33               13 G   13 (17)
T 7                13 G   13 (17)
T 35                9 S    9 (13)
T 20               13 G   13 (17)
T 19               13 G   13 (17)
T 13                8 G    8 (12)
Confidence            3


No 13 
>P14519_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.30  E-value=88  Score=3.06  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      .
T Consensus        34 ~   34 (34)
T signal           34 T   34 (34)
T 94_Galdieria     34 -   33 (33)
T 92_Populus       34 -   33 (33)
T 89_Capsella      34 -   33 (33)
T 71_Capsaspora    34 -   33 (33)
T 65_Ciona         34 -   33 (33)
T 63_Takifugu      34 -   33 (33)
T 56_Maylandia     34 -   33 (33)
T 52_Columba       34 -   33 (33)
T 29_Sus           34 -   33 (33)
Confidence            0


No 14 
>Q05046_32_Mito_ref_sig5_130
Probab=0.30  E-value=89  Score=3.48  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      .
T Consensus        37 ~   37 (37)
T signal           37 V   37 (37)
T 119_Torulaspor   37 -   36 (36)
T 56_Guillardia    37 -   36 (36)
T 51_Chondrus      37 -   36 (36)
T 120_Trichoplax   37 -   36 (36)
T 34_Ostreococcu   37 -   36 (36)
Confidence            0


No 15 
>P23934_28_IM_ref_sig5_130
Probab=0.30  E-value=89  Score=5.75  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus        11 L   11 (33)
T signal           11 L   11 (33)
T 44               16 L   16 (38)
T 64               13 L   13 (35)
T 67               11 L   11 (33)
T 89               11 L   11 (33)
T 63               11 V   11 (35)
T 84               11 L   11 (33)
T 25               11 L   11 (26)
T 101              11 L   11 (25)
Confidence            2


No 16 
>Q9NUB1_37_MM_ref_sig5_130
Probab=0.29  E-value=90  Score=6.06  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      +
T Consensus        14 l   14 (42)
T signal           14 L   14 (42)
T 89               14 L   14 (42)
T 91               14 L   14 (42)
T 95               14 L   14 (33)
T 98               14 V   14 (37)
T 103              14 L   14 (41)
T 107              14 L   14 (39)
T 110              14 L   14 (42)
T 93               14 R   14 (29)
T 105              14 L   14 (38)
Confidence            2


No 17 
>P36519_19_Mito_ref_sig5_130
Probab=0.29  E-value=91  Score=5.31  Aligned_cols=1  Identities=0%  Similarity=-0.695  Sum_probs=0.3

Q 5_Mustela         2 L    2 (7)
Q Consensus         2 l    2 (7)
                      +
T Consensus         5 ~    5 (24)
T signal            5 S    5 (24)
T 131               6 L    6 (25)
T 136               5 S    5 (23)
T 137               5 S    5 (23)
T 127               1 L    1 (20)
T 130               1 -    0 (17)
T 135               1 -    0 (17)
T 129               1 -    0 (18)
T 132               1 -    0 (18)
T 133               1 -    0 (18)
Confidence            2


No 18 
>P24918_33_IM_ref_sig5_130
Probab=0.28  E-value=94  Score=5.76  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.0

Q 5_Mustela         1 F    1 (7)
Q Consensus         1 f    1 (7)
                      .
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 46                1 M    1 (37)
T 49                1 M    1 (39)
T 57                1 -    0 (36)
T 63                1 M    1 (36)
T 90                1 -    0 (36)
T 33                1 -    0 (36)
T 72                1 M    1 (40)
T 20                1 M    1 (45)
Confidence            0


No 19 
>P53590_38_Mito_ref_sig5_130
Probab=0.28  E-value=95  Score=6.03  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus        14 L   14 (43)
T signal           14 L   14 (43)
T 34               12 L   12 (41)
T 62               12 L   12 (41)
T 65               12 L   12 (41)
T 74               14 L   14 (43)
T 84               12 L   12 (41)
T 92               12 L   12 (41)
T 57                1 L    1 (31)
T 44               12 L   12 (40)
T 24                1 -    0 (27)
Confidence            2


No 20 
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.27  E-value=97  Score=2.65  Aligned_cols=1  Identities=0%  Similarity=-0.396  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      -
T Consensus        39 ~   39 (39)
T signal           39 A   39 (39)
T 99_Culex         38 -   37 (37)
T 93_Megachile     38 -   37 (37)
T 68_Ciona         38 -   37 (37)
T 66_Xiphophorus   38 -   37 (37)
T 61_Oryzias       38 -   37 (37)
Confidence            0


Done!
