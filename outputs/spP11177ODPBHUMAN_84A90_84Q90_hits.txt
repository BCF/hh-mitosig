Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:52 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_84A90_84Q90_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P25284_26_MM_ref_sig5_130        0.6      44     0.1    6.3   0.0    1    4-4      28-28  (31)
  2 P21771_33_Mito_ref_sig5_130      0.5      47    0.11    6.6   0.0    2    2-3      30-31  (38)
  3 Q3T0L3_8_Mito_ref_sig5_130       0.5      49    0.11    5.1   0.0    2    3-4       9-10  (13)
  4 Q12349_32_Mito_IM_ref_sig5_130   0.5      49    0.11    6.5   0.0    1    3-3      30-30  (37)
  5 P23786_25_IM_ref_sig5_130        0.5      49    0.11    6.2   0.0    2    3-4      25-26  (30)
  6 P54898_44_Mito_ref_sig5_130      0.5      50    0.11    6.8   0.0    1    3-3      37-37  (49)
  7 P0C2C0_40_Mito_ref_sig5_130      0.5      50    0.11    6.6   0.0    1    2-2      29-29  (45)
  8 P00366_57_MM_ref_sig5_130        0.5      52    0.12    4.8   0.0    1    1-1       1-1   (62)
  9 P00424_20_IM_ref_sig5_130        0.5      53    0.12    5.9   0.0    1    2-2      17-17  (25)
 10 P13182_24_IM_ref_sig5_130        0.5      54    0.12    6.1   0.0    3    2-4      23-25  (29)
 11 P05091_17_MM_ref_sig5_130        0.5      56    0.13    5.6   0.0    3    2-4      16-18  (22)
 12 P13621_23_Mito_IM_ref_sig5_130   0.5      56    0.13    5.9   0.0    1    2-2      23-23  (28)
 13 P56522_34_MM_ref_sig5_130        0.5      57    0.13    2.7   0.0    1    2-2      39-39  (39)
 14 Q8K2C6_36_Mito_ref_sig5_130      0.5      57    0.13    3.4   0.0    1    1-1       1-1   (41)
 15 P08165_32_MM_ref_sig5_130        0.4      60    0.14    3.4   0.0    1    2-2      37-37  (37)
 16 P22142_42_IM_ref_sig5_130        0.4      61    0.14    4.7   0.0    1    2-2      47-47  (47)
 17 P12074_24_IM_ref_sig5_130        0.4      61    0.14    5.9   0.0    2    2-3      23-24  (29)
 18 P18886_25_IM_ref_sig5_130        0.4      62    0.14    5.9   0.0    1    3-3      25-25  (30)
 19 Q9UGC7_26_Mito_ref_sig5_130      0.4      65    0.15    2.3   0.0    1    2-2      31-31  (31)
 20 P10818_26_IM_ref_sig5_130        0.4      66    0.15    5.9   0.0    1    3-3      26-26  (31)

No 1  
>P25284_26_MM_ref_sig5_130
Probab=0.58  E-value=44  Score=6.34  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         4 S    4 (6)
Q Consensus         4 s    4 (6)
                      |
T Consensus        28 S   28 (31)
T signal           28 S   28 (31)
T 188              28 S   28 (31)
T 184              27 S   27 (30)
T 187              26 S   26 (29)
T 171              25 S   25 (27)
T 182              25 S   25 (27)
T 164              24 S   24 (27)
T 183              25 S   25 (28)
Confidence            3


No 2  
>P21771_33_Mito_ref_sig5_130
Probab=0.54  E-value=47  Score=6.60  Aligned_cols=2  Identities=50%  Similarity=1.780  Sum_probs=0.7

Q 5_Mustela         2 PI    3 (6)
Q Consensus         2 pi    3 (6)
                      |+
T Consensus        30 pv   31 (38)
T signal           30 PV   31 (38)
Confidence            33


No 3  
>Q3T0L3_8_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=5.13  Aligned_cols=2  Identities=100%  Similarity=1.032  Sum_probs=1.0

Q 5_Mustela         3 IS    4 (6)
Q Consensus         3 is    4 (6)
                      ||
T Consensus         9 Is   10 (13)
T signal            9 IS   10 (13)
T 81                9 IS   10 (13)
T 95                9 IS   10 (13)
T 83                9 IS   10 (13)
T 85                9 IS   10 (13)
T 88                9 VS   10 (13)
T 89                9 IS   10 (13)
T 76                9 IT   10 (13)
Confidence            44


No 4  
>Q12349_32_Mito_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.50  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        30 i   30 (37)
T signal           30 I   30 (37)
Confidence            2


No 5  
>P23786_25_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.21  Aligned_cols=2  Identities=50%  Similarity=0.833  Sum_probs=0.7

Q 5_Mustela         3 IS    4 (6)
Q Consensus         3 is    4 (6)
                      +|
T Consensus        25 LS   26 (30)
T signal           25 LS   26 (30)
T 101              25 LS   26 (30)
T 104              25 LS   26 (30)
T 108              25 LS   26 (30)
T 109              26 LS   27 (31)
T 117              25 LS   26 (30)
T 119              25 LS   26 (30)
T 59               30 LS   31 (35)
Confidence            33


No 6  
>P54898_44_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.84  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        37 ~   37 (49)
T signal           37 I   37 (49)
T 154              39 F   39 (51)
T 151              38 F   38 (50)
T 104              37 I   37 (43)
T 137              44 F   44 (49)
Confidence            3


No 7  
>P0C2C0_40_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.56  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        29 p   29 (45)
T signal           29 P   29 (45)
T 75               29 P   29 (45)
T 62               29 P   29 (43)
T 37               35 P   35 (51)
T 53               10 P   10 (26)
T 57               10 P   10 (26)
T 43               34 P   34 (51)
T 39               32 Q   32 (44)
T 49               33 Q   33 (45)
T 51               34 L   34 (50)
Confidence            3


No 8  
>P00366_57_MM_ref_sig5_130
Probab=0.50  E-value=52  Score=4.76  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         1 T    1 (6)
Q Consensus         1 t    1 (6)
                      -
T Consensus         1 M    1 (62)
T signal            1 M    1 (62)
T 114_Galdieria     1 M    1 (43)
T 110_Naegleria     1 M    1 (43)
T 104_Ichthyopht    1 M    1 (43)
T 102_Guillardia    1 M    1 (43)
T 93_Amphimedon     1 M    1 (43)
T 92_Schistosoma    1 M    1 (43)
T 89_Culex          1 M    1 (43)
T 69_Gallus         1 M    1 (43)
T 24_Saimiri        1 V    1 (43)
Confidence            0


No 9  
>P00424_20_IM_ref_sig5_130
Probab=0.48  E-value=53  Score=5.89  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        17 p   17 (25)
T signal           17 S   17 (25)
T 30               14 P   14 (22)
T 32               14 P   14 (22)
T 34               14 P   14 (22)
T 35               14 P   14 (22)
T 31               13 P   13 (21)
T 33               13 P   13 (21)
T 28               14 P   14 (20)
T 29               10 P   10 (18)
T 27               14 Q   14 (22)
Confidence            4


No 10 
>P13182_24_IM_ref_sig5_130
Probab=0.48  E-value=54  Score=6.06  Aligned_cols=3  Identities=33%  Similarity=0.179  Sum_probs=1.1

Q 5_Mustela         2 PIS    4 (6)
Q Consensus         2 pis    4 (6)
                      |.|
T Consensus        23 pMS   25 (29)
T signal           23 CMS   25 (29)
T 11               25 PMS   27 (31)
T 16               23 PMS   25 (29)
T 19               23 PMS   25 (29)
T 26               23 PMS   25 (29)
T 30               23 PMS   25 (29)
T 33               23 PMA   25 (29)
Confidence            333


No 11 
>P05091_17_MM_ref_sig5_130
Probab=0.46  E-value=56  Score=5.59  Aligned_cols=3  Identities=33%  Similarity=0.301  Sum_probs=1.3

Q 5_Mustela         2 PIS    4 (6)
Q Consensus         2 pis    4 (6)
                      |+|
T Consensus        16 llS   18 (22)
T signal           16 LLS   18 (22)
T 58               13 LLS   15 (19)
T 61               13 LLS   15 (19)
T 73               11 LLS   13 (17)
T 72               20 LLS   22 (26)
T 45               19 PFS   21 (25)
T 47               19 PLS   21 (22)
T 51               19 -LS   20 (24)
Confidence            344


No 12 
>P13621_23_Mito_IM_ref_sig5_130
Probab=0.46  E-value=56  Score=5.93  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        23 P   23 (28)
T signal           23 P   23 (28)
T 148              23 P   23 (28)
T 131              19 P   19 (24)
T 123              19 P   19 (24)
T 137              16 P   16 (21)
T 130              19 P   19 (24)
T 144              19 P   19 (24)
T 134              19 P   19 (24)
Confidence            3


No 13 
>P56522_34_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=2.68  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 138_Tuber        38 -   37 (37)
T 98_Oryza         38 -   37 (37)
T 88_Apis          38 -   37 (37)
T 81_Amphimedon    38 -   37 (37)
T 79_Trichoplax    38 -   37 (37)
T 61_Meleagris     38 -   37 (37)
Confidence            0


No 14 
>Q8K2C6_36_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=3.45  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         1 T    1 (6)
Q Consensus         1 t    1 (6)
                      -
T Consensus         1 M    1 (41)
T signal            1 M    1 (41)
T 107_Tribolium     1 M    1 (41)
T 92_Nasonia        1 M    1 (41)
T 132_Schizophyl    1 V    1 (41)
T 145_Zymoseptor    1 M    1 (41)
T 112_Nannochlor    1 M    1 (41)
T 102_Metaseiulu    1 M    1 (41)
T 155_Leishmania    1 M    1 (41)
Confidence            0


No 15 
>P08165_32_MM_ref_sig5_130
Probab=0.43  E-value=60  Score=3.36  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 117_Pyrenophor   30 -   29 (29)
T 89_Acyrthosiph   30 -   29 (29)
T 86_Musca         30 -   29 (29)
T 72_Nematostell   30 -   29 (29)
T 37_Myotis        30 -   29 (29)
T 34_Pongo         30 -   29 (29)
Confidence            0


No 16 
>P22142_42_IM_ref_sig5_130
Probab=0.43  E-value=61  Score=4.73  Aligned_cols=1  Identities=0%  Similarity=-1.028  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      =
T Consensus        47 ~   47 (47)
T signal           47 Y   47 (47)
T 180_Trichinell   28 -   27 (27)
T 177_Loa          28 -   27 (27)
T 164_Beta         28 -   27 (27)
T 135_Galdieria    28 -   27 (27)
Confidence            0


No 17 
>P12074_24_IM_ref_sig5_130
Probab=0.42  E-value=61  Score=5.89  Aligned_cols=2  Identities=50%  Similarity=1.680  Sum_probs=0.7

Q 5_Mustela         2 PI    3 (6)
Q Consensus         2 pi    3 (6)
                      |.
T Consensus        23 pM   24 (29)
T signal           23 PM   24 (29)
T 32               23 PM   24 (29)
T 37               25 PM   26 (31)
T 43               23 LM   24 (29)
T 54               23 PM   24 (29)
T 44               23 PM   24 (29)
T 51               23 PM   24 (29)
T 59               23 PM   24 (29)
T 60               23 PM   24 (29)
T 7                21 PM   22 (27)
Confidence            33


No 18 
>P18886_25_IM_ref_sig5_130
Probab=0.42  E-value=62  Score=5.93  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      +
T Consensus        25 L   25 (30)
T signal           25 L   25 (30)
T 84               25 L   25 (30)
T 87               25 L   25 (30)
T 102              26 L   26 (31)
T 114              25 L   25 (30)
T 92               25 L   25 (30)
T 94               25 L   25 (30)
T 48               30 L   30 (35)
Confidence            3


No 19 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.40  E-value=65  Score=2.27  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.1

Q 5_Mustela         2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 119_Populus      31 -   30 (30)
T 107_Phytophtho   31 -   30 (30)
T 101_Setaria      31 -   30 (30)
T 82_Anopheles     31 -   30 (30)
T 69_Chrysemys     31 -   30 (30)
T 55_Geospiza      31 -   30 (30)
Confidence            0


No 20 
>P10818_26_IM_ref_sig5_130
Probab=0.40  E-value=66  Score=5.88  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      .
T Consensus        26 M   26 (31)
T signal           26 M   26 (31)
T 53               23 M   23 (28)
T 56               23 M   23 (28)
T 64               23 M   23 (28)
T 36               25 M   25 (30)
T 44               23 M   23 (28)
T 55               23 M   23 (28)
T 6                25 M   25 (30)
T 7                24 M   24 (29)
T 43               21 F   21 (25)
Confidence            3


Done!
