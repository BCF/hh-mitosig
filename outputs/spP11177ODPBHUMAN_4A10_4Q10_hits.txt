Query         5_Mustela
Match_columns 6
No_of_seqs    5 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:07:50 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_4A10_4Q10_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P10176_25_IM_ref_sig5_130        2.9     6.9   0.016    8.5   0.0    1    5-5      17-17  (30)
  2 Q9SVM8_34_Mito_ref_sig5_130      2.8       7   0.016    8.8   0.0    3    3-5      10-12  (39)
  3 Q5JRX3_15_MM_ref_sig5_130        2.8     7.1   0.016    7.4   0.0    2    4-5      13-14  (20)
  4 Q9FS87_28_Mito_ref_sig5_130      2.8     7.2   0.016    8.6   0.0    3    2-4      13-15  (33)
  5 Q02374_36_IM_ref_sig5_130        2.8     7.2   0.016    8.9   0.0    1    3-3       7-7   (41)
  6 P43265_42_IM_ref_sig5_130        2.5     7.9   0.018    9.0   0.0    3    3-5      13-15  (47)
  7 P16387_33_MM_ref_sig5_130        2.4     8.3   0.019    8.6   0.0    1    3-3      13-13  (38)
  8 P00428_31_IM_ref_sig5_130        2.4     8.5    0.02    8.5   0.0    1    3-3       5-5   (36)
  9 O82662_26_Mito_ref_sig5_130      2.2     9.7   0.022    8.1   0.0    1    3-3       4-4   (31)
 10 Q99757_59_Mito_ref_sig5_130      2.1     9.7   0.022    9.1   0.0    2    4-5       7-8   (64)
 11 Q01859_46_Mito_IM_ref_sig5_130   2.1     9.8   0.023    8.9   0.0    1    3-3      10-10  (51)
 12 O95178_33_IM_ref_sig5_130        2.1      10   0.023    8.2   0.0    1    2-2      38-38  (38)
 13 Q7M0E7_30_Mito_ref_sig5_130      2.1      10   0.023    8.0   0.0    1    5-5      20-20  (35)
 14 Q95108_59_Mito_ref_sig5_130      2.0      10   0.024    9.0   0.0    1    5-5       8-8   (64)
 15 P43165_34_Mito_ref_sig5_130      2.0      11   0.025    8.2   0.0    1    2-2      16-16  (39)
 16 P92507_25_IM_ref_sig5_130        1.9      11   0.025    7.9   0.0    1    5-5      22-22  (30)
 17 Q25423_17_IM_ref_sig5_130        1.7      12   0.028    7.3   0.0    2    5-6       2-3   (22)
 18 P80971_28_IM_ref_sig5_130        1.6      14   0.031    7.8   0.0    1    5-5      15-15  (33)
 19 P00366_57_MM_ref_sig5_130        1.6      14   0.032    6.2   0.0    1    2-2      62-62  (62)
 20 P04182_35_MM_ref_sig5_130        1.6      14   0.032    7.9   0.0    1    5-5      15-15  (40)

No 1  
>P10176_25_IM_ref_sig5_130
Probab=2.86  E-value=6.9  Score=8.46  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      |
T Consensus        17 R   17 (30)
T signal           17 R   17 (30)
T 6                17 R   17 (30)
T 18               17 R   17 (30)
T 23               17 R   17 (30)
T 29               17 R   17 (30)
T 38               17 R   17 (30)
T 2                17 R   17 (31)
T 3                17 R   17 (30)
T 5                17 R   17 (30)
T 27               17 R   17 (30)
Confidence            2


No 2  
>Q9SVM8_34_Mito_ref_sig5_130
Probab=2.82  E-value=7  Score=8.83  Aligned_cols=3  Identities=67%  Similarity=1.276  Sum_probs=1.3

Q 5_Mustela         3 LMR    5 (6)
Q 8_Pteropus        3 LLR    5 (6)
Q 12_Sorex          3 LAR    5 (6)
Q 15_Ictidomys      3 LVR    5 (6)
Q 11_Cavia          3 LVQ    5 (6)
Q Consensus         3 L~r    5 (6)
                      |+|
T Consensus        10 lLR   12 (39)
T signal           10 LLR   12 (39)
T 10               10 LVR   12 (40)
T 8                10 LLR   12 (40)
T 9                10 VLR   12 (40)
T 11               10 ILR   12 (40)
T 4                10 LLR   12 (44)
T 5                10 LLR   12 (41)
T 6                10 LLR   12 (41)
T 7                10 LLR   12 (41)
T 3                10 LLR   12 (49)
Confidence            444


No 3  
>Q5JRX3_15_MM_ref_sig5_130
Probab=2.77  E-value=7.1  Score=7.41  Aligned_cols=2  Identities=50%  Similarity=1.248  Sum_probs=0.7

Q 5_Mustela         4 MR    5 (6)
Q 8_Pteropus        4 LR    5 (6)
Q 12_Sorex          4 AR    5 (6)
Q 15_Ictidomys      4 VR    5 (6)
Q 11_Cavia          4 VQ    5 (6)
Q Consensus         4 ~r    5 (6)
                      +|
T Consensus        13 lr   14 (20)
T signal           13 LR   14 (20)
T 140              15 SR   16 (19)
T 151              12 LW   13 (18)
T 159              13 LW   14 (19)
T 164              13 --   12 (17)
T 112              12 LR   13 (18)
T 143              17 TR   18 (24)
T 146              13 II   14 (19)
Confidence            33


No 4  
>Q9FS87_28_Mito_ref_sig5_130
Probab=2.76  E-value=7.2  Score=8.58  Aligned_cols=3  Identities=33%  Similarity=0.678  Sum_probs=1.2

Q 5_Mustela         2 GLM    4 (6)
Q 8_Pteropus        2 GLL    4 (6)
Q 12_Sorex          2 GLA    4 (6)
Q 15_Ictidomys      2 GLV    4 (6)
Q 11_Cavia          2 ALV    4 (6)
Q Consensus         2 gL~    4 (6)
                      +|.
T Consensus        13 alf   15 (33)
T signal           13 ALF   15 (33)
T 53               14 SLF   16 (35)
Confidence            443


No 5  
>Q02374_36_IM_ref_sig5_130
Probab=2.76  E-value=7.2  Score=8.86  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 8_Pteropus        3 L    3 (6)
Q 12_Sorex          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q Consensus         3 L    3 (6)
                      |
T Consensus         7 l    7 (41)
T signal            7 L    7 (41)
T 4                 4 R    4 (38)
T 34                4 L    4 (38)
T 42                4 V    4 (38)
T 58                4 L    4 (33)
T 5                 4 L    4 (41)
T 13                4 L    4 (38)
T 64                1 -    0 (28)
T 67                1 -    0 (28)
T 45                4 L    4 (35)
Confidence            2


No 6  
>P43265_42_IM_ref_sig5_130
Probab=2.55  E-value=7.9  Score=9.00  Aligned_cols=3  Identities=67%  Similarity=1.143  Sum_probs=1.1

Q 5_Mustela         3 LMR    5 (6)
Q 8_Pteropus        3 LLR    5 (6)
Q 12_Sorex          3 LAR    5 (6)
Q 15_Ictidomys      3 LVR    5 (6)
Q 11_Cavia          3 LVQ    5 (6)
Q Consensus         3 L~r    5 (6)
                      |.|
T Consensus        13 lfr   15 (47)
T signal           13 LFR   15 (47)
Confidence            333


No 7  
>P16387_33_MM_ref_sig5_130
Probab=2.44  E-value=8.3  Score=8.61  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 8_Pteropus        3 L    3 (6)
Q 12_Sorex          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q Consensus         3 L    3 (6)
                      |
T Consensus        13 l   13 (38)
T signal           13 L   13 (38)
Confidence            2


No 8  
>P00428_31_IM_ref_sig5_130
Probab=2.39  E-value=8.5  Score=8.50  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 8_Pteropus        3 L    3 (6)
Q 12_Sorex          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q Consensus         3 L    3 (6)
                      |
T Consensus         5 L    5 (36)
T signal            5 L    5 (36)
T 32                5 L    5 (36)
T 55                5 L    5 (36)
T 56                5 L    5 (36)
T 62                5 L    5 (36)
T 64                5 L    5 (36)
T 80                5 L    5 (35)
T 92                5 L    5 (32)
T 34                5 L    5 (35)
T 103               5 L    5 (33)
Confidence            2


No 9  
>O82662_26_Mito_ref_sig5_130
Probab=2.15  E-value=9.7  Score=8.13  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         3 L    3 (6)
Q 8_Pteropus        3 L    3 (6)
Q 12_Sorex          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q Consensus         3 L    3 (6)
                      |
T Consensus         4 l    4 (31)
T signal            4 L    4 (31)
T 160               4 S    4 (31)
T 165               4 L    4 (31)
T 166               4 L    4 (31)
T 170               4 M    4 (31)
T 171               4 L    4 (31)
T 157               1 L    1 (28)
Confidence            3


No 10 
>Q99757_59_Mito_ref_sig5_130
Probab=2.14  E-value=9.7  Score=9.11  Aligned_cols=2  Identities=50%  Similarity=1.248  Sum_probs=0.7

Q 5_Mustela         4 MR    5 (6)
Q 8_Pteropus        4 LR    5 (6)
Q 12_Sorex          4 AR    5 (6)
Q 15_Ictidomys      4 VR    5 (6)
Q 11_Cavia          4 VQ    5 (6)
Q Consensus         4 ~r    5 (6)
                      +|
T Consensus         7 LR    8 (64)
T signal            7 LR    8 (64)
T 80                7 LR    8 (62)
T 82                7 LR    8 (64)
T 81                7 LR    8 (65)
T 88                7 LR    8 (64)
T 78                7 LR    8 (66)
T 61                7 LR    8 (47)
T 65                7 LR    8 (43)
T 77                7 LR    8 (70)
T 76                6 LR    7 (68)
Confidence            33


No 11 
>Q01859_46_Mito_IM_ref_sig5_130
Probab=2.12  E-value=9.8  Score=8.88  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 8_Pteropus        3 L    3 (6)
Q 12_Sorex          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q Consensus         3 L    3 (6)
                      |
T Consensus        10 l   10 (51)
T signal           10 L   10 (51)
T 138              10 L   10 (52)
T 137              10 L   10 (51)
Confidence            2


No 12 
>O95178_33_IM_ref_sig5_130
Probab=2.07  E-value=10  Score=8.22  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q 8_Pteropus        2 G    2 (6)
Q 12_Sorex          2 G    2 (6)
Q 15_Ictidomys      2 G    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        38 v   38 (38)
T signal           38 V   38 (38)
T 3                38 V   38 (38)
T 59               36 -   35 (35)
T 47               38 -   37 (37)
T 66               28 V   28 (28)
T 46               35 V   35 (35)
T 55               34 -   33 (33)
T 50               34 -   33 (33)
T 57               34 -   33 (33)
T 63               34 -   33 (33)
Confidence            0


No 13 
>Q7M0E7_30_Mito_ref_sig5_130
Probab=2.06  E-value=10  Score=8.02  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.2

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      +
T Consensus        20 q   20 (35)
T signal           20 Q   20 (35)
T 53               20 Q   20 (35)
T 33               19 I   19 (34)
T 49               19 W   19 (34)
T 47               20 Q   20 (35)
T 31               18 Q   18 (33)
T 38               18 Q   18 (33)
T 34               21 Q   21 (36)
T 27               15 K   15 (30)
T 30               15 Q   15 (30)
Confidence            2


No 14 
>Q95108_59_Mito_ref_sig5_130
Probab=2.02  E-value=10  Score=9.00  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      |
T Consensus         8 R    8 (64)
T signal            8 R    8 (64)
T 82                8 R    8 (65)
T 80                8 R    8 (62)
T 79                8 R    8 (66)
T 58                8 R    8 (47)
T 77                8 K    8 (67)
T 78                8 R    8 (70)
T 76                7 R    7 (68)
Confidence            3


No 15 
>P43165_34_Mito_ref_sig5_130
Probab=1.97  E-value=11  Score=8.17  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         2 G    2 (6)
Q 8_Pteropus        2 G    2 (6)
Q 12_Sorex          2 G    2 (6)
Q 15_Ictidomys      2 G    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        16 ~   16 (39)
T signal           16 A   16 (39)
T 16               11 A   11 (39)
T 35               11 A   11 (40)
T 23               16 A   16 (45)
T 28               16 G   16 (44)
T 6                16 G   16 (45)
T 15               16 G   16 (45)
T 24               16 S   16 (45)
T 26               16 G   16 (45)
T 29               16 G   16 (45)
Confidence            2


No 16 
>P92507_25_IM_ref_sig5_130
Probab=1.91  E-value=11  Score=7.92  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.3

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      |
T Consensus        22 q   22 (30)
T signal           22 Q   22 (30)
Confidence            3


No 17 
>Q25423_17_IM_ref_sig5_130
Probab=1.75  E-value=12  Score=7.34  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.7

Q 5_Mustela         5 RR    6 (6)
Q 8_Pteropus        5 RR    6 (6)
Q 12_Sorex          5 RR    6 (6)
Q 15_Ictidomys      5 RR    6 (6)
Q 11_Cavia          5 QR    6 (6)
Q Consensus         5 rR    6 (6)
                      ||
T Consensus         2 rr    3 (22)
T signal            2 RR    3 (22)
T cl|CABBABABA|1    2 RR    3 (25)
Confidence            33


No 18 
>P80971_28_IM_ref_sig5_130
Probab=1.60  E-value=14  Score=7.83  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.3

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      |
T Consensus        15 ~   15 (33)
T signal           15 G   15 (33)
T 94               15 R   15 (31)
T 96               15 R   15 (31)
T 93               15 R   15 (32)
T 92               15 G   15 (32)
Confidence            3


No 19 
>P00366_57_MM_ref_sig5_130
Probab=1.57  E-value=14  Score=6.20  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q 8_Pteropus        2 G    2 (6)
Q 12_Sorex          2 G    2 (6)
Q 15_Ictidomys      2 G    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        62 ~   62 (62)
T signal           62 D   62 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            0


No 20 
>P04182_35_MM_ref_sig5_130
Probab=1.56  E-value=14  Score=7.87  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (6)
Q 8_Pteropus        5 R    5 (6)
Q 12_Sorex          5 R    5 (6)
Q 15_Ictidomys      5 R    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q Consensus         5 r    5 (6)
                      +
T Consensus        15 ~   15 (40)
T signal           15 R   15 (40)
T 138              15 F   15 (40)
T 140              15 C   15 (40)
T 134              17 S   17 (42)
T 141              15 C   15 (40)
T 153              15 R   15 (40)
T 139              15 R   15 (40)
T 127              14 R   14 (39)
T 128              17 R   17 (42)
T 132              16 H   16 (41)
Confidence            2


Done!
