Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:21 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_63A70_63Q70_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9FS87_28_Mito_ref_sig5_130      0.5      55    0.13    6.5   0.0    1    4-4      28-28  (33)
  2 P42028_36_Mito_ref_sig5_130      0.5      57    0.13    6.6   0.0    2    4-5      38-39  (41)
  3 P0C2B9_31_Mito_ref_sig5_130      0.4      61    0.14    6.3   0.0    1    2-2      24-24  (36)
  4 P09622_35_MM_ref_sig5_130        0.4      64    0.15    2.9   0.0    1    3-3      40-40  (40)
  5 P19955_8_Mito_ref_sig5_130       0.4      67    0.15    5.0   0.0    2    3-4      12-13  (13)
  6 A4FUC0_29_Mito_ref_sig5_130      0.4      68    0.16    2.7   0.0    1    3-3      34-34  (34)
  7 Q01992_33_MM_ref_sig5_130        0.4      69    0.16    6.3   0.0    1    6-6      34-34  (38)
  8 P24118_21_MM_Cy_ref_sig5_130     0.4      72    0.16    5.8   0.0    1    4-4      19-19  (26)
  9 P22354_18_Mito_ref_sig5_130      0.3      75    0.17    5.6   0.0    3    3-5      17-19  (23)
 10 P25712_34_IM_ref_sig5_130        0.3      75    0.17    6.1   0.0    1    5-5      16-16  (39)
 11 P11181_61_MM_ref_sig5_130        0.3      76    0.17    6.8   0.0    1    3-3      66-66  (66)
 12 P07806_47_Cy_Mito_ref_sig5_130   0.3      80    0.18    6.5   0.0    1    1-1       1-1   (52)
 13 P23589_29_Mito_ref_sig5_130      0.3      83    0.19    5.9   0.0    1    5-5       8-8   (34)
 14 Q5BJX1_13_Mito_ref_sig5_130      0.3      83    0.19    5.1   0.0    1    3-3      15-15  (18)
 15 Q8WZM3_22_Mito_ref_sig5_130      0.3      85     0.2    5.6   0.0    1    4-4       2-2   (27)
 16 P05165_52_MM_ref_sig5_130        0.3      87     0.2    3.0   0.0    1    1-1       1-1   (57)
 17 P22068_44_Mito_IM_ref_sig5_130   0.3      88     0.2    6.3   0.0    1    4-4      26-26  (49)
 18 P01098_23_Mito_ref_sig5_130      0.3      89     0.2    5.6   0.0    2    1-2      25-26  (28)
 19 Q93170_22_Mito_ref_sig5_130      0.3      90    0.21    5.6   0.0    1    4-4      21-21  (27)
 20 P12074_24_IM_ref_sig5_130        0.3      92    0.21    5.6   0.0    1    6-6       9-9   (29)

No 1  
>Q9FS87_28_Mito_ref_sig5_130
Probab=0.47  E-value=55  Score=6.46  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.3

Q 5_Mustela         4 Y..    4 (7)
Q Consensus         4 y..    4 (7)
                      |  
T Consensus        28 ~..   28 (33)
T signal           28 F..   28 (33)
T 53               28 Yfs   30 (35)
Confidence            2  


No 2  
>P42028_36_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.58  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.9

Q 5_Mustela         4 YK    5 (7)
Q Consensus         4 yk    5 (7)
                      ||
T Consensus        38 YK   39 (41)
T signal           38 YK   39 (41)
T 142              34 YK   35 (37)
T 152              36 YK   37 (39)
T 172              36 YK   37 (39)
T 180              36 YK   37 (39)
T 162              36 YK   37 (39)
T 113              32 YK   33 (35)
T 147              33 YK   34 (36)
T 140              32 YK   33 (35)
T 144              33 YK   34 (36)
Confidence            44


No 3  
>P0C2B9_31_Mito_ref_sig5_130
Probab=0.43  E-value=61  Score=6.27  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         2 G    2 (7)
Q Consensus         2 g    2 (7)
                      |
T Consensus        24 G   24 (36)
T signal           24 G   24 (36)
T 31               24 K   24 (36)
T 49               24 S   24 (36)
T 26               24 G   24 (36)
T 35               24 G   24 (36)
T 10               28 G   28 (40)
T 24               30 G   30 (42)
T 19               29 G   29 (41)
T 8                30 G   30 (40)
T 14               30 G   30 (42)
Confidence            3


No 4  
>P09622_35_MM_ref_sig5_130
Probab=0.40  E-value=64  Score=2.90  Aligned_cols=1  Identities=0%  Similarity=-0.263  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


No 5  
>P19955_8_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=5.00  Aligned_cols=2  Identities=100%  Similarity=1.696  Sum_probs=0.9

Q 5_Mustela         3 AY    4 (7)
Q Consensus         3 ay    4 (7)
                      ||
T Consensus        12 ay   13 (13)
T signal           12 AY   13 (13)
Confidence            44


No 6  
>A4FUC0_29_Mito_ref_sig5_130
Probab=0.39  E-value=68  Score=2.67  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        34 ~   34 (34)
T signal           34 G   34 (34)
T 83_Ceratitis     34 A   34 (34)
T 70_Xiphophorus   34 N   34 (34)
T 62_Anolis        34 G   34 (34)
T 61_Geospiza      34 E   34 (34)
T 60_Anas          34 V   34 (34)
T 59_Zonotrichia   34 K   34 (34)
T 57_Columba       34 E   34 (34)
T 55_Falco         34 A   34 (34)
T 43_Dasypus       34 P   34 (34)
Confidence            0


No 7  
>Q01992_33_MM_ref_sig5_130
Probab=0.38  E-value=69  Score=6.32  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         6 V    6 (7)
Q Consensus         6 v    6 (7)
                      |
T Consensus        34 V   34 (38)
T signal           34 V   34 (38)
T 180              34 V   34 (38)
T 130              33 V   33 (37)
T 102              32 V   32 (36)
T 165              32 V   32 (36)
T 168              32 V   32 (36)
T 182              32 V   32 (36)
T 186              32 V   32 (36)
T 164              32 V   32 (36)
T 169              32 V   32 (36)
Confidence            2


No 8  
>P24118_21_MM_Cy_ref_sig5_130
Probab=0.36  E-value=72  Score=5.80  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (7)
Q Consensus         4 y    4 (7)
                      |
T Consensus        19 y   19 (26)
T signal           19 Y   19 (26)
Confidence            3


No 9  
>P22354_18_Mito_ref_sig5_130
Probab=0.35  E-value=75  Score=5.59  Aligned_cols=3  Identities=67%  Similarity=1.077  Sum_probs=1.1

Q 5_Mustela         3 AYK    5 (7)
Q Consensus         3 ayk    5 (7)
                      |+|
T Consensus        17 awk   19 (23)
T signal           17 AWK   19 (23)
Confidence            343


No 10 
>P25712_34_IM_ref_sig5_130
Probab=0.35  E-value=75  Score=6.11  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         5 K    5 (7)
Q Consensus         5 k    5 (7)
                      |
T Consensus        16 K   16 (39)
T signal           16 K   16 (39)
T 33               16 K   16 (39)
T 60                9 C    9 (32)
T 63                5 Q    5 (28)
T 59               16 K   16 (40)
T 35               16 K   16 (39)
T 5                16 K   16 (39)
T 1                16 K   16 (37)
T 62               16 K   16 (37)
T 10               14 K   14 (35)
Confidence            2


No 11 
>P11181_61_MM_ref_sig5_130
Probab=0.34  E-value=76  Score=6.81  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        66 Q   66 (66)
T signal           66 Q   66 (66)
T 134              66 Q   66 (66)
T 139              66 Q   66 (66)
T 17               49 Q   49 (49)
T 127              45 Q   45 (45)
T 111              64 Q   64 (64)
T 126              66 Q   66 (66)
T 120              66 Q   66 (66)
T 116              65 Q   65 (65)
T 114              65 Q   65 (65)
Confidence            0


No 12 
>P07806_47_Cy_Mito_ref_sig5_130
Probab=0.33  E-value=80  Score=6.53  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (7)
Q Consensus         1 d    1 (7)
                      -
T Consensus         1 m    1 (52)
T signal            1 M    1 (52)
T 176               1 -    0 (29)
Confidence            0


No 13 
>P23589_29_Mito_ref_sig5_130
Probab=0.32  E-value=83  Score=5.90  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         5 K    5 (7)
Q Consensus         5 k    5 (7)
                      |
T Consensus         8 K    8 (34)
T signal            8 K    8 (34)
T 37                8 K    8 (34)
T 39                8 K    8 (34)
T 11                8 K    8 (39)
T 29                8 E    8 (40)
T 33                8 K    8 (40)
T 35                8 K    8 (40)
T 36                8 K    8 (39)
T 22                8 K    8 (40)
Confidence            2


No 14 
>Q5BJX1_13_Mito_ref_sig5_130
Probab=0.32  E-value=83  Score=5.06  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      |
T Consensus        15 A   15 (18)
T signal           15 A   15 (18)
T 12               15 A   15 (18)
T 22               15 A   15 (18)
T 17               15 A   15 (18)
T 26               16 A   16 (19)
T 15               24 S   24 (27)
T 61               15 A   15 (17)
T 62               15 A   15 (17)
Confidence            3


No 15 
>Q8WZM3_22_Mito_ref_sig5_130
Probab=0.31  E-value=85  Score=5.62  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (7)
Q Consensus         4 y    4 (7)
                      |
T Consensus         2 y    2 (27)
T signal            2 Y    2 (27)
Confidence            3


No 16 
>P05165_52_MM_ref_sig5_130
Probab=0.30  E-value=87  Score=3.01  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (7)
Q Consensus         1 d    1 (7)
                      =
T Consensus         1 M    1 (57)
T signal            1 M    1 (57)
T 91_Xenopus        1 M    1 (54)
T 88_Taeniopygia    1 M    1 (54)
T 68_Strongyloce    1 M    1 (54)
T 59_Acanthamoeb    1 M    1 (54)
T 52_Trichoplax     1 M    1 (54)
T 42_Condylura      1 M    1 (54)
T 6_Columba         1 A    1 (54)
T 1_Mustela         1 M    1 (54)
Confidence            0


No 17 
>P22068_44_Mito_IM_ref_sig5_130
Probab=0.30  E-value=88  Score=6.30  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (7)
Q Consensus         4 y    4 (7)
                      |
T Consensus        26 y   26 (49)
T signal           26 Y   26 (49)
Confidence            3


No 18 
>P01098_23_Mito_ref_sig5_130
Probab=0.30  E-value=89  Score=5.61  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=1.0

Q 5_Mustela         1 DG    2 (7)
Q Consensus         1 dg    2 (7)
                      ||
T Consensus        25 dg   26 (28)
T signal           25 DG   26 (28)
Confidence            45


No 19 
>Q93170_22_Mito_ref_sig5_130
Probab=0.29  E-value=90  Score=5.55  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (7)
Q Consensus         4 y    4 (7)
                      |
T Consensus        21 y   21 (27)
T signal           21 Y   21 (27)
Confidence            3


No 20 
>P12074_24_IM_ref_sig5_130
Probab=0.29  E-value=92  Score=5.58  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         6 V    6 (7)
Q Consensus         6 v    6 (7)
                      |
T Consensus         9 v    9 (29)
T signal            9 V    9 (29)
T 32                9 V    9 (29)
T 37               11 L   11 (31)
T 43                9 V    9 (29)
T 54                9 L    9 (29)
T 44                9 V    9 (29)
T 51                9 V    9 (29)
T 59                9 L    9 (29)
T 60                9 V    9 (29)
T 7                 7 V    7 (27)
Confidence            2


Done!
