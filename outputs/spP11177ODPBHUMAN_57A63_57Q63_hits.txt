Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:11 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_57A63_57Q63_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 O14561_68_Mito_ref_sig5_130      0.3      76    0.17    6.7   0.0    1    1-1       1-1   (73)
  2 P09380_17_Mito_MM_Nu_ref_sig5_   0.3      77    0.18    5.3   0.0    1    5-5       8-8   (22)
  3 P36527_26_Mito_ref_sig5_130      0.3      80    0.18    5.7   0.0    1    4-4       3-3   (31)
  4 P09440_34_Mito_ref_sig5_130      0.3      81    0.18    5.9   0.0    1    5-5      34-34  (39)
  5 Q25417_9_Mito_ref_sig5_130       0.3      82    0.19    4.7   0.0    1    3-3      11-11  (14)
  6 Q95108_59_Mito_ref_sig5_130      0.3      86     0.2    6.3   0.0    1    5-5       3-3   (64)
  7 P22315_53_IM_ref_sig5_130        0.3      90    0.21    6.2   0.0    1    2-2      58-58  (58)
  8 P17694_33_IM_ref_sig5_130        0.3      91    0.21    2.5   0.0    1    2-2      38-38  (38)
  9 Q99757_59_Mito_ref_sig5_130      0.3      91    0.21    6.3   0.0    1    5-5       3-3   (64)
 10 P28834_11_Mito_ref_sig5_130      0.3      95    0.22    4.7   0.0    1    4-4      15-15  (16)
 11 P34897_29_Mito_MM_Nu_IM_ref_si   0.3      98    0.22    3.3   0.0    1    2-2      34-34  (34)
 12 Q9Y2D0_33_Mito_ref_sig5_130      0.3      99    0.23    3.6   0.0    1    2-2      38-38  (38)
 13 P14519_29_Mito_MM_Nu_IM_ref_si   0.3      99    0.23    2.8   0.0    1    2-2      34-34  (34)
 14 P83372_28_MM_ref_sig5_130        0.3   1E+02    0.23    5.5   0.0    3    3-5      14-16  (33)
 15 P0CS90_23_MM_Nu_ref_sig5_130     0.3   1E+02    0.24    5.1   0.0    4    3-6      19-22  (28)
 16 P17695_35_Cy_Mito_ref_sig5_130   0.3   1E+02    0.24    5.6   0.0    3    3-5      36-38  (40)
 17 P22570_32_MM_ref_sig5_130        0.3 1.1E+02    0.24    2.8   0.0    2    5-6      31-32  (37)
 18 P26360_45_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.24    5.9   0.0    4    1-4      36-39  (50)
 19 P24918_33_IM_ref_sig5_130        0.2 1.1E+02    0.24    5.4   0.0    2    2-3      35-36  (38)
 20 P30048_62_Mito_ref_sig5_130      0.2 1.1E+02    0.24    3.8   0.0    3    3-5      65-67  (67)

No 1  
>O14561_68_Mito_ref_sig5_130
Probab=0.35  E-value=76  Score=6.72  Aligned_cols=1  Identities=0%  Similarity=-0.661  Sum_probs=0.0

Q 5_Mustela         1 E    1 (6)
Q Consensus         1 e    1 (6)
                      -
T Consensus         1 M    1 (73)
T signal            1 M    1 (73)
T 82                1 M    1 (72)
T 80                1 M    1 (76)
T 78                1 M    1 (66)
T 79                1 M    1 (65)
T 76                1 M    1 (67)
T 73                1 M    1 (70)
T 65                1 M    1 (73)
T 77                1 M    1 (94)
T 72                1 M    1 (73)
Confidence            0


No 2  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=0.34  E-value=77  Score=5.25  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus         8 Q    8 (22)
T signal            8 Q    8 (22)
T 26                8 Q    8 (20)
T 18                8 Q    8 (20)
T 21                8 Q    8 (20)
T 22                8 Q    8 (20)
T 23                8 Q    8 (21)
T 27                8 Q    8 (21)
T 30                8 Q    8 (21)
T 24                8 Q    8 (20)
T 17                7 Q    7 (18)
Confidence            3


No 3  
>P36527_26_Mito_ref_sig5_130
Probab=0.33  E-value=80  Score=5.67  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         3 a    3 (31)
T signal            3 A    3 (31)
Confidence            3


No 4  
>P09440_34_Mito_ref_sig5_130
Probab=0.33  E-value=81  Score=5.93  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus        34 q   34 (39)
T signal           34 Q   34 (39)
Confidence            2


No 5  
>Q25417_9_Mito_ref_sig5_130
Probab=0.32  E-value=82  Score=4.70  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.3

Q 5_Mustela         3 V    3 (6)
Q Consensus         3 v    3 (6)
                      .
T Consensus        11 m   11 (14)
T signal           11 M   11 (14)
Confidence            3


No 6  
>Q95108_59_Mito_ref_sig5_130
Probab=0.31  E-value=86  Score=6.34  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus         3 Q    3 (64)
T signal            3 Q    3 (64)
T 82                3 Q    3 (65)
T 80                3 Q    3 (62)
T 79                3 Q    3 (66)
T 58                3 Q    3 (47)
T 77                3 Q    3 (67)
T 78                3 Q    3 (70)
T 76                3 Q    3 (68)
Confidence            2


No 7  
>P22315_53_IM_ref_sig5_130
Probab=0.29  E-value=90  Score=6.22  Aligned_cols=1  Identities=0%  Similarity=0.568  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q Consensus         2 e    2 (6)
                      +
T Consensus        58 Q   58 (58)
T signal           58 Q   58 (58)
T 170              59 Q   59 (59)
T 162              32 Q   32 (32)
T 165              36 Q   36 (36)
T 155              40 Q   40 (40)
T 141              54 Q   54 (54)
T 142              45 -   44 (44)
T 148              39 Q   39 (39)
T 137              42 -   41 (41)
Confidence            0


No 8  
>P17694_33_IM_ref_sig5_130
Probab=0.29  E-value=91  Score=2.52  Aligned_cols=1  Identities=0%  Similarity=0.568  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q Consensus         2 e    2 (6)
                      .
T Consensus        38 ~   38 (38)
T signal           38 Q   38 (38)
T 203_Boea         38 -   37 (37)
T 189_Naegleria    38 -   37 (37)
T 167_Loa          38 -   37 (37)
T 125_Talaromyce   38 -   37 (37)
T 84_Andalucia     38 -   37 (37)
Confidence            0


No 9  
>Q99757_59_Mito_ref_sig5_130
Probab=0.29  E-value=91  Score=6.28  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus         3 Q    3 (64)
T signal            3 Q    3 (64)
T 80                3 Q    3 (62)
T 82                3 Q    3 (64)
T 81                3 Q    3 (65)
T 88                3 Q    3 (64)
T 78                3 Q    3 (66)
T 61                3 Q    3 (47)
T 65                3 Q    3 (43)
T 77                3 Q    3 (70)
T 76                3 Q    3 (68)
Confidence            2


No 10 
>P28834_11_Mito_ref_sig5_130
Probab=0.28  E-value=95  Score=4.68  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus        15 a   15 (16)
T signal           15 A   15 (16)
Confidence            2


No 11 
>P34897_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.27  E-value=98  Score=3.26  Aligned_cols=1  Identities=0%  Similarity=-0.030  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q Consensus         2 e    2 (6)
                      -
T Consensus        34 ~   34 (34)
T signal           34 T   34 (34)
T 86_Glycine       34 -   33 (33)
T 87_Trichoplax    34 -   33 (33)
T 76_Aplysia       34 -   33 (33)
T 74_Ciona         34 -   33 (33)
T 60_Maylandia     34 -   33 (33)
T 58_Pseudopodoc   34 -   33 (33)
Confidence            0


No 12 
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.27  E-value=99  Score=3.62  Aligned_cols=1  Identities=0%  Similarity=-0.030  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q Consensus         2 e    2 (6)
                      -
T Consensus        38 ~   38 (38)
T signal           38 T   38 (38)
T 57_Xenopus       38 I   38 (38)
T 54_Chrysemys     38 C   38 (38)
T 49_Meleagris     38 K   38 (38)
T 42_Dasypus       38 Y   38 (38)
T 41_Camelus       38 C   38 (38)
T 37_Ailuropoda    38 L   38 (38)
T 22_Myotis        38 M   38 (38)
T 14               33 -   32 (32)
Confidence            0


No 13 
>P14519_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.27  E-value=99  Score=2.81  Aligned_cols=1  Identities=0%  Similarity=-0.030  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q Consensus         2 e    2 (6)
                      -
T Consensus        34 ~   34 (34)
T signal           34 T   34 (34)
T 94_Galdieria     34 -   33 (33)
T 92_Populus       34 -   33 (33)
T 89_Capsella      34 -   33 (33)
T 71_Capsaspora    34 -   33 (33)
T 65_Ciona         34 -   33 (33)
T 63_Takifugu      34 -   33 (33)
T 56_Maylandia     34 -   33 (33)
T 52_Columba       34 -   33 (33)
T 29_Sus           34 -   33 (33)
Confidence            0


No 14 
>P83372_28_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.46  Aligned_cols=3  Identities=33%  Similarity=0.556  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (6)
Q Consensus         3 vaq    5 (6)
                      ++|
T Consensus        14 ~~Q   16 (33)
T signal           14 LGQ   16 (33)
T 167              17 VGQ   19 (36)
T 175              17 AVQ   19 (36)
T 176              17 VGQ   19 (36)
T 177              17 VGQ   19 (36)
T 178              17 LAQ   19 (36)
T 168              17 MAQ   19 (36)


No 15 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.05  Aligned_cols=4  Identities=25%  Similarity=0.310  Sum_probs=0.0

Q 5_Mustela         3 VAQY    6 (6)
Q Consensus         3 vaqy    6 (6)
                      ++++
T Consensus        19 ~a~R   22 (28)
T signal           19 IATR   22 (28)
T 55               20 AVRY   23 (29)
T 65               20 ISTR   23 (29)
T 54               19 FQQR   22 (28)
T 57               17 VATR   20 (26)
T 59               17 SFVR   20 (26)
T 62               15 VATR   18 (24)
T 56               15 MSAR   18 (24)
T 60               14 VARR   17 (23)


No 16 
>P17695_35_Cy_Mito_ref_sig5_130
Probab=0.25  E-value=1e+02  Score=5.64  Aligned_cols=3  Identities=67%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (6)
Q Consensus         3 vaq    5 (6)
                      |.|
T Consensus        36 VSQ   38 (40)
T signal           36 VSQ   38 (40)
T 42               29 VSQ   31 (33)
T 41               27 VSQ   29 (31)
T 40               29 VSQ   31 (33)


No 17 
>P22570_32_MM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=2.76  Aligned_cols=2  Identities=0%  Similarity=1.049  Sum_probs=0.0

Q 5_Mustela         5 QY    6 (6)
Q Consensus         5 qy    6 (6)
                      |+
T Consensus        31 ~~   32 (37)
T signal           31 HF   32 (37)
T 90_Drosophila    30 --   29 (29)
T 84_Megachile     30 --   29 (29)
T 73_Nematostell   30 --   29 (29)
T 49_Columba       30 --   29 (29)
T 38_Myotis        30 --   29 (29)


No 18 
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=5.86  Aligned_cols=4  Identities=50%  Similarity=0.900  Sum_probs=0.0

Q 5_Mustela         1 EEVA    4 (6)
Q Consensus         1 eeva    4 (6)
                      ||.+
T Consensus        36 ~e~~   39 (50)
T signal           36 EEIG   39 (50)
T 181              31 EEQG   34 (45)
T 183              34 EEPV   37 (48)
T 170              37 PEMA   40 (51)
T 171              35 QEIA   38 (49)
T 182              36 EEQA   39 (50)
T 174              33 QEEG   36 (47)
T 178              30 VDQS   33 (44)
T 177              36 EQSS   39 (50)
T 168              38 GDIA   41 (52)


No 19 
>P24918_33_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=5.40  Aligned_cols=2  Identities=100%  Similarity=1.165  Sum_probs=0.0

Q 5_Mustela         2 EV    3 (6)
Q Consensus         2 ev    3 (6)
                      ||
T Consensus        35 EV   36 (38)
T signal           35 EV   36 (38)
T 46               34 EI   35 (37)
T 49               36 EV   37 (39)
T 57               33 EV   34 (36)
T 63               33 EV   34 (36)
T 90               33 EV   34 (36)
T 33               33 EV   34 (36)
T 72               37 EV   38 (40)
T 20               42 EV   43 (45)


No 20 
>P30048_62_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=3.77  Aligned_cols=3  Identities=67%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (6)
Q Consensus         3 vaq    5 (6)
                      +.|
T Consensus        65 ~~~   67 (67)
T signal           65 VTQ   67 (67)
T 79_Drosophila    66 FR-   67 (67)
T 76_Meleagris     66 SD-   67 (67)
T 70_Strongyloce   66 TS-   67 (67)
T 69_Metaseiulus   66 GA-   67 (67)
T 63_Columba       66 VS-   67 (67)
T 57_Falco         66 KV-   67 (67)
T 44_Camelus       66 LD-   67 (67)


Done!
