Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:06 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_54A60_54Q60_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P14063_12_Mito_ref_sig5_130      0.4      72    0.16    5.0   0.0    1    3-3      13-13  (17)
  2 P83484_51_Mito_IM_ref_sig5_130   0.4      74    0.17    5.1   0.0    1    2-2      45-45  (56)
  3 P10515_86_MM_ref_sig5_130        0.3      75    0.17    7.1   0.0    2    2-3      78-79  (91)
  4 Q99K67_32_Mito_ref_sig5_130      0.3      76    0.17    5.7   0.0    1    1-1       1-1   (37)
  5 Q9H300_52_IM_Nu_ref_sig5_130     0.3      76    0.17    6.5   0.0    1    3-3      40-40  (57)
  6 P53590_38_Mito_ref_sig5_130      0.3      77    0.18    6.1   0.0    1    2-2      43-43  (43)
  7 P25708_20_IM_ref_sig5_130        0.3      77    0.18    5.4   0.0    1    2-2      25-25  (25)
  8 P19783_22_IM_ref_sig5_130        0.3      79    0.18    5.5   0.0    1    3-3      11-11  (27)
  9 P38646_46_Mito_Nu_ref_sig5_130   0.3      82    0.19    3.2   0.0    1    2-2      51-51  (51)
 10 Q9SJ12_32_Mito_IM_ref_sig5_130   0.3      84    0.19    4.3   0.0    1    2-2      37-37  (37)
 11 P49364_30_Mito_ref_sig5_130      0.3      87     0.2    5.7   0.0    1    2-2      35-35  (35)
 12 P56522_34_MM_ref_sig5_130        0.3      88     0.2    2.3   0.0    1    2-2      39-39  (39)
 13 P22354_18_Mito_ref_sig5_130      0.3      90    0.21    5.2   0.0    1    3-3       3-3   (23)
 14 P24918_33_IM_ref_sig5_130        0.3      91    0.21    5.6   0.0    1    5-5      35-35  (38)
 15 P13073_22_IM_ref_sig5_130        0.3      94    0.22    4.5   0.0    1    2-2      27-27  (27)
 16 P23709_38_IM_ref_sig5_130        0.3   1E+02    0.23    5.8   0.0    3    1-3      16-18  (43)
 17 Q05046_32_Mito_ref_sig5_130      0.3   1E+02    0.23    3.2   0.0    4    3-6      34-37  (37)
 18 P28350_44_Mito_Cy_ref_sig5_130   0.3   1E+02    0.24    5.9   0.0    3    3-5      46-48  (49)
 19 P43265_42_IM_ref_sig5_130        0.2 1.1E+02    0.24    5.8   0.0    5    2-6      21-25  (47)
 20 P40360_13_Mito_ref_sig5_130      0.2 1.1E+02    0.25    4.6   0.0    6    1-6       5-10  (18)

No 1  
>P14063_12_Mito_ref_sig5_130
Probab=0.36  E-value=72  Score=4.99  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        13 G   13 (17)
T signal           13 G   13 (17)
T 2                13 G   13 (17)
T 33               13 G   13 (17)
T 7                13 G   13 (17)
T 35                9 S    9 (13)
T 20               13 G   13 (17)
T 19               13 G   13 (17)
T 13                8 G    8 (12)
Confidence            3


No 2  
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.35  E-value=74  Score=5.06  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      |
T Consensus        45 ~   45 (56)
T signal           45 L   45 (56)
T 34_Volvox        39 -   38 (38)
T 134_Guillardia   39 -   38 (38)
T 152_Trichinell   39 -   38 (38)
T 159_Ixodes       39 -   38 (38)
T 41_Galdieria     39 -   38 (38)
T 112_Tribolium    39 -   38 (38)
T 117_Ceratitis    39 -   38 (38)
T 111_Musca        39 -   38 (38)
T 39_Selaginella   39 -   38 (38)
Confidence            2


No 3  
>P10515_86_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=7.09  Aligned_cols=2  Identities=100%  Similarity=1.763  Sum_probs=0.7

Q 5_Mustela         2 LG    3 (6)
Q Consensus         2 lg    3 (6)
                      ||
T Consensus        78 lG   79 (91)
T signal           78 LG   79 (91)
T 153              78 LA   79 (91)
T 156              78 LG   79 (91)
T 161              79 WG   80 (92)
T 162              78 LG   79 (91)
T 163              78 WG   79 (91)
T 168              78 FG   79 (91)
T 179              76 LG   77 (89)
T 160              77 LG   78 (90)
T 149              74 LG   75 (87)
Confidence            33


No 4  
>Q99K67_32_Mito_ref_sig5_130
Probab=0.34  E-value=76  Score=5.74  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 74                1 M    1 (39)
T 70                1 M    1 (37)
T 111               1 Q    1 (35)
T 99                1 M    1 (44)
T 103               1 M    1 (41)
T 127               1 M    1 (40)
T 57                1 M    1 (43)
T 85                1 -    0 (29)
Confidence            0


No 5  
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=0.34  E-value=76  Score=6.49  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        40 g   40 (57)
T signal           40 G   40 (57)
T 109              38 G   38 (55)
T 119              40 G   40 (57)
T 120              38 G   38 (55)
T 129              36 G   36 (53)
T 130              40 G   40 (57)
T 136              40 P   40 (57)
T 117              39 S   39 (56)
T 116              15 L   15 (32)
Confidence            3


No 6  
>P53590_38_Mito_ref_sig5_130
Probab=0.34  E-value=77  Score=6.10  Aligned_cols=1  Identities=0%  Similarity=-0.927  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        43 E   43 (43)
T signal           43 E   43 (43)
T 34               41 E   41 (41)
T 62               41 E   41 (41)
T 65               41 E   41 (41)
T 74               43 E   43 (43)
T 84               41 E   41 (41)
T 92               41 E   41 (41)
T 57               31 E   31 (31)
T 44               40 E   40 (40)
T 24               27 E   27 (27)
Confidence            0


No 7  
>P25708_20_IM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.36  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        25 T   25 (25)
T signal           25 T   25 (25)
T 96               27 T   27 (27)
T 103              20 T   20 (20)
T 135              27 T   27 (27)
T 3                25 T   25 (25)
Confidence            0


No 8  
>P19783_22_IM_ref_sig5_130
Probab=0.33  E-value=79  Score=5.51  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        11 G   11 (27)
T signal           11 G   11 (27)
T 57               11 G   11 (27)
T 82               11 G   11 (27)
T 50               11 G   11 (27)
T 56               11 G   11 (27)
T 80               11 G   11 (27)
T 35               11 G   11 (26)
T 25               11 G   11 (27)
T 36               11 G   11 (27)
Confidence            3


No 9  
>P38646_46_Mito_Nu_ref_sig5_130
Probab=0.32  E-value=82  Score=3.21  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      +
T Consensus        51 ~   51 (51)
T signal           51 I   51 (51)
T 80_Apis          51 -   50 (50)
T 179_Acyrthosip   51 -   50 (50)
T 105_Ornithorhy   51 -   50 (50)
T 90_Saccoglossu   51 -   50 (50)
T 98_Trichinella   51 -   50 (50)
T 77_Ixodes        51 -   50 (50)
T 82_Aplysia       51 -   50 (50)
T 46_Pelodiscus    50 -   49 (49)
Confidence            0


No 10 
>Q9SJ12_32_Mito_IM_ref_sig5_130
Probab=0.31  E-value=84  Score=4.34  Aligned_cols=1  Identities=0%  Similarity=-0.396  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        37 ~   37 (37)
T signal           37 A   37 (37)
T cl|DABBABABA|1   37 P   37 (37)
T cl|MABBABABA|1   37 A   37 (37)
T 18_Selaginella   37 E   37 (37)
T 2_Capsella       37 K   37 (37)
T 18_Selaginella   37 -   36 (36)
T 16_Brachypodiu   37 -   36 (36)
T 17_Physcomitre   37 -   36 (36)
T 9_Medicago       37 -   36 (36)
T 16               37 E   37 (37)
Confidence            0


No 11 
>P49364_30_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=5.73  Aligned_cols=1  Identities=0%  Similarity=-0.927  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        35 e   35 (35)
T signal           35 E   35 (35)
T 163              35 E   35 (35)
T 164              34 E   34 (34)
T 159              35 D   35 (35)
Confidence            0


No 12 
>P56522_34_MM_ref_sig5_130
Probab=0.30  E-value=88  Score=2.26  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      .
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 138_Tuber        38 -   37 (37)
T 98_Oryza         38 -   37 (37)
T 88_Apis          38 -   37 (37)
T 81_Amphimedon    38 -   37 (37)
T 79_Trichoplax    38 -   37 (37)
T 61_Meleagris     38 -   37 (37)
Confidence            0


No 13 
>P22354_18_Mito_ref_sig5_130
Probab=0.29  E-value=90  Score=5.17  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus         3 g    3 (23)
T signal            3 G    3 (23)
Confidence            3


No 14 
>P24918_33_IM_ref_sig5_130
Probab=0.29  E-value=91  Score=5.61  Aligned_cols=1  Identities=100%  Similarity=1.199  Sum_probs=0.2

Q 5_Mustela         5 E    5 (6)
Q Consensus         5 e    5 (6)
                      |
T Consensus        35 E   35 (38)
T signal           35 E   35 (38)
T 46               34 E   34 (37)
T 49               36 E   36 (39)
T 57               33 E   33 (36)
T 63               33 E   33 (36)
T 90               33 E   33 (36)
T 33               33 E   33 (36)
T 72               37 E   37 (40)
T 20               42 E   42 (45)
Confidence            1


No 15 
>P13073_22_IM_ref_sig5_130
Probab=0.28  E-value=94  Score=4.46  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q Consensus         2 l    2 (6)
                      .
T Consensus        27 v   27 (27)
T signal           27 V   27 (27)
T 4_Nomascus       27 V   27 (27)
T 89_Anas          27 A   27 (27)
T 53_Columba       27 F   27 (27)
T 72_Xenopus       27 V   27 (27)
T 65_Pelodiscus    27 I   27 (27)
T 14_Equus         27 E   27 (27)
T 41_Condylura     27 G   27 (27)
T 8_Dasypus        27 V   27 (27)
T 80_Danio         27 V   27 (27)
Confidence            0


No 16 
>P23709_38_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.76  Aligned_cols=3  Identities=67%  Similarity=1.376  Sum_probs=0.0

Q 5_Mustela         1 LLG    3 (6)
Q Consensus         1 llg    3 (6)
                      |+|
T Consensus        16 lvG   18 (43)
T signal           16 LVG   18 (43)
T 202              13 LVG   15 (40)
T 205              13 LLG   15 (40)
T 217              13 LVR   15 (40)
T 220              13 LLG   15 (40)
T 192              13 LLG   15 (40)
T 196              13 LVG   15 (40)
T 197              13 IVG   15 (39)
T 225              13 FLG   15 (40)
T 191              14 LAG   16 (41)


No 17 
>Q05046_32_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=3.19  Aligned_cols=4  Identities=25%  Similarity=0.651  Sum_probs=0.0

Q 5_Mustela         3 GEEV    6 (6)
Q Consensus         3 geev    6 (6)
                      +.++
T Consensus        34 ~~~~   37 (37)
T signal           34 AKDV   37 (37)
T 119_Torulaspor   34 VDA-   36 (36)
T 56_Guillardia    34 PKG-   36 (36)
T 51_Chondrus      34 RNL-   36 (36)
T 120_Trichoplax   34 GVE-   36 (36)
T 34_Ostreococcu   34 YAK-   36 (36)


No 18 
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.87  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=0.0

Q 5_Mustela         3 GEE    5 (6)
Q Consensus         3 gee    5 (6)
                      ||+
T Consensus        46 ged   48 (49)
T signal           46 GED   48 (49)


No 19 
>P43265_42_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=5.79  Aligned_cols=5  Identities=40%  Similarity=1.106  Sum_probs=0.0

Q 5_Mustela         2 LGEEV    6 (6)
Q Consensus         2 lgeev    6 (6)
                      ||..+
T Consensus        21 lgdsi   25 (47)
T signal           21 LGDSI   25 (47)


No 20 
>P40360_13_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.59  Aligned_cols=6  Identities=17%  Similarity=0.617  Sum_probs=0.0

Q 5_Mustela         1 LLG.EEV    6 (6)
Q Consensus         1 llg.eev    6 (6)
                      ||+ .+.
T Consensus         5 ll~.~~~   10 (18)
T signal            5 IFA.HEL   10 (18)
T 54                5 IFS.NGL   10 (18)
T 53                5 ALN.RGF   10 (18)
T 52                5 LLG.QRL   10 (17)
T 50                5 LFK.HGF   10 (17)
T 51                5 LLS.GGL   10 (18)
T 49                5 LLSgARF   11 (19)


Done!
