Query         5_Mustela
Match_columns 6
No_of_seqs    6 out of 20
Neff          1.6 
Searched_HMMs 436
Date          Wed Sep  6 16:08:01 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_11A17_11Q17_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36527_26_Mito_ref_sig5_130     83.2  0.0038 8.8E-06   16.9   0.0    6    1-6      14-19  (31)
  2 P34899_31_Mito_ref_sig5_130      1.9      11   0.026    8.1   0.0    1    2-2       8-8   (36)
  3 Q99797_35_MM_ref_sig5_130        1.8      12   0.027    8.3   0.0    3    3-5      35-37  (40)
  4 Q02253_32_Mito_ref_sig5_130      1.8      12   0.027    8.2   0.0    3    3-5      17-19  (37)
  5 P10818_26_IM_ref_sig5_130        1.8      12   0.027    7.8   0.0    2    4-5      11-12  (31)
  6 Q02376_27_IM_ref_sig5_130        1.7      12   0.028    7.8   0.0    1    3-3       9-9   (32)
  7 P10817_9_IM_ref_sig5_130         1.7      13    0.03    6.6   0.0    4    2-5       3-6   (14)
  8 Q7YR75_45_Mito_ref_sig5_130      1.5      14   0.033    8.3   0.0    1    3-3      27-27  (50)
  9 P07471_12_IM_ref_sig5_130        1.5      14   0.033    6.8   0.0    4    2-5       6-9   (17)
 10 P12074_24_IM_ref_sig5_130        1.5      15   0.034    7.5   0.0    2    4-5       9-10  (29)
 11 Q5JRX3_15_MM_ref_sig5_130        1.5      15   0.034    6.6   0.0    1    5-5      17-17  (20)
 12 P11177_30_MM_ref_sig5_130        1.4      15   0.035    7.7   0.0    1    4-4      15-15  (35)
 13 Q3SZ86_26_Mito_ref_sig5_130      1.4      16   0.037    7.5   0.0    1    4-4       5-5   (31)
 14 P80971_28_IM_ref_sig5_130        1.3      17   0.038    7.6   0.0    1    2-2       3-3   (33)
 15 Q12165_22_Mito_IM_ref_sig5_130   1.3      17   0.039    7.1   0.0    1    2-2      27-27  (27)
 16 P24118_21_MM_Cy_ref_sig5_130     1.3      17   0.039    7.2   0.0    1    2-2      26-26  (26)
 17 P08165_32_MM_ref_sig5_130        1.3      17    0.04    4.6   0.0    1    2-2      37-37  (37)
 18 P25705_43_IM_Cm_ref_sig5_130     1.3      18    0.04    5.3   0.0    1    2-2      48-48  (48)
 19 Q7Z6M4_42_Mito_ref_sig5_130      1.2      19   0.043    7.8   0.0    2    3-4       7-8   (47)
 20 Q7M0E7_30_Mito_ref_sig5_130      1.2      19   0.043    7.3   0.0    1    4-4      14-14  (35)

No 1  
>P36527_26_Mito_ref_sig5_130
Probab=83.17  E-value=0.0038  Score=16.94  Aligned_cols=6  Identities=100%  Similarity=1.248  Sum_probs=4.8

Q 5_Mustela         1 LEQVSG    6 (6)
Q 2_Macaca          1 LREVSG    6 (6)
Q 15_Ictidomys      1 LQQVSG    6 (6)
Q 11_Cavia          1 LRQVSG    6 (6)
Q 17_Tupaia         1 FQQVSG    6 (6)
Q 4_Pongo           1 LREVSR    6 (6)
Q Consensus         1 l~qVSg    6 (6)
                      |+||||
T Consensus        14 leqvsg   19 (31)
T signal           14 LEQVSG   19 (31)
Confidence            578987


No 2  
>P34899_31_Mito_ref_sig5_130
Probab=1.87  E-value=11  Score=8.12  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.3

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      |
T Consensus         8 R    8 (36)
T signal            8 R    8 (36)
T 145               8 R    8 (36)
T 135               8 R    8 (35)
T 141               8 R    8 (36)
T 140               8 R    8 (35)
T 134               6 R    6 (34)
T 138               6 R    6 (35)
T 139               6 R    6 (33)
T 143               6 R    6 (34)
T 144               6 R    6 (34)
Confidence            3


No 3  
>Q99797_35_MM_ref_sig5_130
Probab=1.85  E-value=12  Score=8.25  Aligned_cols=3  Identities=67%  Similarity=0.789  Sum_probs=1.3

Q 5_Mustela         3 QVS    5 (6)
Q 2_Macaca          3 EVS    5 (6)
Q 15_Ictidomys      3 QVS    5 (6)
Q 11_Cavia          3 QVS    5 (6)
Q 17_Tupaia         3 QVS    5 (6)
Q 4_Pongo           3 EVS    5 (6)
Q Consensus         3 qVS    5 (6)
                      .||
T Consensus        35 ~VS   37 (40)
T signal           35 RVS   37 (40)
T 102              35 TVS   37 (40)
T 164              35 RVS   37 (40)
T 180              35 RVS   37 (40)
T 183              35 RVS   37 (40)
T 122              34 GVS   36 (39)
T 158              32 DVS   34 (37)
T 168              35 WVS   37 (40)
T 174              35 GVS   37 (40)
T 155              36 RVS   38 (41)
Confidence            344


No 4  
>Q02253_32_Mito_ref_sig5_130
Probab=1.84  E-value=12  Score=8.18  Aligned_cols=3  Identities=100%  Similarity=0.922  Sum_probs=1.4

Q 5_Mustela         3 QVS    5 (6)
Q 2_Macaca          3 EVS    5 (6)
Q 15_Ictidomys      3 QVS    5 (6)
Q 11_Cavia          3 QVS    5 (6)
Q 17_Tupaia         3 QVS    5 (6)
Q 4_Pongo           3 EVS    5 (6)
Q Consensus         3 qVS    5 (6)
                      |||
T Consensus        17 qVS   19 (37)
T signal           17 QVS   19 (37)
T 23               17 QVS   19 (37)
T 135              16 QVS   18 (35)
T 138              17 QVS   19 (37)
T 134              14 RVS   16 (33)
Confidence            444


No 5  
>P10818_26_IM_ref_sig5_130
Probab=1.81  E-value=12  Score=7.84  Aligned_cols=2  Identities=100%  Similarity=0.933  Sum_probs=0.9

Q 5_Mustela         4 VS    5 (6)
Q 2_Macaca          4 VS    5 (6)
Q 15_Ictidomys      4 VS    5 (6)
Q 11_Cavia          4 VS    5 (6)
Q 17_Tupaia         4 VS    5 (6)
Q 4_Pongo           4 VS    5 (6)
Q Consensus         4 VS    5 (6)
                      ||
T Consensus        11 vS   12 (31)
T signal           11 VS   12 (31)
T 53                8 VS    9 (28)
T 56                8 VF    9 (28)
T 64                8 LS    9 (28)
T 36               10 LS   11 (30)
T 44                8 VS    9 (28)
T 55                8 LS    9 (28)
T 6                10 VL   11 (30)
T 7                 9 LS   10 (29)
T 43                6 LS    7 (25)
Confidence            44


No 6  
>Q02376_27_IM_ref_sig5_130
Probab=1.74  E-value=12  Score=7.83  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (6)
Q 2_Macaca          3 E    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q 17_Tupaia         3 Q    3 (6)
Q 4_Pongo           3 E    3 (6)
Q Consensus         3 q    3 (6)
                      .
T Consensus         9 p    9 (32)
T signal            9 P    9 (32)
T 10                9 P    9 (33)
T 11                9 S    9 (32)
T 17                9 S    9 (32)
T 30                9 S    9 (32)
T 13                9 P    9 (32)
T 12                9 P    9 (32)
T 8                10 Q   10 (33)
Confidence            3


No 7  
>P10817_9_IM_ref_sig5_130
Probab=1.68  E-value=13  Score=6.56  Aligned_cols=4  Identities=25%  Similarity=0.310  Sum_probs=1.5

Q 5_Mustela         2 EQVS    5 (6)
Q 2_Macaca          2 REVS    5 (6)
Q 15_Ictidomys      2 QQVS    5 (6)
Q 11_Cavia          2 RQVS    5 (6)
Q 17_Tupaia         2 QQVS    5 (6)
Q 4_Pongo           2 REVS    5 (6)
Q Consensus         2 ~qVS    5 (6)
                      |-.|
T Consensus         3 r~Ls    6 (14)
T signal            3 KVLS    6 (14)
T 14                3 QLLN    6 (14)
T 18                3 RSLS    6 (14)
T 22                3 KSLS    6 (14)
T 23                3 RLLG    6 (14)
T 46                3 RALN    6 (14)
T 4                 3 RTLS    6 (14)
Confidence            3333


No 8  
>Q7YR75_45_Mito_ref_sig5_130
Probab=1.55  E-value=14  Score=8.31  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (6)
Q 2_Macaca          3 E    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q 17_Tupaia         3 Q    3 (6)
Q 4_Pongo           3 E    3 (6)
Q Consensus         3 q    3 (6)
                      |
T Consensus        27 q   27 (50)
T signal           27 Q   27 (50)
T 12               27 Q   27 (50)
T 77               27 Q   27 (50)
T 107              27 P   27 (50)
T 24               27 P   27 (50)
T 62               28 Q   28 (51)
T 88               16 Q   16 (39)
T 109              11 H   11 (34)
T 84               13 A   13 (36)
T 89               27 E   27 (49)
Confidence            2


No 9  
>P07471_12_IM_ref_sig5_130
Probab=1.54  E-value=14  Score=6.77  Aligned_cols=4  Identities=25%  Similarity=0.451  Sum_probs=1.5

Q 5_Mustela         2 EQVS    5 (6)
Q 2_Macaca          2 REVS    5 (6)
Q 15_Ictidomys      2 QQVS    5 (6)
Q 11_Cavia          2 RQVS    5 (6)
Q 17_Tupaia         2 QQVS    5 (6)
Q 4_Pongo           2 REVS    5 (6)
Q Consensus         2 ~qVS    5 (6)
                      |-.|
T Consensus         6 r~Ls    9 (17)
T signal            6 KSLS    9 (17)
T 8                 6 QLLN    9 (17)
T 15                6 KVLS    9 (17)
T 17                6 RLLG    9 (17)
T 18                6 RALS    9 (17)
T 42                6 RALN    9 (17)
T 3                 6 RTLS    9 (17)
Confidence            3333


No 10 
>P12074_24_IM_ref_sig5_130
Probab=1.50  E-value=15  Score=7.50  Aligned_cols=2  Identities=100%  Similarity=0.933  Sum_probs=0.8

Q 5_Mustela         4 VS    5 (6)
Q 2_Macaca          4 VS    5 (6)
Q 15_Ictidomys      4 VS    5 (6)
Q 11_Cavia          4 VS    5 (6)
Q 17_Tupaia         4 VS    5 (6)
Q 4_Pongo           4 VS    5 (6)
Q Consensus         4 VS    5 (6)
                      ||
T Consensus         9 vs   10 (29)
T signal            9 VS   10 (29)
T 32                9 VS   10 (29)
T 37               11 LS   12 (31)
T 43                9 VS   10 (29)
T 54                9 LS   10 (29)
T 44                9 VS   10 (29)
T 51                9 VF   10 (29)
T 59                9 LS   10 (29)
T 60                9 VA   10 (29)
T 7                 7 VL    8 (27)
Confidence            33


No 11 
>Q5JRX3_15_MM_ref_sig5_130
Probab=1.48  E-value=15  Score=6.63  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S.    5 (6)
Q 2_Macaca          5 S.    5 (6)
Q 15_Ictidomys      5 S.    5 (6)
Q 11_Cavia          5 S.    5 (6)
Q 17_Tupaia         5 S.    5 (6)
Q 4_Pongo           5 S.    5 (6)
Q Consensus         5 S.    5 (6)
                      | 
T Consensus        17 ~.   17 (20)
T signal           17 S.   17 (20)
T 140              19 N.   19 (19)
T 151              16 S.   16 (18)
T 159              17 K.   17 (19)
T 164              15 S.   15 (17)
T 112              16 G.   16 (18)
T 143              21 Gi   22 (24)
T 146              17 K.   17 (19)
Confidence            2 


No 12 
>P11177_30_MM_ref_sig5_130
Probab=1.44  E-value=15  Score=7.65  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.4

Q 5_Mustela         4 V    4 (6)
Q 2_Macaca          4 V    4 (6)
Q 15_Ictidomys      4 V    4 (6)
Q 11_Cavia          4 V    4 (6)
Q 17_Tupaia         4 V    4 (6)
Q 4_Pongo           4 V    4 (6)
Q Consensus         4 V    4 (6)
                      |
T Consensus        15 v   15 (35)
T signal           15 V   15 (35)
T 143              19 L   19 (39)
T 144              15 V   15 (35)
T 145              15 L   15 (35)
T 147              15 V   15 (35)
T 135              24 L   24 (44)
T 141              29 Q   29 (49)
T 142              14 V   14 (34)
T 149              14 V   14 (34)
T 150              14 V   14 (34)
Confidence            3


No 13 
>Q3SZ86_26_Mito_ref_sig5_130
Probab=1.40  E-value=16  Score=7.47  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.3

Q 5_Mustela         4 V    4 (6)
Q 2_Macaca          4 V    4 (6)
Q 15_Ictidomys      4 V    4 (6)
Q 11_Cavia          4 V    4 (6)
Q 17_Tupaia         4 V    4 (6)
Q 4_Pongo           4 V    4 (6)
Q Consensus         4 V    4 (6)
                      +
T Consensus         5 L    5 (31)
T signal            5 L    5 (31)
T 54                5 L    5 (31)
T 32                5 L    5 (31)
T 52                5 L    5 (31)
T 47                5 L    5 (31)
T 22                5 L    5 (30)
T 27                5 V    5 (35)
T 25                5 L    5 (33)
T 15                5 L    5 (38)
Confidence            2


No 14 
>P80971_28_IM_ref_sig5_130
Probab=1.35  E-value=17  Score=7.58  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.2

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      +
T Consensus         3 r    3 (33)
T signal            3 R    3 (33)
T 94                3 R    3 (31)
T 96                3 R    3 (31)
T 93                3 C    3 (32)
T 92                3 R    3 (32)
Confidence            2


No 15 
>Q12165_22_Mito_IM_ref_sig5_130
Probab=1.34  E-value=17  Score=7.11  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      -
T Consensus        27 ~   27 (27)
T signal           27 A   27 (27)
T 167              27 A   27 (27)
T 155              26 A   26 (26)
T 165              27 -   26 (26)
T 158              27 -   26 (26)
T 160              28 -   27 (27)
T 159              26 A   26 (26)
T 124              24 -   23 (23)
T 147              24 -   23 (23)
T 150              24 -   23 (23)
Confidence            0


No 16 
>P24118_21_MM_Cy_ref_sig5_130
Probab=1.32  E-value=17  Score=7.20  Aligned_cols=1  Identities=0%  Similarity=-0.927  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      -
T Consensus        26 l   26 (26)
T signal           26 L   26 (26)
Confidence            0


No 17 
>P08165_32_MM_ref_sig5_130
Probab=1.30  E-value=17  Score=4.63  Aligned_cols=1  Identities=0%  Similarity=0.568  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      .
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 117_Pyrenophor   30 -   29 (29)
T 89_Acyrthosiph   30 -   29 (29)
T 86_Musca         30 -   29 (29)
T 72_Nematostell   30 -   29 (29)
T 37_Myotis        30 -   29 (29)
T 34_Pongo         30 -   29 (29)
Confidence            0


No 18 
>P25705_43_IM_Cm_ref_sig5_130
Probab=1.30  E-value=18  Score=5.26  Aligned_cols=1  Identities=0%  Similarity=-0.030  Sum_probs=0.0

Q 5_Mustela         2 E    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 Q    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 ~    2 (6)
                      -
T Consensus        48 ~   48 (48)
T signal           48 T   48 (48)
T 185_Reclinomon   47 -   46 (46)
T 179_Theileria    47 -   46 (46)
T 143_Galdieria    47 -   46 (46)
T 135_Naumovozym   47 -   46 (46)
T 124_Cryptococc   47 -   46 (46)
T 108_Paramecium   47 -   46 (46)
T 101_Trichoplax   47 -   46 (46)
T 100_Trichinell   47 -   46 (46)
T 96_Strongyloce   47 -   46 (46)
Confidence            0


No 19 
>Q7Z6M4_42_Mito_ref_sig5_130
Probab=1.22  E-value=19  Score=7.83  Aligned_cols=2  Identities=100%  Similarity=1.016  Sum_probs=0.8

Q 5_Mustela         3 QV    4 (6)
Q 2_Macaca          3 EV    4 (6)
Q 15_Ictidomys      3 QV    4 (6)
Q 11_Cavia          3 QV    4 (6)
Q 17_Tupaia         3 QV    4 (6)
Q 4_Pongo           3 EV    4 (6)
Q Consensus         3 qV    4 (6)
                      ||
T Consensus         7 qV    8 (47)
T signal            7 QV    8 (47)
T 36                7 RV    8 (46)
T 42                7 QV    8 (47)
T 38                7 QV    8 (47)
T 33                1 -V    1 (40)
T 39                7 QV    8 (43)
T 34                5 GV    6 (43)
T 30                2 TV    3 (43)
T 31                1 -A    1 (32)
T 29                7 QV    8 (44)
Confidence            33


No 20 
>Q7M0E7_30_Mito_ref_sig5_130
Probab=1.22  E-value=19  Score=7.30  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         4 V    4 (6)
Q 2_Macaca          4 V    4 (6)
Q 15_Ictidomys      4 V    4 (6)
Q 11_Cavia          4 V    4 (6)
Q 17_Tupaia         4 V    4 (6)
Q 4_Pongo           4 V    4 (6)
Q Consensus         4 V    4 (6)
                      |
T Consensus        14 ~   14 (35)
T signal           14 V   14 (35)
T 53               14 M   14 (35)
T 33               13 A   13 (34)
T 49               13 V   13 (34)
T 47               14 V   14 (35)
T 31               12 A   12 (33)
T 38               12 L   12 (33)
T 34               15 V   15 (36)
T 27                9 T    9 (30)
T 30                9 T    9 (30)
Confidence            2


Done!
