Query         5_Mustela
Match_columns 6
No_of_seqs    6 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:08:00 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_10A16_10Q16_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P10817_9_IM_ref_sig5_130         8.6     1.7  0.0039    8.6   0.0    6    1-6       1-6   (14)
  2 P32799_9_IM_ref_sig5_130         1.5      14   0.033    6.4   0.0    3    2-4       2-4   (14)
  3 Q5A8K2_8_Mito_Cy_ref_sig5_130    1.5      14   0.033    6.4   0.0    3    3-5       7-9   (13)
  4 P12007_30_MM_ref_sig5_130        1.5      14   0.033    7.8   0.0    1    2-2      35-35  (35)
  5 P33421_50_IM_ref_sig5_130        1.5      15   0.035    8.4   0.0    2    2-3      49-50  (55)
  6 P08165_32_MM_ref_sig5_130        1.4      16   0.036    4.7   0.0    1    2-2      37-37  (37)
  7 P36516_59_Mito_ref_sig5_130      1.3      18    0.04    8.5   0.0    1    4-4      42-42  (64)
  8 Q0P5L8_21_Mito_Nu_ref_sig5_130   1.2      18   0.042    4.0   0.0    1    2-2      26-26  (26)
  9 P24118_21_MM_Cy_ref_sig5_130     1.1      21   0.048    7.0   0.0    1    4-4      10-10  (26)
 10 Q9NUB1_37_MM_ref_sig5_130        1.1      22   0.049    7.6   0.0    1    2-2      28-28  (42)
 11 Q01992_33_MM_ref_sig5_130        1.0      23   0.052    7.4   0.0    1    5-5      34-34  (38)
 12 P20069_32_MM_ref_sig5_130        1.0      24   0.056    7.3   0.0    1    4-4      31-31  (37)
 13 Q7Z6M4_42_Mito_ref_sig5_130      1.0      24   0.056    7.5   0.0    1    4-4       7-7   (47)
 14 Q99797_35_MM_ref_sig5_130        1.0      25   0.056    7.4   0.0    1    5-5      36-36  (40)
 15 P13621_23_Mito_IM_ref_sig5_130   0.9      26   0.059    6.8   0.0    1    4-4      12-12  (28)
 16 Q02376_27_IM_ref_sig5_130        0.9      26    0.06    7.0   0.0    1    3-3       8-8   (32)
 17 Q02337_46_MM_ref_sig5_130        0.9      27   0.061    5.5   0.0    1    1-1       1-1   (51)
 18 P34899_31_Mito_ref_sig5_130      0.9      28   0.064    7.1   0.0    1    3-3       8-8   (36)
 19 Q25423_17_IM_ref_sig5_130        0.8      28   0.065    6.4   0.0    1    5-5       4-4   (22)
 20 P07271_45_Nu_IM_ref_sig5_130     0.8      29   0.066    7.5   0.0    1    2-2      50-50  (50)

No 1  
>P10817_9_IM_ref_sig5_130
Probab=8.60  E-value=1.7  Score=8.63  Aligned_cols=6  Identities=50%  Similarity=0.850  Sum_probs=3.1

Q 5_Mustela         1 PLEQVS    6 (6)
Q 2_Macaca          1 PLREVS    6 (6)
Q 15_Ictidomys      1 PLQQVS    6 (6)
Q 11_Cavia          1 PLRQVS    6 (6)
Q 17_Tupaia         1 PFQQVS    6 (6)
Q 16_Equus          1 SLEQVS    6 (6)
Q Consensus         1 pl~qVS    6 (6)
                      ||+-.|
T Consensus         1 plr~Ls    6 (14)
T signal            1 PLKVLS    6 (14)
T 14                1 PLQLLN    6 (14)
T 18                1 RLRSLS    6 (14)
T 22                1 PLKSLS    6 (14)
T 23                1 ARRLLG    6 (14)
T 46                1 PLRALN    6 (14)
T 4                 1 PLRTLS    6 (14)
Confidence            565443


No 2  
>P32799_9_IM_ref_sig5_130
Probab=1.55  E-value=14  Score=6.41  Aligned_cols=3  Identities=33%  Similarity=0.567  Sum_probs=1.5

Q 5_Mustela         2 LEQ    4 (6)
Q 2_Macaca          2 LRE    4 (6)
Q 15_Ictidomys      2 LQQ    4 (6)
Q 11_Cavia          2 LRQ    4 (6)
Q 17_Tupaia         2 FQQ    4 (6)
Q 16_Equus          2 LEQ    4 (6)
Q Consensus         2 l~q    4 (6)
                      |+|
T Consensus         2 ~rq    4 (14)
T signal            2 FRQ    4 (14)
T 25                2 LAR    4 (12)
T 27                2 LRT    4 (12)
T 33                2 FRQ    4 (14)
T 34                2 FRQ    4 (13)
T 35                2 YRQ    4 (13)
T 40                2 FKQ    4 (13)
T 24                2 LRQ    4 (13)
T 43                2 LRQ    4 (13)
Confidence            455


No 3  
>Q5A8K2_8_Mito_Cy_ref_sig5_130
Probab=1.53  E-value=14  Score=6.39  Aligned_cols=3  Identities=0%  Similarity=0.390  Sum_probs=1.2

Q 5_Mustela         3 EQV    5 (6)
Q 2_Macaca          3 REV    5 (6)
Q 15_Ictidomys      3 QQV    5 (6)
Q 11_Cavia          3 RQV    5 (6)
Q 17_Tupaia         3 QQV    5 (6)
Q 16_Equus          3 EQV    5 (6)
Q Consensus         3 ~qV    5 (6)
                      +..
T Consensus         7 rrm    9 (13)
T signal            7 RRM    9 (13)
Confidence            433


No 4  
>P12007_30_MM_ref_sig5_130
Probab=1.53  E-value=14  Score=7.82  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q 17_Tupaia         2 F    2 (6)
Q 16_Equus          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        35 P   35 (35)
T signal           35 P   35 (35)
T 106              37 P   37 (37)
T 129              34 P   34 (34)
T 134              34 P   34 (34)
T 136              37 P   37 (37)
T 141              37 P   37 (37)
T 122              26 -   25 (25)
Confidence            0


No 5  
>P33421_50_IM_ref_sig5_130
Probab=1.45  E-value=15  Score=8.43  Aligned_cols=2  Identities=50%  Similarity=0.866  Sum_probs=0.7

Q 5_Mustela         2 LE    3 (6)
Q 2_Macaca          2 LR    3 (6)
Q 15_Ictidomys      2 LQ    3 (6)
Q 11_Cavia          2 LR    3 (6)
Q 17_Tupaia         2 FQ    3 (6)
Q 16_Equus          2 LE    3 (6)
Q Consensus         2 l~    3 (6)
                      |.
T Consensus        49 lk   50 (55)
T signal           49 LK   50 (55)
Confidence            33


No 6  
>P08165_32_MM_ref_sig5_130
Probab=1.43  E-value=16  Score=4.73  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q 17_Tupaia         2 F    2 (6)
Q 16_Equus          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      .
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 117_Pyrenophor   30 -   29 (29)
T 89_Acyrthosiph   30 -   29 (29)
T 86_Musca         30 -   29 (29)
T 72_Nematostell   30 -   29 (29)
T 37_Myotis        30 -   29 (29)
T 34_Pongo         30 -   29 (29)
Confidence            0


No 7  
>P36516_59_Mito_ref_sig5_130
Probab=1.29  E-value=18  Score=8.47  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         4 Q    4 (6)
Q 2_Macaca          4 E    4 (6)
Q 15_Ictidomys      4 Q    4 (6)
Q 11_Cavia          4 Q    4 (6)
Q 17_Tupaia         4 Q    4 (6)
Q 16_Equus          4 Q    4 (6)
Q Consensus         4 q    4 (6)
                      |
T Consensus        42 q   42 (64)
T signal           42 Q   42 (64)
Confidence            2


No 8  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=1.24  E-value=18  Score=3.99  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q 17_Tupaia         2 F    2 (6)
Q 16_Equus          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 9  
>P24118_21_MM_Cy_ref_sig5_130
Probab=1.12  E-value=21  Score=7.00  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         4 Q    4 (6)
Q 2_Macaca          4 E    4 (6)
Q 15_Ictidomys      4 Q    4 (6)
Q 11_Cavia          4 Q    4 (6)
Q 17_Tupaia         4 Q    4 (6)
Q 16_Equus          4 Q    4 (6)
Q Consensus         4 q    4 (6)
                      |
T Consensus        10 q   10 (26)
T signal           10 Q   10 (26)
Confidence            2


No 10 
>Q9NUB1_37_MM_ref_sig5_130
Probab=1.08  E-value=22  Score=7.60  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.2

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q 17_Tupaia         2 F    2 (6)
Q 16_Equus          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      +
T Consensus        28 ~   28 (42)
T signal           28 P   28 (42)
T 89               28 L   28 (42)
T 91               28 P   28 (42)
T 95               28 L   28 (33)
T 98               28 G   28 (37)
T 103              27 L   27 (41)
T 107              28 L   28 (39)
T 110              28 L   28 (42)
T 93               29 P   29 (29)
T 105              28 L   28 (38)
Confidence            1


No 11 
>Q01992_33_MM_ref_sig5_130
Probab=1.03  E-value=23  Score=7.43  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.4

Q 5_Mustela         5 V    5 (6)
Q 2_Macaca          5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q 11_Cavia          5 V    5 (6)
Q 17_Tupaia         5 V    5 (6)
Q 16_Equus          5 V    5 (6)
Q Consensus         5 V    5 (6)
                      |
T Consensus        34 V   34 (38)
T signal           34 V   34 (38)
T 180              34 V   34 (38)
T 130              33 V   33 (37)
T 102              32 V   32 (36)
T 165              32 V   32 (36)
T 168              32 V   32 (36)
T 182              32 V   32 (36)
T 186              32 V   32 (36)
T 164              32 V   32 (36)
T 169              32 V   32 (36)
Confidence            3


No 12 
>P20069_32_MM_ref_sig5_130
Probab=0.98  E-value=24  Score=7.28  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (6)
Q 2_Macaca          4 E    4 (6)
Q 15_Ictidomys      4 Q    4 (6)
Q 11_Cavia          4 Q    4 (6)
Q 17_Tupaia         4 Q    4 (6)
Q 16_Equus          4 Q    4 (6)
Q Consensus         4 q    4 (6)
                      |
T Consensus        31 q   31 (37)
T signal           31 R   31 (37)
T 143              31 Q   31 (37)
T 58               32 R   32 (37)
T 118              35 Q   35 (40)
T 119              33 R   33 (38)
T 123              32 Q   32 (37)
T 127              33 G   33 (38)
T 129              33 Q   33 (38)
T 134              32 Q   32 (37)
T 121              22 R   22 (27)
Confidence            3


No 13 
>Q7Z6M4_42_Mito_ref_sig5_130
Probab=0.97  E-value=24  Score=7.53  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.4

Q 5_Mustela         4 Q    4 (6)
Q 2_Macaca          4 E    4 (6)
Q 15_Ictidomys      4 Q    4 (6)
Q 11_Cavia          4 Q    4 (6)
Q 17_Tupaia         4 Q    4 (6)
Q 16_Equus          4 Q    4 (6)
Q Consensus         4 q    4 (6)
                      |
T Consensus         7 q    7 (47)
T signal            7 Q    7 (47)
T 36                7 R    7 (46)
T 42                7 Q    7 (47)
T 38                7 Q    7 (47)
T 33                1 -    0 (40)
T 39                7 Q    7 (43)
T 34                5 G    5 (43)
T 30                2 T    2 (43)
T 31                1 -    0 (32)
T 29                7 Q    7 (44)
Confidence            3


No 14 
>Q99797_35_MM_ref_sig5_130
Probab=0.97  E-value=25  Score=7.39  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         5 V    5 (6)
Q 2_Macaca          5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q 11_Cavia          5 V    5 (6)
Q 17_Tupaia         5 V    5 (6)
Q 16_Equus          5 V    5 (6)
Q Consensus         5 V    5 (6)
                      |
T Consensus        36 V   36 (40)
T signal           36 V   36 (40)
T 102              36 V   36 (40)
T 164              36 V   36 (40)
T 180              36 V   36 (40)
T 183              36 V   36 (40)
T 122              35 V   35 (39)
T 158              33 V   33 (37)
T 168              36 V   36 (40)
T 174              36 V   36 (40)
T 155              37 V   37 (41)
Confidence            3


No 15 
>P13621_23_Mito_IM_ref_sig5_130
Probab=0.93  E-value=26  Score=6.83  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.4

Q 5_Mustela         4 Q    4 (6)
Q 2_Macaca          4 E    4 (6)
Q 15_Ictidomys      4 Q    4 (6)
Q 11_Cavia          4 Q    4 (6)
Q 17_Tupaia         4 Q    4 (6)
Q 16_Equus          4 Q    4 (6)
Q Consensus         4 q    4 (6)
                      |
T Consensus        12 q   12 (28)
T signal           12 Q   12 (28)
T 148              12 Q   12 (28)
T 131               8 Q    8 (24)
T 123               8 K    8 (24)
T 137               5 K    5 (21)
T 130               8 K    8 (24)
T 144               8 K    8 (24)
T 134               8 K    8 (24)
Confidence            3


No 16 
>Q02376_27_IM_ref_sig5_130
Probab=0.91  E-value=26  Score=6.98  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.2

Q 5_Mustela         3 E    3 (6)
Q 2_Macaca          3 R    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 R    3 (6)
Q 17_Tupaia         3 Q    3 (6)
Q 16_Equus          3 E    3 (6)
Q Consensus         3 ~    3 (6)
                      +
T Consensus         8 r    8 (32)
T signal            8 R    8 (32)
T 10                8 R    8 (33)
T 11                8 R    8 (32)
T 17                8 R    8 (32)
T 30                8 R    8 (32)
T 13                8 R    8 (32)
T 12                8 H    8 (32)
T 8                 9 S    9 (33)
Confidence            2


No 17 
>Q02337_46_MM_ref_sig5_130
Probab=0.89  E-value=27  Score=5.46  Aligned_cols=1  Identities=0%  Similarity=-0.796  Sum_probs=0.0

Q 5_Mustela         1 P    1 (6)
Q 2_Macaca          1 P    1 (6)
Q 15_Ictidomys      1 P    1 (6)
Q 11_Cavia          1 P    1 (6)
Q 17_Tupaia         1 P    1 (6)
Q 16_Equus          1 S    1 (6)
Q Consensus         1 p    1 (6)
                      =
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 76_Pediculus      1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 69_Papio          1 V    1 (50)
T 67_Maylandia      1 M    1 (50)
T 60_Sus            1 M    1 (50)
T 72_Ciona          1 M    1 (50)
T 59_Anolis         1 I    1 (50)
T 51_Canis          1 M    1 (50)
T 38_Nomascus       1 M    1 (50)
Confidence            0


No 18 
>P34899_31_Mito_ref_sig5_130
Probab=0.87  E-value=28  Score=7.10  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.3

Q 5_Mustela         3 E    3 (6)
Q 2_Macaca          3 R    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 R    3 (6)
Q 17_Tupaia         3 Q    3 (6)
Q 16_Equus          3 E    3 (6)
Q Consensus         3 ~    3 (6)
                      |
T Consensus         8 R    8 (36)
T signal            8 R    8 (36)
T 145               8 R    8 (36)
T 135               8 R    8 (35)
T 141               8 R    8 (36)
T 140               8 R    8 (35)
T 134               6 R    6 (34)
T 138               6 R    6 (35)
T 139               6 R    6 (33)
T 143               6 R    6 (34)
T 144               6 R    6 (34)
Confidence            2


No 19 
>Q25423_17_IM_ref_sig5_130
Probab=0.85  E-value=28  Score=6.43  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.3

Q 5_Mustela         5 V    5 (6)
Q 2_Macaca          5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q 11_Cavia          5 V    5 (6)
Q 17_Tupaia         5 V    5 (6)
Q 16_Equus          5 V    5 (6)
Q Consensus         5 V    5 (6)
                      |
T Consensus         4 ~    4 (22)
T signal            4 L    4 (22)
T cl|CABBABABA|1    4 V    4 (25)
Confidence            2


No 20 
>P07271_45_Nu_IM_ref_sig5_130
Probab=0.83  E-value=29  Score=7.51  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q 17_Tupaia         2 F    2 (6)
Q 16_Equus          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      +
T Consensus        50 f   50 (50)
T signal           50 F   50 (50)
Confidence            0


Done!
