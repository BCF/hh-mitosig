Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:49 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_82A89_82Q89_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P19955_8_Mito_ref_sig5_130      35.1    0.17  0.0004   11.3   0.0    6    1-6       2-7   (13)
  2 P42847_59_Mito_ref_sig5_130      0.5      55    0.13    7.3   0.0    1    3-3      64-64  (64)
  3 P13182_24_IM_ref_sig5_130        0.5      56    0.13    6.2   0.0    3    4-6      23-25  (29)
  4 Q3T0L3_8_Mito_ref_sig5_130       0.5      57    0.13    5.1   0.0    2    5-6       9-10  (13)
  5 P54898_44_Mito_ref_sig5_130      0.4      61    0.14    6.8   0.0    1    5-5      37-37  (49)
  6 P0C2C0_40_Mito_ref_sig5_130      0.4      62    0.14    6.5   0.0    1    4-4      29-29  (45)
  7 P23786_25_IM_ref_sig5_130        0.4      63    0.14    6.1   0.0    1    5-5      25-25  (30)
  8 P12074_24_IM_ref_sig5_130        0.4      63    0.14    6.1   0.0    3    4-6      23-25  (29)
  9 P12687_27_Mito_ref_sig5_130      0.4      63    0.14    6.2   0.0    1    2-2       8-8   (32)
 10 P42026_37_Mito_ref_sig5_130      0.4      64    0.15    6.4   0.0    1    4-4      38-38  (42)
 11 P00366_57_MM_ref_sig5_130        0.4      65    0.15    4.7   0.0    1    1-1       1-1   (62)
 12 P10818_26_IM_ref_sig5_130        0.4      68    0.16    6.0   0.0    2    4-5      25-26  (31)
 13 P05091_17_MM_ref_sig5_130        0.4      72    0.16    5.5   0.0    3    4-6      16-18  (22)
 14 P22557_49_MM_ref_sig5_130        0.4      73    0.17    3.7   0.0    1    3-3      54-54  (54)
 15 P22142_42_IM_ref_sig5_130        0.4      73    0.17    4.7   0.0    1    1-1       1-1   (47)
 16 P80433_25_IM_ref_sig5_130        0.4      73    0.17    6.0   0.0    1    4-4       6-6   (30)
 17 Q9UGC7_26_Mito_ref_sig5_130      0.3      75    0.17    2.3   0.0    1    3-3      31-31  (31)
 18 P56522_34_MM_ref_sig5_130        0.3      76    0.18    2.5   0.0    1    1-1       1-1   (39)
 19 P25284_26_MM_ref_sig5_130        0.3      77    0.18    5.9   0.0    1    6-6      28-28  (31)
 20 P13216_19_MM_ref_sig5_130        0.3      77    0.18    5.6   0.0    1    3-3      24-24  (24)

No 1  
>P19955_8_Mito_ref_sig5_130
Probab=35.13  E-value=0.17  Score=11.32  Aligned_cols=6  Identities=67%  Similarity=0.977  Sum_probs=4.1

Q 5_Mustela         1 IDTPIS    6 (7)
Q Consensus         1 idtpis    6 (7)
                      |-|||.
T Consensus         2 iatpir    7 (13)
T signal            2 IATPIR    7 (13)
Confidence            568884


No 2  
>P42847_59_Mito_ref_sig5_130
Probab=0.47  E-value=55  Score=7.30  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q Consensus         3 t    3 (7)
                      .
T Consensus        64 s   64 (64)
T signal           64 S   64 (64)
Confidence            0


No 3  
>P13182_24_IM_ref_sig5_130
Probab=0.46  E-value=56  Score=6.24  Aligned_cols=3  Identities=33%  Similarity=0.179  Sum_probs=1.2

Q 5_Mustela         4 PIS    6 (7)
Q Consensus         4 pis    6 (7)
                      |.|
T Consensus        23 pMS   25 (29)
T signal           23 CMS   25 (29)
T 11               25 PMS   27 (31)
T 16               23 PMS   25 (29)
T 19               23 PMS   25 (29)
T 26               23 PMS   25 (29)
T 30               23 PMS   25 (29)
T 33               23 PMA   25 (29)
Confidence            443


No 4  
>Q3T0L3_8_Mito_ref_sig5_130
Probab=0.46  E-value=57  Score=5.15  Aligned_cols=2  Identities=100%  Similarity=1.032  Sum_probs=0.9

Q 5_Mustela         5 IS    6 (7)
Q Consensus         5 is    6 (7)
                      ||
T Consensus         9 Is   10 (13)
T signal            9 IS   10 (13)
T 81                9 IS   10 (13)
T 95                9 IS   10 (13)
T 83                9 IS   10 (13)
T 85                9 IS   10 (13)
T 88                9 VS   10 (13)
T 89                9 IS   10 (13)
T 76                9 IT   10 (13)
Confidence            44


No 5  
>P54898_44_Mito_ref_sig5_130
Probab=0.42  E-value=61  Score=6.81  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 I    5 (7)
Q Consensus         5 i    5 (7)
                      |
T Consensus        37 ~   37 (49)
T signal           37 I   37 (49)
T 154              39 F   39 (51)
T 151              38 F   38 (50)
T 104              37 I   37 (43)
T 137              44 F   44 (49)
Confidence            2


No 6  
>P0C2C0_40_Mito_ref_sig5_130
Probab=0.42  E-value=62  Score=6.52  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        29 p   29 (45)
T signal           29 P   29 (45)
T 75               29 P   29 (45)
T 62               29 P   29 (43)
T 37               35 P   35 (51)
T 53               10 P   10 (26)
T 57               10 P   10 (26)
T 43               34 P   34 (51)
T 39               32 Q   32 (44)
T 49               33 Q   33 (45)
T 51               34 L   34 (50)
Confidence            2


No 7  
>P23786_25_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=6.13  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         5 I    5 (7)
Q Consensus         5 i    5 (7)
                      +
T Consensus        25 L   25 (30)
T signal           25 L   25 (30)
T 101              25 L   25 (30)
T 104              25 L   25 (30)
T 108              25 L   25 (30)
T 109              26 L   26 (31)
T 117              25 L   25 (30)
T 119              25 L   25 (30)
T 59               30 L   30 (35)
Confidence            3


No 8  
>P12074_24_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=6.07  Aligned_cols=3  Identities=67%  Similarity=1.364  Sum_probs=1.1

Q 5_Mustela         4 PIS    6 (7)
Q Consensus         4 pis    6 (7)
                      |.|
T Consensus        23 pMS   25 (29)
T signal           23 PMS   25 (29)
T 32               23 PMS   25 (29)
T 37               25 PMS   27 (31)
T 43               23 LMS   25 (29)
T 54               23 PMS   25 (29)
T 44               23 PMS   25 (29)
T 51               23 PMS   25 (29)
T 59               23 PMS   25 (29)
T 60               23 PMS   25 (29)
T 7                21 PMS   23 (27)
Confidence            333


No 9  
>P12687_27_Mito_ref_sig5_130
Probab=0.41  E-value=63  Score=6.22  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         2 D    2 (7)
Q Consensus         2 d    2 (7)
                      |
T Consensus         8 d    8 (32)
T signal            8 D    8 (32)
T 93                7 G    7 (31)
T 84                1 D    1 (25)
Confidence            2


No 10 
>P42026_37_Mito_ref_sig5_130
Probab=0.40  E-value=64  Score=6.42  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        38 p   38 (42)
T signal           38 P   38 (42)
T 163              39 P   39 (43)
T 166              39 P   39 (43)
T 160              39 P   39 (43)
T 150              35 P   35 (39)
T 144              37 P   37 (37)
T 133              31 A   31 (33)
T 136              40 A   40 (42)
T 138              28 A   28 (30)
T 143              27 A   27 (29)
Confidence            2


No 11 
>P00366_57_MM_ref_sig5_130
Probab=0.40  E-value=65  Score=4.68  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         1 I    1 (7)
Q Consensus         1 i    1 (7)
                      .
T Consensus         1 M    1 (62)
T signal            1 M    1 (62)
T 114_Galdieria     1 M    1 (43)
T 110_Naegleria     1 M    1 (43)
T 104_Ichthyopht    1 M    1 (43)
T 102_Guillardia    1 M    1 (43)
T 93_Amphimedon     1 M    1 (43)
T 92_Schistosoma    1 M    1 (43)
T 89_Culex          1 M    1 (43)
T 69_Gallus         1 M    1 (43)
T 24_Saimiri        1 V    1 (43)
Confidence            0


No 12 
>P10818_26_IM_ref_sig5_130
Probab=0.38  E-value=68  Score=6.04  Aligned_cols=2  Identities=50%  Similarity=1.680  Sum_probs=0.7

Q 5_Mustela         4 PI    5 (7)
Q Consensus         4 pi    5 (7)
                      |.
T Consensus        25 pM   26 (31)
T signal           25 PM   26 (31)
T 53               22 PM   23 (28)
T 56               22 PM   23 (28)
T 64               22 PM   23 (28)
T 36               24 PM   25 (30)
T 44               22 LM   23 (28)
T 55               22 PM   23 (28)
T 6                24 PM   25 (30)
T 7                23 SM   24 (29)
T 43               20 LF   21 (25)
Confidence            33


No 13 
>P05091_17_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.50  Aligned_cols=3  Identities=33%  Similarity=0.301  Sum_probs=1.1

Q 5_Mustela         4 PIS    6 (7)
Q Consensus         4 pis    6 (7)
                      |+|
T Consensus        16 llS   18 (22)
T signal           16 LLS   18 (22)
T 58               13 LLS   15 (19)
T 61               13 LLS   15 (19)
T 73               11 LLS   13 (17)
T 72               20 LLS   22 (26)
T 45               19 PFS   21 (25)
T 47               19 PLS   21 (22)
T 51               19 -LS   20 (24)
Confidence            333


No 14 
>P22557_49_MM_ref_sig5_130
Probab=0.36  E-value=73  Score=3.67  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q Consensus         3 t    3 (7)
                      -
T Consensus        54 ~   54 (54)
T signal           54 K   54 (54)
T 115_Kazachstan   54 -   53 (53)
T 106_Phytophtho   54 -   53 (53)
T 91_Dictyosteli   54 -   53 (53)
T 88_Puccinia      54 -   53 (53)
T 73_Aplysia       54 -   53 (53)
T 69_Bombus        54 -   53 (53)
T 68_Ceratitis     54 -   53 (53)
Confidence            0


No 15 
>P22142_42_IM_ref_sig5_130
Probab=0.36  E-value=73  Score=4.69  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         1 I    1 (7)
Q Consensus         1 i    1 (7)
                      .
T Consensus         1 M    1 (47)
T signal            1 M    1 (47)
T 180_Trichinell    1 M    1 (27)
T 177_Loa           1 M    1 (27)
T 164_Beta          1 M    1 (27)
T 135_Galdieria     1 M    1 (27)
Confidence            0


No 16 
>P80433_25_IM_ref_sig5_130
Probab=0.36  E-value=73  Score=5.96  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus         6 p    6 (30)
T signal            6 P    6 (30)
T 3                 6 P    6 (30)
T 20                6 P    6 (30)
T 22                6 P    6 (30)
T 31                6 P    6 (30)
T 36                6 P    6 (30)
T 5                 6 S    6 (30)
T 8                 6 P    6 (30)
T 19                6 P    6 (30)
Confidence            3


No 17 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.35  E-value=75  Score=2.25  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q Consensus         3 t    3 (7)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 119_Populus      31 -   30 (30)
T 107_Phytophtho   31 -   30 (30)
T 101_Setaria      31 -   30 (30)
T 82_Anopheles     31 -   30 (30)
T 69_Chrysemys     31 -   30 (30)
T 55_Geospiza      31 -   30 (30)
Confidence            0


No 18 
>P56522_34_MM_ref_sig5_130
Probab=0.34  E-value=76  Score=2.52  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         1 I    1 (7)
Q Consensus         1 i    1 (7)
                      .
T Consensus         1 M    1 (39)
T signal            1 M    1 (39)
T 138_Tuber         1 M    1 (37)
T 98_Oryza          1 M    1 (37)
T 88_Apis           1 M    1 (37)
T 81_Amphimedon     1 M    1 (37)
T 79_Trichoplax     1 M    1 (37)
T 61_Meleagris      1 M    1 (37)
Confidence            0


No 19 
>P25284_26_MM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.86  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         6 S    6 (7)
Q Consensus         6 s    6 (7)
                      |
T Consensus        28 S   28 (31)
T signal           28 S   28 (31)
T 188              28 S   28 (31)
T 184              27 S   27 (30)
T 187              26 S   26 (29)
T 171              25 S   25 (27)
T 182              25 S   25 (27)
T 164              24 S   24 (27)
T 183              25 S   25 (28)
Confidence            2


No 20 
>P13216_19_MM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.60  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q Consensus         3 t    3 (7)
                      -
T Consensus        24 D   24 (24)
T signal           24 D   24 (24)
T 128              18 D   18 (18)
T 135              23 D   23 (23)
T 143              23 D   23 (23)
T 127              18 D   18 (18)
T 105              17 D   17 (17)
T 130              17 D   17 (17)
Confidence            0


Done!
