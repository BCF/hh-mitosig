Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:08:51 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_44A50_44Q50_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P83565_46_Mito_ref_sig5_130      0.2 1.3E+02    0.29    3.1   0.0    5    1-5      47-51  (51)
  2 P10719_46_Mito_IM_ref_sig5_130   0.2 1.3E+02    0.29    5.6   0.0    2    5-6      44-45  (51)
  3 Q04467_39_Mito_ref_sig5_130      0.2 1.3E+02     0.3    2.4   0.0    1    5-5      43-43  (44)
  4 P29147_46_MM_ref_sig5_130        0.2 1.3E+02     0.3    3.0   0.0    6    1-6      46-51  (51)
  5 P00829_48_Mito_IM_ref_sig5_130   0.2 1.3E+02     0.3    5.6   0.0    2    5-6      44-45  (53)
  6 P10612_39_MitoM_ref_sig5_130     0.2 1.3E+02    0.31    3.3   0.0    3    3-5       2-4   (44)
  7 P00430_16_IM_ref_sig5_130        0.2 1.3E+02    0.31    4.5   0.0    2    1-2      20-21  (21)
  8 P36526_16_Mito_ref_sig5_130      0.2 1.4E+02    0.32    4.4   0.0    3    3-5      17-19  (21)
  9 P07471_12_IM_ref_sig5_130        0.2 1.4E+02    0.32    4.3   0.0    3    3-5       8-10  (17)
 10 P00366_57_MM_ref_sig5_130        0.2 1.4E+02    0.32    3.6   0.0    3    4-6      59-61  (62)
 11 P28834_11_Mito_ref_sig5_130      0.2 1.4E+02    0.33    4.2   0.0    3    3-5       2-4   (16)
 12 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.2 1.4E+02    0.33    1.9   0.0    1    3-3      24-24  (26)
 13 P25348_71_Mito_ref_sig5_130      0.2 1.5E+02    0.34    5.9   0.0    6    1-6      49-54  (76)
 14 Q9UGC7_26_Mito_ref_sig5_130      0.2 1.5E+02    0.34    1.5   0.0    5    1-5      27-31  (31)
 15 P15538_24_MitoM_ref_sig5_130     0.2 1.5E+02    0.34    4.8   0.0    3    3-5      18-20  (29)
 16 P83484_51_Mito_IM_ref_sig5_130   0.2 1.5E+02    0.34    4.2   0.0    3    3-5      45-47  (56)
 17 P36528_19_Mito_ref_sig5_130      0.2 1.5E+02    0.34    4.6   0.0    3    3-5       7-9   (24)
 18 P11325_9_MM_ref_sig5_130         0.2 1.5E+02    0.35    4.0   0.0    3    3-5       2-4   (14)
 19 P08165_32_MM_ref_sig5_130        0.2 1.5E+02    0.35    2.4   0.0    2    2-3      36-37  (37)
 20 P22570_32_MM_ref_sig5_130        0.2 1.5E+02    0.35    2.3   0.0    2    4-5      36-37  (37)

No 1  
>P83565_46_Mito_ref_sig5_130
Probab=0.21  E-value=1.3e+02  Score=3.10  Aligned_cols=5  Identities=40%  Similarity=0.215  Sum_probs=0.0

Q 5_Mustela         1 EELER    5 (6)
Q Consensus         1 eeler    5 (6)
                      .+..|
T Consensus        47 ~~~~~   51 (51)
T signal           47 AEPLR   51 (51)
T 76_Brugia        47 AKLR-   50 (50)
T 72_Bombyx        47 KKID-   50 (50)
T 65_Ficedula      47 PGGP-   50 (50)
T 58_Gallus        47 EANS-   50 (50)
T 44_Pantholops    47 PTSS-   50 (50)
T 39_Tursiops      47 RDAH-   50 (50)
T 18_Ictidomys     47 DRVE-   50 (50)


No 2  
>P10719_46_Mito_IM_ref_sig5_130
Probab=0.21  E-value=1.3e+02  Score=5.55  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (6)
Q Consensus         5 rd    6 (6)
                      ||
T Consensus        44 Rd   45 (51)
T signal           44 RD   45 (51)
T 130              44 GD   45 (50)
T 148              46 RD   47 (52)
T 110              37 RS   38 (44)
T 131              36 RD   37 (43)
T 135              37 RG   38 (44)
T 151              41 RD   42 (48)
T 149              51 RD   52 (58)
T 144              49 RD   50 (56)
T 145              44 RN   45 (51)


No 3  
>Q04467_39_Mito_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=2.38  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 R    5 (6)
Q Consensus         5 r    5 (6)
                      |
T Consensus        43 ~   43 (44)
T signal           43 R   43 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)


No 4  
>P29147_46_MM_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=2.98  Aligned_cols=6  Identities=17%  Similarity=0.053  Sum_probs=0.0

Q 5_Mustela         1 EELERD    6 (6)
Q Consensus         1 eelerd    6 (6)
                      ...+.|
T Consensus        46 ~~~~~~   51 (51)
T signal           46 YTSQAD   51 (51)
T 77_Aplysia       46 VVVIR-   50 (50)
T 61_Papio         46 SEVTG-   50 (50)
T 19_Tupaia        46 ASEVD-   50 (50)
T 80_Tribolium     46 WDVLD-   50 (50)
T 79_Apis          46 SQTSS-   50 (50)
T 74_Branchiosto   46 CRFLP-   50 (50)
T 54_Canis         46 GSRRA-   50 (50)
T 39_Nomascus      46 PHRGG-   50 (50)


No 5  
>P00829_48_Mito_IM_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=5.56  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (6)
Q Consensus         5 rd    6 (6)
                      ||
T Consensus        44 Rd   45 (53)
T signal           44 RD   45 (53)
T 125              44 GD   45 (52)
T 111              37 RS   38 (45)
T 131              37 RG   38 (45)
T 136              36 RD   37 (44)
T 152              41 RD   42 (49)
T 149              46 RD   47 (54)
T 142              49 RD   50 (58)
T 151              51 RD   52 (60)
T 147              44 RN   45 (53)


No 6  
>P10612_39_MitoM_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=3.33  Aligned_cols=3  Identities=67%  Similarity=0.966  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.+
T Consensus         2 l~~    4 (44)
T signal            2 LAR    4 (44)
T 73_Anas           2 RRG    4 (43)
T 71_Monodelphis    2 VPT    4 (43)
T 67_Ornithorhyn    2 LEG    4 (43)
T 65_Anolis         2 LEA    4 (43)
T 63_Danio          2 ARW    4 (43)
T 52_Xenopus        2 LLL    4 (43)
T 50_Tupaia         2 ALN    4 (43)
T 48_Melopsittac    2 PIP    4 (43)
T 47_Falco          2 LAR    4 (43)


No 7  
>P00430_16_IM_ref_sig5_130
Probab=0.19  E-value=1.3e+02  Score=4.53  Aligned_cols=2  Identities=100%  Similarity=1.199  Sum_probs=0.0

Q 5_Mustela         1 EE    2 (6)
Q Consensus         1 ee    2 (6)
                      ||
T Consensus        20 ee   21 (21)
T signal           20 EE   21 (21)
T 13               20 EE   21 (21)
T 14               20 ED   21 (21)
T 15               20 ED   21 (21)
T 16               20 EE   21 (21)
T 24               20 EE   21 (21)
T 25               20 AE   21 (21)
T 35               20 EE   21 (21)
T 44               20 EE   21 (21)
T 6                17 --   16 (16)


No 8  
>P36526_16_Mito_ref_sig5_130
Probab=0.19  E-value=1.4e+02  Score=4.37  Aligned_cols=3  Identities=67%  Similarity=0.955  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |-|
T Consensus        17 LtR   19 (21)
T signal           17 LTR   19 (21)
T 11               17 LTR   19 (21)
T 13               17 LTR   19 (21)
T 14               17 LTR   19 (21)
T 18               16 LTR   18 (20)
T 20               17 LLR   19 (21)
T 5                17 LRR   19 (21)
T 7                17 LRR   19 (21)
T 8                17 LRR   19 (21)
T 9                13 LKR   15 (17)


No 9  
>P07471_12_IM_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=4.26  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus         8 LsR   10 (17)
T signal            8 LSR   10 (17)
T 8                 8 LNR   10 (17)
T 15                8 LSR   10 (17)
T 17                8 LGR   10 (17)
T 18                8 LSR   10 (17)
T 42                8 LNR   10 (17)
T 3                 8 LSR   10 (17)


No 10 
>P00366_57_MM_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=3.60  Aligned_cols=3  Identities=33%  Similarity=1.121  Sum_probs=0.0

Q 5_Mustela         4 ERD    6 (6)
Q Consensus         4 erd    6 (6)
                      +|.
T Consensus        59 ~~~   61 (62)
T signal           59 DRE   61 (62)
T 114_Galdieria    44 ---   43 (43)
T 110_Naegleria    44 ---   43 (43)
T 104_Ichthyopht   44 ---   43 (43)
T 102_Guillardia   44 ---   43 (43)
T 93_Amphimedon    44 ---   43 (43)
T 92_Schistosoma   44 ---   43 (43)
T 89_Culex         44 ---   43 (43)
T 69_Gallus        44 ---   43 (43)
T 24_Saimiri       44 ---   43 (43)


No 11 
>P28834_11_Mito_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=4.19  Aligned_cols=3  Identities=67%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus         2 lnr    4 (16)
T signal            2 LNR    4 (16)


No 12 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=1.95  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         3 L    3 (6)
Q Consensus         3 l    3 (6)
                      |
T Consensus        24 ~   24 (26)
T signal           24 L   24 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)


No 13 
>P25348_71_Mito_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=5.86  Aligned_cols=6  Identities=50%  Similarity=0.927  Sum_probs=0.0

Q 5_Mustela         1 EELERD    6 (6)
Q Consensus         1 eelerd    6 (6)
                      |-++++
T Consensus        49 ekl~~~   54 (76)
T signal           49 EKLQQD   54 (76)
T 21               35 QRTKKN   40 (63)
T 19               29 EKLEQR   34 (57)
T 20               28 ---NQR   30 (52)
T 15               26 ------   25 (44)
T 17                6 EMLQRR   11 (34)
T 12                1 GKLLGD    6 (27)
T 14               12 SIDNDN   17 (38)
T 9                21 ELKEQI   26 (49)
T 13                7 KKLFGE   12 (36)


No 14 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=1.48  Aligned_cols=5  Identities=0%  Similarity=-0.309  Sum_probs=0.0

Q 5_Mustela         1 EELER    5 (6)
Q Consensus         1 eeler    5 (6)
                      +...+
T Consensus        27 ~~~~~   31 (31)
T signal           27 SSGSP   31 (31)
T 119_Populus      27 TPSR-   30 (30)
T 107_Phytophtho   27 DGSF-   30 (30)
T 101_Setaria      27 EKDM-   30 (30)
T 82_Anopheles     27 DDKI-   30 (30)
T 69_Chrysemys     27 AAYK-   30 (30)
T 55_Geospiza      27 QQIV-   30 (30)


No 15 
>P15538_24_MitoM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=4.76  Aligned_cols=3  Identities=67%  Similarity=1.154  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus        18 lqr   20 (29)
T signal           18 LQR   20 (29)


No 16 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=4.18  Aligned_cols=3  Identities=67%  Similarity=0.877  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus        45 ~~~   47 (56)
T signal           45 LGR   47 (56)
T 34_Volvox        39 ---   38 (38)
T 134_Guillardia   39 ---   38 (38)
T 152_Trichinell   39 ---   38 (38)
T 159_Ixodes       39 ---   38 (38)
T 41_Galdieria     39 ---   38 (38)
T 112_Tribolium    39 ---   38 (38)
T 117_Ceratitis    39 ---   38 (38)
T 111_Musca        39 ---   38 (38)
T 39_Selaginella   39 ---   38 (38)


No 17 
>P36528_19_Mito_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=4.55  Aligned_cols=3  Identities=67%  Similarity=1.099  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus         7 lkr    9 (24)
T signal            7 LKR    9 (24)


No 18 
>P11325_9_MM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=3.97  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (6)
Q Consensus         3 ler    5 (6)
                      |.|
T Consensus         2 lsr    4 (14)
T signal            2 LSR    4 (14)


No 19 
>P08165_32_MM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=2.38  Aligned_cols=2  Identities=50%  Similarity=0.335  Sum_probs=0.0

Q 5_Mustela         2 EL    3 (6)
Q Consensus         2 el    3 (6)
                      |.
T Consensus        36 ~~   37 (37)
T signal           36 EQ   37 (37)
T 117_Pyrenophor   30 --   29 (29)
T 89_Acyrthosiph   30 --   29 (29)
T 86_Musca         30 --   29 (29)
T 72_Nematostell   30 --   29 (29)
T 37_Myotis        30 --   29 (29)
T 34_Pongo         30 --   29 (29)


No 20 
>P22570_32_MM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=2.35  Aligned_cols=2  Identities=50%  Similarity=1.049  Sum_probs=0.0

Q 5_Mustela         4 ER    5 (6)
Q Consensus         4 er    5 (6)
                      |.
T Consensus        36 ~~   37 (37)
T signal           36 EK   37 (37)
T 90_Drosophila    30 --   29 (29)
T 84_Megachile     30 --   29 (29)
T 73_Nematostell   30 --   29 (29)
T 49_Columba       30 --   29 (29)
T 38_Myotis        30 --   29 (29)


Done!
