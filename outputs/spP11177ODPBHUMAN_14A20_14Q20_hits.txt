Query         5_Mustela
Match_columns 6
No_of_seqs    4 out of 20
Neff          1.4 
Searched_HMMs 436
Date          Wed Sep  6 16:08:06 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_14A20_14Q20_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P14604_29_MM_ref_sig5_130        2.4     8.5    0.02    8.3   0.0    1    3-3      13-13  (34)
  2 Q16595_41_Cy_Mito_ref_sig5_130   2.3     8.7    0.02    8.8   0.0    2    3-4      11-12  (46)
  3 P10175_24_IM_ref_sig5_130        2.3     8.9    0.02    8.1   0.0    3    3-5       9-11  (29)
  4 Q5BJX1_13_Mito_ref_sig5_130      2.3     9.1   0.021    7.3   0.0    1    2-2      18-18  (18)
  5 P10849_15_MM_ref_sig5_130        2.1     9.9   0.023    7.5   0.0    4    2-5       5-8   (20)
  6 P23786_25_IM_ref_sig5_130        2.1      10   0.023    8.0   0.0    1    3-3       4-4   (30)
  7 P18886_25_IM_ref_sig5_130        2.0      10   0.024    8.0   0.0    1    3-3       4-4   (30)
  8 P31039_44_IM_ref_sig5_130        1.8      12   0.027    5.3   0.0    1    2-2      49-49  (49)
  9 P43165_34_Mito_ref_sig5_130      1.8      12   0.028    8.0   0.0    1    3-3      16-16  (39)
 10 P05166_28_MM_ref_sig5_130        1.7      12   0.028    7.9   0.0    2    2-3      19-20  (33)
 11 P14063_12_Mito_ref_sig5_130      1.7      13   0.029    6.8   0.0    3    3-5      14-16  (17)
 12 P36527_26_Mito_ref_sig5_130      1.7      13    0.03    7.8   0.0    3    1-3      17-19  (31)
 13 Q96252_26_Mito_IM_ref_sig5_130   1.6      13    0.03    7.7   0.0    4    2-5       6-9   (31)
 14 Q9SVM8_34_Mito_ref_sig5_130      1.6      13    0.03    8.1   0.0    4    3-6       9-12  (39)
 15 P36523_28_Mito_ref_sig5_130      1.6      13   0.031    7.9   0.0    1    2-2       9-9   (33)
 16 Q94IN5_37_Mito_ref_sig5_130      1.5      14   0.033    8.1   0.0    1    5-5      13-13  (42)
 17 P13621_23_Mito_IM_ref_sig5_130   1.5      14   0.033    7.5   0.0    1    3-3       8-8   (28)
 18 P42026_37_Mito_ref_sig5_130      1.4      16   0.037    7.9   0.0    3    3-5       8-10  (42)
 19 P00258_58_MM_ref_sig5_130        1.4      16   0.037    8.5   0.0    4    3-6       4-7   (63)
 20 P06576_47_Mito_IM_ref_sig5_130   1.4      16   0.037    8.2   0.0    1    3-3      15-15  (52)

No 1  
>P14604_29_MM_ref_sig5_130
Probab=2.38  E-value=8.5  Score=8.31  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      +
T Consensus        13 ~   13 (34)
T signal           13 N   13 (34)
T 122              13 R   13 (34)
T 133              13 G   13 (34)
T 142              13 A   13 (34)
T 145              13 C   13 (34)
T 146              13 I   13 (34)
T 152              13 R   13 (31)
T 139              13 G   13 (34)
T 129              13 R   13 (34)
T 120              13 A   13 (34)
Confidence            2


No 2  
>Q16595_41_Cy_Mito_ref_sig5_130
Probab=2.34  E-value=8.7  Score=8.79  Aligned_cols=2  Identities=100%  Similarity=1.763  Sum_probs=0.8

Q 5_Mustela         3 GL    4 (6)
Q 2_Macaca          3 GL    4 (6)
Q 18_Ceratotheri    3 GF    4 (6)
Q 4_Pongo           3 RL    4 (6)
Q Consensus         3 gl    4 (6)
                      ||
T Consensus        11 Gl   12 (46)
T signal           11 GL   12 (46)
T 159              11 GF   12 (49)
T 157              11 GF   12 (46)
T 156              11 GL   12 (46)
T 153              11 GL   12 (43)
T 145              11 SF   12 (46)
T 142              11 GL   12 (44)
T 147              11 GL   12 (44)
T 141               4 GL    5 (38)
Confidence            33


No 3  
>P10175_24_IM_ref_sig5_130
Probab=2.31  E-value=8.9  Score=8.05  Aligned_cols=3  Identities=67%  Similarity=0.778  Sum_probs=1.2

Q 5_Mustela         3 GLL    5 (6)
Q 2_Macaca          3 GLL    5 (6)
Q 18_Ceratotheri    3 GFL    5 (6)
Q 4_Pongo           3 RLL    5 (6)
Q Consensus         3 gll    5 (6)
                      +||
T Consensus         9 rLL   11 (29)
T signal            9 RLL   11 (29)
T 4                 9 RLL   11 (29)
T 15                9 QLL   11 (29)
T 7                 9 KLL   11 (29)
T 6                 9 RLL   11 (29)
T 9                 9 RLL   11 (29)
T 28                9 RLL   11 (29)
T 3                 3 TLL    5 (23)
T 30                7 RLF    9 (27)
Confidence            343


No 4  
>Q5BJX1_13_Mito_ref_sig5_130
Probab=2.26  E-value=9.1  Score=7.26  Aligned_cols=1  Identities=0%  Similarity=-0.462  Sum_probs=0.0

Q 5_Mustela         2 S    2 (6)
Q 2_Macaca          2 S    2 (6)
Q 18_Ceratotheri    2 S    2 (6)
Q 4_Pongo           2 S    2 (6)
Q Consensus         2 s    2 (6)
                      -
T Consensus        18 m   18 (18)
T signal           18 M   18 (18)
T 12               18 M   18 (18)
T 22               18 M   18 (18)
T 17               18 M   18 (18)
T 26               19 M   19 (19)
T 15               27 V   27 (27)
T 61               18 -   17 (17)
T 62               18 -   17 (17)
Confidence            0


No 5  
>P10849_15_MM_ref_sig5_130
Probab=2.10  E-value=9.9  Score=7.45  Aligned_cols=4  Identities=50%  Similarity=0.783  Sum_probs=1.7

Q 5_Mustela         2 SGLL    5 (6)
Q 2_Macaca          2 SGLL    5 (6)
Q 18_Ceratotheri    2 SGFL    5 (6)
Q 4_Pongo           2 SRLL    5 (6)
Q Consensus         2 sgll    5 (6)
                      |..|
T Consensus         5 ssil    8 (20)
T signal            5 SSIL    8 (20)
Confidence            3444


No 6  
>P23786_25_IM_ref_sig5_130
Probab=2.10  E-value=10  Score=8.04  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      +
T Consensus         4 r    4 (30)
T signal            4 R    4 (30)
T 101               4 R    4 (30)
T 104               4 R    4 (30)
T 108               4 G    4 (30)
T 109               4 P    4 (31)
T 117               4 R    4 (30)
T 119               4 R    4 (30)
T 59                4 R    4 (35)
Confidence            2


No 7  
>P18886_25_IM_ref_sig5_130
Probab=2.03  E-value=10  Score=7.99  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      +
T Consensus         4 r    4 (30)
T signal            4 R    4 (30)
T 84                4 R    4 (30)
T 87                4 R    4 (30)
T 102               4 P    4 (31)
T 114               4 R    4 (30)
T 92                4 G    4 (30)
T 94                4 R    4 (30)
T 48                4 R    4 (35)
Confidence            2


No 8  
>P31039_44_IM_ref_sig5_130
Probab=1.82  E-value=12  Score=5.32  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         2 S    2 (6)
Q 2_Macaca          2 S    2 (6)
Q 18_Ceratotheri    2 S    2 (6)
Q 4_Pongo           2 S    2 (6)
Q Consensus         2 s    2 (6)
                      |
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            2


No 9  
>P43165_34_Mito_ref_sig5_130
Probab=1.77  E-value=12  Score=8.03  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      +
T Consensus        16 ~   16 (39)
T signal           16 A   16 (39)
T 16               11 A   11 (39)
T 35               11 A   11 (40)
T 23               16 A   16 (45)
T 28               16 G   16 (44)
T 6                16 G   16 (45)
T 15               16 G   16 (45)
T 24               16 S   16 (45)
T 26               16 G   16 (45)
T 29               16 G   16 (45)
Confidence            2


No 10 
>P05166_28_MM_ref_sig5_130
Probab=1.74  E-value=12  Score=7.88  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.7

Q 5_Mustela         2 SG    3 (6)
Q 2_Macaca          2 SG    3 (6)
Q 18_Ceratotheri    2 SG    3 (6)
Q 4_Pongo           2 SR    3 (6)
Q Consensus         2 sg    3 (6)
                      ||
T Consensus        19 s~   20 (33)
T signal           19 SG   20 (33)
T 86               19 SG   20 (32)
T 49               20 GS   21 (34)
T 62               19 SG   20 (36)
T 64               19 CG   20 (33)
T 67               21 CG   22 (35)
T 70               19 TS   20 (39)
T 74               19 SS   20 (39)
T 89               19 RS   20 (33)
T 65               19 SV   20 (37)
Confidence            33


No 11 
>P14063_12_Mito_ref_sig5_130
Probab=1.70  E-value=13  Score=6.84  Aligned_cols=3  Identities=100%  Similarity=1.619  Sum_probs=1.3

Q 5_Mustela         3 GLL    5 (6)
Q 2_Macaca          3 GLL    5 (6)
Q 18_Ceratotheri    3 GFL    5 (6)
Q 4_Pongo           3 RLL    5 (6)
Q Consensus         3 gll    5 (6)
                      |+|
T Consensus        14 GlL   16 (17)
T signal           14 GLL   16 (17)
T 2                14 GLL   16 (17)
T 33               14 GLL   16 (17)
T 7                14 GLL   16 (17)
T 35               10 GLL   12 (13)
T 20               14 GYL   16 (17)
T 19               14 GYL   16 (17)
T 13                9 GLL   11 (12)
Confidence            444


No 12 
>P36527_26_Mito_ref_sig5_130
Probab=1.67  E-value=13  Score=7.81  Aligned_cols=3  Identities=100%  Similarity=1.354  Sum_probs=1.3

Q 5_Mustela         1 VSG    3 (6)
Q 2_Macaca          1 VSG    3 (6)
Q 18_Ceratotheri    1 VSG    3 (6)
Q 4_Pongo           1 VSR    3 (6)
Q Consensus         1 vsg    3 (6)
                      |||
T Consensus        17 vsg   19 (31)
T signal           17 VSG   19 (31)
Confidence            344


No 13 
>Q96252_26_Mito_IM_ref_sig5_130
Probab=1.65  E-value=13  Score=7.74  Aligned_cols=4  Identities=75%  Similarity=0.767  Sum_probs=1.5

Q 5_Mustela         2 SGLL    5 (6)
Q 2_Macaca          2 SGLL    5 (6)
Q 18_Ceratotheri    2 SGFL    5 (6)
Q 4_Pongo           2 SRLL    5 (6)
Q Consensus         2 sgll    5 (6)
                      |+||
T Consensus         6 SrLl    9 (31)
T signal            6 SRLL    9 (31)
T 164               6 TRLL    9 (32)
T 165               6 STLL    9 (26)
T 166               6 SRLL    9 (26)
T 163               6 TALL    9 (27)
T 159               7 SSLF   10 (23)
T 161               7 STFL   10 (23)
T 167               6 SRLF    9 (30)
Confidence            3343


No 14 
>Q9SVM8_34_Mito_ref_sig5_130
Probab=1.64  E-value=13  Score=8.10  Aligned_cols=4  Identities=100%  Similarity=1.605  Sum_probs=1.7

Q 5_Mustela         3 GLLR    6 (6)
Q 2_Macaca          3 GLLK    6 (6)
Q 18_Ceratotheri    3 GFLR    6 (6)
Q 4_Pongo           3 RLLK    6 (6)
Q Consensus         3 gll~    6 (6)
                      +|||
T Consensus         9 ~lLR   12 (39)
T signal            9 GLLR   12 (39)
T 10                9 SLVR   12 (40)
T 8                 9 NLLR   12 (40)
T 9                 9 NVLR   12 (40)
T 11                9 NILR   12 (40)
T 4                 9 NLLR   12 (44)
T 5                 9 SLLR   12 (41)
T 6                 9 SLLR   12 (41)
T 7                 9 GLLR   12 (41)
T 3                 9 GLLR   12 (49)
Confidence            3443


No 15 
>P36523_28_Mito_ref_sig5_130
Probab=1.63  E-value=13  Score=7.87  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         2 S    2 (6)
Q 2_Macaca          2 S    2 (6)
Q 18_Ceratotheri    2 S    2 (6)
Q 4_Pongo           2 S    2 (6)
Q Consensus         2 s    2 (6)
                      |
T Consensus         9 s    9 (33)
T signal            9 S    9 (33)
Confidence            2


No 16 
>Q94IN5_37_Mito_ref_sig5_130
Probab=1.54  E-value=14  Score=8.14  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (6)
Q 2_Macaca          5 L    5 (6)
Q 18_Ceratotheri    5 L    5 (6)
Q 4_Pongo           5 L    5 (6)
Q Consensus         5 l    5 (6)
                      |
T Consensus        13 l   13 (42)
T signal           13 L   13 (42)
Confidence            2


No 17 
>P13621_23_Mito_IM_ref_sig5_130
Probab=1.53  E-value=14  Score=7.48  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus         8 g    8 (28)
T signal            8 G    8 (28)
T 148               8 G    8 (28)
T 131               4 M    4 (24)
T 123               4 G    4 (24)
T 137               1 S    1 (21)
T 130               4 G    4 (24)
T 144               4 R    4 (24)
T 134               4 G    4 (24)
Confidence            3


No 18 
>P42026_37_Mito_ref_sig5_130
Probab=1.40  E-value=16  Score=7.88  Aligned_cols=3  Identities=67%  Similarity=0.778  Sum_probs=1.2

Q 5_Mustela         3 GLL.    5 (6)
Q 2_Macaca          3 GLL.    5 (6)
Q 18_Ceratotheri    3 GFL.    5 (6)
Q 4_Pongo           3 RLL.    5 (6)
Q Consensus         3 gll.    5 (6)
                      ||| 
T Consensus         8 gll.   10 (42)
T signal            8 RLL.   10 (42)
T 163               8 GLV.   10 (43)
T 166               8 GLFc   11 (43)
T 160               8 GPLc   11 (43)
T 150               4 GLLw    7 (39)
T 144               8 GLL.   10 (37)
T 133               1 ---.    0 (33)
T 136               1 ---.    0 (42)
T 138               1 ---.    0 (30)
T 143               1 ---.    0 (29)
Confidence            343 


No 19 
>P00258_58_MM_ref_sig5_130
Probab=1.40  E-value=16  Score=8.54  Aligned_cols=4  Identities=75%  Similarity=0.974  Sum_probs=1.8

Q 5_Mustela         3 GLLR    6 (6)
Q 2_Macaca          3 GLLK    6 (6)
Q 18_Ceratotheri    3 GFLR    6 (6)
Q 4_Pongo           3 RLLK    6 (6)
Q Consensus         3 gll~    6 (6)
                      +|||
T Consensus         4 rLLR    7 (63)
T signal            4 RLLR    7 (63)
T 106               2 RLLR    5 (59)
T 85                2 GLLR    5 (52)
T 96                2 RLLR    5 (63)
T 97                2 RLLR    5 (55)
T 87                2 RLLL    5 (63)
T 65                2 SLLR    5 (53)
T 77                1 -LLR    3 (50)
T 60                3 GLLR    6 (49)
T 62                2 GLLR    5 (57)
Confidence            4553


No 20 
>P06576_47_Mito_IM_ref_sig5_130
Probab=1.40  E-value=16  Score=8.23  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q 2_Macaca          3 G    3 (6)
Q 18_Ceratotheri    3 G    3 (6)
Q 4_Pongo           3 R    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        15 g   15 (52)
T signal           15 G   15 (52)
T 130              15 R   15 (52)
T 148              15 P   15 (53)
T 147              17 A   17 (59)
T 112              11 G   11 (44)
T 131              11 G   11 (44)
T 135              11 G   11 (43)
T 151              11 G   11 (48)
T 143              15 A   15 (57)
T 144              15 S   15 (52)
Confidence            2


Done!
