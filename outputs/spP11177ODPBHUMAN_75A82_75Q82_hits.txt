Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:39 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_75A82_75Q82_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q04467_39_Mito_ref_sig5_130     91.5 0.00038 8.8E-07   14.4   0.0    6    2-7      39-44  (44)
  2 P33198_8_Mito_ref_sig5_130      48.3   0.079 0.00018   12.1   0.0    6    2-7       8-13  (13)
  3 P38071_9_MM_ref_sig5_130         0.5      54    0.12    5.3   0.0    2    5-6       6-7   (14)
  4 P43265_42_IM_ref_sig5_130        0.5      55    0.13    6.9   0.0    1    3-3      47-47  (47)
  5 P42028_36_Mito_ref_sig5_130      0.5      57    0.13    6.6   0.0    2    1-2      39-40  (41)
  6 Q6SZW1_27_Cy_Mito_ref_sig5_130   0.4      60    0.14    3.2   0.0    1    3-3      32-32  (32)
  7 P07806_47_Cy_Mito_ref_sig5_130   0.4      60    0.14    6.9   0.0    1    5-5      42-42  (52)
  8 P34942_23_MM_ref_sig5_130        0.4      61    0.14    3.1   0.0    1    3-3      28-28  (28)
  9 P31334_19_Mito_ref_sig5_130      0.4      62    0.14    5.9   0.0    1    2-2      19-19  (24)
 10 P40360_13_Mito_ref_sig5_130      0.4      63    0.14    5.4   0.0    2    1-2      11-12  (18)
 11 P53219_38_Mito_ref_sig5_130      0.4      66    0.15    6.5   0.0    2    2-3      13-14  (43)
 12 Q02372_28_IM_ref_sig5_130        0.4      67    0.15    2.8   0.0    1    3-3      33-33  (33)
 13 Q9HCC0_22_MM_ref_sig5_130        0.3      77    0.18    5.7   0.0    2    3-4      25-26  (27)
 14 P09624_21_MM_ref_sig5_130        0.3      78    0.18    5.6   0.0    1    5-5      11-11  (26)
 15 P07756_38_Mito_Nu_ref_sig5_130   0.3      79    0.18    4.0   0.0    1    6-6       3-3   (43)
 16 P01098_23_Mito_ref_sig5_130      0.3      80    0.18    5.7   0.0    3    2-4      23-25  (28)
 17 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   0.3      80    0.18    5.7   0.0    2    3-4      24-25  (28)
 18 P36520_57_Mito_ref_sig5_130      0.3      81    0.19    6.7   0.0    1    5-5      20-20  (62)
 19 P05165_52_MM_ref_sig5_130        0.3      81    0.19    3.1   0.0    1    3-3      57-57  (57)
 20 P18155_35_Mito_ref_sig5_130      0.3      82    0.19    2.9   0.0    1    3-3      40-40  (40)

No 1  
>Q04467_39_Mito_ref_sig5_130
Probab=91.46  E-value=0.00038  Score=14.43  Aligned_cols=6  Identities=83%  Similarity=1.381  Sum_probs=4.2

Q 5_Mustela         2 YGDKRI    7 (7)
Q Consensus         2 ygdkri    7 (7)
                      +.||||
T Consensus        39 ~~~~~~   44 (44)
T signal           39 YADKRI   44 (44)
T 92_Chlorella     39 LT----   40 (40)
T 86_Trichoplax    39 VE----   40 (40)
T 76_Bombyx        39 DG----   40 (40)
T 69_Meleagris     39 WT----   40 (40)
T 65_Latimeria     39 II----   40 (40)
T 38_Sarcophilus   39 TA----   40 (40)
Confidence            567876


No 2  
>P33198_8_Mito_ref_sig5_130
Probab=48.25  E-value=0.079  Score=12.14  Aligned_cols=6  Identities=67%  Similarity=1.287  Sum_probs=4.8

Q 5_Mustela         2 YGDKRI    7 (7)
Q Consensus         2 ygdkri    7 (7)
                      |.|.||
T Consensus         8 yadqri   13 (13)
T signal            8 YADQRI   13 (13)
Confidence            788887


No 3  
>P38071_9_MM_ref_sig5_130
Probab=0.48  E-value=54  Score=5.35  Aligned_cols=2  Identities=100%  Similarity=1.315  Sum_probs=0.9

Q 5_Mustela         5 KR    6 (7)
Q Consensus         5 kr    6 (7)
                      ||
T Consensus         6 KR    7 (14)
T signal            6 KR    7 (14)
T 173               6 KR    7 (12)
T 174               6 KR    7 (12)
Confidence            44


No 4  
>P43265_42_IM_ref_sig5_130
Probab=0.46  E-value=55  Score=6.89  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      .
T Consensus        47 d   47 (47)
T signal           47 D   47 (47)
Confidence            0


No 5  
>P42028_36_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.59  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.7

Q 5_Mustela         1 KY    2 (7)
Q Consensus         1 ky    2 (7)
                      ||
T Consensus        39 KY   40 (41)
T signal           39 KY   40 (41)
T 142              35 KY   36 (37)
T 152              37 KY   38 (39)
T 172              37 KY   38 (39)
T 180              37 KY   38 (39)
T 162              37 KY   38 (39)
T 113              33 KY   34 (35)
T 147              34 KY   35 (36)
T 140              33 KY   34 (35)
T 144              34 KY   35 (36)
Confidence            33


No 6  
>Q6SZW1_27_Cy_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=3.20  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      +
T Consensus        32 ~   32 (32)
T signal           32 G   32 (32)
T 93_Amphimedon    32 I   32 (32)
T 87_Branchiosto   32 V   32 (32)
T 83_Aplysia       32 N   32 (32)
T 81_Aedes         32 G   32 (32)
T 61_Chrysemys     32 L   32 (32)
T 57_Ficedula      32 I   32 (32)
T 52_Falco         32 V   32 (32)
Confidence            0


No 7  
>P07806_47_Cy_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=6.92  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.3

Q 5_Mustela         5 K    5 (7)
Q Consensus         5 k    5 (7)
                      +
T Consensus        42 ~   42 (52)
T signal           42 V   42 (52)
T 176              19 K   19 (29)
Confidence            2


No 8  
>P34942_23_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=3.10  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 9  
>P31334_19_Mito_ref_sig5_130
Probab=0.42  E-value=62  Score=5.89  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         2 Y    2 (7)
Q Consensus         2 y    2 (7)
                      |
T Consensus        19 y   19 (24)
T signal           19 Y   19 (24)
Confidence            3


No 10 
>P40360_13_Mito_ref_sig5_130
Probab=0.41  E-value=63  Score=5.40  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.9

Q 5_Mustela         1 KY    2 (7)
Q Consensus         1 ky    2 (7)
                      ||
T Consensus        11 ky   12 (18)
T signal           11 KY   12 (18)
T 54               11 RY   12 (18)
T 53               11 KY   12 (18)
T 52               11 GY   12 (17)
T 50               11 KY   12 (17)
T 51               11 KV   12 (18)
T 49               12 KL   13 (19)
Confidence            44


No 11 
>P53219_38_Mito_ref_sig5_130
Probab=0.39  E-value=66  Score=6.53  Aligned_cols=2  Identities=100%  Similarity=2.394  Sum_probs=0.8

Q 5_Mustela         2 YG    3 (7)
Q Consensus         2 yg    3 (7)
                      ||
T Consensus        13 yg   14 (43)
T signal           13 YG   14 (43)
Confidence            33


No 12 
>Q02372_28_IM_ref_sig5_130
Probab=0.39  E-value=67  Score=2.81  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      -
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 13 
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.73  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.9

Q 5_Mustela         3 GD    4 (7)
Q Consensus         3 gd    4 (7)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 14 
>P09624_21_MM_ref_sig5_130
Probab=0.34  E-value=78  Score=5.55  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.4

Q 5_Mustela         5 K    5 (7)
Q Consensus         5 k    5 (7)
                      |
T Consensus        11 k   11 (26)
T signal           11 K   11 (26)
T 188               7 T    7 (22)
T 183              11 K   11 (26)
T 186              11 R   11 (25)
T 191              11 S   11 (27)
T 187               8 K    8 (21)
Confidence            3


No 15 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.33  E-value=79  Score=4.05  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         6 R    6 (7)
Q Consensus         6 r    6 (7)
                      +
T Consensus         3 ~    3 (43)
T signal            3 R    3 (43)
T 79_Emiliania      3 A    3 (42)
T 77_Thalassiosi    3 T    3 (42)
T 60_Anolis         3 A    3 (42)
T 48_Alligator      3 K    3 (42)
T 44_Papio          3 M    3 (42)
T 28_Ovis           3 Q    3 (42)
T 18_Pongo          3 Q    3 (42)
T cl|HABBABABA|1    3 R    3 (52)
T 72_Branchiosto    3 H    3 (41)
Confidence            1


No 16 
>P01098_23_Mito_ref_sig5_130
Probab=0.33  E-value=80  Score=5.75  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=1.3

Q 5_Mustela         2 YGD    4 (7)
Q Consensus         2 ygd    4 (7)
                      |.|
T Consensus        23 ysd   25 (28)
T signal           23 YSD   25 (28)
Confidence            444


No 17 
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=0.33  E-value=80  Score=5.70  Aligned_cols=2  Identities=50%  Similarity=1.464  Sum_probs=1.1

Q 5_Mustela         3 GD    4 (7)
Q Consensus         3 gd    4 (7)
                      ||
T Consensus        24 GD   25 (28)
T signal           24 GN   25 (28)
T 102              22 GD   23 (26)
T 105              22 GD   23 (26)
T 110              22 GD   23 (26)
T 111              22 GD   23 (26)
T 108              22 GD   23 (26)
T 113              22 GD   23 (26)
T 76               22 GD   23 (26)
Confidence            55


No 18 
>P36520_57_Mito_ref_sig5_130
Probab=0.32  E-value=81  Score=6.70  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.2

Q 5_Mustela         5 K    5 (7)
Q Consensus         5 k    5 (7)
                      |
T Consensus        20 k   20 (62)
T signal           20 K   20 (62)
Confidence            2


No 19 
>P05165_52_MM_ref_sig5_130
Probab=0.32  E-value=81  Score=3.08  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.1

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      .
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 20 
>P18155_35_Mito_ref_sig5_130
Probab=0.32  E-value=82  Score=2.87  Aligned_cols=1  Identities=0%  Similarity=-1.493  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      =
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 95_Emiliania     40 -   39 (39)
T 93_Volvox        40 -   39 (39)
T 89_Ornithorhyn   40 -   39 (39)
T 77_Pediculus     40 -   39 (39)
T 75_Trichoplax    40 -   39 (39)
T 71_Saccoglossu   40 -   39 (39)
T 67_Monodelphis   40 -   39 (39)
T 41_Capra         40 -   39 (39)
Confidence            0


Done!
