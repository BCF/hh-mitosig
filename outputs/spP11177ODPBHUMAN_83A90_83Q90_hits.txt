Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:51 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_83A90_83Q90_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P21771_33_Mito_ref_sig5_130      0.5      51    0.12    6.7   0.0    3    3-5      30-32  (38)
  2 Q12349_32_Mito_IM_ref_sig5_130   0.5      51    0.12    6.7   0.0    1    4-4      30-30  (37)
  3 P39697_29_Mito_ref_sig5_130      0.5      52    0.12    6.6   0.0    1    5-5      20-20  (34)
  4 P25284_26_MM_ref_sig5_130        0.5      53    0.12    6.3   0.0    1    5-5      28-28  (31)
  5 Q3T0L3_8_Mito_ref_sig5_130       0.5      57    0.13    5.1   0.0    2    4-5       9-10  (13)
  6 P00424_20_IM_ref_sig5_130        0.4      60    0.14    5.9   0.0    2    2-3      16-17  (25)
  7 P00366_57_MM_ref_sig5_130        0.4      61    0.14    4.7   0.0    1    1-1       1-1   (62)
  8 P54898_44_Mito_ref_sig5_130      0.4      62    0.14    6.8   0.0    1    4-4      37-37  (49)
  9 P0C2C0_40_Mito_ref_sig5_130      0.4      64    0.15    6.5   0.0    1    3-3      29-29  (45)
 10 P23786_25_IM_ref_sig5_130        0.4      65    0.15    6.1   0.0    1    4-4      25-25  (30)
 11 P34899_31_Mito_ref_sig5_130      0.4      66    0.15    6.3   0.0    1    3-3      17-17  (36)
 12 P13621_23_Mito_IM_ref_sig5_130   0.4      66    0.15    5.9   0.0    1    3-3      23-23  (28)
 13 Q8K2C6_36_Mito_ref_sig5_130      0.4      67    0.15    3.4   0.0    1    1-1       1-1   (41)
 14 P42847_59_Mito_ref_sig5_130      0.4      67    0.15    7.0   0.0    1    4-4      47-47  (64)
 15 P56522_34_MM_ref_sig5_130        0.4      67    0.15    2.6   0.0    1    3-3      39-39  (39)
 16 P01098_23_Mito_ref_sig5_130      0.4      68    0.16    6.0   0.0    1    3-3      27-27  (28)
 17 P13182_24_IM_ref_sig5_130        0.4      69    0.16    6.0   0.0    2    3-4      23-24  (29)
 18 Q04467_39_Mito_ref_sig5_130      0.4      69    0.16    3.1   0.0    1    3-3      44-44  (44)
 19 P08165_32_MM_ref_sig5_130        0.4      70    0.16    3.4   0.0    1    3-3      37-37  (37)
 20 P05091_17_MM_ref_sig5_130        0.4      73    0.17    5.5   0.0    3    3-5      16-18  (22)

No 1  
>P21771_33_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=6.72  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=1.2

Q 5_Mustela         3 PIS    5 (7)
Q Consensus         3 pis    5 (7)
                      |+|
T Consensus        30 pvs   32 (38)
T signal           30 PVS   32 (38)
Confidence            443


No 2  
>Q12349_32_Mito_IM_ref_sig5_130
Probab=0.50  E-value=51  Score=6.68  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        30 i   30 (37)
T signal           30 I   30 (37)
Confidence            2


No 3  
>P39697_29_Mito_ref_sig5_130
Probab=0.49  E-value=52  Score=6.56  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (7)
Q Consensus         5 s    5 (7)
                      |
T Consensus        20 ~   20 (34)
T signal           20 S   20 (34)
T 4                19 R   19 (33)
Confidence            2


No 4  
>P25284_26_MM_ref_sig5_130
Probab=0.48  E-value=53  Score=6.32  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (7)
Q Consensus         5 s    5 (7)
                      |
T Consensus        28 S   28 (31)
T signal           28 S   28 (31)
T 188              28 S   28 (31)
T 184              27 S   27 (30)
T 187              26 S   26 (29)
T 171              25 S   25 (27)
T 182              25 S   25 (27)
T 164              24 S   24 (27)
T 183              25 S   25 (28)
Confidence            3


No 5  
>Q3T0L3_8_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=5.14  Aligned_cols=2  Identities=100%  Similarity=1.032  Sum_probs=1.0

Q 5_Mustela         4 IS    5 (7)
Q Consensus         4 is    5 (7)
                      ||
T Consensus         9 Is   10 (13)
T signal            9 IS   10 (13)
T 81                9 IS   10 (13)
T 95                9 IS   10 (13)
T 83                9 IS   10 (13)
T 85                9 IS   10 (13)
T 88                9 VS   10 (13)
T 89                9 IS   10 (13)
T 76                9 IT   10 (13)
Confidence            44


No 6  
>P00424_20_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=5.95  Aligned_cols=2  Identities=50%  Similarity=0.484  Sum_probs=0.8

Q 5_Mustela         2 TP    3 (7)
Q Consensus         2 tp    3 (7)
                      ||
T Consensus        16 tp   17 (25)
T signal           16 TS   17 (25)
T 30               13 SP   14 (22)
T 32               13 TP   14 (22)
T 34               13 TP   14 (22)
T 35               13 TP   14 (22)
T 31               12 TP   13 (21)
T 33               12 TP   13 (21)
T 28               13 TP   14 (20)
T 29                9 TP   10 (18)
T 27               13 AQ   14 (22)
Confidence            34


No 7  
>P00366_57_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=4.74  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (7)
Q Consensus         1 d    1 (7)
                      =
T Consensus         1 M    1 (62)
T signal            1 M    1 (62)
T 114_Galdieria     1 M    1 (43)
T 110_Naegleria     1 M    1 (43)
T 104_Ichthyopht    1 M    1 (43)
T 102_Guillardia    1 M    1 (43)
T 93_Amphimedon     1 M    1 (43)
T 92_Schistosoma    1 M    1 (43)
T 89_Culex          1 M    1 (43)
T 69_Gallus         1 M    1 (43)
T 24_Saimiri        1 V    1 (43)
Confidence            0


No 8  
>P54898_44_Mito_ref_sig5_130
Probab=0.42  E-value=62  Score=6.79  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        37 ~   37 (49)
T signal           37 I   37 (49)
T 154              39 F   39 (51)
T 151              38 F   38 (50)
T 104              37 I   37 (43)
T 137              44 F   44 (49)
Confidence            3


No 9  
>P0C2C0_40_Mito_ref_sig5_130
Probab=0.40  E-value=64  Score=6.47  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      |
T Consensus        29 p   29 (45)
T signal           29 P   29 (45)
T 75               29 P   29 (45)
T 62               29 P   29 (43)
T 37               35 P   35 (51)
T 53               10 P   10 (26)
T 57               10 P   10 (26)
T 43               34 P   34 (51)
T 39               32 Q   32 (44)
T 49               33 Q   33 (45)
T 51               34 L   34 (50)
Confidence            3


No 10 
>P23786_25_IM_ref_sig5_130
Probab=0.40  E-value=65  Score=6.09  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.4

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      +
T Consensus        25 L   25 (30)
T signal           25 L   25 (30)
T 101              25 L   25 (30)
T 104              25 L   25 (30)
T 108              25 L   25 (30)
T 109              26 L   26 (31)
T 117              25 L   25 (30)
T 119              25 L   25 (30)
T 59               30 L   30 (35)
Confidence            3


No 11 
>P34899_31_Mito_ref_sig5_130
Probab=0.40  E-value=66  Score=6.30  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         3 P.    3 (7)
Q Consensus         3 p.    3 (7)
                      | 
T Consensus        17 p.   17 (36)
T signal           17 S.   17 (36)
T 145              17 P.   17 (36)
T 135              17 G.   17 (35)
T 141              17 P.   17 (36)
T 140              17 P.   17 (35)
T 134              12 Pl   13 (34)
T 138              15 P.   15 (35)
T 139              15 P.   15 (33)
T 143              15 P.   15 (34)
T 144              15 P.   15 (34)
Confidence            2 


No 12 
>P13621_23_Mito_IM_ref_sig5_130
Probab=0.39  E-value=66  Score=5.94  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      |
T Consensus        23 P   23 (28)
T signal           23 P   23 (28)
T 148              23 P   23 (28)
T 131              19 P   19 (24)
T 123              19 P   19 (24)
T 137              16 P   16 (21)
T 130              19 P   19 (24)
T 144              19 P   19 (24)
T 134              19 P   19 (24)
Confidence            2


No 13 
>Q8K2C6_36_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=3.44  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (7)
Q Consensus         1 d    1 (7)
                      =
T Consensus         1 M    1 (41)
T signal            1 M    1 (41)
T 107_Tribolium     1 M    1 (41)
T 92_Nasonia        1 M    1 (41)
T 132_Schizophyl    1 V    1 (41)
T 145_Zymoseptor    1 M    1 (41)
T 112_Nannochlor    1 M    1 (41)
T 102_Metaseiulu    1 M    1 (41)
T 155_Leishmania    1 M    1 (41)
Confidence            0


No 14 
>P42847_59_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=7.02  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        47 i   47 (64)
T signal           47 I   47 (64)
Confidence            1


No 15 
>P56522_34_MM_ref_sig5_130
Probab=0.39  E-value=67  Score=2.64  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      .
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 138_Tuber        38 -   37 (37)
T 98_Oryza         38 -   37 (37)
T 88_Apis          38 -   37 (37)
T 81_Amphimedon    38 -   37 (37)
T 79_Trichoplax    38 -   37 (37)
T 61_Meleagris     38 -   37 (37)
Confidence            0


No 16 
>P01098_23_Mito_ref_sig5_130
Probab=0.38  E-value=68  Score=5.96  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      |
T Consensus        27 p   27 (28)
T signal           27 P   27 (28)
Confidence            2


No 17 
>P13182_24_IM_ref_sig5_130
Probab=0.38  E-value=69  Score=5.96  Aligned_cols=2  Identities=0%  Similarity=-0.098  Sum_probs=0.7

Q 5_Mustela         3 PI    4 (7)
Q Consensus         3 pi    4 (7)
                      |.
T Consensus        23 pM   24 (29)
T signal           23 CM   24 (29)
T 11               25 PM   26 (31)
T 16               23 PM   24 (29)
T 19               23 PM   24 (29)
T 26               23 PM   24 (29)
T 30               23 PM   24 (29)
T 33               23 PM   24 (29)
Confidence            33


No 18 
>Q04467_39_Mito_ref_sig5_130
Probab=0.38  E-value=69  Score=3.14  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      =
T Consensus        44 ~   44 (44)
T signal           44 I   44 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)
Confidence            0


No 19 
>P08165_32_MM_ref_sig5_130
Probab=0.37  E-value=70  Score=3.35  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 P    3 (7)
Q Consensus         3 p    3 (7)
                      -
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 117_Pyrenophor   30 -   29 (29)
T 89_Acyrthosiph   30 -   29 (29)
T 86_Musca         30 -   29 (29)
T 72_Nematostell   30 -   29 (29)
T 37_Myotis        30 -   29 (29)
T 34_Pongo         30 -   29 (29)
Confidence            0


No 20 
>P05091_17_MM_ref_sig5_130
Probab=0.36  E-value=73  Score=5.47  Aligned_cols=3  Identities=33%  Similarity=0.301  Sum_probs=1.2

Q 5_Mustela         3 PIS    5 (7)
Q Consensus         3 pis    5 (7)
                      |+|
T Consensus        16 llS   18 (22)
T signal           16 LLS   18 (22)
T 58               13 LLS   15 (19)
T 61               13 LLS   15 (19)
T 73               11 LLS   13 (17)
T 72               20 LLS   22 (26)
T 45               19 PFS   21 (25)
T 47               19 PLS   21 (22)
T 51               19 -LS   20 (24)
Confidence            344


Done!
