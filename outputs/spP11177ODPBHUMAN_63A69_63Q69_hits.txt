Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:20 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_63A69_63Q69_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q8WZM3_22_Mito_ref_sig5_130      0.3      78    0.18    5.5   0.0    1    4-4       2-2   (27)
  2 P05165_52_MM_ref_sig5_130        0.3      81    0.18    3.0   0.0    1    1-1       1-1   (57)
  3 P22068_44_Mito_IM_ref_sig5_130   0.3      82    0.19    6.2   0.0    1    4-4      26-26  (49)
  4 Q93170_22_Mito_ref_sig5_130      0.3      83    0.19    5.5   0.0    1    4-4      21-21  (27)
  5 P30099_34_MitoM_ref_sig5_130     0.3      85    0.19    4.2   0.0    1    1-1       1-1   (39)
  6 P22142_42_IM_ref_sig5_130        0.3      90    0.21    4.3   0.0    1    2-2      47-47  (47)
  7 O00142_33_Mito_ref_sig5_130      0.3      93    0.21    2.9   0.0    1    2-2      38-38  (38)
  8 P08249_24_MM_ref_sig5_130        0.3      95    0.22    2.8   0.0    1    2-2      29-29  (29)
  9 P0CS90_23_MM_Nu_ref_sig5_130     0.3      98    0.22    5.1   0.0    1    2-2      28-28  (28)
 10 Q96CB9_25_Mito_ref_sig5_130      0.3      99    0.23    2.2   0.0    1    2-2      30-30  (30)
 11 Q12349_32_Mito_IM_ref_sig5_130   0.3   1E+02    0.23    5.6   0.0    1    4-4      32-32  (37)
 12 P46367_24_MM_ref_sig5_130        0.3   1E+02    0.23    5.3   0.0    5    2-6      17-21  (29)
 13 Q02773_122_Mito_ref_sig5_130     0.3   1E+02    0.23    5.5   0.0    3    3-5       2-4   (127)
 14 Q02318_33_MitoM_ref_sig5_130     0.3   1E+02    0.23    3.1   0.0    5    2-6       5-9   (38)
 15 P23004_14_IM_ref_sig5_130        0.2 1.1E+02    0.24    4.7   0.0    2    5-6      17-18  (19)
 16 P36520_57_Mito_ref_sig5_130      0.2 1.1E+02    0.25    6.1   0.0    4    2-5      43-46  (62)
 17 Q3SYS0_9_Mito_ref_sig5_130       0.2 1.1E+02    0.25    4.3   0.0    4    3-6      10-13  (14)
 18 Q9BEA2_31_MM_ref_sig5_130        0.2 1.1E+02    0.25    4.9   0.0    4    2-5      12-15  (36)
 19 P38077_33_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.25    5.5   0.0    2    4-5      25-26  (38)
 20 P38646_46_Mito_Nu_ref_sig5_130   0.2 1.1E+02    0.25    2.9   0.0    6    1-6      45-50  (51)

No 1  
>Q8WZM3_22_Mito_ref_sig5_130
Probab=0.33  E-value=78  Score=5.53  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (6)
Q Consensus         4 y    4 (6)
                      |
T Consensus         2 y    2 (27)
T signal            2 Y    2 (27)
Confidence            3


No 2  
>P05165_52_MM_ref_sig5_130
Probab=0.33  E-value=81  Score=2.96  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      =
T Consensus         1 M    1 (57)
T signal            1 M    1 (57)
T 91_Xenopus        1 M    1 (54)
T 88_Taeniopygia    1 M    1 (54)
T 68_Strongyloce    1 M    1 (54)
T 59_Acanthamoeb    1 M    1 (54)
T 52_Trichoplax     1 M    1 (54)
T 42_Condylura      1 M    1 (54)
T 6_Columba         1 A    1 (54)
T 1_Mustela         1 M    1 (54)
Confidence            0


No 3  
>P22068_44_Mito_IM_ref_sig5_130
Probab=0.32  E-value=82  Score=6.18  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (6)
Q Consensus         4 y    4 (6)
                      |
T Consensus        26 y   26 (49)
T signal           26 Y   26 (49)
Confidence            2


No 4  
>Q93170_22_Mito_ref_sig5_130
Probab=0.32  E-value=83  Score=5.46  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         4 Y    4 (6)
Q Consensus         4 y    4 (6)
                      |
T Consensus        21 y   21 (27)
T signal           21 Y   21 (27)
Confidence            2


No 5  
>P30099_34_MitoM_ref_sig5_130
Probab=0.31  E-value=85  Score=4.23  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      -
T Consensus         1 ~    1 (39)
T signal            1 M    1 (39)
T 46_Amphimedon     1 -    0 (28)
T 44_Anolis         1 -    0 (28)
T 42_Monodelphis    1 -    0 (28)
T 39_Sarcophilus    1 -    0 (28)
T 35_Mustela        1 -    0 (28)
T 7_Heterocephal    1 -    0 (28)
T cl|DABBABABA|1    1 -    0 (28)
T cl|RABBABABA|1    1 -    0 (28)
T cl|VABBABABA|2    1 -    0 (28)
Confidence            0


No 6  
>P22142_42_IM_ref_sig5_130
Probab=0.29  E-value=90  Score=4.28  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        47 ~   47 (47)
T signal           47 Y   47 (47)
T 180_Trichinell   28 -   27 (27)
T 177_Loa          28 -   27 (27)
T 164_Beta         28 -   27 (27)
T 135_Galdieria    28 -   27 (27)
Confidence            0


No 7  
>O00142_33_Mito_ref_sig5_130
Probab=0.28  E-value=93  Score=2.92  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        38 ~   38 (38)
T signal           38 A   38 (38)
T 92_Pediculus     35 -   34 (34)
T 83_Nematostell   35 -   34 (34)
T 79_Bombus        35 -   34 (34)
T 75_Bombyx        35 -   34 (34)
T 57_Xiphophorus   35 -   34 (34)
T 45_Anolis        35 -   34 (34)
T 40_Monodelphis   35 -   34 (34)
Confidence            0


No 8  
>P08249_24_MM_ref_sig5_130
Probab=0.28  E-value=95  Score=2.81  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      =
T Consensus        29 ~   29 (29)
T signal           29 V   29 (29)
T 138_Ustilago     29 -   28 (28)
T 102_Micromonas   29 -   28 (28)
T 94_Aedes         29 -   28 (28)
T 86_Capra         29 -   28 (28)
T 51_Anas          29 -   28 (28)
T 41_Ovis          29 -   28 (28)
Confidence            0


No 9  
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=0.27  E-value=98  Score=5.12  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      =
T Consensus        28 V   28 (28)
T signal           28 V   28 (28)
T 55               29 I   29 (29)
T 65               29 V   29 (29)
T 54               28 V   28 (28)
T 57               26 V   26 (26)
T 59               26 V   26 (26)
T 62               24 V   24 (24)
T 56               24 V   24 (24)
T 60               23 V   23 (23)
Confidence            0


No 10 
>Q96CB9_25_Mito_ref_sig5_130
Probab=0.27  E-value=99  Score=2.16  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      .
T Consensus        30 ~   30 (30)
T signal           30 K   30 (30)
T 100_Pediculus    30 F   30 (30)
T 96_Nematostell   30 V   30 (30)
T 80_Bombus        30 L   30 (30)
T 78_Aedes         30 T   30 (30)
T 63_Columba       30 H   30 (30)
Confidence            0


No 11 
>Q12349_32_Mito_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.59  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.2

Q 5_Mustela         4 Y    4 (6)
Q Consensus         4 y    4 (6)
                      |
T Consensus        32 y   32 (37)
T signal           32 Y   32 (37)
Confidence            2


No 12 
>P46367_24_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.29  Aligned_cols=5  Identities=20%  Similarity=0.620  Sum_probs=0.0

Q 5_Mustela         2 GAYKV    6 (6)
Q Consensus         2 gaykv    6 (6)
                      |+|..
T Consensus        17 ~r~Ql   21 (29)
T signal           17 GRLQL   21 (29)
T 98               18 ARYQL   22 (30)
T 105              18 GRIQL   22 (30)
T 100              25 NALQF   29 (37)


No 13 
>Q02773_122_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.54  Aligned_cols=3  Identities=67%  Similarity=1.187  Sum_probs=0.0

Q 5_Mustela         3 AYK    5 (6)
Q Consensus         3 ayk    5 (6)
                      ++|
T Consensus         2 ~Fk    4 (127)
T signal            2 AFK    4 (127)
T 22_Clavispora     2 HPR    4 (125)
T cl|LABBABABA|1    2 VFI    4 (125)
T cl|RABBABABA|1    2 VFC    4 (125)
T 22_Clavispora     1 ---    0 (45)
T 2_Torulaspora     1 ---    0 (45)
T 12_Vanderwalto    1 ---    0 (45)
T 8_Naumovozyma     1 ---    0 (45)
T 20_Millerozyma    1 ---    0 (45)
T 10                2 VFK    4 (126)


No 14 
>Q02318_33_MitoM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=3.09  Aligned_cols=5  Identities=20%  Similarity=0.627  Sum_probs=0.0

Q 5_Mustela         2 GAYKV    6 (6)
Q Consensus         2 gaykv    6 (6)
                      +...+
T Consensus         5 ~~~~~    9 (38)
T signal            5 GCARL    9 (38)
T 60_Falco          5 VCCTW    9 (41)
T 51_Chrysemys      5 VNTGT    9 (41)
T 45_Tursiops       5 GKYPV    9 (41)
T 44_Capra          5 LLKAS    9 (41)


No 15 
>P23004_14_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.68  Aligned_cols=2  Identities=100%  Similarity=1.099  Sum_probs=0.0

Q 5_Mustela         5 KV    6 (6)
Q Consensus         5 kv    6 (6)
                      ||
T Consensus        17 Kv   18 (19)
T signal           17 KV   18 (19)
T 83               17 KV   18 (19)
T 38               14 KV   15 (16)
T 44               14 KT   15 (16)
T 45               15 KV   16 (17)
T 46               14 KV   15 (16)
T 39               16 KV   17 (18)
T 47               15 KV   16 (17)
T 51               16 KV   17 (18)
T 40               18 KA   19 (20)


No 16 
>P36520_57_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=6.08  Aligned_cols=4  Identities=50%  Similarity=1.140  Sum_probs=0.0

Q 5_Mustela         2 GAYK    5 (6)
Q Consensus         2 gayk    5 (6)
                      |-+|
T Consensus        43 glfk   46 (62)
T signal           43 GLFK   46 (62)


No 17 
>Q3SYS0_9_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=4.34  Aligned_cols=4  Identities=75%  Similarity=0.592  Sum_probs=0.0

Q 5_Mustela         3 AYKV    6 (6)
Q Consensus         3 aykv    6 (6)
                      |-|+
T Consensus        10 Aak~   13 (14)
T signal           10 ASKV   13 (14)
T 88               10 AVKA   13 (14)
T 80               10 AEKF   13 (13)
T 69               10 AARM   13 (13)
T 75                9 AARP   12 (12)
T 84               10 AARV   13 (13)
T 85               10 AAKL   13 (13)
T 73               10 ALRV   13 (13)
T 77               10 AARV   13 (13)
T 72               11 A---   11 (11)


No 18 
>Q9BEA2_31_MM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=4.89  Aligned_cols=4  Identities=25%  Similarity=0.185  Sum_probs=0.0

Q 5_Mustela         2 GAYK    5 (6)
Q Consensus         2 gayk    5 (6)
                      |-+|
T Consensus        12 ~i~k   15 (36)
T signal           12 GIHM   15 (36)
T 16               12 GISK   15 (36)
T 27               12 NIYK   15 (36)
T 39_Odobenus      12 GIRK   15 (36)
T 38_Sarcophilus   12 ERTS   15 (36)
T 31_Mesocricetu   12 GIYK   15 (36)
T 29_Rattus        12 GICK   15 (36)
T 26_Mus           12 VLQG   15 (36)
T 7_Condylura      12 KSFH   15 (36)
T 4                12 HLEM   15 (36)


No 19 
>P38077_33_Mito_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.49  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.0

Q 5_Mustela         4 YK    5 (6)
Q Consensus         4 yk    5 (6)
                      ||
T Consensus        25 yk   26 (38)
T signal           25 YK   26 (38)


No 20 
>P38646_46_Mito_Nu_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=2.90  Aligned_cols=6  Identities=33%  Similarity=0.141  Sum_probs=0.0

Q 5_Mustela         1 DGAYKV    6 (6)
Q Consensus         1 dgaykv    6 (6)
                      +.++..
T Consensus        45 ~~~~~~   50 (51)
T signal           45 DYASEA   50 (51)
T 80_Apis          45 DLQQYR   50 (50)
T 179_Acyrthosip   45 GHVIGI   50 (50)
T 105_Ornithorhy   45 LSHEVF   50 (50)
T 90_Saccoglossu   45 LRHYSS   50 (50)
T 98_Trichinella   45 GIDLGT   50 (50)
T 77_Ixodes        45 TTNSCV   50 (50)
T 82_Aplysia       45 CVAVME   50 (50)
T 46_Pelodiscus    45 LGTTN-   49 (49)


Done!
