Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:07 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_94A100_94Q100_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P18886_25_IM_ref_sig5_130        0.7      36   0.082    6.6   0.0    1    3-3      16-16  (30)
  2 P28350_44_Mito_Cy_ref_sig5_130   0.6      39   0.089    7.2   0.0    1    4-4      11-11  (49)
  3 P14604_29_MM_ref_sig5_130        0.6      40   0.092    6.5   0.0    1    4-4      30-30  (34)
  4 P09671_24_MM_ref_sig5_130        0.6      41   0.093    6.1   0.0    1    4-4      18-18  (29)
  5 P35914_27_MM_Pe_ref_sig5_130     0.6      41   0.094    6.5   0.0    1    4-4      14-14  (32)
  6 P05166_28_MM_ref_sig5_130        0.6      42   0.096    6.5   0.0    1    2-2      33-33  (33)
  7 P12787_37_IM_ref_sig5_130        0.5      48    0.11    6.6   0.0    1    4-4       3-3   (42)
  8 P04179_24_MM_ref_sig5_130        0.5      48    0.11    5.9   0.0    1    4-4      18-18  (29)
  9 Q9N2I8_21_Mito_ref_sig5_130      0.5      50    0.11    6.0   0.0    1    4-4      21-21  (26)
 10 P30099_34_MitoM_ref_sig5_130     0.5      50    0.11    4.8   0.0    1    1-1       1-1   (39)
 11 P05165_52_MM_ref_sig5_130        0.5      52    0.12    3.4   0.0    1    1-1      12-12  (57)
 12 Q16740_56_MM_ref_sig5_130        0.5      52    0.12    7.1   0.0    1    4-4       8-8   (61)
 13 Q9UQX0_21_MM_ref_sig5_130        0.5      55    0.13    5.9   0.0    1    2-2      19-19  (26)
 14 P20000_21_MM_ref_sig5_130        0.5      56    0.13    5.8   0.0    1    2-2      26-26  (26)
 15 Q6UPE0_34_IM_ref_sig5_130        0.5      57    0.13    3.1   0.0    1    2-2      39-39  (39)
 16 P00507_29_MM_Cm_ref_sig5_130     0.4      62    0.14    4.1   0.0    1    1-1       1-1   (34)
 17 P11884_19_MM_ref_sig5_130        0.4      64    0.15    5.6   0.0    1    2-2      24-24  (24)
 18 P32904_16_Mito_ref_sig5_130      0.4      70    0.16    5.3   0.0    1    4-4      20-20  (21)
 19 P16276_27_Mito_ref_sig5_130      0.4      70    0.16    5.8   0.0    1    4-4      16-16  (32)
 20 P00426_43_IM_ref_sig5_130        0.4      74    0.17    6.2   0.0    1    4-4       3-3   (48)

No 1  
>P18886_25_IM_ref_sig5_130
Probab=0.69  E-value=36  Score=6.59  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.4

Q 5_Mustela         3 V    3 (6)
Q Consensus         3 v    3 (6)
                      +
T Consensus        16 ~   16 (30)
T signal           16 L   16 (30)
T 84               16 L   16 (30)
T 87               16 A   16 (30)
T 102              17 I   17 (31)
T 114              16 V   16 (30)
T 92               16 F   16 (30)
T 94               16 V   16 (30)
T 48               20 V   20 (35)
Confidence            3


No 2  
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.64  E-value=39  Score=7.16  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        11 g   11 (49)
T signal           11 G   11 (49)
Confidence            3


No 3  
>P14604_29_MM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.54  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        30 G   30 (34)
T signal           30 G   30 (34)
T 122              30 G   30 (34)
T 133              30 G   30 (34)
T 142              30 S   30 (34)
T 145              30 G   30 (34)
T 146              30 G   30 (34)
T 152              27 G   27 (31)
T 139              30 G   30 (34)
T 129              30 G   30 (34)
T 120              30 G   30 (34)
Confidence            3


No 4  
>P09671_24_MM_ref_sig5_130
Probab=0.62  E-value=41  Score=6.15  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G..    4 (6)
Q Consensus         4 g..    4 (6)
                      |  
T Consensus        18 g..   18 (29)
T signal           18 G..   18 (29)
T 128              20 Swk   22 (33)
T 134              20 G..   20 (31)
T 138              20 T..   20 (31)
T 124              19 A..   19 (30)
T 145              18 G..   18 (29)
T 149              20 A..   20 (31)
T 156              18 G..   18 (29)
T 113              19 A..   19 (30)
T 114              18 G..   18 (29)
Confidence            2  


No 5  
>P35914_27_MM_Pe_ref_sig5_130
Probab=0.61  E-value=41  Score=6.47  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        14 g   14 (32)
T signal           14 G   14 (32)
T 49               15 G   15 (33)
T 68               14 G   14 (32)
T 51               14 -   13 (26)
T 52               13 -   12 (25)
T 75               14 G   14 (32)
T 76               14 G   14 (32)
T 77               14 G   14 (32)
T 78               14 G   14 (32)
T 59               14 -   13 (29)
Confidence            2


No 6  
>P05166_28_MM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.48  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        33 v   33 (33)
T signal           33 T   33 (33)
T 86               33 -   32 (32)
T 49               34 A   34 (34)
T 62               36 V   36 (36)
T 64               33 V   33 (33)
T 67               35 V   35 (35)
T 70               39 V   39 (39)
T 74               39 V   39 (39)
T 89               33 V   33 (33)
T 65               38 -   37 (37)
Confidence            0


No 7  
>P12787_37_IM_ref_sig5_130
Probab=0.53  E-value=48  Score=6.61  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus         3 g    3 (42)
T signal            3 A    3 (42)
T 122               3 G    3 (46)
T 99                3 G    3 (44)
T 100               3 G    3 (43)
T 101               3 A    3 (41)
T 103               3 A    3 (48)
T 108               1 -    0 (36)
T 129               3 G    3 (46)
T 130               3 G    3 (39)
T 78                1 -    0 (29)
Confidence            2


No 8  
>P04179_24_MM_ref_sig5_130
Probab=0.53  E-value=48  Score=5.95  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G.    4 (6)
Q Consensus         4 g.    4 (6)
                      | 
T Consensus        18 g.   18 (29)
T signal           18 G.   18 (29)
T 162              18 G.   18 (29)
T 127              20 Rq   21 (32)
T 133              20 G.   20 (31)
T 135              20 A.   20 (31)
T 139              20 T.   20 (31)
T 118              11 P.   11 (22)
T 123              11 A.   11 (22)
T 113              19 A.   19 (30)
T 117              18 -.   17 (26)
Confidence            2 


No 9  
>Q9N2I8_21_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.01  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        21 g   21 (26)
T signal           21 G   21 (26)
T 69               24 G   24 (34)
T 78               24 G   24 (34)
T 5                24 G   24 (29)
T 34               31 G   31 (41)
T 60               24 G   24 (36)
T 25               24 G   24 (34)
T 88               24 G   24 (34)
T 46               24 S   24 (25)
Confidence            1


No 10 
>P30099_34_MitoM_ref_sig5_130
Probab=0.51  E-value=50  Score=4.83  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         1 I    1 (6)
Q Consensus         1 i    1 (6)
                      +
T Consensus         1 ~    1 (39)
T signal            1 M    1 (39)
T 46_Amphimedon     1 -    0 (28)
T 44_Anolis         1 -    0 (28)
T 42_Monodelphis    1 -    0 (28)
T 39_Sarcophilus    1 -    0 (28)
T 35_Mustela        1 -    0 (28)
T 7_Heterocephal    1 -    0 (28)
T cl|DABBABABA|1    1 -    0 (28)
T cl|RABBABABA|1    1 -    0 (28)
T cl|VABBABABA|2    1 -    0 (28)
Confidence            0


No 11 
>P05165_52_MM_ref_sig5_130
Probab=0.49  E-value=52  Score=3.39  Aligned_cols=1  Identities=0%  Similarity=1.032  Sum_probs=0.0

Q 5_Mustela         1 I    1 (6)
Q Consensus         1 i    1 (6)
                      .
T Consensus        12 ~   12 (57)
T signal           12 V   12 (57)
T 91_Xenopus       12 K   12 (54)
T 88_Taeniopygia   12 S   12 (54)
T 68_Strongyloce   12 V   12 (54)
T 59_Acanthamoeb   12 P   12 (54)
T 52_Trichoplax    12 K   12 (54)
T 42_Condylura     12 D   12 (54)
T 6_Columba        12 T   12 (54)
T 1_Mustela        12 L   12 (54)
Confidence            0


No 12 
>Q16740_56_MM_ref_sig5_130
Probab=0.49  E-value=52  Score=7.06  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus         8 g    8 (61)
T signal            8 G    8 (61)
T 158               8 K    8 (61)
T 139               8 G    8 (54)
T 141               8 G    8 (58)
T 143               8 G    8 (57)
T 152               8 R    8 (57)
T 133               8 G    8 (57)
T 138               8 G    8 (57)
T 144               8 R    8 (57)
T 132               1 -    0 (40)
Confidence            1


No 13 
>Q9UQX0_21_MM_ref_sig5_130
Probab=0.47  E-value=55  Score=5.92  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      |
T Consensus        19 a   19 (26)
T signal           19 A   19 (26)
Confidence            2


No 14 
>P20000_21_MM_ref_sig5_130
Probab=0.46  E-value=56  Score=5.83  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 54               26 -   25 (25)
T 69               24 -   23 (23)
T 79               26 -   25 (25)
T 81               26 -   25 (25)
T 42               25 -   24 (24)
T 43               23 -   22 (22)
T 49               29 -   28 (28)
T 50               24 -   23 (23)
T 78               26 -   25 (25)
Confidence            0


No 15 
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.45  E-value=57  Score=3.06  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        39 ~   39 (39)
T signal           39 A   39 (39)
T 99_Culex         38 -   37 (37)
T 93_Megachile     38 -   37 (37)
T 68_Ciona         38 -   37 (37)
T 66_Xiphophorus   38 -   37 (37)
T 61_Oryzias       38 -   37 (37)
Confidence            0


No 16 
>P00507_29_MM_Cm_ref_sig5_130
Probab=0.42  E-value=62  Score=4.08  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         1 I    1 (6)
Q Consensus         1 i    1 (6)
                      .
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 39_Myotis         1 M    1 (31)
T 158_Cryptococc    1 M    1 (31)
T 102_Metaseiulu    1 M    1 (31)
T 104_Pediculus     1 M    1 (31)
T 111_Neosartory    1 M    1 (31)
T 72_Nomascus       1 S    1 (31)
T 43_Sarcophilus    1 M    1 (31)
T 78_Aplysia        1 M    1 (31)
Confidence            0


No 17 
>P11884_19_MM_ref_sig5_130
Probab=0.41  E-value=64  Score=5.57  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      +
T Consensus        24 t   24 (24)
T signal           24 T   24 (24)
T 63               26 A   26 (26)
T 45               25 A   25 (25)
T 46               29 -   28 (28)
T 49               24 A   24 (24)
T 52               29 -   28 (28)
T 55               24 T   24 (24)
T 58               26 T   26 (26)
T 60               26 T   26 (26)
T 70               26 T   26 (26)
Confidence            0


No 18 
>P32904_16_Mito_ref_sig5_130
Probab=0.37  E-value=70  Score=5.26  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        20 G   20 (21)
T signal           20 G   20 (21)
T 41               20 G   20 (21)
T 39               20 G   20 (21)
T 20               20 G   20 (21)
T 26               20 G   20 (21)
T 24               20 G   20 (21)
T 25               20 G   20 (21)
T 28               20 G   20 (21)
T 40               18 G   18 (19)
T 9                17 G   17 (17)
Confidence            2


No 19 
>P16276_27_Mito_ref_sig5_130
Probab=0.37  E-value=70  Score=5.84  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G..    4 (6)
Q Consensus         4 g..    4 (6)
                      |  
T Consensus        16 G..   16 (32)
T signal           16 G..   16 (32)
T 50               18 G..   18 (34)
T 97               18 G..   18 (34)
T 111              18 N..   18 (34)
T 112              16 G..   16 (32)
T 113              20 G..   20 (36)
T 122              20 Gsg   22 (38)
T 115              17 G..   17 (33)
T 105              18 G..   18 (34)
T 108              18 G..   18 (34)
Confidence            2  


No 20 
>P00426_43_IM_ref_sig5_130
Probab=0.35  E-value=74  Score=6.19  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus         3 ~    3 (48)
T signal            3 G    3 (48)
T 85                3 A    3 (48)
T 106               3 G    3 (44)
T 118               3 G    3 (46)
T 3                 3 A    3 (41)
T 77                3 R    3 (37)
T 89                3 A    3 (43)
T 103               3 G    3 (43)
T 121               3 R    3 (37)
T 107               1 -    0 (32)
Confidence            1


Done!
