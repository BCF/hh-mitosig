Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:35 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_72A79_72Q79_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P22354_18_Mito_ref_sig5_130     73.0   0.014 3.1E-05   15.5   0.0    6    1-6      17-22  (23)
  2 P0C2B9_31_Mito_ref_sig5_130      0.9      28   0.065    7.2   0.0    3    2-4      15-17  (36)
  3 P80404_28_MM_ref_sig5_130        0.8      29   0.066    7.1   0.0    1    3-3      16-16  (33)
  4 P25719_20_MM_ref_sig5_130        0.8      31   0.072    4.8   0.0    1    2-2      21-21  (25)
  5 Q49AM1_35_Mito_MM_Nu_ref_sig5_   0.7      33   0.075    4.9   0.0    1    3-3      40-40  (40)
  6 Q9N0F3_34_MM_ref_sig5_130        0.7      33   0.075    7.3   0.0    1    2-2      10-10  (39)
  7 P39697_29_Mito_ref_sig5_130      0.7      33   0.076    7.1   0.0    2    2-3      11-12  (34)
  8 P42847_59_Mito_ref_sig5_130      0.7      33   0.076    8.0   0.0    1    2-2       7-7   (64)
  9 P33198_8_Mito_ref_sig5_130       0.7      36   0.083    5.7   0.0    4    4-7       7-10  (13)
 10 P53077_37_Mito_ref_sig5_130      0.6      39   0.089    7.2   0.0    3    1-3      31-33  (42)
 11 Q04467_39_Mito_ref_sig5_130      0.6      40   0.092    3.7   0.0    1    1-1       1-1   (44)
 12 Q00711_28_IM_ref_sig5_130        0.6      42   0.096    6.6   0.0    1    6-6      32-32  (33)
 13 P55809_39_MM_ref_sig5_130        0.6      44     0.1    7.0   0.0    1    2-2      24-24  (44)
 14 Q9Y2D0_33_Mito_ref_sig5_130      0.6      44     0.1    4.7   0.0    1    2-2      20-20  (38)
 15 P05165_52_MM_ref_sig5_130        0.6      45     0.1    3.7   0.0    1    3-3      57-57  (57)
 16 P34942_23_MM_ref_sig5_130        0.5      46    0.11    3.4   0.0    1    3-3      28-28  (28)
 17 Q9UGM6_18_MM_ref_sig5_130        0.5      49    0.11    6.1   0.0    1    3-3       8-8   (23)
 18 Q6UPE0_34_IM_ref_sig5_130        0.5      50    0.11    3.3   0.0    1    3-3      39-39  (39)
 19 P53219_38_Mito_ref_sig5_130      0.5      51    0.12    6.9   0.0    3    4-6      12-14  (43)
 20 Q7YR75_45_Mito_ref_sig5_130      0.5      51    0.12    7.0   0.0    1    2-2      10-10  (50)

No 1  
>P22354_18_Mito_ref_sig5_130
Probab=73.00  E-value=0.014  Score=15.54  Aligned_cols=6  Identities=50%  Similarity=1.630  Sum_probs=4.8

Q 5_Mustela         1 LWKKYG    6 (7)
Q Consensus         1 lwkkyg    6 (7)
                      .||.||
T Consensus        17 awkqfg   22 (23)
T signal           17 AWKQFG   22 (23)
Confidence            399987


No 2  
>P0C2B9_31_Mito_ref_sig5_130
Probab=0.85  E-value=28  Score=7.22  Aligned_cols=3  Identities=33%  Similarity=1.941  Sum_probs=1.4

Q 5_Mustela         2 WKK    4 (7)
Q Consensus         2 wkk    4 (7)
                      ||.
T Consensus        15 ~kh   17 (36)
T signal           15 WRH   17 (36)
T 31               15 LKH   17 (36)
T 49               15 WKH   17 (36)
T 26               15 PKF   17 (36)
T 35               15 LKH   17 (36)
T 10                8 WPS   10 (40)
T 24               15 LPR   17 (42)
T 19               15 WMR   17 (41)
T 8                15 WLR   17 (40)
T 14               15 WMH   17 (42)
Confidence            553


No 3  
>P80404_28_MM_ref_sig5_130
Probab=0.83  E-value=29  Score=7.15  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.3

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      +
T Consensus        16 ~   16 (33)
T signal           16 H   16 (33)
T 95               16 P   16 (33)
T 101              16 Q   16 (33)
T 103              16 Q   16 (33)
T 105              16 R   16 (33)
T 89               16 K   16 (33)
T 92               16 Q   16 (33)
T 94               16 K   16 (33)
T 98               16 Q   16 (27)
Confidence            3


No 4  
>P25719_20_MM_ref_sig5_130
Probab=0.78  E-value=31  Score=4.80  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.2

Q 5_Mustela         2 W    2 (7)
Q Consensus         2 w    2 (7)
                      |
T Consensus        21 ~   21 (25)
T signal           21 G   21 (25)
T 18_Naegleria     21 P   21 (25)
T 15_Tetrahymena   21 E   21 (25)
T 14_Clavispora    21 I   21 (25)
T 13_Debaryomyce   21 W   21 (25)
T 12_Kazachstani   21 T   21 (25)
T 10_Kluyveromyc   21 F   21 (25)
T 9_Ashbya         21 A   21 (25)
T cl|GABBABABA|1   21 H   21 (25)
Confidence            2


No 5  
>Q49AM1_35_Mito_MM_Nu_ref_sig5_130
Probab=0.75  E-value=33  Score=4.93  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      .
T Consensus        40 ~   40 (40)
T signal           40 Q   40 (40)
T 69_Xiphophorus   40 A   40 (40)
T 50_Columba       40 R   40 (40)
T 48_Anas          40 S   40 (40)
T 45_Echinops      40 E   40 (40)
T 40_Cavia         40 A   40 (40)
T 38_Cricetulus    40 C   40 (40)
T 30_Dasypus       40 F   40 (40)
T cl|PIBBABABA|9   40 -   39 (39)
Confidence            0


No 6  
>Q9N0F3_34_MM_ref_sig5_130
Probab=0.75  E-value=33  Score=7.29  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.3

Q 5_Mustela         2 W    2 (7)
Q Consensus         2 w    2 (7)
                      |
T Consensus        10 w   10 (39)
T signal           10 G   10 (39)
T 132              10 W   10 (39)
T 136              10 W   10 (39)
T 104              10 R   10 (39)
T 130              10 W   10 (39)
T 141              10 W   10 (39)
T 142              10 W   10 (39)
T 137              10 W   10 (39)
T 107              10 W   10 (40)
Confidence            3


No 7  
>P39697_29_Mito_ref_sig5_130
Probab=0.74  E-value=33  Score=7.14  Aligned_cols=2  Identities=100%  Similarity=2.893  Sum_probs=0.8

Q 5_Mustela         2 WK    3 (7)
Q Consensus         2 wk    3 (7)
                      ||
T Consensus        11 ~k   12 (34)
T signal           11 WK   12 (34)
T 4                11 LK   12 (33)
Confidence            44


No 8  
>P42847_59_Mito_ref_sig5_130
Probab=0.74  E-value=33  Score=7.99  Aligned_cols=1  Identities=100%  Similarity=4.721  Sum_probs=0.3

Q 5_Mustela         2 W    2 (7)
Q Consensus         2 w    2 (7)
                      |
T Consensus         7 w    7 (64)
T signal            7 W    7 (64)
Confidence            3


No 9  
>P33198_8_Mito_ref_sig5_130
Probab=0.69  E-value=36  Score=5.69  Aligned_cols=4  Identities=50%  Similarity=1.132  Sum_probs=1.9

Q 5_Mustela         4 KYGD    7 (7)
Q Consensus         4 kygd    7 (7)
                      .|.|
T Consensus         7 hyad   10 (13)
T signal            7 HYAD   10 (13)
Confidence            4544


No 10 
>P53077_37_Mito_ref_sig5_130
Probab=0.64  E-value=39  Score=7.21  Aligned_cols=3  Identities=67%  Similarity=1.951  Sum_probs=1.6

Q 5_Mustela         1 LWK    3 (7)
Q Consensus         1 lwk    3 (7)
                      ||-
T Consensus        31 lwp   33 (42)
T signal           31 LWP   33 (42)
Confidence            464


No 11 
>Q04467_39_Mito_ref_sig5_130
Probab=0.62  E-value=40  Score=3.67  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (7)
Q Consensus         1 l    1 (7)
                      .
T Consensus         1 M    1 (44)
T signal            1 M    1 (44)
T 92_Chlorella      1 M    1 (40)
T 86_Trichoplax     1 M    1 (40)
T 76_Bombyx         1 M    1 (40)
T 69_Meleagris      1 F    1 (40)
T 65_Latimeria      1 A    1 (40)
T 38_Sarcophilus    1 M    1 (40)
Confidence            0


No 12 
>Q00711_28_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.64  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus        32 G   32 (33)
T signal           32 G   32 (33)
T 13               28 -   27 (27)
T 46               32 G   32 (33)
T 71               32 G   32 (33)
T 14               33 G   33 (34)
T 18               31 G   31 (32)
Confidence            3


No 13 
>P55809_39_MM_ref_sig5_130
Probab=0.58  E-value=44  Score=7.01  Aligned_cols=1  Identities=100%  Similarity=4.721  Sum_probs=0.3

Q 5_Mustela         2 W    2 (7)
Q Consensus         2 w    2 (7)
                      |
T Consensus        24 w   24 (44)
T signal           24 W   24 (44)
T 109              24 R   24 (44)
T 123              24 W   24 (44)
T 120              24 W   24 (44)
T 5                26 -   25 (44)
T 100              26 -   25 (44)
T 97               24 Q   24 (44)
T 105              21 -   20 (40)
T 112              11 -   10 (29)
T 79                7 L    7 (27)
Confidence            3


No 14 
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.57  E-value=44  Score=4.66  Aligned_cols=1  Identities=100%  Similarity=4.721  Sum_probs=0.3

Q 5_Mustela         2 W.    2 (7)
Q Consensus         2 w.    2 (7)
                      | 
T Consensus        20 ~.   20 (38)
T signal           20 W.   20 (38)
T 57_Xenopus       20 R.   20 (38)
T 54_Chrysemys     20 P.   20 (38)
T 49_Meleagris     20 W.   20 (38)
T 42_Dasypus       20 I.   20 (38)
T 41_Camelus       20 S.   20 (38)
T 37_Ailuropoda    20 I.   20 (38)
T 22_Myotis        20 V.   20 (38)
T 14               20 Ss   21 (32)
Confidence            2 


No 15 
>P05165_52_MM_ref_sig5_130
Probab=0.56  E-value=45  Score=3.68  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      .
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 16 
>P34942_23_MM_ref_sig5_130
Probab=0.55  E-value=46  Score=3.38  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 17 
>Q9UGM6_18_MM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.09  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      |
T Consensus         8 k    8 (23)
T signal            8 K    8 (23)
T 126               8 K    8 (23)
T 153               8 K    8 (23)
T 162               8 K    8 (23)
T 167               8 K    8 (23)
T 175               8 K    8 (23)
T 177               8 K    8 (23)
T 152               8 K    8 (22)
T 166               8 N    8 (23)
Confidence            3


No 18 
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.51  E-value=50  Score=3.32  Aligned_cols=1  Identities=0%  Similarity=-0.130  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      -
T Consensus        39 ~   39 (39)
T signal           39 A   39 (39)
T 99_Culex         38 -   37 (37)
T 93_Megachile     38 -   37 (37)
T 68_Ciona         38 -   37 (37)
T 66_Xiphophorus   38 -   37 (37)
T 61_Oryzias       38 -   37 (37)
Confidence            0


No 19 
>P53219_38_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=6.89  Aligned_cols=3  Identities=67%  Similarity=1.763  Sum_probs=1.6

Q 5_Mustela         4 KYG    6 (7)
Q Consensus         4 kyg    6 (7)
                      .||
T Consensus        12 qyg   14 (43)
T signal           12 QYG   14 (43)
Confidence            455


No 20 
>Q7YR75_45_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=7.02  Aligned_cols=1  Identities=0%  Similarity=-0.527  Sum_probs=0.3

Q 5_Mustela         2 W    2 (7)
Q Consensus         2 w    2 (7)
                      |
T Consensus        10 w   10 (50)
T signal           10 R   10 (50)
T 12               10 W   10 (50)
T 77               10 W   10 (50)
T 107              10 R   10 (50)
T 24                9 G    9 (50)
T 62               10 W   10 (51)
T 88                1 -    0 (39)
T 109               1 -    0 (34)
T 84                1 -    0 (36)
T 89                2 W    2 (49)
Confidence            3


Done!
