Query         5_Mustela
Match_columns 7
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:09:03 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_51A58_51Q58_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P13182_24_IM_ref_sig5_130        0.7      38   0.086    6.7   0.0    1    5-5      13-13  (29)
  2 P12007_30_MM_ref_sig5_130        0.6      40   0.091    6.9   0.0    3    4-6       7-9   (35)
  3 P36527_26_Mito_ref_sig5_130      0.6      40   0.092    6.8   0.0    3    2-4      22-24  (31)
  4 P10818_26_IM_ref_sig5_130        0.6      40   0.093    6.7   0.0    1    5-5      15-15  (31)
  5 P15150_24_MitoM_ref_sig5_130     0.6      41   0.093    4.7   0.0    1    3-3      29-29  (29)
  6 Q8HXG5_29_IM_ref_sig5_130        0.6      42   0.096    6.8   0.0    1    5-5       6-6   (34)
  7 O13401_34_MM_ref_sig5_130        0.6      45     0.1    6.9   0.0    1    2-2      10-10  (39)
  8 Q02784_29_MM_ref_sig5_130        0.6      45     0.1    6.5   0.0    1    3-3      29-29  (34)
  9 P43165_34_Mito_ref_sig5_130      0.5      48    0.11    6.7   0.0    1    5-5       7-7   (39)
 10 Q91YT0_20_IM_ref_sig5_130        0.5      49    0.11    6.2   0.0    1    6-6       9-9   (25)
 11 P13619_42_Mito_IM_ref_sig5_130   0.5      49    0.11    4.7   0.0    1    1-1       1-1   (47)
 12 P34942_23_MM_ref_sig5_130        0.5      50    0.11    3.3   0.0    1    3-3      28-28  (28)
 13 P41563_27_Mito_ref_sig5_130      0.5      50    0.12    6.3   0.0    1    6-6      15-15  (32)
 14 P25285_22_Mito_ref_sig5_130      0.5      51    0.12    6.2   0.0    1    5-5       5-5   (27)
 15 P07271_45_Nu_IM_ref_sig5_130     0.5      54    0.12    7.0   0.0    1    4-4      26-26  (50)
 16 Q93170_22_Mito_ref_sig5_130      0.5      56    0.13    6.2   0.0    1    5-5      26-26  (27)
 17 P86924_17_Mito_ref_sig5_130      0.5      56    0.13    5.5   0.0    1    6-6      20-20  (22)
 18 P37298_31_IM_ref_sig5_130        0.5      57    0.13    6.5   0.0    1    4-4      10-10  (36)
 19 P25719_20_MM_ref_sig5_130        0.4      57    0.13    4.2   0.0    1    3-3      25-25  (25)
 20 P25712_34_IM_ref_sig5_130        0.4      60    0.14    6.4   0.0    1    4-4       6-6   (39)

No 1  
>P13182_24_IM_ref_sig5_130
Probab=0.66  E-value=38  Score=6.72  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus        13 L   13 (29)
T signal           13 L   13 (29)
T 11               15 L   15 (31)
T 16               13 L   13 (29)
T 19               13 L   13 (29)
T 26               13 L   13 (29)
T 30               13 P   13 (29)
T 33               13 L   13 (29)
Confidence            3


No 2  
>P12007_30_MM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.91  Aligned_cols=3  Identities=100%  Similarity=1.619  Sum_probs=1.6

Q 5_Mustela         4 LLG    6 (7)
Q 11_Cavia          4 LLG    6 (7)
Q Consensus         4 llg    6 (7)
                      |||
T Consensus         7 llg    9 (35)
T signal            7 LLG    9 (35)
T 106               7 ILG    9 (37)
T 129               6 ---    5 (34)
T 134               7 LLG    9 (34)
T 136               7 RLG    9 (37)
T 141               7 LLG    9 (37)
T 122               7 LVC    9 (25)
Confidence            555


No 3  
>P36527_26_Mito_ref_sig5_130
Probab=0.62  E-value=40  Score=6.75  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.4

Q 5_Mustela         2 VFL    4 (7)
Q 11_Cavia          2 VFL    4 (7)
Q Consensus         2 vfl    4 (7)
                      ||+
T Consensus        22 vfi   24 (31)
T signal           22 VFI   24 (31)
Confidence            454


No 4  
>P10818_26_IM_ref_sig5_130
Probab=0.62  E-value=40  Score=6.70  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus        15 L   15 (31)
T signal           15 L   15 (31)
T 53               12 L   12 (28)
T 56               12 P   12 (28)
T 64               12 L   12 (28)
T 36               14 L   14 (30)
T 44               12 L   12 (28)
T 55               12 L   12 (28)
T 6                14 L   14 (30)
T 7                13 L   13 (29)
T 43               10 L   10 (25)
Confidence            3


No 5  
>P15150_24_MitoM_ref_sig5_130
Probab=0.62  E-value=41  Score=4.74  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.0

Q 5_Mustela         3 F    3 (7)
Q 11_Cavia          3 F    3 (7)
Q Consensus         3 f    3 (7)
                      -
T Consensus        29 ~   29 (29)
T signal           29 A   29 (29)
T 53_Capra         29 V   29 (29)
T 51_Pundamilia    29 G   29 (29)
T 47_Xiphophorus   29 S   29 (29)
T 37_Sarcophilus   29 A   29 (29)
T 29_Mus           29 T   29 (29)
T 28_Ochotona      29 V   29 (29)
T cl|CABBABABA|1   29 I   29 (29)
Confidence            0


No 6  
>Q8HXG5_29_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.82  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus         6 L    6 (34)
T signal            6 L    6 (34)
T 47                6 L    6 (34)
T 52                6 L    6 (34)
T 29                6 L    6 (35)
T 41                6 L    6 (33)
T 45                6 L    6 (35)
T 46                6 L    6 (35)
T 51                6 L    6 (35)
T 53                6 A    6 (35)
T 21                4 L    4 (28)
Confidence            3


No 7  
>O13401_34_MM_ref_sig5_130
Probab=0.56  E-value=45  Score=6.91  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         2 V    2 (7)
Q 11_Cavia          2 V    2 (7)
Q Consensus         2 v    2 (7)
                      |
T Consensus        10 v   10 (39)
T signal           10 V   10 (39)
Confidence            2


No 8  
>Q02784_29_MM_ref_sig5_130
Probab=0.56  E-value=45  Score=6.54  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.2

Q 5_Mustela         3 F    3 (7)
Q 11_Cavia          3 F    3 (7)
Q Consensus         3 f    3 (7)
                      |
T Consensus        29 f   29 (34)
T signal           29 Y   29 (34)
T 86               29 F   29 (34)
T 62               30 F   30 (35)
T 10               31 F   31 (36)
T 18               30 F   30 (35)
T 56               30 F   30 (35)
T 61               29 Y   29 (34)
Confidence            2


No 9  
>P43165_34_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.67  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus         7 l    7 (39)
T signal            7 L    7 (39)
T 16                2 L    2 (39)
T 35                2 L    2 (40)
T 23                7 L    7 (45)
T 28                7 W    7 (44)
T 6                 7 L    7 (45)
T 15                7 L    7 (45)
T 24                7 L    7 (45)
T 26                7 S    7 (45)
T 29                7 L    7 (45)
Confidence            3


No 10 
>Q91YT0_20_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.21  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         6 G    6 (7)
Q 11_Cavia          6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus         9 G    9 (25)
T signal            9 G    9 (25)
T 132              11 G   11 (29)
T 93                9 G    9 (25)
Confidence            3


No 11 
>P13619_42_Mito_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=4.70  Aligned_cols=1  Identities=0%  Similarity=-0.463  Sum_probs=0.0

Q 5_Mustela         1 K    1 (7)
Q 11_Cavia          1 R    1 (7)
Q Consensus         1 ~    1 (7)
                      -
T Consensus         1 M    1 (47)
T signal            1 M    1 (47)
T 104_Caenorhabd    1 M    1 (45)
T 102_Salpingoec    1 M    1 (45)
T 85_Nasonia        1 M    1 (45)
T 79_Ixodes         1 M    1 (45)
T 71_Ficedula       1 G    1 (45)
T 61_Anas           1 K    1 (45)
T 60_Zonotrichia    1 P    1 (45)
T 53_Pelodiscus     1 M    1 (45)
T 47_Sarcophilus    1 M    1 (45)
Confidence            0


No 12 
>P34942_23_MM_ref_sig5_130
Probab=0.51  E-value=50  Score=3.32  Aligned_cols=1  Identities=0%  Similarity=-1.260  Sum_probs=0.0

Q 5_Mustela         3 F    3 (7)
Q 11_Cavia          3 F    3 (7)
Q Consensus         3 f    3 (7)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 13 
>P41563_27_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.30  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         6 G    6 (7)
Q 11_Cavia          6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus        15 G   15 (32)
T signal           15 G   15 (32)
T 20               15 G   15 (30)
T 42               15 G   15 (32)
T 45               15 G   15 (31)
T 46               14 G   14 (30)
T 61               15 G   15 (31)
T 76               15 G   15 (30)
T 84               15 G   15 (31)
T 94               15 G   15 (31)
T 114              15 G   15 (31)
Confidence            3


No 14 
>P25285_22_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=6.18  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus         5 l    5 (27)
T signal            5 L    5 (27)
T 107               5 L    5 (27)
T 127               5 L    5 (27)
T 128               5 L    5 (27)
T 134               5 L    5 (27)
T 138               5 V    5 (27)
T 145               5 L    5 (27)
T 146               5 L    5 (27)
T 147               5 L    5 (27)
T 152               5 L    5 (27)
Confidence            3


No 15 
>P07271_45_Nu_IM_ref_sig5_130
Probab=0.47  E-value=54  Score=7.00  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 L    4 (7)
Q 11_Cavia          4 L    4 (7)
Q Consensus         4 l    4 (7)
                      |
T Consensus        26 l   26 (50)
T signal           26 L   26 (50)
Confidence            2


No 16 
>Q93170_22_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=6.16  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus        26 l   26 (27)
T signal           26 L   26 (27)
Confidence            2


No 17 
>P86924_17_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=5.50  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         6 G    6 (7)
Q 11_Cavia          6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus        20 G   20 (22)
T signal           20 G   20 (22)
T 4_Naegleria      20 G   20 (22)
T 2_Leishmania     20 G   20 (22)
T cl|CABBABABA|1   20 P   20 (22)
T cl|FABBABABA|1   20 G   20 (22)
Confidence            3


No 18 
>P37298_31_IM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.51  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 L    4 (7)
Q 11_Cavia          4 L    4 (7)
Q Consensus         4 l    4 (7)
                      +
T Consensus        10 ~   10 (36)
T signal           10 M   10 (36)
T 12                1 -    0 (25)
T 15                1 L    1 (27)
T 11                1 -    0 (25)
T 16                4 I    4 (30)
Confidence            3


No 19 
>P25719_20_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=4.19  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.1

Q 5_Mustela         3 F    3 (7)
Q 11_Cavia          3 F    3 (7)
Q Consensus         3 f    3 (7)
                      |
T Consensus        25 ~   25 (25)
T signal           25 F   25 (25)
T 18_Naegleria     25 T   25 (25)
T 15_Tetrahymena   25 F   25 (25)
T 14_Clavispora    25 I   25 (25)
T 13_Debaryomyce   25 V   25 (25)
T 12_Kazachstani   25 F   25 (25)
T 10_Kluyveromyc   25 A   25 (25)
T 9_Ashbya         25 G   25 (25)
T cl|GABBABABA|1   25 R   25 (25)
Confidence            1


No 20 
>P25712_34_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=6.41  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 L    4 (7)
Q 11_Cavia          4 L    4 (7)
Q Consensus         4 l    4 (7)
                      |
T Consensus         6 L    6 (39)
T signal            6 L    6 (39)
T 33                6 V    6 (39)
T 60                1 -    0 (32)
T 63                1 -    0 (28)
T 59                6 L    6 (40)
T 35                6 L    6 (39)
T 5                 6 L    6 (39)
T 1                 6 F    6 (37)
T 62                6 L    6 (37)
T 10                4 L    4 (35)
Confidence            2


Done!
