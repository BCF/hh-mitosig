Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:47 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_81A87_81Q87_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P19955_8_Mito_ref_sig5_130      45.5   0.093 0.00021   11.6   0.0    6    1-6       1-6   (13)
  2 P31937_36_Mito_ref_sig5_130      0.4      61    0.14    6.4   0.0    1    4-4      40-40  (41)
  3 Q04467_39_Mito_ref_sig5_130      0.4      62    0.14    3.1   0.0    1    2-2      44-44  (44)
  4 P05165_52_MM_ref_sig5_130        0.4      64    0.15    3.2   0.0    1    2-2      57-57  (57)
  5 Q64591_34_Mito_ref_sig5_130      0.4      64    0.15    6.1   0.0    1    3-3      37-37  (39)
  6 P25846_21_Mito_ref_sig5_130      0.4      66    0.15    5.7   0.0    1    5-5      13-13  (26)
  7 Q9UGC7_26_Mito_ref_sig5_130      0.4      66    0.15    2.3   0.0    1    2-2      31-31  (31)
  8 P16387_33_MM_ref_sig5_130        0.4      67    0.15    6.1   0.0    1    4-4      23-23  (38)
  9 P80433_25_IM_ref_sig5_130        0.4      67    0.15    5.9   0.0    2    4-5       5-6   (30)
 10 Q3ZBF3_26_Mito_ref_sig5_130      0.4      67    0.15    2.9   0.0    1    2-2      31-31  (31)
 11 P18155_35_Mito_ref_sig5_130      0.4      68    0.16    2.9   0.0    1    2-2      40-40  (40)
 12 P13272_78_IM_ref_sig5_130        0.4      71    0.16    5.6   0.0    1    2-2      83-83  (83)
 13 P48201_67_MitoM_ref_sig5_130     0.3      77    0.18    4.7   0.0    1    3-3      68-68  (72)
 14 P29147_46_MM_ref_sig5_130        0.3      77    0.18    3.5   0.0    1    1-1       9-9   (51)
 15 Q9BYV1_41_Mito_ref_sig5_130      0.3      82    0.19    2.7   0.0    1    2-2      46-46  (46)
 16 P33316_69_Nu_Mito_ref_sig5_130   0.3      82    0.19    6.7   0.0    1    5-5       3-3   (74)
 17 P14066_25_IM_ref_sig5_130        0.3      82    0.19    5.6   0.0    1    5-5      22-22  (30)
 18 P26360_45_Mito_IM_ref_sig5_130   0.3      82    0.19    6.2   0.0    1    5-5      23-23  (50)
 19 P53875_73_Mito_ref_sig5_130      0.3      87     0.2    6.5   0.0    1    5-5      55-55  (78)
 20 P56522_34_MM_ref_sig5_130        0.3      95    0.22    2.2   0.0    1    2-2      39-39  (39)

No 1  
>P19955_8_Mito_ref_sig5_130
Probab=45.53  E-value=0.093  Score=11.57  Aligned_cols=6  Identities=67%  Similarity=1.126  Sum_probs=4.2

Q 5_Mustela         1 IIDTPI    6 (6)
Q Consensus         1 iidtpi    6 (6)
                      .|-|||
T Consensus         1 miatpi    6 (13)
T signal            1 MIATPI    6 (13)
Confidence            367886


No 2  
>P31937_36_Mito_ref_sig5_130
Probab=0.43  E-value=61  Score=6.36  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (6)
Q Consensus         4 t    4 (6)
                      |
T Consensus        40 T   40 (41)
T signal           40 T   40 (41)
T 43               33 T   33 (34)
T 5                31 T   31 (32)
T 33               33 T   33 (34)
T 52               37 T   37 (38)
T 98               37 T   37 (38)
T 55               37 T   37 (38)
Confidence            2


No 3  
>Q04467_39_Mito_ref_sig5_130
Probab=0.42  E-value=62  Score=3.11  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus        44 ~   44 (44)
T signal           44 I   44 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)
Confidence            0


No 4  
>P05165_52_MM_ref_sig5_130
Probab=0.41  E-value=64  Score=3.20  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      =
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 5  
>Q64591_34_Mito_ref_sig5_130
Probab=0.40  E-value=64  Score=6.14  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 D    3 (6)
Q Consensus         3 d    3 (6)
                      |
T Consensus        37 ~   37 (39)
T signal           37 D   37 (39)
T 76               37 N   37 (39)
T 78               37 E   37 (39)
T 52               31 E   31 (32)
T 55               35 E   35 (36)
T 80               37 E   37 (38)
T 31               26 S   26 (28)
T 34               23 N   23 (25)
T 46               26 K   26 (28)
T 85               27 S   27 (29)
Confidence            2


No 6  
>P25846_21_Mito_ref_sig5_130
Probab=0.39  E-value=66  Score=5.70  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (6)
Q Consensus         5 p    5 (6)
                      |
T Consensus        13 p   13 (26)
T signal           13 P   13 (26)
Confidence            3


No 7  
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.39  E-value=66  Score=2.26  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 119_Populus      31 -   30 (30)
T 107_Phytophtho   31 -   30 (30)
T 101_Setaria      31 -   30 (30)
T 82_Anopheles     31 -   30 (30)
T 69_Chrysemys     31 -   30 (30)
T 55_Geospiza      31 -   30 (30)
Confidence            0


No 8  
>P16387_33_MM_ref_sig5_130
Probab=0.39  E-value=67  Score=6.15  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (6)
Q Consensus         4 t    4 (6)
                      |
T Consensus        23 t   23 (38)
T signal           23 T   23 (38)
Confidence            3


No 9  
>P80433_25_IM_ref_sig5_130
Probab=0.39  E-value=67  Score=5.86  Aligned_cols=2  Identities=100%  Similarity=1.680  Sum_probs=0.8

Q 5_Mustela         4 TP    5 (6)
Q Consensus         4 tp    5 (6)
                      ||
T Consensus         5 tp    6 (30)
T signal            5 TP    6 (30)
T 3                 5 TP    6 (30)
T 20                5 KP    6 (30)
T 22                5 TP    6 (30)
T 31                5 TP    6 (30)
T 36                5 TP    6 (30)
T 5                 5 TS    6 (30)
T 8                 5 TP    6 (30)
T 19                5 TP    6 (30)
Confidence            34


No 10 
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=2.95  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      =
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 11 
>P18155_35_Mito_ref_sig5_130
Probab=0.39  E-value=68  Score=2.93  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.1

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 95_Emiliania     40 -   39 (39)
T 93_Volvox        40 -   39 (39)
T 89_Ornithorhyn   40 -   39 (39)
T 77_Pediculus     40 -   39 (39)
T 75_Trichoplax    40 -   39 (39)
T 71_Saccoglossu   40 -   39 (39)
T 67_Monodelphis   40 -   39 (39)
T 41_Capra         40 -   39 (39)
Confidence            0


No 12 
>P13272_78_IM_ref_sig5_130
Probab=0.37  E-value=71  Score=5.60  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus        83 ~   83 (83)
T signal           83 I   83 (83)
T 191_Volvox       36 -   35 (35)
T 183_Physcomitr   36 -   35 (35)
T 171_Trichinell   36 -   35 (35)
T 91_Trichoplax    36 -   35 (35)
T 85_Branchiosto   36 -   35 (35)
T 66_Geospiza      36 -   35 (35)
T 41_Camelus       36 -   35 (35)
Confidence            0


No 13 
>P48201_67_MitoM_ref_sig5_130
Probab=0.34  E-value=77  Score=4.73  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 D    3 (6)
Q Consensus         3 d    3 (6)
                      |
T Consensus        68 ~   68 (72)
T signal           68 D   68 (72)
T 54_Falco         67 S   67 (70)
T 66_Ficedula      41 -   40 (40)
T 49_Sarcophilus   41 -   40 (40)
T 66_Ficedula      69 P   69 (72)
T 65_Anas          69 R   69 (72)
T 57_Melopsittac   69 E   69 (72)
T 56_Pelodiscus    69 A   69 (72)
Confidence            2


No 14 
>P29147_46_MM_ref_sig5_130
Probab=0.34  E-value=77  Score=3.54  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         1 I    1 (6)
Q Consensus         1 i    1 (6)
                      .
T Consensus         9 ~    9 (51)
T signal            9 P    9 (51)
T 77_Aplysia        9 L    9 (50)
T 61_Papio          9 G    9 (50)
T 19_Tupaia         9 R    9 (50)
T 80_Tribolium      9 P    9 (50)
T 79_Apis           9 E    9 (50)
T 74_Branchiosto    9 V    9 (50)
T 54_Canis          9 G    9 (50)
T 39_Nomascus       9 N    9 (50)
Confidence            0


No 15 
>Q9BYV1_41_Mito_ref_sig5_130
Probab=0.32  E-value=82  Score=2.71  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      =
T Consensus        46 ~   46 (46)
T signal           46 P   46 (46)
T 122_Ixodes       44 -   43 (43)
T 118_Zea          44 -   43 (43)
T 94_Bombus        44 -   43 (43)
T 91_Nasonia       44 -   43 (43)
T 78_Musca         44 -   43 (43)
T 74_Strongyloce   44 -   43 (43)
T 64_Meleagris     44 -   43 (43)
T 58_Ficedula      44 -   43 (43)
Confidence            0


No 16 
>P33316_69_Nu_Mito_ref_sig5_130
Probab=0.32  E-value=82  Score=6.69  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (6)
Q Consensus         5 p    5 (6)
                      |
T Consensus         3 p    3 (74)
T signal            3 P    3 (74)
T 164               3 P    3 (78)
T 161               3 S    3 (78)
T 152               3 S    3 (72)
T 159               3 P    3 (56)
T 151               3 P    3 (55)
T 157               3 P    3 (58)
T 158               3 A    3 (61)
T 138               3 P    3 (77)
T 137               3 P    3 (47)
Confidence            3


No 17 
>P14066_25_IM_ref_sig5_130
Probab=0.32  E-value=82  Score=5.60  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.2

Q 5_Mustela         5 P    5 (6)
Q Consensus         5 p    5 (6)
                      |
T Consensus        22 p   22 (30)
T signal           22 P   22 (30)
Confidence            2


No 18 
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.32  E-value=82  Score=6.20  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.2

Q 5_Mustela         5 P    5 (6)
Q Consensus         5 p    5 (6)
                      |
T Consensus        23 p   23 (50)
T signal           23 P   23 (50)
T 181              19 P   19 (45)
T 183              22 P   22 (48)
T 170              24 P   24 (51)
T 171              21 P   21 (49)
T 182              22 P   22 (50)
T 174              21 P   21 (47)
T 178              17 P   17 (44)
T 177              23 P   23 (50)
T 168              23 G   23 (52)
Confidence            2


No 19 
>P53875_73_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=6.53  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.2

Q 5_Mustela         5 P    5 (6)
Q Consensus         5 p    5 (6)
                      |
T Consensus        55 P   55 (78)
T signal           55 P   55 (78)
T 27               49 I   49 (71)
T 144              48 P   48 (71)
T 63               59 P   59 (81)
T 139              54 P   54 (77)
T 23               47 P   47 (68)
T 12               46 P   46 (67)
T 52               47 P   47 (69)
T 95               48 P   48 (70)
T 25               38 P   38 (60)
Confidence            1


No 20 
>P56522_34_MM_ref_sig5_130
Probab=0.28  E-value=95  Score=2.19  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      -
T Consensus        39 ~   39 (39)
T signal           39 T   39 (39)
T 138_Tuber        38 -   37 (37)
T 98_Oryza         38 -   37 (37)
T 88_Apis          38 -   37 (37)
T 81_Amphimedon    38 -   37 (37)
T 79_Trichoplax    38 -   37 (37)
T 61_Meleagris     38 -   37 (37)
Confidence            0


Done!
