Query         5_Mustela
Match_columns 7
No_of_seqs    7 out of 20
Neff          1.6 
Searched_HMMs 436
Date          Wed Sep  6 16:08:00 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_10A17_10Q17_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36527_26_Mito_ref_sig5_130     85.3  0.0026 5.9E-06   18.0   0.0    6    2-7      14-19  (31)
  2 P10817_9_IM_ref_sig5_130        19.5    0.51  0.0012   10.2   0.0    6    1-6       1-6   (14)
  3 P07471_12_IM_ref_sig5_130       15.2    0.76  0.0017   10.2   0.0    6    1-6       4-9   (17)
  4 Q7M0E7_30_Mito_ref_sig5_130     11.8     1.1  0.0025   10.9   0.0    6    1-6      10-15  (35)
  5 P28042_16_Mito_ref_sig5_130      2.2     9.5   0.022    7.7   0.0    1    4-4       8-8   (21)
  6 Q5A8K2_8_Mito_Cy_ref_sig5_130    2.1     9.8   0.023    7.0   0.0    5    2-6       6-10  (13)
  7 P31039_44_IM_ref_sig5_130        2.0      11   0.025    5.7   0.0    1    3-3      49-49  (49)
  8 P21839_50_MM_ref_sig5_130        1.9      11   0.026    9.1   0.0    3    3-5      49-51  (55)
  9 P36516_59_Mito_ref_sig5_130      1.9      11   0.026    9.3   0.0    1    4-4      42-42  (64)
 10 Q02253_32_Mito_ref_sig5_130      1.9      11   0.026    8.5   0.0    3    4-6      17-19  (37)
 11 P36517_14_Mito_ref_sig5_130      1.8      12   0.027    7.4   0.0    3    1-3      12-14  (19)
 12 Q9BEA2_31_MM_ref_sig5_130        1.7      12   0.028    7.7   0.0    1    6-6      33-33  (36)
 13 P10818_26_IM_ref_sig5_130        1.6      14   0.032    7.9   0.0    4    4-7      10-13  (31)
 14 P12007_30_MM_ref_sig5_130        1.5      14   0.033    8.1   0.0    1    5-5      26-26  (35)
 15 Q01205_68_Mito_ref_sig5_130      1.5      15   0.035    9.1   0.0    1    5-5      34-34  (73)
 16 Q02376_27_IM_ref_sig5_130        1.5      15   0.035    7.9   0.0    1    4-4       9-9   (32)
 17 Q01992_33_MM_ref_sig5_130        1.4      16   0.036    8.2   0.0    3    4-6      33-35  (38)
 18 P32799_9_IM_ref_sig5_130         1.4      16   0.036    6.5   0.0    4    2-5       2-5   (14)
 19 P34899_31_Mito_ref_sig5_130      1.4      17   0.038    8.0   0.0    2    3-4       8-9   (36)
 20 Q25423_17_IM_ref_sig5_130        1.3      17   0.038    7.3   0.0    2    5-6       4-5   (22)

No 1  
>P36527_26_Mito_ref_sig5_130
Probab=85.34  E-value=0.0026  Score=18.04  Aligned_cols=6  Identities=100%  Similarity=1.248  Sum_probs=5.1

Q 5_Mustela         2 LEQVSG    7 (7)
Q 2_Macaca          2 LREVSG    7 (7)
Q 15_Ictidomys      2 LQQVSG    7 (7)
Q 11_Cavia          2 LRQVSG    7 (7)
Q 17_Tupaia         2 FQQVSG    7 (7)
Q 4_Pongo           2 LREVSR    7 (7)
Q 16_Equus          2 LEQVSG    7 (7)
Q Consensus         2 l~qVSg    7 (7)
                      |+||||
T Consensus        14 leqvsg   19 (31)
T signal           14 LEQVSG   19 (31)
Confidence            689997


No 2  
>P10817_9_IM_ref_sig5_130
Probab=19.53  E-value=0.51  Score=10.15  Aligned_cols=6  Identities=50%  Similarity=0.850  Sum_probs=4.0

Q 5_Mustela         1 PLEQVS    6 (7)
Q 2_Macaca          1 PLREVS    6 (7)
Q 15_Ictidomys      1 PLQQVS    6 (7)
Q 11_Cavia          1 PLRQVS    6 (7)
Q 17_Tupaia         1 PFQQVS    6 (7)
Q 4_Pongo           1 PLREVS    6 (7)
Q 16_Equus          1 SLEQVS    6 (7)
Q Consensus         1 pl~qVS    6 (7)
                      ||+-+|
T Consensus         1 plr~Ls    6 (14)
T signal            1 PLKVLS    6 (14)
T 14                1 PLQLLN    6 (14)
T 18                1 RLRSLS    6 (14)
T 22                1 PLKSLS    6 (14)
T 23                1 ARRLLG    6 (14)
T 46                1 PLRALN    6 (14)
T 4                 1 PLRTLS    6 (14)
Confidence            677666


No 3  
>P07471_12_IM_ref_sig5_130
Probab=15.18  E-value=0.76  Score=10.17  Aligned_cols=6  Identities=50%  Similarity=0.944  Sum_probs=3.9

Q 5_Mustela         1 PLEQVS    6 (7)
Q 2_Macaca          1 PLREVS    6 (7)
Q 15_Ictidomys      1 PLQQVS    6 (7)
Q 11_Cavia          1 PLRQVS    6 (7)
Q 17_Tupaia         1 PFQQVS    6 (7)
Q 4_Pongo           1 PLREVS    6 (7)
Q 16_Equus          1 SLEQVS    6 (7)
Q Consensus         1 pl~qVS    6 (7)
                      ||+-+|
T Consensus         4 plr~Ls    9 (17)
T signal            4 PLKSLS    9 (17)
T 8                 4 PLQLLN    9 (17)
T 15                4 PLKVLS    9 (17)
T 17                4 ARRLLG    9 (17)
T 18                4 PLRALS    9 (17)
T 42                4 PLRALN    9 (17)
T 3                 4 PLRTLS    9 (17)
Confidence            666665


No 4  
>Q7M0E7_30_Mito_ref_sig5_130
Probab=11.76  E-value=1.1  Score=10.89  Aligned_cols=6  Identities=17%  Similarity=-0.014  Sum_probs=2.7

Q 5_Mustela         1 PLEQVS    6 (7)
Q 2_Macaca          1 PLREVS    6 (7)
Q 15_Ictidomys      1 PLQQVS    6 (7)
Q 11_Cavia          1 PLRQVS    6 (7)
Q 17_Tupaia         1 PFQQVS    6 (7)
Q 4_Pongo           1 PLREVS    6 (7)
Q 16_Equus          1 SLEQVS    6 (7)
Q Consensus         1 pl~qVS    6 (7)
                      ||.+||
T Consensus        10 ~~~~~s   15 (35)
T signal           10 FFAYVR   15 (35)
T 53               10 PCVHMS   15 (35)
T 33                9 LAAQAC   14 (34)
T 49                9 PLACVI   14 (34)
T 47               10 PLAQVG   15 (35)
T 31                8 LRTLAT   13 (33)
T 38                8 SLTHLS   13 (33)
T 34               11 LLTQVN   16 (36)
T 27                5 TLGDTP   10 (30)
T 30                5 LLMETA   10 (30)
Confidence            344444


No 5  
>P28042_16_Mito_ref_sig5_130
Probab=2.18  E-value=9.5  Score=7.69  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (7)
Q 2_Macaca          4 E    4 (7)
Q 15_Ictidomys      4 Q    4 (7)
Q 11_Cavia          4 Q    4 (7)
Q 17_Tupaia         4 Q    4 (7)
Q 4_Pongo           4 E    4 (7)
Q 16_Equus          4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      |
T Consensus         8 Q    8 (21)
T signal            8 Q    8 (21)
T 10                8 Q    8 (22)
T 28                8 Q    8 (20)
T 31                8 Q    8 (22)
T 49                8 Q    8 (20)
T 62                8 Q    8 (21)
T 64                8 Q    8 (21)
T 67                8 Q    8 (21)
T 81                8 Q    8 (21)
T 11                8 K    8 (19)
Confidence            2


No 6  
>Q5A8K2_8_Mito_Cy_ref_sig5_130
Probab=2.12  E-value=9.8  Score=7.00  Aligned_cols=5  Identities=40%  Similarity=0.647  Sum_probs=2.3

Q 5_Mustela         2 LEQVS    6 (7)
Q 2_Macaca          2 LREVS    6 (7)
Q 15_Ictidomys      2 LQQVS    6 (7)
Q 11_Cavia          2 LRQVS    6 (7)
Q 17_Tupaia         2 FQQVS    6 (7)
Q 4_Pongo           2 LREVS    6 (7)
Q 16_Equus          2 LEQVS    6 (7)
Q Consensus         2 l~qVS    6 (7)
                      ||..|
T Consensus         6 lrrms   10 (13)
T signal            6 LRRMS   10 (13)
Confidence            44444


No 7  
>P31039_44_IM_ref_sig5_130
Probab=1.96  E-value=11  Score=5.65  Aligned_cols=1  Identities=0%  Similarity=0.069  Sum_probs=0.0

Q 5_Mustela         3 E    3 (7)
Q 2_Macaca          3 R    3 (7)
Q 15_Ictidomys      3 Q    3 (7)
Q 11_Cavia          3 R    3 (7)
Q 17_Tupaia         3 Q    3 (7)
Q 4_Pongo           3 R    3 (7)
Q 16_Equus          3 E    3 (7)
Q Consensus         3 ~    3 (7)
                      .
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            0


No 8  
>P21839_50_MM_ref_sig5_130
Probab=1.88  E-value=11  Score=9.10  Aligned_cols=3  Identities=67%  Similarity=0.722  Sum_probs=1.3

Q 5_Mustela         3 EQV    5 (7)
Q 2_Macaca          3 REV    5 (7)
Q 15_Ictidomys      3 QQV    5 (7)
Q 11_Cavia          3 RQV    5 (7)
Q 17_Tupaia         3 QQV    5 (7)
Q 4_Pongo           3 REV    5 (7)
Q 16_Equus          3 EQV    5 (7)
Q Consensus         3 ~qV    5 (7)
                      |||
T Consensus        49 RqV   51 (55)
T signal           49 RQV   51 (55)
T 150              47 RRV   49 (53)
T 153              49 RHV   51 (55)
T 155              49 RLV   51 (55)
T 160              49 RQV   51 (55)
T 154              52 RQV   54 (58)
T 164              49 REV   51 (55)
T 112              52 RQV   54 (58)
T 148              52 RQV   54 (58)
T 127              49 RQV   51 (55)
Confidence            344


No 9  
>P36516_59_Mito_ref_sig5_130
Probab=1.88  E-value=11  Score=9.34  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (7)
Q 2_Macaca          4 E    4 (7)
Q 15_Ictidomys      4 Q    4 (7)
Q 11_Cavia          4 Q    4 (7)
Q 17_Tupaia         4 Q    4 (7)
Q 4_Pongo           4 E    4 (7)
Q 16_Equus          4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      |
T Consensus        42 q   42 (64)
T signal           42 Q   42 (64)
Confidence            2


No 10 
>Q02253_32_Mito_ref_sig5_130
Probab=1.86  E-value=11  Score=8.50  Aligned_cols=3  Identities=100%  Similarity=0.922  Sum_probs=1.5

Q 5_Mustela         4 QVS    6 (7)
Q 2_Macaca          4 EVS    6 (7)
Q 15_Ictidomys      4 QVS    6 (7)
Q 11_Cavia          4 QVS    6 (7)
Q 17_Tupaia         4 QVS    6 (7)
Q 4_Pongo           4 EVS    6 (7)
Q 16_Equus          4 QVS    6 (7)
Q Consensus         4 qVS    6 (7)
                      |||
T Consensus        17 qVS   19 (37)
T signal           17 QVS   19 (37)
T 23               17 QVS   19 (37)
T 135              16 QVS   18 (35)
T 138              17 QVS   19 (37)
T 134              14 RVS   16 (33)
Confidence            554


No 11 
>P36517_14_Mito_ref_sig5_130
Probab=1.83  E-value=12  Score=7.42  Aligned_cols=3  Identities=67%  Similarity=1.331  Sum_probs=1.8

Q 5_Mustela         1 PLE    3 (7)
Q 2_Macaca          1 PLR    3 (7)
Q 15_Ictidomys      1 PLQ    3 (7)
Q 11_Cavia          1 PLR    3 (7)
Q 17_Tupaia         1 PFQ    3 (7)
Q 4_Pongo           1 PLR    3 (7)
Q 16_Equus          1 SLE    3 (7)
Q Consensus         1 pl~    3 (7)
                      ||+
T Consensus        12 plr   14 (19)
T signal           12 PLR   14 (19)
Confidence            565


No 12 
>Q9BEA2_31_MM_ref_sig5_130
Probab=1.74  E-value=12  Score=7.72  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         6 S    6 (7)
Q 2_Macaca          6 S    6 (7)
Q 15_Ictidomys      6 S    6 (7)
Q 11_Cavia          6 S    6 (7)
Q 17_Tupaia         6 S    6 (7)
Q 4_Pongo           6 S    6 (7)
Q 16_Equus          6 S    6 (7)
Q Consensus         6 S    6 (7)
                      |
T Consensus        33 s   33 (36)
T signal           33 S   33 (36)
T 16               33 S   33 (36)
T 27               33 S   33 (36)
T 39_Odobenus      33 G   33 (36)
T 38_Sarcophilus   33 L   33 (36)
T 31_Mesocricetu   33 S   33 (36)
T 29_Rattus        33 S   33 (36)
T 26_Mus           33 A   33 (36)
T 7_Condylura      33 G   33 (36)
T 4                33 Q   33 (36)
Confidence            2


No 13 
>P10818_26_IM_ref_sig5_130
Probab=1.59  E-value=14  Score=7.94  Aligned_cols=4  Identities=75%  Similarity=1.140  Sum_probs=1.7

Q 5_Mustela         4 QVSG    7 (7)
Q 2_Macaca          4 EVSG    7 (7)
Q 15_Ictidomys      4 QVSG    7 (7)
Q 11_Cavia          4 QVSG    7 (7)
Q 17_Tupaia         4 QVSG    7 (7)
Q 4_Pongo           4 EVSR    7 (7)
Q 16_Equus          4 QVSG    7 (7)
Q Consensus         4 qVSg    7 (7)
                      .|||
T Consensus        10 RvSg   13 (31)
T signal           10 RVSG   13 (31)
T 53                7 RVSG   10 (28)
T 56                7 RVFG   10 (28)
T 64                7 GLSR   10 (28)
T 36                9 RLSL   12 (30)
T 44                7 GVSR   10 (28)
T 55                7 RLSR   10 (28)
T 6                 9 RVLR   12 (30)
T 7                 8 RLSG   11 (29)
T 43                5 RLSG    8 (25)
Confidence            3443


No 14 
>P12007_30_MM_ref_sig5_130
Probab=1.54  E-value=14  Score=8.11  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q 2_Macaca          5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q 11_Cavia          5 V    5 (7)
Q 17_Tupaia         5 V    5 (7)
Q 4_Pongo           5 V    5 (7)
Q 16_Equus          5 V    5 (7)
Q Consensus         5 V    5 (7)
                      |
T Consensus        26 v   26 (35)
T signal           26 V   26 (35)
T 106              28 I   28 (37)
T 129              25 V   25 (34)
T 134              25 L   25 (34)
T 136              28 V   28 (37)
T 141              28 V   28 (37)
T 122              25 L   25 (25)
Confidence            2


No 15 
>Q01205_68_Mito_ref_sig5_130
Probab=1.46  E-value=15  Score=9.07  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         5 V    5 (7)
Q 2_Macaca          5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q 11_Cavia          5 V    5 (7)
Q 17_Tupaia         5 V    5 (7)
Q 4_Pongo           5 V    5 (7)
Q 16_Equus          5 V    5 (7)
Q Consensus         5 V    5 (7)
                      |
T Consensus        34 v   34 (73)
T signal           34 V   34 (73)
T 126              33 V   33 (72)
T 144              35 V   35 (74)
T 148              23 V   23 (62)
T 128              34 L   34 (72)
T 129              34 I   34 (72)
T 131              34 L   34 (72)
T 142              30 V   30 (70)
T 141              30 I   30 (69)
Confidence            2


No 16 
>Q02376_27_IM_ref_sig5_130
Probab=1.45  E-value=15  Score=7.87  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (7)
Q 2_Macaca          4 E    4 (7)
Q 15_Ictidomys      4 Q    4 (7)
Q 11_Cavia          4 Q    4 (7)
Q 17_Tupaia         4 Q    4 (7)
Q 4_Pongo           4 E    4 (7)
Q 16_Equus          4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      .
T Consensus         9 p    9 (32)
T signal            9 P    9 (32)
T 10                9 P    9 (33)
T 11                9 S    9 (32)
T 17                9 S    9 (32)
T 30                9 S    9 (32)
T 13                9 P    9 (32)
T 12                9 P    9 (32)
T 8                10 Q   10 (33)
Confidence            3


No 17 
>Q01992_33_MM_ref_sig5_130
Probab=1.43  E-value=16  Score=8.15  Aligned_cols=3  Identities=67%  Similarity=0.645  Sum_probs=1.3

Q 5_Mustela         4 QVS    6 (7)
Q 2_Macaca          4 EVS    6 (7)
Q 15_Ictidomys      4 QVS    6 (7)
Q 11_Cavia          4 QVS    6 (7)
Q 17_Tupaia         4 QVS    6 (7)
Q 4_Pongo           4 EVS    6 (7)
Q 16_Equus          4 QVS    6 (7)
Q Consensus         4 qVS    6 (7)
                      .||
T Consensus        33 ~VS   35 (38)
T signal           33 SVS   35 (38)
T 180              33 RVS   35 (38)
T 130              32 GVS   34 (37)
T 102              31 TVS   33 (36)
T 165              31 RVS   33 (36)
T 168              31 RVS   33 (36)
T 182              31 RVS   33 (36)
T 186              31 RVS   33 (36)
T 164              31 WVS   33 (36)
T 169              31 GVS   33 (36)
Confidence            344


No 18 
>P32799_9_IM_ref_sig5_130
Probab=1.41  E-value=16  Score=6.52  Aligned_cols=4  Identities=25%  Similarity=0.426  Sum_probs=1.8

Q 5_Mustela         2 LEQV    5 (7)
Q 2_Macaca          2 LREV    5 (7)
Q 15_Ictidomys      2 LQQV    5 (7)
Q 11_Cavia          2 LRQV    5 (7)
Q 17_Tupaia         2 FQQV    5 (7)
Q 4_Pongo           2 LREV    5 (7)
Q 16_Equus          2 LEQV    5 (7)
Q Consensus         2 l~qV    5 (7)
                      |+|.
T Consensus         2 ~rq~    5 (14)
T signal            2 FRQC    5 (14)
T 25                2 LARS    5 (12)
T 27                2 LRTN    5 (12)
T 33                2 FRQA    5 (14)
T 34                2 FRQV    5 (13)
T 35                2 YRQA    5 (13)
T 40                2 FKQA    5 (13)
T 24                2 LRQT    5 (13)
T 43                2 LRQA    5 (13)
Confidence            4553


No 19 
>P34899_31_Mito_ref_sig5_130
Probab=1.35  E-value=17  Score=7.97  Aligned_cols=2  Identities=0%  Similarity=0.318  Sum_probs=0.7

Q 5_Mustela         3 EQ    4 (7)
Q 2_Macaca          3 RE    4 (7)
Q 15_Ictidomys      3 QQ    4 (7)
Q 11_Cavia          3 RQ    4 (7)
Q 17_Tupaia         3 QQ    4 (7)
Q 4_Pongo           3 RE    4 (7)
Q 16_Equus          3 EQ    4 (7)
Q Consensus         3 ~q    4 (7)
                      |.
T Consensus         8 Rr    9 (36)
T signal            8 RK    9 (36)
T 145               8 RR    9 (36)
T 135               8 RK    9 (35)
T 141               8 RR    9 (36)
T 140               8 RR    9 (35)
T 134               6 RR    7 (34)
T 138               6 RR    7 (35)
T 139               6 RR    7 (33)
T 143               6 RR    7 (34)
T 144               6 RR    7 (34)
Confidence            33


No 20 
>Q25423_17_IM_ref_sig5_130
Probab=1.35  E-value=17  Score=7.26  Aligned_cols=2  Identities=50%  Similarity=0.667  Sum_probs=0.8

Q 5_Mustela         5 VS    6 (7)
Q 2_Macaca          5 VS    6 (7)
Q 15_Ictidomys      5 VS    6 (7)
Q 11_Cavia          5 VS    6 (7)
Q 17_Tupaia         5 VS    6 (7)
Q 4_Pongo           5 VS    6 (7)
Q 16_Equus          5 VS    6 (7)
Q Consensus         5 VS    6 (7)
                      ||
T Consensus         4 ~s    5 (22)
T signal            4 LS    5 (22)
T cl|CABBABABA|1    4 VS    5 (25)
Confidence            33


Done!
