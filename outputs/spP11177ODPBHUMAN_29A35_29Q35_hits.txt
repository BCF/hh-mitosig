Query         5_Mustela
Match_columns 6
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:28 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_29A35_29Q35_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       89.4 0.00088   2E-06   18.9   0.0    6    1-6      30-35  (35)
  2 P0C2C4_29_Mito_ref_sig5_130      0.5      48    0.11    6.4   0.0    2    2-3      24-25  (34)
  3 P46367_24_MM_ref_sig5_130        0.5      48    0.11    6.2   0.0    3    2-4      19-21  (29)
  4 P51998_26_Mito_ref_sig5_130      0.5      52    0.12    6.2   0.0    1    3-3      12-12  (31)
  5 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.5      53    0.12    3.0   0.0    1    2-2      26-26  (26)
  6 Q02372_28_IM_ref_sig5_130        0.5      54    0.12    2.9   0.0    1    2-2      33-33  (33)
  7 P20821_48_Mito_ref_sig5_130      0.5      54    0.12    6.8   0.0    1    2-2       3-3   (53)
  8 P24310_21_IM_ref_sig5_130        0.5      54    0.12    5.9   0.0    1    2-2       4-4   (26)
  9 P23434_48_Mito_ref_sig5_130      0.4      58    0.13    6.7   0.0    1    2-2       3-3   (53)
 10 P32387_45_Mito_ref_sig5_130      0.4      60    0.14    6.6   0.0    1    3-3      16-16  (50)
 11 P34942_23_MM_ref_sig5_130        0.4      61    0.14    3.0   0.0    1    2-2      28-28  (28)
 12 P17694_33_IM_ref_sig5_130        0.4      63    0.14    2.9   0.0    1    2-2      38-38  (38)
 13 P43265_42_IM_ref_sig5_130        0.4      63    0.14    6.5   0.0    1    3-3      29-29  (47)
 14 Q9UGC7_26_Mito_ref_sig5_130      0.4      67    0.15    2.3   0.0    1    1-1       1-1   (31)
 15 P09622_35_MM_ref_sig5_130        0.4      69    0.16    2.8   0.0    1    2-2      40-40  (40)
 16 Q007T0_28_IM_ref_sig5_130        0.4      71    0.16    5.8   0.0    1    3-3      24-24  (33)
 17 P11310_25_MM_ref_sig5_130        0.4      71    0.16    2.9   0.0    1    2-2      30-30  (30)
 18 P08503_25_MM_ref_sig5_130        0.4      72    0.16    2.8   0.0    1    2-2      30-30  (30)
 19 P13184_23_IM_ref_sig5_130        0.4      73    0.17    5.6   0.0    1    2-2      28-28  (28)
 20 P07756_38_Mito_Nu_ref_sig5_130   0.4      74    0.17    4.0   0.0    1    1-1       1-1   (43)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=89.43  E-value=0.00088  Score=18.85  Aligned_cols=6  Identities=100%  Similarity=1.021  Sum_probs=5.0

Q 5_Mustela         1 ALQVTV    6 (6)
Q 15_Ictidomys      1 ALQMTV    6 (6)
Q 11_Cavia          1 TLQVTV    6 (6)
Q Consensus         1 ~lq~tv    6 (6)
                      |+||||
T Consensus        30 AvQVTV   35 (35)
T signal           30 ALQVTV   35 (35)
T 143              34 AIQVTV   39 (39)
T 144              30 ALQVTV   35 (35)
T 145              30 SLQVTV   35 (35)
T 147              30 AVQVTV   35 (35)
T 135              39 AVQVTV   44 (44)
T 141              44 AIQVTV   49 (49)
T 142              29 AVQVTV   34 (34)
T 149              29 AVQMTV   34 (34)
T 150              29 AVQVTV   34 (34)
Confidence            689997


No 2  
>P0C2C4_29_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.42  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.8

Q 5_Mustela         2 LQ    3 (6)
Q 15_Ictidomys      2 LQ    3 (6)
Q 11_Cavia          2 LQ    3 (6)
Q Consensus         2 lq    3 (6)
                      ||
T Consensus        24 lq   25 (34)
T signal           24 LQ   25 (34)
Confidence            33


No 3  
>P46367_24_MM_ref_sig5_130
Probab=0.53  E-value=48  Score=6.19  Aligned_cols=3  Identities=67%  Similarity=0.944  Sum_probs=1.4

Q 5_Mustela         2 LQV    4 (6)
Q 15_Ictidomys      2 LQM    4 (6)
Q 11_Cavia          2 LQV    4 (6)
Q Consensus         2 lq~    4 (6)
                      +|.
T Consensus        19 ~Ql   21 (29)
T signal           19 LQL   21 (29)
T 98               20 YQL   22 (30)
T 105              20 IQL   22 (30)
T 100              27 LQF   29 (37)
Confidence            453


No 4  
>P51998_26_Mito_ref_sig5_130
Probab=0.49  E-value=52  Score=6.19  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q Consensus         3 q    3 (6)
                      |
T Consensus        12 q   12 (31)
T signal           12 Q   12 (31)
Confidence            2


No 5  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.48  E-value=53  Score=2.98  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 6  
>Q02372_28_IM_ref_sig5_130
Probab=0.48  E-value=54  Score=2.93  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 7  
>P20821_48_Mito_ref_sig5_130
Probab=0.47  E-value=54  Score=6.79  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      |
T Consensus         3 L    3 (53)
T signal            3 L    3 (53)
T 91                3 L    3 (53)
T 98                3 L    3 (53)
T 101               3 L    3 (53)
T 90                3 L    3 (53)
T 92                3 L    3 (53)
T 93                3 L    3 (50)
T 96                3 L    3 (53)
T 105               1 -    0 (47)
T 87                3 L    3 (70)
Confidence            2


No 8  
>P24310_21_IM_ref_sig5_130
Probab=0.47  E-value=54  Score=5.92  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      |
T Consensus         4 l    4 (26)
T signal            4 L    4 (26)
T 6                 4 S    4 (26)
T 8                 4 L    4 (26)
T 10                4 L    4 (26)
T 11                4 L    4 (26)
T 13                4 L    4 (26)
T 15                4 L    4 (26)
T 18                4 L    4 (26)
T 7                 4 Y    4 (27)
T 2                 2 I    2 (23)
Confidence            2


No 9  
>P23434_48_Mito_ref_sig5_130
Probab=0.44  E-value=58  Score=6.71  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      |
T Consensus         3 L    3 (53)
T signal            3 L    3 (53)
T 120               3 L    3 (53)
T 123               3 L    3 (53)
T 110               3 L    3 (53)
T 114               3 L    3 (53)
T 117               3 L    3 (53)
T 119               3 L    3 (50)
T 108               3 L    3 (42)
T 112               3 A    3 (47)
T 106               3 L    3 (70)
Confidence            2


No 10 
>P32387_45_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=6.62  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q Consensus         3 q    3 (6)
                      |
T Consensus        16 q   16 (50)
T signal           16 Q   16 (50)
Confidence            2


No 11 
>P34942_23_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=3.01  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      .
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 12 
>P17694_33_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=2.93  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        38 ~   38 (38)
T signal           38 Q   38 (38)
T 203_Boea         38 -   37 (37)
T 189_Naegleria    38 -   37 (37)
T 167_Loa          38 -   37 (37)
T 125_Talaromyce   38 -   37 (37)
T 84_Andalucia     38 -   37 (37)
Confidence            0


No 13 
>P43265_42_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=6.48  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         3 Q    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q Consensus         3 q    3 (6)
                      |
T Consensus        29 q   29 (47)
T signal           29 Q   29 (47)
Confidence            2


No 14 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=2.29  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q 15_Ictidomys      1 A    1 (6)
Q 11_Cavia          1 T    1 (6)
Q Consensus         1 ~    1 (6)
                      -
T Consensus         1 M    1 (31)
T signal            1 M    1 (31)
T 119_Populus       1 M    1 (30)
T 107_Phytophtho    1 M    1 (30)
T 101_Setaria       1 M    1 (30)
T 82_Anopheles      1 R    1 (30)
T 69_Chrysemys      1 M    1 (30)
T 55_Geospiza       1 S    1 (30)
Confidence            0


No 15 
>P09622_35_MM_ref_sig5_130
Probab=0.38  E-value=69  Score=2.75  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.1

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      +
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


No 16 
>Q007T0_28_IM_ref_sig5_130
Probab=0.37  E-value=71  Score=5.85  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.4

Q 5_Mustela         3 Q    3 (6)
Q 15_Ictidomys      3 Q    3 (6)
Q 11_Cavia          3 Q    3 (6)
Q Consensus         3 q    3 (6)
                      |
T Consensus        24 q   24 (33)
T signal           24 Q   24 (33)
T 20               26 Q   26 (35)
T 21               24 Q   24 (33)
T 38               24 Q   24 (33)
T 69               24 Q   24 (33)
T 76               24 Q   24 (33)
T 79               18 Q   18 (27)
T 113              21 Q   21 (30)
T 114              24 Q   24 (33)
T 68               23 P   23 (32)
Confidence            3


No 17 
>P11310_25_MM_ref_sig5_130
Probab=0.37  E-value=71  Score=2.92  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        30 ~   30 (30)
T signal           30 Q   30 (30)
T 81_Nematostell   29 -   28 (28)
T 80_Nasonia       29 -   28 (28)
T 70_Bombyx        29 -   28 (28)
T 64_Branchiosto   29 -   28 (28)
T 63_Aplysia       29 -   28 (28)
T 61_Saccoglossu   29 -   28 (28)
T 50_Columba       29 -   28 (28)
Confidence            0


No 18 
>P08503_25_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=2.84  Aligned_cols=1  Identities=0%  Similarity=-0.695  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      -
T Consensus        30 ~   30 (30)
T signal           30 K   30 (30)
T 96_Nematostell   29 -   28 (28)
T 51_Xenopus       29 -   28 (28)
T 47_Ornithorhyn   29 -   28 (28)
T 46_Geospiza      29 -   28 (28)
Confidence            0


No 19 
>P13184_23_IM_ref_sig5_130
Probab=0.36  E-value=73  Score=5.58  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 15_Ictidomys      2 L    2 (6)
Q 11_Cavia          2 L    2 (6)
Q Consensus         2 l    2 (6)
                      .
T Consensus        28 V   28 (28)
T signal           28 V   28 (28)
T 63               28 V   28 (28)
T 28               28 V   28 (28)
T 15               28 V   28 (28)
T 25               28 V   28 (28)
T 16               28 V   28 (28)
T 19               28 V   28 (28)
T 12               28 V   28 (28)
T 14               28 V   28 (28)
Confidence            0


No 20 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.36  E-value=74  Score=4.01  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q 15_Ictidomys      1 A    1 (6)
Q 11_Cavia          1 T    1 (6)
Q Consensus         1 ~    1 (6)
                      -
T Consensus         1 M    1 (43)
T signal            1 M    1 (43)
T 79_Emiliania      1 M    1 (42)
T 77_Thalassiosi    1 M    1 (42)
T 60_Anolis         1 M    1 (42)
T 48_Alligator      1 M    1 (42)
T 44_Papio          1 M    1 (42)
T 28_Ovis           1 M    1 (42)
T 18_Pongo          1 M    1 (42)
T cl|HABBABABA|1    1 M    1 (52)
T 72_Branchiosto    1 M    1 (41)
Confidence            0


Done!
