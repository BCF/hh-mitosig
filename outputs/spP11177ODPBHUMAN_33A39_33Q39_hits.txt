Query         5_Mustela
Match_columns 6
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:34 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_33A39_33Q39_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P00447_26_MM_ref_sig5_130        0.8      29   0.067    6.8   0.0    1    3-3      11-11  (31)
  2 Q5BJX1_13_Mito_ref_sig5_130      0.8      29   0.067    6.0   0.0    1    3-3      13-13  (18)
  3 Q02380_46_IM_ref_sig5_130        0.8      30   0.068    7.5   0.0    2    2-3      44-45  (51)
  4 P21642_33_Mito_ref_sig5_130      0.7      35    0.08    6.9   0.0    1    5-5      30-30  (38)
  5 P21571_32_Mito_IM_ref_sig5_130   0.7      35   0.081    6.8   0.0    1    3-3      14-14  (37)
  6 O00142_33_Mito_ref_sig5_130      0.7      37   0.085    3.9   0.0    1    2-2      38-38  (38)
  7 P22068_44_Mito_IM_ref_sig5_130   0.7      37   0.086    7.2   0.0    1    3-3      43-43  (49)
  8 P02721_32_Mito_IM_ref_sig5_130   0.6      40   0.092    6.7   0.0    1    3-3      14-14  (37)
  9 Q3T0L3_8_Mito_ref_sig5_130       0.6      40   0.092    5.3   0.0    1    5-5       8-8   (13)
 10 P0C2C4_29_Mito_ref_sig5_130      0.6      43   0.098    6.6   0.0    2    2-3      27-28  (34)
 11 P38077_33_Mito_IM_ref_sig5_130   0.6      45     0.1    6.6   0.0    2    2-3      30-31  (38)
 12 O75489_36_IM_ref_sig5_130        0.6      46     0.1    6.7   0.0    2    2-3      34-35  (41)
 13 Q7YR75_45_Mito_ref_sig5_130      0.5      48    0.11    6.8   0.0    1    5-5      47-47  (50)
 14 P23709_38_IM_ref_sig5_130        0.5      50    0.11    6.7   0.0    2    2-3      36-37  (43)
 15 P13183_24_IM_ref_sig5_130        0.5      51    0.12    6.1   0.0    1    5-5       8-8   (29)
 16 P19783_22_IM_ref_sig5_130        0.5      52    0.12    6.0   0.0    1    5-5      14-14  (27)
 17 P30038_24_MM_ref_sig5_130        0.5      55    0.13    6.0   0.0    1    2-2      29-29  (29)
 18 Q9HCC0_22_MM_ref_sig5_130        0.5      56    0.13    5.9   0.0    1    3-3       6-6   (27)
 19 Q94IN5_37_Mito_ref_sig5_130      0.5      57    0.13    6.5   0.0    1    2-2      42-42  (42)
 20 P15651_24_MM_ref_sig5_130        0.4      58    0.13    5.8   0.0    1    5-5      16-16  (29)

No 1  
>P00447_26_MM_ref_sig5_130
Probab=0.82  E-value=29  Score=6.84  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        11 ~   11 (31)
T signal           11 T   11 (31)
T 183              11 R   11 (26)
T 185              11 R   11 (29)
T 184              11 R   11 (32)
T 176              11 L   11 (25)
T 181               8 Q    8 (26)
Confidence            2


No 2  
>Q5BJX1_13_Mito_ref_sig5_130
Probab=0.82  E-value=29  Score=6.04  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        13 R   13 (18)
T signal           13 R   13 (18)
T 12               13 R   13 (18)
T 22               13 R   13 (18)
T 17               13 R   13 (18)
T 26               14 R   14 (19)
T 15               22 R   22 (27)
T 61               13 R   13 (17)
T 62               13 R   13 (17)
Confidence            3


No 3  
>Q02380_46_IM_ref_sig5_130
Probab=0.82  E-value=30  Score=7.52  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.9

Q 5_Mustela         2 VR    3 (6)
Q 12_Sorex          2 VR    3 (6)
Q 9_Ailuropoda      2 VR    3 (6)
Q Consensus         2 vr    3 (6)
                      ||
T Consensus        44 VR   45 (51)
T signal           44 VR   45 (51)
T 55               44 VR   45 (51)
T 59               44 VR   45 (51)
T 51               50 VR   51 (57)
T 94               43 --   42 (42)
T 49               45 VR   46 (52)
T 46               45 VR   46 (51)
T 42               53 VR   54 (60)
T 24               41 VR   42 (44)
T 40               42 VR   43 (48)
Confidence            44


No 4  
>P21642_33_Mito_ref_sig5_130
Probab=0.70  E-value=35  Score=6.93  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      |
T Consensus        30 A   30 (38)
T signal           30 A   30 (38)
T cl|CABBABABA|1   30 A   30 (38)
Confidence            2


No 5  
>P21571_32_Mito_IM_ref_sig5_130
Probab=0.70  E-value=35  Score=6.82  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        14 r   14 (37)
T signal           14 R   14 (37)
T 15               14 Q   14 (37)
T 19               14 R   14 (37)
T 51               14 H   14 (39)
T 14               15 R   15 (40)
T 2                14 R   14 (39)
T 16                7 R    7 (27)
T 11               13 R   13 (33)
T 87                8 -    7 (28)
T 88                8 -    7 (28)
Confidence            3


No 6  
>O00142_33_Mito_ref_sig5_130
Probab=0.67  E-value=37  Score=3.87  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 V    2 (6)
Q Consensus         2 v    2 (6)
                      .
T Consensus        38 ~   38 (38)
T signal           38 A   38 (38)
T 92_Pediculus     35 -   34 (34)
T 83_Nematostell   35 -   34 (34)
T 79_Bombus        35 -   34 (34)
T 75_Bombyx        35 -   34 (34)
T 57_Xiphophorus   35 -   34 (34)
T 45_Anolis        35 -   34 (34)
T 40_Monodelphis   35 -   34 (34)
Confidence            0


No 7  
>P22068_44_Mito_IM_ref_sig5_130
Probab=0.67  E-value=37  Score=7.20  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        43 r   43 (49)
T signal           43 R   43 (49)
Confidence            2


No 8  
>P02721_32_Mito_IM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.66  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.2

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        14 r   14 (37)
T signal           14 Q   14 (37)
T 20               15 R   15 (40)
T 52               14 H   14 (39)
T 22               14 Q   14 (37)
T 2                14 R   14 (39)
T 18               13 R   13 (33)
T 23                7 R    7 (27)
T 84                7 P    7 (27)
T 87                8 -    7 (28)
Confidence            2


No 9  
>Q3T0L3_8_Mito_ref_sig5_130
Probab=0.62  E-value=40  Score=5.32  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      +
T Consensus         8 ~    8 (13)
T signal            8 A    8 (13)
T 81                8 L    8 (13)
T 95                8 A    8 (13)
T 83                8 F    8 (13)
T 85                8 L    8 (13)
T 88                8 A    8 (13)
T 89                8 L    8 (13)
T 76                8 L    8 (13)
Confidence            3


No 10 
>P0C2C4_29_Mito_ref_sig5_130
Probab=0.59  E-value=43  Score=6.55  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.9

Q 5_Mustela         2 VR    3 (6)
Q 12_Sorex          2 VR    3 (6)
Q 9_Ailuropoda      2 VR    3 (6)
Q Consensus         2 vr    3 (6)
                      ||
T Consensus        27 vr   28 (34)
T signal           27 VR   28 (34)
Confidence            44


No 11 
>P38077_33_Mito_IM_ref_sig5_130
Probab=0.57  E-value=45  Score=6.65  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.8

Q 5_Mustela         2 VR    3 (6)
Q 12_Sorex          2 VR    3 (6)
Q 9_Ailuropoda      2 VR    3 (6)
Q Consensus         2 vr    3 (6)
                      ||
T Consensus        30 vr   31 (38)
T signal           30 VR   31 (38)
Confidence            33


No 12 
>O75489_36_IM_ref_sig5_130
Probab=0.55  E-value=46  Score=6.72  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.9

Q 5_Mustela         2 VR    3 (6)
Q 12_Sorex          2 VR    3 (6)
Q 9_Ailuropoda      2 VR    3 (6)
Q Consensus         2 vr    3 (6)
                      ||
T Consensus        34 VR   35 (41)
T signal           34 VR   35 (41)
T 206              32 VR   33 (39)
T 214              32 VR   33 (39)
T 221              32 VR   33 (39)
T 192              32 VR   33 (39)
T 194              32 VR   33 (39)
T 201              32 VR   33 (39)
T 203              32 VR   33 (39)
T 193              32 VR   33 (39)
Confidence            44


No 13 
>Q7YR75_45_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.84  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      |
T Consensus        47 A   47 (50)
T signal           47 A   47 (50)
T 12               47 A   47 (50)
T 77               47 A   47 (50)
T 107              47 T   47 (50)
T 24               47 A   47 (50)
T 62               48 A   48 (51)
T 88               36 A   36 (39)
T 109              31 A   31 (34)
T 84               33 A   33 (36)
T 89               47 A   47 (49)
Confidence            1


No 14 
>P23709_38_IM_ref_sig5_130
Probab=0.51  E-value=50  Score=6.68  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.8

Q 5_Mustela         2 VR    3 (6)
Q 12_Sorex          2 VR    3 (6)
Q 9_Ailuropoda      2 VR    3 (6)
Q Consensus         2 vr    3 (6)
                      ||
T Consensus        36 VR   37 (43)
T signal           36 VR   37 (43)
T 202              33 VR   34 (40)
T 205              33 VR   34 (40)
T 217              33 VR   34 (40)
T 220              33 VR   34 (40)
T 192              33 VR   34 (40)
T 196              33 VR   34 (40)
T 197              32 VR   33 (39)
T 225              33 VR   34 (40)
T 191              34 VR   35 (41)
Confidence            44


No 15 
>P13183_24_IM_ref_sig5_130
Probab=0.50  E-value=51  Score=6.07  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      |
T Consensus         8 A    8 (29)
T signal            8 A    8 (29)
T 11                8 A    8 (29)
T 12                8 T    8 (29)
T 16                8 A    8 (29)
T 10                8 A    8 (28)
T 49                8 A    8 (28)
T 50                8 A    8 (28)
T 56                8 A    8 (28)
T 58                8 A    8 (28)
T 1                 8 A    8 (24)
Confidence            2


No 16 
>P19783_22_IM_ref_sig5_130
Probab=0.50  E-value=52  Score=6.02  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      |
T Consensus        14 a   14 (27)
T signal           14 A   14 (27)
T 57               14 V   14 (27)
T 82               14 A   14 (27)
T 50               14 A   14 (27)
T 56               14 S   14 (27)
T 80               14 A   14 (27)
T 35               14 A   14 (26)
T 25               14 A   14 (27)
T 36               14 A   14 (27)
Confidence            2


No 17 
>P30038_24_MM_ref_sig5_130
Probab=0.46  E-value=55  Score=5.99  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        29 S   29 (29)
T signal           29 S   29 (29)
T 77               29 S   29 (29)
T 102              26 S   26 (26)
T 126              29 S   29 (29)
T 127              30 S   30 (30)
T 128              29 S   29 (29)
T 129              29 S   29 (29)
T 141              27 S   27 (27)
T 145              28 S   28 (28)
T 121              29 S   29 (29)
Confidence            0


No 18 
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.46  E-value=56  Score=5.92  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 9_Ailuropoda      3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus         6 R    6 (27)
T signal            6 R    6 (27)
T 6                 6 R    6 (27)
T 23                6 R    6 (27)
T 30                6 R    6 (27)
T 36                6 R    6 (27)
T 44                6 R    6 (27)
T 50                6 R    6 (27)
T 57                6 R    6 (27)
T 63                6 R    6 (27)
T 74                6 R    6 (27)
Confidence            2


No 19 
>Q94IN5_37_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.46  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        42 k   42 (42)
T signal           42 K   42 (42)
Confidence            0


No 20 
>P15651_24_MM_ref_sig5_130
Probab=0.44  E-value=58  Score=5.79  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         5 A    5 (6)
Q 12_Sorex          5 A    5 (6)
Q 9_Ailuropoda      5 A    5 (6)
Q Consensus         5 a    5 (6)
                      |
T Consensus        16 A   16 (29)
T signal           16 A   16 (29)
T 54               17 A   17 (29)
T 57               15 M   15 (27)
T 65               16 A   16 (29)
T 68               10 A   10 (23)
T 71               13 A   13 (26)
T 78               18 A   18 (31)
T 83               15 A   15 (28)
T 94               16 A   16 (29)
T 52               27 A   27 (40)
Confidence            2


Done!
