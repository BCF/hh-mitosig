Query         5_Mustela
Match_columns 6
No_of_seqs    9 out of 20
Neff          1.7 
Searched_HMMs 436
Date          Wed Sep  6 16:07:55 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_7A13_7Q13_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9FLX7_11_IM_ref_sig5_130        4.7     3.7  0.0085    8.0   0.0    3    3-5       8-10  (16)
  2 P17764_30_Mito_ref_sig5_130      4.3     4.2  0.0097    9.1   0.0    1    4-4      14-14  (35)
  3 Q16740_56_MM_ref_sig5_130        4.0     4.6   0.011   10.0   0.0    1    4-4      33-33  (61)
  4 P00430_16_IM_ref_sig5_130        3.2     5.9   0.014    8.0   0.0    2    2-3      15-16  (21)
  5 P26440_29_MM_ref_sig5_130        3.1     6.3   0.014    8.7   0.0    3    3-5      19-21  (34)
  6 P23786_25_IM_ref_sig5_130        3.0     6.4   0.015    8.5   0.0    3    3-5      23-25  (30)
  7 P13184_23_IM_ref_sig5_130        3.0     6.4   0.015    8.3   0.0    1    3-3      22-22  (28)
  8 P25285_22_Mito_ref_sig5_130      3.0     6.4   0.015    8.2   0.0    4    2-5      17-20  (27)
  9 P34899_31_Mito_ref_sig5_130      2.9     6.8   0.016    8.7   0.0    1    4-4      17-17  (36)
 10 P13621_23_Mito_IM_ref_sig5_130   2.9     6.9   0.016    8.3   0.0    3    3-5      22-24  (28)
 11 P30048_62_Mito_ref_sig5_130      2.7     7.4   0.017    6.7   0.0    1    1-1       1-1   (67)
 12 Q01205_68_Mito_ref_sig5_130      2.6     7.8   0.018    9.6   0.0    3    2-4      28-30  (73)
 13 P07926_68_MitoM_ref_sig5_130     2.5     8.2   0.019    9.5   0.0    3    3-5      23-25  (73)
 14 P18886_25_IM_ref_sig5_130        2.4     8.4   0.019    8.2   0.0    3    3-5      23-25  (30)
 15 P0CS90_23_MM_Nu_ref_sig5_130     2.4     8.5    0.02    7.9   0.0    2    4-5      16-17  (28)
 16 P35434_22_Mito_IM_ref_sig5_130   2.3     8.7    0.02    7.9   0.0    1    2-2      27-27  (27)
 17 P54898_44_Mito_ref_sig5_130      2.2     9.2   0.021    8.8   0.0    2    3-4      35-36  (49)
 18 P49411_43_Mito_ref_sig5_130      2.2     9.5   0.022    8.8   0.0    2    3-4      29-30  (48)
 19 Q91YT0_20_IM_ref_sig5_130        2.1     9.9   0.023    7.7   0.0    3    3-5       5-7   (25)
 20 P07806_47_Cy_Mito_ref_sig5_130   2.0      10   0.024    8.8   0.0    3    2-4      22-24  (52)

No 1  
>Q9FLX7_11_IM_ref_sig5_130
Probab=4.71  E-value=3.7  Score=8.05  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.8

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      |||
T Consensus         8 ~pl   10 (16)
T signal            8 RPL   10 (16)
T 12                8 RPL   10 (16)
T 11                8 GPL   10 (16)
T 9                 8 RPL   10 (16)
T 17               11 GGL   13 (24)
Confidence            565


No 2  
>P17764_30_Mito_ref_sig5_130
Probab=4.26  E-value=4.2  Score=9.13  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (6)
Q 8_Pteropus        4 P    4 (6)
Q 2_Macaca          4 P    4 (6)
Q 12_Sorex          4 P    4 (6)
Q 15_Ictidomys      4 P    4 (6)
Q 11_Cavia          4 P    4 (6)
Q 17_Tupaia         4 P    4 (6)
Q 10_Panthera       4 P    4 (6)
Q 16_Equus          4 S    4 (6)
Q Consensus         4 p    4 (6)
                      |
T Consensus        14 p   14 (35)
T signal           14 P   14 (35)
T 144              17 P   17 (38)
T 149              17 P   17 (38)
T 152              22 R   22 (43)
T 154              17 P   17 (38)
T 157               5 K    5 (26)
T 161              17 A   17 (38)
T 153               6 F    6 (26)
T 134              17 P   17 (38)
T 159              17 P   17 (38)
Confidence            3


No 3  
>Q16740_56_MM_ref_sig5_130
Probab=3.95  E-value=4.6  Score=10.00  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (6)
Q 8_Pteropus        4 P    4 (6)
Q 2_Macaca          4 P    4 (6)
Q 12_Sorex          4 P    4 (6)
Q 15_Ictidomys      4 P    4 (6)
Q 11_Cavia          4 P    4 (6)
Q 17_Tupaia         4 P    4 (6)
Q 10_Panthera       4 P    4 (6)
Q 16_Equus          4 S    4 (6)
Q Consensus         4 p    4 (6)
                      |
T Consensus        33 p   33 (61)
T signal           33 P   33 (61)
T 158              33 P   33 (61)
T 139              32 -   31 (54)
T 141              34 -   33 (58)
T 143              33 -   32 (57)
T 152              33 -   32 (57)
T 133              33 -   32 (57)
T 138              33 -   32 (57)
T 144              33 -   32 (57)
T 132              13 P   13 (40)
Confidence            2


No 4  
>P00430_16_IM_ref_sig5_130
Probab=3.22  E-value=5.9  Score=7.97  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.9

Q 5_Mustela         2 RR    3 (6)
Q 8_Pteropus        2 RR    3 (6)
Q 2_Macaca          2 RR    3 (6)
Q 12_Sorex          2 RR    3 (6)
Q 15_Ictidomys      2 RR    3 (6)
Q 11_Cavia          2 QR    3 (6)
Q 17_Tupaia         2 RR    3 (6)
Q 10_Panthera       2 RR    3 (6)
Q 16_Equus          2 RR    3 (6)
Q Consensus         2 rR    3 (6)
                      ||
T Consensus        15 rR   16 (21)
T signal           15 RR   16 (21)
T 13               15 CR   16 (21)
T 14               15 RR   16 (21)
T 15               15 RR   16 (21)
T 16               15 RR   16 (21)
T 24               15 RR   16 (21)
T 25               15 RS   16 (21)
T 35               15 RR   16 (21)
T 44               15 RR   16 (21)
T 6                12 RR   13 (16)
Confidence            44


No 5  
>P26440_29_MM_ref_sig5_130
Probab=3.07  E-value=6.3  Score=8.69  Aligned_cols=3  Identities=67%  Similarity=1.187  Sum_probs=1.2

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      .||
T Consensus        19 ~pl   21 (34)
T signal           19 PPL   21 (34)
T 107              22 QPL   24 (37)
T 130              22 QAF   24 (37)
T 132              19 RPP   21 (34)
T 136              22 APL   24 (37)
T 138              19 PPL   21 (34)
T 150              22 ARL   24 (37)
T 140              22 LPL   24 (37)
T 144              19 APL   21 (34)
T 139              22 LPL   24 (37)
Confidence            343


No 6  
>P23786_25_IM_ref_sig5_130
Probab=3.04  E-value=6.4  Score=8.49  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.7

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      |||
T Consensus        23 RpL   25 (30)
T signal           23 RPL   25 (30)
T 101              23 RPL   25 (30)
T 104              23 RPL   25 (30)
T 108              23 RPL   25 (30)
T 109              24 RSL   26 (31)
T 117              23 RSL   25 (30)
T 119              23 RPL   25 (30)
T 59               28 RSL   30 (35)
Confidence            555


No 7  
>P13184_23_IM_ref_sig5_130
Probab=3.03  E-value=6.4  Score=8.31  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         3 R    3 (6)
Q 8_Pteropus        3 R    3 (6)
Q 2_Macaca          3 R    3 (6)
Q 12_Sorex          3 R    3 (6)
Q 15_Ictidomys      3 R    3 (6)
Q 11_Cavia          3 R    3 (6)
Q 17_Tupaia         3 R    3 (6)
Q 10_Panthera       3 R    3 (6)
Q 16_Equus          3 R    3 (6)
Q Consensus         3 R    3 (6)
                      |
T Consensus        22 R   22 (28)
T signal           22 R   22 (28)
T 63               22 R   22 (28)
T 28               22 R   22 (28)
T 15               22 R   22 (28)
T 25               22 R   22 (28)
T 16               22 R   22 (28)
T 19               22 R   22 (28)
T 12               22 R   22 (28)
T 14               22 S   22 (28)
Confidence            3


No 8  
>P25285_22_Mito_ref_sig5_130
Probab=3.02  E-value=6.4  Score=8.24  Aligned_cols=4  Identities=50%  Similarity=0.576  Sum_probs=2.0

Q 5_Mustela         2 RRPL    5 (6)
Q 8_Pteropus        2 RRPL    5 (6)
Q 2_Macaca          2 RRPL    5 (6)
Q 12_Sorex          2 RRPL    5 (6)
Q 15_Ictidomys      2 RRPL    5 (6)
Q 11_Cavia          2 QRPL    5 (6)
Q 17_Tupaia         2 RRPF    5 (6)
Q 10_Panthera       2 RRPL    5 (6)
Q 16_Equus          2 RRSL    5 (6)
Q Consensus         2 rRpl    5 (6)
                      .|||
T Consensus        17 ~R~l   20 (27)
T signal           17 CRSL   20 (27)
T 107              17 SRSL   20 (27)
T 127              17 GRSF   20 (27)
T 128              17 GRGL   20 (27)
T 134              17 SRPL   20 (27)
T 138              17 SRSL   20 (27)
T 145              17 TRPF   20 (27)
T 146              17 SRLY   20 (27)
T 147              17 CRPL   20 (27)
T 152              17 GRQL   20 (27)
Confidence            3555


No 9  
>P34899_31_Mito_ref_sig5_130
Probab=2.88  E-value=6.8  Score=8.69  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         4 P.    4 (6)
Q 8_Pteropus        4 P.    4 (6)
Q 2_Macaca          4 P.    4 (6)
Q 12_Sorex          4 P.    4 (6)
Q 15_Ictidomys      4 P.    4 (6)
Q 11_Cavia          4 P.    4 (6)
Q 17_Tupaia         4 P.    4 (6)
Q 10_Panthera       4 P.    4 (6)
Q 16_Equus          4 S.    4 (6)
Q Consensus         4 p.    4 (6)
                      | 
T Consensus        17 p.   17 (36)
T signal           17 S.   17 (36)
T 145              17 P.   17 (36)
T 135              17 G.   17 (35)
T 141              17 P.   17 (36)
T 140              17 P.   17 (35)
T 134              12 Pl   13 (34)
T 138              15 P.   15 (35)
T 139              15 P.   15 (33)
T 143              15 P.   15 (34)
T 144              15 P.   15 (34)
Confidence            3 


No 10 
>P13621_23_Mito_IM_ref_sig5_130
Probab=2.86  E-value=6.9  Score=8.26  Aligned_cols=3  Identities=67%  Similarity=1.586  Sum_probs=1.7

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      ||+
T Consensus        22 RP~   24 (28)
T signal           22 RPF   24 (28)
T 148              22 RPF   24 (28)
T 131              18 RPA   20 (24)
T 123              18 RPV   20 (24)
T 137              15 RPI   17 (21)
T 130              18 RPA   20 (24)
T 144              18 RPA   20 (24)
T 134              18 RPV   20 (24)
Confidence            664


No 11 
>P30048_62_Mito_ref_sig5_130
Probab=2.70  E-value=7.4  Score=6.67  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.0

Q 5_Mustela         1 M    1 (6)
Q 8_Pteropus        1 L    1 (6)
Q 2_Macaca          1 V    1 (6)
Q 12_Sorex          1 A    1 (6)
Q 15_Ictidomys      1 V    1 (6)
Q 11_Cavia          1 V    1 (6)
Q 17_Tupaia         1 V    1 (6)
Q 10_Panthera       1 V    1 (6)
Q 16_Equus          1 V    1 (6)
Q Consensus         1 v    1 (6)
                      |
T Consensus         1 M    1 (67)
T signal            1 M    1 (67)
T 79_Drosophila     1 M    1 (67)
T 76_Meleagris      1 M    1 (67)
T 70_Strongyloce    1 M    1 (67)
T 69_Metaseiulus    1 M    1 (67)
T 63_Columba        1 Q    1 (67)
T 57_Falco          1 P    1 (67)
T 44_Camelus        1 M    1 (67)
Confidence            0


No 12 
>Q01205_68_Mito_ref_sig5_130
Probab=2.56  E-value=7.8  Score=9.55  Aligned_cols=3  Identities=67%  Similarity=1.088  Sum_probs=1.2

Q 5_Mustela         2 RRP    4 (6)
Q 8_Pteropus        2 RRP    4 (6)
Q 2_Macaca          2 RRP    4 (6)
Q 12_Sorex          2 RRP    4 (6)
Q 15_Ictidomys      2 RRP    4 (6)
Q 11_Cavia          2 QRP    4 (6)
Q 17_Tupaia         2 RRP    4 (6)
Q 10_Panthera       2 RRP    4 (6)
Q 16_Equus          2 RRS    4 (6)
Q Consensus         2 rRp    4 (6)
                      ||+
T Consensus        28 Rrs   30 (73)
T signal           28 RRS   30 (73)
T 126              27 RRS   29 (72)
T 144              29 KRS   31 (74)
T 148              17 RRS   19 (62)
T 128              28 RRA   30 (72)
T 129              28 RRA   30 (72)
T 131              28 RRS   30 (72)
T 142              24 RCA   26 (70)
T 141              24 SRP   26 (69)
Confidence            343


No 13 
>P07926_68_MitoM_ref_sig5_130
Probab=2.46  E-value=8.2  Score=9.49  Aligned_cols=3  Identities=67%  Similarity=1.010  Sum_probs=1.3

Q 5_Mustela         3 RP.....L    5 (6)
Q 8_Pteropus        3 RP.....L    5 (6)
Q 2_Macaca          3 RP.....L    5 (6)
Q 12_Sorex          3 RP.....L    5 (6)
Q 15_Ictidomys      3 RP.....L    5 (6)
Q 11_Cavia          3 RP.....L    5 (6)
Q 17_Tupaia         3 RP.....F    5 (6)
Q 10_Panthera       3 RP.....L    5 (6)
Q 16_Equus          3 RS.....L    5 (6)
Q Consensus         3 Rp.....l    5 (6)
                      ||     |
T Consensus        23 RP.....l   25 (73)
T signal           23 RS.....L   25 (73)
T 10               23 RP.....L   25 (82)
T 14               23 RP.....L   25 (55)
T 22               23 RP.....L   25 (37)
T 9                23 RP.....V   25 (67)
T 5                26 RP.....L   28 (74)
T 6                26 RP.....L   28 (68)
T 2                20 RSalrplG   27 (84)
T 3                25 RP.....F   27 (84)
T 53                7 RP.....L    9 (40)
Confidence            44     4


No 14 
>P18886_25_IM_ref_sig5_130
Probab=2.42  E-value=8.4  Score=8.18  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.6

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      |||
T Consensus        23 R~L   25 (30)
T signal           23 RPL   25 (30)
T 84               23 RTL   25 (30)
T 87               23 RPL   25 (30)
T 102              24 RSL   26 (31)
T 114              23 RPL   25 (30)
T 92               23 RLL   25 (30)
T 94               23 RSL   25 (30)
T 48               28 RSL   30 (35)
Confidence            554


No 15 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=2.39  E-value=8.5  Score=7.87  Aligned_cols=2  Identities=0%  Similarity=0.401  Sum_probs=0.8

Q 5_Mustela         4 PL    5 (6)
Q 8_Pteropus        4 PL    5 (6)
Q 2_Macaca          4 PL    5 (6)
Q 12_Sorex          4 PL    5 (6)
Q 15_Ictidomys      4 PL    5 (6)
Q 11_Cavia          4 PL    5 (6)
Q 17_Tupaia         4 PF    5 (6)
Q 10_Panthera       4 PL    5 (6)
Q 16_Equus          4 SL    5 (6)
Q Consensus         4 pl    5 (6)
                      |+
T Consensus        16 ~~   17 (28)
T signal           16 SF   17 (28)
T 55               17 AL   18 (29)
T 65               17 VV   18 (29)
T 54               17 -P   17 (28)
T 57               14 PI   15 (26)
T 59               14 PL   15 (26)
T 62               13 -N   13 (24)
T 56               12 PL   13 (24)
T 60               11 NV   12 (23)
Confidence            33


No 16 
>P35434_22_Mito_IM_ref_sig5_130
Probab=2.34  E-value=8.7  Score=7.94  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         2 R    2 (6)
Q 8_Pteropus        2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 12_Sorex          2 R    2 (6)
Q 15_Ictidomys      2 R    2 (6)
Q 11_Cavia          2 Q    2 (6)
Q 17_Tupaia         2 R    2 (6)
Q 10_Panthera       2 R    2 (6)
Q 16_Equus          2 R    2 (6)
Q Consensus         2 r    2 (6)
                      -
T Consensus        27 ~   27 (27)
T signal           27 A   27 (27)
T 160              27 A   27 (27)
T 152              27 A   27 (27)
T 140              25 P   25 (25)
T 143              33 P   33 (33)
T 132              34 A   34 (34)
T 137              29 G   29 (29)
T 121              26 -   25 (25)
T 127              25 -   24 (24)
T 120              26 P   26 (26)
Confidence            0


No 17 
>P54898_44_Mito_ref_sig5_130
Probab=2.24  E-value=9.2  Score=8.84  Aligned_cols=2  Identities=50%  Similarity=0.750  Sum_probs=0.8

Q 5_Mustela         3 RP    4 (6)
Q 8_Pteropus        3 RP    4 (6)
Q 2_Macaca          3 RP    4 (6)
Q 12_Sorex          3 RP    4 (6)
Q 15_Ictidomys      3 RP    4 (6)
Q 11_Cavia          3 RP    4 (6)
Q 17_Tupaia         3 RP    4 (6)
Q 10_Panthera       3 RP    4 (6)
Q 16_Equus          3 RS    4 (6)
Q Consensus         3 Rp    4 (6)
                      ||
T Consensus        35 R~   36 (49)
T signal           35 RQ   36 (49)
T 154              37 RP   38 (51)
T 151              36 RP   37 (50)
T 104              35 RS   36 (43)
T 137              42 RS   43 (49)
Confidence            44


No 18 
>P49411_43_Mito_ref_sig5_130
Probab=2.18  E-value=9.5  Score=8.77  Aligned_cols=2  Identities=50%  Similarity=0.401  Sum_probs=0.7

Q 5_Mustela         3 RP    4 (6)
Q 8_Pteropus        3 RP    4 (6)
Q 2_Macaca          3 RP    4 (6)
Q 12_Sorex          3 RP    4 (6)
Q 15_Ictidomys      3 RP    4 (6)
Q 11_Cavia          3 RP    4 (6)
Q 17_Tupaia         3 RP    4 (6)
Q 10_Panthera       3 RP    4 (6)
Q 16_Equus          3 RS    4 (6)
Q Consensus         3 Rp    4 (6)
                      ||
T Consensus        29 rp   30 (48)
T signal           29 RL   30 (48)
T 163              29 RP   30 (48)
T 166              24 RP   25 (43)
T 167              29 RP   30 (48)
T 170              26 RP   27 (45)
T 171              29 RP   30 (48)
T 177              29 RL   30 (48)
T 157              43 HP   44 (62)
Confidence            33


No 19 
>Q91YT0_20_IM_ref_sig5_130
Probab=2.11  E-value=9.9  Score=7.72  Aligned_cols=3  Identities=33%  Similarity=0.623  Sum_probs=1.5

Q 5_Mustela         3 RPL    5 (6)
Q 8_Pteropus        3 RPL    5 (6)
Q 2_Macaca          3 RPL    5 (6)
Q 12_Sorex          3 RPL    5 (6)
Q 15_Ictidomys      3 RPL    5 (6)
Q 11_Cavia          3 RPL    5 (6)
Q 17_Tupaia         3 RPF    5 (6)
Q 10_Panthera       3 RPL    5 (6)
Q 16_Equus          3 RSL    5 (6)
Q Consensus         3 Rpl    5 (6)
                      |||
T Consensus         5 R~l    7 (25)
T signal            5 RHF    7 (25)
T 132               5 RPL    7 (29)
T 93                5 RRL    7 (25)
Confidence            554


No 20 
>P07806_47_Cy_Mito_ref_sig5_130
Probab=2.02  E-value=10  Score=8.79  Aligned_cols=3  Identities=67%  Similarity=1.088  Sum_probs=1.3

Q 5_Mustela         2 RRP    4 (6)
Q 8_Pteropus        2 RRP    4 (6)
Q 2_Macaca          2 RRP    4 (6)
Q 12_Sorex          2 RRP    4 (6)
Q 15_Ictidomys      2 RRP    4 (6)
Q 11_Cavia          2 QRP    4 (6)
Q 17_Tupaia         2 RRP    4 (6)
Q 10_Panthera       2 RRP    4 (6)
Q 16_Equus          2 RRS    4 (6)
Q Consensus         2 rRp    4 (6)
                      ||+
T Consensus        22 rrS   24 (52)
T signal           22 RRS   24 (52)
T 176               1 --S    1 (29)
Confidence            444


Done!
