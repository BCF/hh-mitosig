Query         5_Mustela
Match_columns 7
No_of_seqs    5 out of 20
Neff          1.4 
Searched_HMMs 436
Date          Wed Sep  6 16:08:20 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_23A30_23Q30_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       93.8 7.5E-05 1.7E-07   22.4   0.0    7    1-7      24-30  (35)
  2 Q01859_46_Mito_IM_ref_sig5_130   9.6     1.5  0.0034   11.6   0.0    6    1-6      31-36  (51)
  3 Q3ZBF3_26_Mito_ref_sig5_130      1.4      17   0.038    4.5   0.0    1    3-3      31-31  (31)
  4 P22778_45_Mito_IM_ref_sig5_130   1.3      17   0.039    8.4   0.0    1    5-5      49-49  (50)
  5 Q7Z6M4_42_Mito_ref_sig5_130      1.3      18   0.041    8.2   0.0    1    2-2      13-13  (47)
  6 P32445_17_Mito_ref_sig5_130      1.3      18   0.041    7.1   0.0    4    1-4      11-14  (22)
  7 P10175_24_IM_ref_sig5_130        1.2      19   0.043    7.5   0.0    1    5-5      21-21  (29)
  8 P32089_13_IM_ref_sig5_130        1.2      19   0.043    6.8   0.0    1    5-5       4-4   (18)
  9 P20821_48_Mito_ref_sig5_130      1.2      20   0.045    8.4   0.0    1    5-5      45-45  (53)
 10 Q09544_18_Mito_IM_ref_sig5_130   1.2      20   0.045    7.2   0.0    1    5-5      22-22  (23)
 11 P23434_48_Mito_ref_sig5_130      1.1      21   0.048    8.3   0.0    1    5-5      45-45  (53)
 12 P40513_47_MM_ref_sig5_130        1.1      21   0.049    8.2   0.0    1    5-5      41-41  (52)
 13 P10176_25_IM_ref_sig5_130        0.9      25   0.058    7.3   0.0    2    4-5      14-15  (30)
 14 P32473_33_MM_ref_sig5_130        0.8      30   0.069    7.4   0.0    1    5-5      18-18  (38)
 15 Q16740_56_MM_ref_sig5_130        0.8      31   0.071    8.0   0.0    1    3-3      51-51  (61)
 16 P80433_25_IM_ref_sig5_130        0.8      31   0.071    7.0   0.0    1    5-5      15-15  (30)
 17 O00142_33_Mito_ref_sig5_130      0.8      32   0.073    4.2   0.0    1    3-3      38-38  (38)
 18 P35218_38_Mito_ref_sig5_130      0.7      33   0.075    4.7   0.0    1    3-3      43-43  (43)
 19 P42116_26_IM_ref_sig5_130        0.7      33   0.076    6.4   0.0    1    5-5      10-10  (31)
 20 P05630_22_Mito_IM_ref_sig5_130   0.7      34   0.078    6.7   0.0    2    5-6       3-4   (27)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=93.80  E-value=7.5e-05  Score=22.38  Aligned_cols=7  Identities=86%  Similarity=1.033  Sum_probs=6.3

Q 5_Mustela         1 HRTAPAA    7 (7)
Q 2_Macaca          1 HWTAPTA    7 (7)
Q 1_Homo            1 HWTAPAA    7 (7)
Q 15_Ictidomys      1 HRTTPAA    7 (7)
Q 11_Cavia          1 HRTAPAT    7 (7)
Q Consensus         1 h~tapaa    7 (7)
                      ||++|||
T Consensus        24 Hrs~PAA   30 (35)
T signal           24 HWTAPAA   30 (35)
T 143              28 HRSPPAA   34 (39)
T 144              24 HRSTPAA   30 (35)
T 145              24 HRTAPVS   30 (35)
T 147              24 HRTPSAA   30 (35)
T 135              33 HGSAPAA   39 (44)
T 141              38 RLSAPAA   44 (49)
T 142              23 HGSGSAA   29 (34)
T 149              23 HRSVPAA   29 (34)
T 150              23 HRTVPAA   29 (34)
Confidence            8999986


No 2  
>Q01859_46_Mito_IM_ref_sig5_130
Probab=9.58  E-value=1.5  Score=11.62  Aligned_cols=6  Identities=50%  Similarity=1.143  Sum_probs=2.6

Q 5_Mustela         1 HRTAPA    6 (7)
Q 2_Macaca          1 HWTAPT    6 (7)
Q 1_Homo            1 HWTAPA    6 (7)
Q 15_Ictidomys      1 HRTTPA    6 (7)
Q 11_Cavia          1 HRTAPA    6 (7)
Q Consensus         1 h~tapa    6 (7)
                      ||..|+
T Consensus        31 hrpspa   36 (51)
T signal           31 HRPSPS   36 (51)
T 138              32 HRPSPA   37 (52)
T 137              31 HRPSPA   36 (51)
Confidence            444443


No 3  
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=1.36  E-value=17  Score=4.51  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 2_Macaca          3 T    3 (7)
Q 1_Homo            3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 T    3 (7)
Q Consensus         3 t    3 (7)
                      .
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 4  
>P22778_45_Mito_IM_ref_sig5_130
Probab=1.33  E-value=17  Score=8.42  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        49 s   49 (50)
T signal           49 S   49 (50)
T 194              53 S   53 (53)
T 196              51 S   51 (51)
T 197              44 P   44 (45)
T 185              51 S   51 (51)
T 191              49 T   49 (49)
T 192              47 P   47 (47)
T 195              47 P   47 (47)
T 189              42 S   42 (43)
T 193              49 T   49 (50)
Confidence            2


No 5  
>Q7Z6M4_42_Mito_ref_sig5_130
Probab=1.27  E-value=18  Score=8.20  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         2 R    2 (7)
Q 2_Macaca          2 W    2 (7)
Q 1_Homo            2 W    2 (7)
Q 15_Ictidomys      2 R    2 (7)
Q 11_Cavia          2 R    2 (7)
Q Consensus         2 ~    2 (7)
                      |
T Consensus        13 r   13 (47)
T signal           13 R   13 (47)
T 36               13 R   13 (46)
T 42               13 R   13 (47)
T 38               13 R   13 (47)
T 33                6 C    6 (40)
T 39               13 R   13 (43)
T 34               11 L   11 (43)
T 30                8 C    8 (43)
T 31                6 R    6 (32)
T 29               21 R   21 (44)
Confidence            2


No 6  
>P32445_17_Mito_ref_sig5_130
Probab=1.27  E-value=18  Score=7.15  Aligned_cols=4  Identities=50%  Similarity=0.708  Sum_probs=1.5

Q 5_Mustela         1 HRTA    4 (7)
Q 2_Macaca          1 HWTA    4 (7)
Q 1_Homo            1 HWTA    4 (7)
Q 15_Ictidomys      1 HRTT    4 (7)
Q 11_Cavia          1 HRTA    4 (7)
Q Consensus         1 h~ta    4 (7)
                      |-|+
T Consensus        11 hATa   14 (22)
T signal           11 HATT   14 (22)
T 7                11 HATA   14 (22)
T 8                11 SQTA   14 (22)
T 12               11 HATS   14 (22)
T 15               11 HATA   14 (22)
T 16               10 HATA   13 (21)
T 17               10 HATA   13 (21)
T 11                7 HATA   10 (18)
T 14                7 HASS   10 (18)
T 5                11 SATA   14 (22)
Confidence            3343


No 7  
>P10175_24_IM_ref_sig5_130
Probab=1.23  E-value=19  Score=7.49  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        21 P   21 (29)
T signal           21 P   21 (29)
T 4                21 P   21 (29)
T 15               21 S   21 (29)
T 7                21 P   21 (29)
T 6                21 P   21 (29)
T 9                21 P   21 (29)
T 28               21 P   21 (29)
T 3                15 P   15 (23)
T 30               19 P   19 (27)
Confidence            3


No 8  
>P32089_13_IM_ref_sig5_130
Probab=1.22  E-value=19  Score=6.84  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus         4 p    4 (18)
T signal            4 P    4 (18)
T 136               2 P    2 (16)
T 126               4 P    4 (18)
T 127               4 P    4 (18)
Confidence            3


No 9  
>P20821_48_Mito_ref_sig5_130
Probab=1.18  E-value=20  Score=8.35  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        45 ~   45 (53)
T signal           45 P   45 (53)
T 91               45 P   45 (53)
T 98               45 P   45 (53)
T 101              45 T   45 (53)
T 90               45 R   45 (53)
T 92               45 R   45 (53)
T 93               42 S   42 (50)
T 96               45 F   45 (53)
T 105              39 P   39 (47)
T 87               62 A   62 (70)
Confidence            2


No 10 
>Q09544_18_Mito_IM_ref_sig5_130
Probab=1.17  E-value=20  Score=7.16  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        22 p   22 (23)
T signal           22 P   22 (23)
Confidence            3


No 11 
>P23434_48_Mito_ref_sig5_130
Probab=1.12  E-value=21  Score=8.29  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        45 ~   45 (53)
T signal           45 P   45 (53)
T 120              45 P   45 (53)
T 123              45 T   45 (53)
T 110              45 R   45 (53)
T 114              45 R   45 (53)
T 117              45 F   45 (53)
T 119              42 S   42 (50)
T 108              34 P   34 (42)
T 112              39 P   39 (47)
T 106              62 A   62 (70)
Confidence            2


No 12 
>P40513_47_MM_ref_sig5_130
Probab=1.09  E-value=21  Score=8.24  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        41 p   41 (52)
T signal           41 P   41 (52)
Confidence            2


No 13 
>P10176_25_IM_ref_sig5_130
Probab=0.95  E-value=25  Score=7.26  Aligned_cols=2  Identities=0%  Similarity=0.152  Sum_probs=0.7

Q 5_Mustela         4 AP    5 (7)
Q 2_Macaca          4 AP    5 (7)
Q 1_Homo            4 AP    5 (7)
Q 15_Ictidomys      4 TP    5 (7)
Q 11_Cavia          4 AP    5 (7)
Q Consensus         4 ap    5 (7)
                      .|
T Consensus        14 g~   15 (30)
T signal           14 GS   15 (30)
T 6                14 AP   15 (30)
T 18               14 GP   15 (30)
T 23               14 GP   15 (30)
T 29               14 GL   15 (30)
T 38               14 GS   15 (30)
T 2                14 RP   15 (31)
T 3                14 GL   15 (30)
T 5                14 GP   15 (30)
T 27               14 AP   15 (30)
Confidence            33


No 14 
>P32473_33_MM_ref_sig5_130
Probab=0.80  E-value=30  Score=7.36  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        18 p   18 (38)
T signal           18 P   18 (38)
Confidence            3


No 15 
>Q16740_56_MM_ref_sig5_130
Probab=0.79  E-value=31  Score=8.00  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         3 T    3 (7)
Q 2_Macaca          3 T    3 (7)
Q 1_Homo            3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 T    3 (7)
Q Consensus         3 t    3 (7)
                      |
T Consensus        51 T   51 (61)
T signal           51 T   51 (61)
T 158              51 T   51 (61)
T 139              44 T   44 (54)
T 141              48 T   48 (58)
T 143              47 T   47 (57)
T 152              47 T   47 (57)
T 133              47 T   47 (57)
T 138              47 T   47 (57)
T 144              47 T   47 (57)
T 132              30 T   30 (40)
Confidence            3


No 16 
>P80433_25_IM_ref_sig5_130
Probab=0.79  E-value=31  Score=7.02  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        15 ~   15 (30)
T signal           15 P   15 (30)
T 3                15 L   15 (30)
T 20               15 P   15 (30)
T 22               15 P   15 (30)
T 31               15 S   15 (30)
T 36               15 P   15 (30)
T 5                15 S   15 (30)
T 8                15 S   15 (30)
T 19               15 P   15 (30)
Confidence            3


No 17 
>O00142_33_Mito_ref_sig5_130
Probab=0.77  E-value=32  Score=4.22  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 2_Macaca          3 T    3 (7)
Q 1_Homo            3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 T    3 (7)
Q Consensus         3 t    3 (7)
                      .
T Consensus        38 ~   38 (38)
T signal           38 A   38 (38)
T 92_Pediculus     35 -   34 (34)
T 83_Nematostell   35 -   34 (34)
T 79_Bombus        35 -   34 (34)
T 75_Bombyx        35 -   34 (34)
T 57_Xiphophorus   35 -   34 (34)
T 45_Anolis        35 -   34 (34)
T 40_Monodelphis   35 -   34 (34)
Confidence            0


No 18 
>P35218_38_Mito_ref_sig5_130
Probab=0.75  E-value=33  Score=4.68  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 2_Macaca          3 T    3 (7)
Q 1_Homo            3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 T    3 (7)
Q Consensus         3 t    3 (7)
                      |
T Consensus        43 ~   43 (43)
T signal           43 T   43 (43)
T 35_Oryctolagus   48 -   47 (47)
T 26_Rattus        48 -   47 (47)
T 40_Octodon       48 -   47 (47)
T 30_Jaculus       48 -   47 (47)
T 42_Sarcophilus   48 -   47 (47)
T 2_Pan            42 -   41 (41)
T 39_Echinops      42 -   41 (41)
T 6_Pongo          28 -   27 (27)
Confidence            0


No 19 
>P42116_26_IM_ref_sig5_130
Probab=0.74  E-value=33  Score=6.41  Aligned_cols=1  Identities=0%  Similarity=-0.296  Sum_probs=0.3

Q 5_Mustela         5 P    5 (7)
Q 2_Macaca          5 P    5 (7)
Q 1_Homo            5 P    5 (7)
Q 15_Ictidomys      5 P    5 (7)
Q 11_Cavia          5 P    5 (7)
Q Consensus         5 p    5 (7)
                      |
T Consensus        10 ~   10 (31)
T signal           10 R   10 (31)
T 23_Arthroderma   10 P   10 (31)
T 21_Talaromyces   10 P   10 (31)
T 17_Leptosphaer   10 A   10 (31)
T 15_Zymoseptori   10 V   10 (31)
T 13_Aspergillus   10 S   10 (31)
T 8_Sclerotinia    10 Q   10 (31)
T 5_Thielavia      10 Q   10 (31)
T 3_Magnaporthe    10 V   10 (31)
T cl|DABBABABA|2   10 C   10 (31)
Confidence            2


No 20 
>P05630_22_Mito_IM_ref_sig5_130
Probab=0.72  E-value=34  Score=6.74  Aligned_cols=2  Identities=50%  Similarity=1.447  Sum_probs=0.8

Q 5_Mustela         5 PA    6 (7)
Q 2_Macaca          5 PT    6 (7)
Q 1_Homo            5 PA    6 (7)
Q 15_Ictidomys      5 PA    6 (7)
Q 11_Cavia          5 PA    6 (7)
Q Consensus         5 pa    6 (7)
                      |+
T Consensus         3 pa    4 (27)
T signal            3 PS    4 (27)
T 133               3 PV    4 (27)
T 148               3 PA    4 (27)
T 167               3 PA    4 (27)
T 155               3 PA    4 (27)
T 147               3 PA    4 (27)
T 140               3 SA    4 (26)
T 142               3 PA    4 (32)
T 121               3 PA    4 (34)
T 134               3 PA    4 (29)
Confidence            43


Done!
