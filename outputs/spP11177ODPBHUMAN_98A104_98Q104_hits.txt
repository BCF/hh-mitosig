Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:13 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_98A104_98Q104_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130        3.9     4.6   0.011    9.1   0.0    1    3-3       4-4   (35)
  2 Q3ZBF3_26_Mito_ref_sig5_130      3.8     4.8   0.011    5.4   0.0    1    3-3       1-1   (31)
  3 Q96E29_68_Mito_ref_sig5_130      3.7     5.1   0.012    6.1   0.0    1    3-3       1-1   (73)
  4 A4FUC0_29_Mito_ref_sig5_130      3.4     5.5   0.013    4.8   0.0    1    1-1       1-1   (34)
  5 Q9NR28_55_Mito_ref_sig5_130      3.2       6   0.014    6.4   0.0    1    3-3       1-1   (60)
  6 P18155_35_Mito_ref_sig5_130      3.0     6.5   0.015    5.1   0.0    1    3-3       1-1   (40)
  7 Q16595_41_Cy_Mito_ref_sig5_130   3.0     6.6   0.015    9.2   0.0    1    5-5      11-11  (46)
  8 Q07021_73_MM_Nu_Cm_Cy_ref_sig5   2.9     6.6   0.015   10.0   0.0    1    4-4      17-17  (78)
  9 P15650_30_MM_ref_sig5_130        2.9     6.8   0.016    5.0   0.0    1    3-3       1-1   (35)
 10 P56522_34_MM_ref_sig5_130        2.8     6.9   0.016    4.5   0.0    1    1-1       1-1   (39)
 11 P29117_29_MM_ref_sig5_130        2.8     7.1   0.016    5.5   0.0    1    3-3       1-1   (34)
 12 P35218_38_Mito_ref_sig5_130      2.8     7.1   0.016    5.9   0.0    1    3-3       1-1   (43)
 13 Q9BYV1_41_Mito_ref_sig5_130      2.6     7.5   0.017    4.9   0.0    1    1-1       1-1   (46)
 14 P42026_37_Mito_ref_sig5_130      2.6     7.6   0.017    8.8   0.0    1    4-4       2-2   (42)
 15 P14604_29_MM_ref_sig5_130        2.4     8.5   0.019    8.4   0.0    1    4-4       2-2   (34)
 16 P30084_27_MM_ref_sig5_130        2.4     8.6    0.02    8.2   0.0    1    4-4       2-2   (32)
 17 P34899_31_Mito_ref_sig5_130      2.4     8.6    0.02    8.5   0.0    1    3-3       3-3   (36)
 18 P21953_50_MM_ref_sig5_130        2.3     8.7    0.02    4.8   0.0    1    1-1       1-1   (55)
 19 P45954_33_MM_ref_sig5_130        2.3     8.7    0.02    4.5   0.0    1    1-1       1-1   (38)
 20 Q9YHT1_44_IM_ref_sig5_130        2.3     8.8    0.02    8.8   0.0    2    3-4       1-2   (49)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=3.94  E-value=4.6  Score=9.09  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.3

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      +
T Consensus         4 l    4 (35)
T signal            4 V    4 (35)
T 143               8 L    8 (39)
T 144               4 L    4 (35)
T 145               4 M    4 (35)
T 147               4 L    4 (35)
T 135               4 A    4 (44)
T 141               4 A    4 (49)
T 142               3 L    3 (34)
T 149               3 L    3 (34)
T 150               3 L    3 (34)
Confidence            2


No 2  
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=3.82  E-value=4.8  Score=5.38  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (31)
T signal            1 M    1 (31)
T 96_Brugia         1 M    1 (30)
T 87_Branchiosto    1 M    1 (30)
T 86_Oryzias        1 C    1 (30)
T 84_Pediculus      1 M    1 (30)
T 70_Anas           1 M    1 (30)
T 69_Strongyloce    1 S    1 (30)
T 63_Zonotrichia    1 M    1 (30)
T 55_Alligator      1 M    1 (30)
T 44_Myotis         1 M    1 (30)
Confidence            2


No 3  
>Q96E29_68_Mito_ref_sig5_130
Probab=3.66  E-value=5.1  Score=6.15  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (73)
T signal            1 M    1 (73)
T 93_Musca          1 M    1 (72)
T 107_Ciona         1 M    1 (72)
T 58_Taeniopygia    1 M    1 (72)
T 112_Loa           1 M    1 (72)
T 77_Takifugu       1 M    1 (72)
T 85_Acyrthosiph    1 M    1 (72)
Confidence            2


No 4  
>A4FUC0_29_Mito_ref_sig5_130
Probab=3.42  E-value=5.5  Score=4.78  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 83_Ceratitis      1 M    1 (34)
T 70_Xiphophorus    1 M    1 (34)
T 62_Anolis         1 M    1 (34)
T 61_Geospiza       1 W    1 (34)
T 60_Anas           1 C    1 (34)
T 59_Zonotrichia    1 R    1 (34)
T 57_Columba        1 P    1 (34)
T 55_Falco          1 W    1 (34)
T 43_Dasypus        1 M    1 (34)
Confidence            0


No 5  
>Q9NR28_55_Mito_ref_sig5_130
Probab=3.19  E-value=6  Score=6.40  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (60)
T signal            1 M    1 (60)
T 71_Takifugu       1 F    1 (57)
T 67_Haplochromi    1 M    1 (57)
T 65_Xiphophorus    1 M    1 (57)
T 58_Meleagris      1 M    1 (57)
T 57_Falco          1 M    1 (57)
T 38_Cricetulus     1 M    1 (57)
T cl|DABBABABA|1    1 M    1 (57)
T cl|KABBABABA|1    1 M    1 (57)
T cl|BEBBABABA|1    1 M    1 (57)
Confidence            2


No 6  
>P18155_35_Mito_ref_sig5_130
Probab=3.00  E-value=6.5  Score=5.08  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (40)
T signal            1 M    1 (40)
T 95_Emiliania      1 M    1 (39)
T 93_Volvox         1 M    1 (39)
T 89_Ornithorhyn    1 H    1 (39)
T 77_Pediculus      1 M    1 (39)
T 75_Trichoplax     1 M    1 (39)
T 71_Saccoglossu    1 M    1 (39)
T 67_Monodelphis    1 M    1 (39)
T 41_Capra          1 M    1 (39)
Confidence            1


No 7  
>Q16595_41_Cy_Mito_ref_sig5_130
Probab=2.95  E-value=6.6  Score=9.16  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         5 G    5 (6)
Q Consensus         5 g    5 (6)
                      |
T Consensus        11 G   11 (46)
T signal           11 G   11 (46)
T 159              11 G   11 (49)
T 157              11 G   11 (46)
T 156              11 G   11 (46)
T 153              11 G   11 (43)
T 145              11 S   11 (46)
T 142              11 G   11 (44)
T 147              11 G   11 (44)
T 141               4 G    4 (38)
Confidence            3


No 8  
>Q07021_73_MM_Nu_Cm_Cy_ref_sig5_130
Probab=2.94  E-value=6.6  Score=9.97  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      +
T Consensus        17 ~   17 (78)
T signal           17 A   17 (78)
T 52               10 A   10 (78)
T 36                1 -    0 (31)
T 26               13 R   13 (85)
T 48               14 R   14 (80)
T 30               11 R   11 (68)
T 19                1 -    0 (49)
T 16                1 -    0 (49)
T 21                1 -    0 (45)
T 13                1 -    0 (47)
Confidence            2


No 9  
>P15650_30_MM_ref_sig5_130
Probab=2.87  E-value=6.8  Score=4.99  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (35)
T signal            1 M    1 (35)
T 78_Saimiri        1 M    1 (34)
T 74_Tursiops       1 A    1 (34)
T 67_Xiphophorus    1 R    1 (34)
T 57_Meleagris      1 M    1 (34)
T 48_Anas           1 M    1 (34)
T 22_Ceratotheri    1 M    1 (34)
T 5_Cricetulus      1 M    1 (34)
Confidence            1


No 10 
>P56522_34_MM_ref_sig5_130
Probab=2.83  E-value=6.9  Score=4.53  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (39)
T signal            1 M    1 (39)
T 138_Tuber         1 M    1 (37)
T 98_Oryza          1 M    1 (37)
T 88_Apis           1 M    1 (37)
T 81_Amphimedon     1 M    1 (37)
T 79_Trichoplax     1 M    1 (37)
T 61_Meleagris      1 M    1 (37)
Confidence            0


No 11 
>P29117_29_MM_ref_sig5_130
Probab=2.79  E-value=7.1  Score=5.47  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 82_Bombus         1 P    1 (33)
T 80_Ceratitis      1 M    1 (33)
T 77_Pediculus      1 M    1 (33)
T 74_Columba        1 M    1 (33)
T 65_Meleagris      1 M    1 (33)
T 58_Pundamilia     1 M    1 (33)
T 57_Branchiosto    1 M    1 (33)
T 56_Canis          1 M    1 (33)
T 37_Vicugna        1 M    1 (33)
Confidence            1


No 12 
>P35218_38_Mito_ref_sig5_130
Probab=2.77  E-value=7.1  Score=5.91  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         1 M    1 (43)
T signal            1 M    1 (43)
T 35_Oryctolagus    1 M    1 (47)
T 26_Rattus         1 M    1 (47)
T 40_Octodon        1 M    1 (47)
T 30_Jaculus        1 M    1 (47)
T 42_Sarcophilus    1 M    1 (47)
T 2_Pan             1 -    0 (41)
T 39_Echinops       1 -    0 (41)
T 6_Pongo           1 -    0 (27)
Confidence            2


No 13 
>Q9BYV1_41_Mito_ref_sig5_130
Probab=2.64  E-value=7.5  Score=4.89  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (46)
T signal            1 M    1 (46)
T 122_Ixodes        1 M    1 (43)
T 118_Zea           1 M    1 (43)
T 94_Bombus         1 M    1 (43)
T 91_Nasonia        1 M    1 (43)
T 78_Musca          1 M    1 (43)
T 74_Strongyloce    1 M    1 (43)
T 64_Meleagris      1 M    1 (43)
T 58_Ficedula       1 M    1 (43)
Confidence            0


No 14 
>P42026_37_Mito_ref_sig5_130
Probab=2.63  E-value=7.6  Score=8.78  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         2 a    2 (42)
T signal            2 A    2 (42)
T 163               2 A    2 (43)
T 166               2 A    2 (43)
T 160               2 A    2 (43)
T 150               1 -    0 (39)
T 144               2 A    2 (37)
T 133               1 -    0 (33)
T 136               1 -    0 (42)
T 138               1 -    0 (30)
T 143               1 -    0 (29)
Confidence            2


No 15 
>P14604_29_MM_ref_sig5_130
Probab=2.41  E-value=8.5  Score=8.36  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         2 A    2 (34)
T signal            2 A    2 (34)
T 122               2 A    2 (34)
T 133               2 A    2 (34)
T 142               2 A    2 (34)
T 145               2 A    2 (34)
T 146               2 A    2 (34)
T 152               2 A    2 (31)
T 139               2 A    2 (34)
T 129               2 A    2 (34)
T 120               2 A    2 (34)
Confidence            3


No 16 
>P30084_27_MM_ref_sig5_130
Probab=2.38  E-value=8.6  Score=8.25  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         2 A    2 (32)
T signal            2 A    2 (32)
T 128               2 A    2 (32)
T 131               2 A    2 (32)
T 140               2 A    2 (32)
T 136               2 A    2 (31)
T 135               2 A    2 (29)
T 120               2 A    2 (32)
Confidence            3


No 17 
>P34899_31_Mito_ref_sig5_130
Probab=2.37  E-value=8.6  Score=8.52  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         3 M    3 (6)
Q Consensus         3 m    3 (6)
                      |
T Consensus         3 M    3 (36)
T signal            3 M    3 (36)
T 145               3 M    3 (36)
T 135               3 M    3 (35)
T 141               3 M    3 (36)
T 140               3 M    3 (35)
T 134               1 M    1 (34)
T 138               1 M    1 (35)
T 139               1 M    1 (33)
T 143               1 M    1 (34)
T 144               1 M    1 (34)
Confidence            3


No 18 
>P21953_50_MM_ref_sig5_130
Probab=2.35  E-value=8.7  Score=4.79  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (55)
T signal            1 M    1 (55)
T 145_Nectria       1 M    1 (53)
T 134_Cyanidiosc    1 M    1 (53)
T 123_Trichinell    1 K    1 (53)
T 111_Phytophtho    1 M    1 (53)
T 110_Toxoplasma    1 M    1 (53)
T 105_Glycine       1 M    1 (53)
T 104_Plasmodium    1 M    1 (53)
T 78_Loa            1 M    1 (53)
T 76_Trichoplax     1 G    1 (53)
Confidence            0


No 19 
>P45954_33_MM_ref_sig5_130
Probab=2.34  E-value=8.7  Score=4.53  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 137_Thielavia     1 M    1 (36)
T 91_Hydra          1 M    1 (36)
T 90_Nasonia        1 M    1 (36)
T 77_Megachile      1 M    1 (36)
T 70_Trichechus     1 M    1 (36)
T 69_Geospiza       1 M    1 (36)
Confidence            0


No 20 
>Q9YHT1_44_IM_ref_sig5_130
Probab=2.32  E-value=8.8  Score=8.77  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.7

Q 5_Mustela         3 MA    4 (6)
Q Consensus         3 ma    4 (6)
                      ||
T Consensus         1 Ma    2 (49)
T signal            1 MA    2 (49)
T 127               1 MS    2 (43)
T 141               1 MW    2 (47)
T 160               1 MS    2 (49)
T 126               1 MA    2 (47)
T 129               1 MA    2 (45)
T 133               1 MA    2 (47)
T 140               1 MA    2 (51)
T 163               1 --    0 (48)
T 176               1 MA    2 (44)
Confidence            33


Done!
