Query         5_Mustela
Match_columns 7
No_of_seqs    10 out of 20
Neff          1.7 
Searched_HMMs 436
Date          Wed Sep  6 16:07:56 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_7A14_7Q14_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P30084_27_MM_ref_sig5_130       43.8     0.1 0.00024   13.6   0.0    6    1-6      11-16  (32)
  2 P14604_29_MM_ref_sig5_130       41.4    0.12 0.00027   13.5   0.0    6    1-6      11-16  (34)
  3 P28042_16_Mito_ref_sig5_130     34.2    0.18 0.00042   12.0   0.0    7    1-7       2-8   (21)
  4 P10818_26_IM_ref_sig5_130        4.4     4.1  0.0093    9.4   0.0    4    2-5      23-26  (31)
  5 Q9FLX7_11_IM_ref_sig5_130        3.9     4.8   0.011    8.1   0.0    3    3-5       8-10  (16)
  6 P04182_35_MM_ref_sig5_130        3.4     5.5   0.013    9.3   0.0    4    2-5      35-38  (40)
  7 P35171_23_IM_ref_sig5_130        3.1     6.2   0.014    8.6   0.0    4    2-5      21-24  (28)
  8 P30048_62_Mito_ref_sig5_130      2.9     6.6   0.015    7.1   0.0    1    1-1       1-1   (67)
  9 P13621_23_Mito_IM_ref_sig5_130   2.7     7.2   0.017    8.5   0.0    3    3-5      22-24  (28)
 10 P00430_16_IM_ref_sig5_130        2.5     7.9   0.018    8.0   0.0    2    2-3      15-16  (21)
 11 P34899_31_Mito_ref_sig5_130      2.5       8   0.018    8.8   0.0    1    4-4      17-17  (36)
 12 P26440_29_MM_ref_sig5_130        2.5     8.1   0.019    8.7   0.0    4    2-5      18-21  (34)
 13 P23786_25_IM_ref_sig5_130        2.5     8.2   0.019    8.5   0.0    3    3-5      23-25  (30)
 14 P25285_22_Mito_ref_sig5_130      2.4     8.3   0.019    8.3   0.0    4    2-5      17-20  (27)
 15 Q01205_68_Mito_ref_sig5_130      2.1     9.9   0.023    9.6   0.0    4    2-5      28-31  (73)
 16 Q02337_46_MM_ref_sig5_130        2.1      10   0.023    6.7   0.0    1    1-1       1-1   (51)
 17 P0CS90_23_MM_Nu_ref_sig5_130     2.1      10   0.023    8.0   0.0    2    4-5      16-17  (28)
 18 P18886_25_IM_ref_sig5_130        2.0      11   0.025    8.2   0.0    3    3-5      23-25  (30)
 19 P13184_23_IM_ref_sig5_130        2.0      11   0.025    8.0   0.0    3    3-5      22-24  (28)
 20 P07926_68_MitoM_ref_sig5_130     1.9      11   0.025    9.5   0.0    3    3-5      23-25  (73)

No 1  
>P30084_27_MM_ref_sig5_130
Probab=43.79  E-value=0.1  Score=13.57  Aligned_cols=6  Identities=50%  Similarity=0.960  Sum_probs=3.4

Q 5_Mustela         1 MRRPLE    6 (7)
Q 8_Pteropus        1 LRRPLE    6 (7)
Q 2_Macaca          1 VRRPLR    6 (7)
Q 12_Sorex          1 ARRPLQ    6 (7)
Q 15_Ictidomys      1 VRRPLQ    6 (7)
Q 11_Cavia          1 VQRPLR    6 (7)
Q 17_Tupaia         1 VRRPFQ    6 (7)
Q 10_Panthera       1 VRRPLE    6 (7)
Q 19_Trichechus     1 VRRPLR    6 (7)
Q 16_Equus          1 VRRSLE    6 (7)
Q Consensus         1 vrRpl~    6 (7)
                      ||+||+
T Consensus        11 vr~~Lr   16 (32)
T signal           11 VRGPLR   16 (32)
T 128              11 LRRPLP   16 (32)
T 131              11 VRAPLR   16 (32)
T 140              11 VRGLLR   16 (32)
T 136              11 VRRLLR   16 (31)
T 135              11 LCR---   13 (29)
T 120              11 GSALLR   16 (32)
Confidence            466664


No 2  
>P14604_29_MM_ref_sig5_130
Probab=41.38  E-value=0.12  Score=13.55  Aligned_cols=6  Identities=17%  Similarity=-0.053  Sum_probs=3.3

Q 5_Mustela         1 MRRPLE    6 (7)
Q 8_Pteropus        1 LRRPLE    6 (7)
Q 2_Macaca          1 VRRPLR    6 (7)
Q 12_Sorex          1 ARRPLQ    6 (7)
Q 15_Ictidomys      1 VRRPLQ    6 (7)
Q 11_Cavia          1 VQRPLR    6 (7)
Q 17_Tupaia         1 VRRPFQ    6 (7)
Q 10_Panthera       1 VRRPLE    6 (7)
Q 19_Trichechus     1 VRRPLR    6 (7)
Q 16_Equus          1 VRRSLE    6 (7)
Q Consensus         1 vrRpl~    6 (7)
                      ||+||+
T Consensus        11 vr~~Lr   16 (34)
T signal           11 ACNSLL   16 (34)
T 122              11 LRRPLP   16 (34)
T 133              11 VRGPLR   16 (34)
T 142              11 VRAPLR   16 (34)
T 145              11 VRCPPR   16 (34)
T 146              11 LRIQLR   16 (34)
T 152              11 LCR---   13 (31)
T 139              11 VCGPLR   16 (34)
T 129              11 VRRLLR   16 (34)
T 120              11 GSALLR   16 (34)
Confidence            466654


No 3  
>P28042_16_Mito_ref_sig5_130
Probab=34.23  E-value=0.18  Score=11.97  Aligned_cols=7  Identities=57%  Similarity=0.966  Sum_probs=4.1

Q 5_Mustela         1 MRRPLEQ    7 (7)
Q 8_Pteropus        1 LRRPLEQ    7 (7)
Q 2_Macaca          1 VRRPLRE    7 (7)
Q 12_Sorex          1 ARRPLQQ    7 (7)
Q 15_Ictidomys      1 VRRPLQQ    7 (7)
Q 11_Cavia          1 VQRPLRQ    7 (7)
Q 17_Tupaia         1 VRRPFQQ    7 (7)
Q 10_Panthera       1 VRRPLEQ    7 (7)
Q 19_Trichechus     1 VRRPLRQ    7 (7)
Q 16_Equus          1 VRRSLEQ    7 (7)
Q Consensus         1 vrRpl~q    7 (7)
                      .|||.-|
T Consensus         2 ~Rrpv~Q    8 (21)
T signal            2 FRRPVLQ    8 (21)
T 10                2 WRRPAWQ    8 (22)
T 28                2 LRYASAQ    8 (20)
T 31                2 LRRRVAQ    8 (22)
T 49                2 FRSASAQ    8 (20)
T 62                2 FQRPVLQ    8 (21)
T 64                2 FRRPIIQ    8 (21)
T 67                2 LRKPVFQ    8 (21)
T 81                2 LRRPIIQ    8 (21)
T 11                2 FXRTVLK    8 (19)
Confidence            3777643


No 4  
>P10818_26_IM_ref_sig5_130
Probab=4.37  E-value=4.1  Score=9.35  Aligned_cols=4  Identities=50%  Similarity=1.174  Sum_probs=2.1

Q 5_Mustela         2 RRPL    5 (7)
Q 8_Pteropus        2 RRPL    5 (7)
Q 2_Macaca          2 RRPL    5 (7)
Q 12_Sorex          2 RRPL    5 (7)
Q 15_Ictidomys      2 RRPL    5 (7)
Q 11_Cavia          2 QRPL    5 (7)
Q 17_Tupaia         2 RRPF    5 (7)
Q 10_Panthera       2 RRPL    5 (7)
Q 19_Trichechus     2 RRPL    5 (7)
Q 16_Equus          2 RRSL    5 (7)
Q Consensus         2 rRpl    5 (7)
                      .||.
T Consensus        23 gRpM   26 (31)
T signal           23 GRPM   26 (31)
T 53               20 RRPM   23 (28)
T 56               20 GRPM   23 (28)
T 64               20 TRPM   23 (28)
T 36               22 RRPM   25 (30)
T 44               20 GRLM   23 (28)
T 55               20 GRPM   23 (28)
T 6                22 GRPM   25 (30)
T 7                21 GRSM   24 (29)
T 43               18 GRLF   21 (25)
Confidence            4554


No 5  
>Q9FLX7_11_IM_ref_sig5_130
Probab=3.86  E-value=4.8  Score=8.08  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.8

Q 5_Mustela         3 RPL    5 (7)
Q 8_Pteropus        3 RPL    5 (7)
Q 2_Macaca          3 RPL    5 (7)
Q 12_Sorex          3 RPL    5 (7)
Q 15_Ictidomys      3 RPL    5 (7)
Q 11_Cavia          3 RPL    5 (7)
Q 17_Tupaia         3 RPF    5 (7)
Q 10_Panthera       3 RPL    5 (7)
Q 19_Trichechus     3 RPL    5 (7)
Q 16_Equus          3 RSL    5 (7)
Q Consensus         3 Rpl    5 (7)
                      |||
T Consensus         8 ~pl   10 (16)
T signal            8 RPL   10 (16)
T 12                8 RPL   10 (16)
T 11                8 GPL   10 (16)
T 9                 8 RPL   10 (16)
T 17               11 GGL   13 (24)
Confidence            565


No 6  
>P04182_35_MM_ref_sig5_130
Probab=3.44  E-value=5.5  Score=9.27  Aligned_cols=4  Identities=25%  Similarity=0.484  Sum_probs=1.7

Q 5_Mustela         2 RRPL    5 (7)
Q 8_Pteropus        2 RRPL    5 (7)
Q 2_Macaca          2 RRPL    5 (7)
Q 12_Sorex          2 RRPL    5 (7)
Q 15_Ictidomys      2 RRPL    5 (7)
Q 11_Cavia          2 QRPL    5 (7)
Q 17_Tupaia         2 RRPF    5 (7)
Q 10_Panthera       2 RRPL    5 (7)
Q 19_Trichechus     2 RRPL    5 (7)
Q 16_Equus          2 RRSL    5 (7)
Q Consensus         2 rRpl    5 (7)
                      ++|+
T Consensus        35 q~p~   38 (40)
T signal           35 QGPP   38 (40)
T 138              35 EEAL   38 (40)
T 140              35 RGPL   38 (40)
T 134              37 EKPL   40 (42)
T 141              35 QRPL   38 (40)
T 153              35 QGPP   38 (40)
T 139              35 QGPL   38 (40)
T 127              34 ERLL   37 (39)
T 128              37 ERPV   40 (42)
T 132              36 EHPL   39 (41)
Confidence            3444


No 7  
>P35171_23_IM_ref_sig5_130
Probab=3.10  E-value=6.2  Score=8.61  Aligned_cols=4  Identities=50%  Similarity=0.858  Sum_probs=1.8

Q 5_Mustela         2 RRPL    5 (7)
Q 8_Pteropus        2 RRPL    5 (7)
Q 2_Macaca          2 RRPL    5 (7)
Q 12_Sorex          2 RRPL    5 (7)
Q 15_Ictidomys      2 RRPL    5 (7)
Q 11_Cavia          2 QRPL    5 (7)
Q 17_Tupaia         2 RRPF    5 (7)
Q 10_Panthera       2 RRPL    5 (7)
Q 19_Trichechus     2 RRPL    5 (7)
Q 16_Equus          2 RRSL    5 (7)
Q Consensus         2 rRpl    5 (7)
                      ||.+
T Consensus        21 rRq~   24 (28)
T signal           21 RRHF   24 (28)
T 73               21 RRQL   24 (28)
T 29               21 RRPL   24 (28)
T 18               21 CRQF   24 (28)
T 21               21 HSYF   24 (28)
T 20               21 RRQV   24 (28)
T 3                21 HRQL   24 (28)
T 22               20 RTQV   23 (27)
T 71               17 ARQL   20 (24)
Confidence            4444


No 8  
>P30048_62_Mito_ref_sig5_130
Probab=2.95  E-value=6.6  Score=7.05  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.0

Q 5_Mustela         1 M    1 (7)
Q 8_Pteropus        1 L    1 (7)
Q 2_Macaca          1 V    1 (7)
Q 12_Sorex          1 A    1 (7)
Q 15_Ictidomys      1 V    1 (7)
Q 11_Cavia          1 V    1 (7)
Q 17_Tupaia         1 V    1 (7)
Q 10_Panthera       1 V    1 (7)
Q 19_Trichechus     1 V    1 (7)
Q 16_Equus          1 V    1 (7)
Q Consensus         1 v    1 (7)
                      |
T Consensus         1 M    1 (67)
T signal            1 M    1 (67)
T 79_Drosophila     1 M    1 (67)
T 76_Meleagris      1 M    1 (67)
T 70_Strongyloce    1 M    1 (67)
T 69_Metaseiulus    1 M    1 (67)
T 63_Columba        1 Q    1 (67)
T 57_Falco          1 P    1 (67)
T 44_Camelus        1 M    1 (67)
Confidence            0


No 9  
>P13621_23_Mito_IM_ref_sig5_130
Probab=2.75  E-value=7.2  Score=8.52  Aligned_cols=3  Identities=67%  Similarity=1.586  Sum_probs=1.7

Q 5_Mustela         3 RPL    5 (7)
Q 8_Pteropus        3 RPL    5 (7)
Q 2_Macaca          3 RPL    5 (7)
Q 12_Sorex          3 RPL    5 (7)
Q 15_Ictidomys      3 RPL    5 (7)
Q 11_Cavia          3 RPL    5 (7)
Q 17_Tupaia         3 RPF    5 (7)
Q 10_Panthera       3 RPL    5 (7)
Q 19_Trichechus     3 RPL    5 (7)
Q 16_Equus          3 RSL    5 (7)
Q Consensus         3 Rpl    5 (7)
                      ||+
T Consensus        22 RP~   24 (28)
T signal           22 RPF   24 (28)
T 148              22 RPF   24 (28)
T 131              18 RPA   20 (24)
T 123              18 RPV   20 (24)
T 137              15 RPI   17 (21)
T 130              18 RPA   20 (24)
T 144              18 RPA   20 (24)
T 134              18 RPV   20 (24)
Confidence            564


No 10 
>P00430_16_IM_ref_sig5_130
Probab=2.54  E-value=7.9  Score=7.95  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.9

Q 5_Mustela         2 RR    3 (7)
Q 8_Pteropus        2 RR    3 (7)
Q 2_Macaca          2 RR    3 (7)
Q 12_Sorex          2 RR    3 (7)
Q 15_Ictidomys      2 RR    3 (7)
Q 11_Cavia          2 QR    3 (7)
Q 17_Tupaia         2 RR    3 (7)
Q 10_Panthera       2 RR    3 (7)
Q 19_Trichechus     2 RR    3 (7)
Q 16_Equus          2 RR    3 (7)
Q Consensus         2 rR    3 (7)
                      ||
T Consensus        15 rR   16 (21)
T signal           15 RR   16 (21)
T 13               15 CR   16 (21)
T 14               15 RR   16 (21)
T 15               15 RR   16 (21)
T 16               15 RR   16 (21)
T 24               15 RR   16 (21)
T 25               15 RS   16 (21)
T 35               15 RR   16 (21)
T 44               15 RR   16 (21)
T 6                12 RR   13 (16)
Confidence            44


No 11 
>P34899_31_Mito_ref_sig5_130
Probab=2.51  E-value=8  Score=8.82  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         4 P.    4 (7)
Q 8_Pteropus        4 P.    4 (7)
Q 2_Macaca          4 P.    4 (7)
Q 12_Sorex          4 P.    4 (7)
Q 15_Ictidomys      4 P.    4 (7)
Q 11_Cavia          4 P.    4 (7)
Q 17_Tupaia         4 P.    4 (7)
Q 10_Panthera       4 P.    4 (7)
Q 19_Trichechus     4 P.    4 (7)
Q 16_Equus          4 S.    4 (7)
Q Consensus         4 p.    4 (7)
                      | 
T Consensus        17 p.   17 (36)
T signal           17 S.   17 (36)
T 145              17 P.   17 (36)
T 135              17 G.   17 (35)
T 141              17 P.   17 (36)
T 140              17 P.   17 (35)
T 134              12 Pl   13 (34)
T 138              15 P.   15 (35)
T 139              15 P.   15 (33)
T 143              15 P.   15 (34)
T 144              15 P.   15 (34)
Confidence            3 


No 12 
>P26440_29_MM_ref_sig5_130
Probab=2.50  E-value=8.1  Score=8.72  Aligned_cols=4  Identities=75%  Similarity=1.281  Sum_probs=1.7

Q 5_Mustela         2 R....RPL    5 (7)
Q 8_Pteropus        2 R....RPL    5 (7)
Q 2_Macaca          2 R....RPL    5 (7)
Q 12_Sorex          2 R....RPL    5 (7)
Q 15_Ictidomys      2 R....RPL    5 (7)
Q 11_Cavia          2 Q....RPL    5 (7)
Q 17_Tupaia         2 R....RPF    5 (7)
Q 10_Panthera       2 R....RPL    5 (7)
Q 19_Trichechus     2 R....RPL    5 (7)
Q 16_Equus          2 R....RSL    5 (7)
Q Consensus         2 r....Rpl    5 (7)
                      |    .||
T Consensus        18 r....~pl   21 (34)
T signal           18 R....PPL   21 (34)
T 107              18 Rppl.QPL   24 (37)
T 130              18 Rpql.QAF   24 (37)
T 132              14 QlpllRPP   21 (34)
T 136              18 Rplv.APL   24 (37)
T 138              18 R....PPL   21 (34)
T 150              18 Rapr.ARL   24 (37)
T 140              18 Qppl.LPL   24 (37)
T 144              18 Q....APL   21 (34)
T 139              18 Rpll.LPL   24 (37)
Confidence            3    444


No 13 
>P23786_25_IM_ref_sig5_130
Probab=2.46  E-value=8.2  Score=8.51  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.8

Q 5_Mustela         3 RPL    5 (7)
Q 8_Pteropus        3 RPL    5 (7)
Q 2_Macaca          3 RPL    5 (7)
Q 12_Sorex          3 RPL    5 (7)
Q 15_Ictidomys      3 RPL    5 (7)
Q 11_Cavia          3 RPL    5 (7)
Q 17_Tupaia         3 RPF    5 (7)
Q 10_Panthera       3 RPL    5 (7)
Q 19_Trichechus     3 RPL    5 (7)
Q 16_Equus          3 RSL    5 (7)
Q Consensus         3 Rpl    5 (7)
                      |||
T Consensus        23 RpL   25 (30)
T signal           23 RPL   25 (30)
T 101              23 RPL   25 (30)
T 104              23 RPL   25 (30)
T 108              23 RPL   25 (30)
T 109              24 RSL   26 (31)
T 117              23 RSL   25 (30)
T 119              23 RPL   25 (30)
T 59               28 RSL   30 (35)
Confidence            565


No 14 
>P25285_22_Mito_ref_sig5_130
Probab=2.43  E-value=8.3  Score=8.26  Aligned_cols=4  Identities=50%  Similarity=0.576  Sum_probs=2.1

Q 5_Mustela         2 RRPL    5 (7)
Q 8_Pteropus        2 RRPL    5 (7)
Q 2_Macaca          2 RRPL    5 (7)
Q 12_Sorex          2 RRPL    5 (7)
Q 15_Ictidomys      2 RRPL    5 (7)
Q 11_Cavia          2 QRPL    5 (7)
Q 17_Tupaia         2 RRPF    5 (7)
Q 10_Panthera       2 RRPL    5 (7)
Q 19_Trichechus     2 RRPL    5 (7)
Q 16_Equus          2 RRSL    5 (7)
Q Consensus         2 rRpl    5 (7)
                      .|||
T Consensus        17 ~R~l   20 (27)
T signal           17 CRSL   20 (27)
T 107              17 SRSL   20 (27)
T 127              17 GRSF   20 (27)
T 128              17 GRGL   20 (27)
T 134              17 SRPL   20 (27)
T 138              17 SRSL   20 (27)
T 145              17 TRPF   20 (27)
T 146              17 SRLY   20 (27)
T 147              17 CRPL   20 (27)
T 152              17 GRQL   20 (27)
Confidence            3555


No 15 
>Q01205_68_Mito_ref_sig5_130
Probab=2.11  E-value=9.9  Score=9.61  Aligned_cols=4  Identities=75%  Similarity=1.149  Sum_probs=1.6

Q 5_Mustela         2 RRPL    5 (7)
Q 8_Pteropus        2 RRPL    5 (7)
Q 2_Macaca          2 RRPL    5 (7)
Q 12_Sorex          2 RRPL    5 (7)
Q 15_Ictidomys      2 RRPL    5 (7)
Q 11_Cavia          2 QRPL    5 (7)
Q 17_Tupaia         2 RRPF    5 (7)
Q 10_Panthera       2 RRPL    5 (7)
Q 19_Trichechus     2 RRPL    5 (7)
Q 16_Equus          2 RRSL    5 (7)
Q Consensus         2 rRpl    5 (7)
                      ||+|
T Consensus        28 Rrsl   31 (73)
T signal           28 RRSL   31 (73)
T 126              27 RRSL   30 (72)
T 144              29 KRSL   32 (74)
T 148              17 RRSL   20 (62)
T 128              28 RRAP   31 (72)
T 129              28 RRAT   31 (72)
T 131              28 RRSS   31 (72)
T 142              24 RCAL   27 (70)
T 141              24 SRPL   27 (69)
Confidence            3443


No 16 
>Q02337_46_MM_ref_sig5_130
Probab=2.07  E-value=10  Score=6.74  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.0

Q 5_Mustela         1 M    1 (7)
Q 8_Pteropus        1 L    1 (7)
Q 2_Macaca          1 V    1 (7)
Q 12_Sorex          1 A    1 (7)
Q 15_Ictidomys      1 V    1 (7)
Q 11_Cavia          1 V    1 (7)
Q 17_Tupaia         1 V    1 (7)
Q 10_Panthera       1 V    1 (7)
Q 19_Trichechus     1 V    1 (7)
Q 16_Equus          1 V    1 (7)
Q Consensus         1 v    1 (7)
                      +
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 76_Pediculus      1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 69_Papio          1 V    1 (50)
T 67_Maylandia      1 M    1 (50)
T 60_Sus            1 M    1 (50)
T 72_Ciona          1 M    1 (50)
T 59_Anolis         1 I    1 (50)
T 51_Canis          1 M    1 (50)
T 38_Nomascus       1 M    1 (50)
Confidence            0


No 17 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=2.07  E-value=10  Score=7.97  Aligned_cols=2  Identities=0%  Similarity=0.401  Sum_probs=0.8

Q 5_Mustela         4 PL    5 (7)
Q 8_Pteropus        4 PL    5 (7)
Q 2_Macaca          4 PL    5 (7)
Q 12_Sorex          4 PL    5 (7)
Q 15_Ictidomys      4 PL    5 (7)
Q 11_Cavia          4 PL    5 (7)
Q 17_Tupaia         4 PF    5 (7)
Q 10_Panthera       4 PL    5 (7)
Q 19_Trichechus     4 PL    5 (7)
Q 16_Equus          4 SL    5 (7)
Q Consensus         4 pl    5 (7)
                      |+
T Consensus        16 ~~   17 (28)
T signal           16 SF   17 (28)
T 55               17 AL   18 (29)
T 65               17 VV   18 (29)
T 54               17 -P   17 (28)
T 57               14 PI   15 (26)
T 59               14 PL   15 (26)
T 62               13 -N   13 (24)
T 56               12 PL   13 (24)
T 60               11 NV   12 (23)
Confidence            44


No 18 
>P18886_25_IM_ref_sig5_130
Probab=1.97  E-value=11  Score=8.20  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.7

Q 5_Mustela         3 RPL    5 (7)
Q 8_Pteropus        3 RPL    5 (7)
Q 2_Macaca          3 RPL    5 (7)
Q 12_Sorex          3 RPL    5 (7)
Q 15_Ictidomys      3 RPL    5 (7)
Q 11_Cavia          3 RPL    5 (7)
Q 17_Tupaia         3 RPF    5 (7)
Q 10_Panthera       3 RPL    5 (7)
Q 19_Trichechus     3 RPL    5 (7)
Q 16_Equus          3 RSL    5 (7)
Q Consensus         3 Rpl    5 (7)
                      |||
T Consensus        23 R~L   25 (30)
T signal           23 RPL   25 (30)
T 84               23 RTL   25 (30)
T 87               23 RPL   25 (30)
T 102              24 RSL   26 (31)
T 114              23 RPL   25 (30)
T 92               23 RLL   25 (30)
T 94               23 RSL   25 (30)
T 48               28 RSL   30 (35)
Confidence            555


No 19 
>P13184_23_IM_ref_sig5_130
Probab=1.96  E-value=11  Score=8.02  Aligned_cols=3  Identities=33%  Similarity=0.722  Sum_probs=1.1

Q 5_Mustela         3 RPL    5 (7)
Q 8_Pteropus        3 RPL    5 (7)
Q 2_Macaca          3 RPL    5 (7)
Q 12_Sorex          3 RPL    5 (7)
Q 15_Ictidomys      3 RPL    5 (7)
Q 11_Cavia          3 RPL    5 (7)
Q 17_Tupaia         3 RPF    5 (7)
Q 10_Panthera       3 RPL    5 (7)
Q 19_Trichechus     3 RPL    5 (7)
Q 16_Equus          3 RSL    5 (7)
Q Consensus         3 Rpl    5 (7)
                      |.+
T Consensus        22 Rq~   24 (28)
T signal           22 RQF   24 (28)
T 63               22 RQL   24 (28)
T 28               22 RHF   24 (28)
T 15               22 RQL   24 (28)
T 25               22 RQF   24 (28)
T 16               22 RQL   24 (28)
T 19               22 RQV   24 (28)
T 12               22 RQF   24 (28)
T 14               22 SYF   24 (28)
Confidence            333


No 20 
>P07926_68_MitoM_ref_sig5_130
Probab=1.95  E-value=11  Score=9.49  Aligned_cols=3  Identities=67%  Similarity=1.010  Sum_probs=1.3

Q 5_Mustela         3 RP.....L    5 (7)
Q 8_Pteropus        3 RP.....L    5 (7)
Q 2_Macaca          3 RP.....L    5 (7)
Q 12_Sorex          3 RP.....L    5 (7)
Q 15_Ictidomys      3 RP.....L    5 (7)
Q 11_Cavia          3 RP.....L    5 (7)
Q 17_Tupaia         3 RP.....F    5 (7)
Q 10_Panthera       3 RP.....L    5 (7)
Q 19_Trichechus     3 RP.....L    5 (7)
Q 16_Equus          3 RS.....L    5 (7)
Q Consensus         3 Rp.....l    5 (7)
                      ||     |
T Consensus        23 RP.....l   25 (73)
T signal           23 RS.....L   25 (73)
T 10               23 RP.....L   25 (82)
T 14               23 RP.....L   25 (55)
T 22               23 RP.....L   25 (37)
T 9                23 RP.....V   25 (67)
T 5                26 RP.....L   28 (74)
T 6                26 RP.....L   28 (68)
T 2                20 RSalrplG   27 (84)
T 3                25 RP.....F   27 (84)
T 53                7 RP.....L    9 (40)
Confidence            44     3


Done!
