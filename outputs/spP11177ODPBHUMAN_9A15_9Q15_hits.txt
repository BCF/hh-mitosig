Query         5_Mustela
Match_columns 6
No_of_seqs    6 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:07:58 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_9A15_9Q15_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P28042_16_Mito_ref_sig5_130     26.1    0.31 0.00072   11.0   0.0    6    1-6       4-9   (21)
  2 P07263_20_Cy_Mito_ref_sig5_130   2.0      11   0.025    7.7   0.0    2    2-3       5-6   (25)
  3 P24118_21_MM_Cy_ref_sig5_130     1.9      11   0.025    7.7   0.0    1    3-3       4-4   (26)
  4 Q91YT0_20_IM_ref_sig5_130        1.9      11   0.025    7.6   0.0    1    2-2       6-6   (25)
  5 P49411_43_Mito_ref_sig5_130      1.8      12   0.028    8.5   0.0    1    2-2      30-30  (48)
  6 P42847_59_Mito_ref_sig5_130      1.7      12   0.028    8.9   0.0    1    2-2      18-18  (64)
  7 P32799_9_IM_ref_sig5_130         1.7      13    0.03    6.5   0.0    3    3-5       2-4   (14)
  8 P10817_9_IM_ref_sig5_130         1.6      13    0.03    6.6   0.0    2    2-3       1-2   (14)
  9 P07926_68_MitoM_ref_sig5_130     1.6      13   0.031    8.9   0.0    1    2-2      24-24  (73)
 10 Q40089_21_Mito_IM_ref_sig5_130   1.6      14   0.032    7.4   0.0    1    5-5       8-8   (26)
 11 P13216_19_MM_ref_sig5_130        1.6      14   0.033    7.3   0.0    1    2-2      11-11  (24)
 12 P36517_14_Mito_ref_sig5_130      1.5      15   0.035    6.9   0.0    3    2-4      12-14  (19)
 13 P36526_16_Mito_ref_sig5_130      1.4      15   0.036    6.8   0.0    2    1-2      19-20  (21)
 14 P12074_24_IM_ref_sig5_130        1.4      16   0.037    7.4   0.0    1    5-5      19-19  (29)
 15 P05626_35_Mito_IM_ref_sig5_130   1.4      17   0.038    7.9   0.0    1    2-2      18-18  (40)
 16 P29147_46_MM_ref_sig5_130        1.3      17   0.038    5.1   0.0    1    1-1       1-1   (51)
 17 P09624_21_MM_ref_sig5_130        1.3      17   0.039    7.1   0.0    1    3-3      14-14  (26)
 18 Q0P5L8_21_Mito_Nu_ref_sig5_130   1.3      17   0.039    4.1   0.0    1    2-2      26-26  (26)
 19 P26267_25_MM_ref_sig5_130        1.3      17   0.039    7.4   0.0    3    2-4      12-14  (30)
 20 P34899_31_Mito_ref_sig5_130      1.3      17   0.039    7.7   0.0    1    2-2      20-20  (36)

No 1  
>P28042_16_Mito_ref_sig5_130
Probab=26.09  E-value=0.31  Score=11.04  Aligned_cols=6  Identities=67%  Similarity=0.966  Sum_probs=3.0

Q 5_Mustela         1 RPLEQV    6 (6)
Q 2_Macaca          1 RPLREV    6 (6)
Q 15_Ictidomys      1 RPLQQV    6 (6)
Q 11_Cavia          1 RPLRQV    6 (6)
Q 17_Tupaia         1 RPFQQV    6 (6)
Q 16_Equus          1 RSLEQV    6 (6)
Q Consensus         1 Rpl~qV    6 (6)
                      ||.-||
T Consensus         4 rpv~Qv    9 (21)
T signal            4 RPVLQV    9 (21)
T 10                4 RPAWQV    9 (22)
T 28                4 YASAQI    9 (20)
T 31                4 RRVAQV    9 (22)
T 49                4 SASAQI    9 (20)
T 62                4 RPVLQV    9 (21)
T 64                4 RPIIQV    9 (21)
T 67                4 KPVFQV    9 (21)
T 81                4 RPIIQV    9 (21)
T 11                4 RTVLKM    9 (19)
Confidence            455554


No 2  
>P07263_20_Cy_Mito_ref_sig5_130
Probab=1.97  E-value=11  Score=7.69  Aligned_cols=2  Identities=50%  Similarity=0.733  Sum_probs=0.8

Q 5_Mustela         2 PL    3 (6)
Q 2_Macaca          2 PL    3 (6)
Q 15_Ictidomys      2 PL    3 (6)
Q 11_Cavia          2 PL    3 (6)
Q 17_Tupaia         2 PF    3 (6)
Q 16_Equus          2 SL    3 (6)
Q Consensus         2 pl    3 (6)
                      +|
T Consensus         5 sl    6 (25)
T signal            5 SL    6 (25)
Confidence            33


No 3  
>P24118_21_MM_Cy_ref_sig5_130
Probab=1.93  E-value=11  Score=7.72  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 2_Macaca          3 L    3 (6)
Q 15_Ictidomys      3 L    3 (6)
Q 11_Cavia          3 L    3 (6)
Q 17_Tupaia         3 F    3 (6)
Q 16_Equus          3 L    3 (6)
Q Consensus         3 l    3 (6)
                      +
T Consensus         4 i    4 (26)
T signal            4 I    4 (26)
Confidence            2


No 4  
>Q91YT0_20_IM_ref_sig5_130
Probab=1.93  E-value=11  Score=7.64  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus         6 ~    6 (25)
T signal            6 H    6 (25)
T 132               6 P    6 (29)
T 93                6 R    6 (25)
Confidence            3


No 5  
>P49411_43_Mito_ref_sig5_130
Probab=1.76  E-value=12  Score=8.51  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        30 p   30 (48)
T signal           30 L   30 (48)
T 163              30 P   30 (48)
T 166              25 P   25 (43)
T 167              30 P   30 (48)
T 170              27 P   27 (45)
T 171              30 P   30 (48)
T 177              30 L   30 (48)
T 157              44 P   44 (62)
Confidence            3


No 6  
>P42847_59_Mito_ref_sig5_130
Probab=1.74  E-value=12  Score=8.91  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        18 p   18 (64)
T signal           18 P   18 (64)
Confidence            3


No 7  
>P32799_9_IM_ref_sig5_130
Probab=1.65  E-value=13  Score=6.49  Aligned_cols=3  Identities=33%  Similarity=0.567  Sum_probs=1.5

Q 5_Mustela         3 LEQ    5 (6)
Q 2_Macaca          3 LRE    5 (6)
Q 15_Ictidomys      3 LQQ    5 (6)
Q 11_Cavia          3 LRQ    5 (6)
Q 17_Tupaia         3 FQQ    5 (6)
Q 16_Equus          3 LEQ    5 (6)
Q Consensus         3 l~q    5 (6)
                      |+|
T Consensus         2 ~rq    4 (14)
T signal            2 FRQ    4 (14)
T 25                2 LAR    4 (12)
T 27                2 LRT    4 (12)
T 33                2 FRQ    4 (14)
T 34                2 FRQ    4 (13)
T 35                2 YRQ    4 (13)
T 40                2 FKQ    4 (13)
T 24                2 LRQ    4 (13)
T 43                2 LRQ    4 (13)
Confidence            455


No 8  
>P10817_9_IM_ref_sig5_130
Probab=1.64  E-value=13  Score=6.55  Aligned_cols=2  Identities=100%  Similarity=1.929  Sum_probs=0.8

Q 5_Mustela         2 PL    3 (6)
Q 2_Macaca          2 PL    3 (6)
Q 15_Ictidomys      2 PL    3 (6)
Q 11_Cavia          2 PL    3 (6)
Q 17_Tupaia         2 PF    3 (6)
Q 16_Equus          2 SL    3 (6)
Q Consensus         2 pl    3 (6)
                      ||
T Consensus         1 pl    2 (14)
T signal            1 PL    2 (14)
T 14                1 PL    2 (14)
T 18                1 RL    2 (14)
T 22                1 PL    2 (14)
T 23                1 AR    2 (14)
T 46                1 PL    2 (14)
T 4                 1 PL    2 (14)
Confidence            34


No 9  
>P07926_68_MitoM_ref_sig5_130
Probab=1.63  E-value=13  Score=8.91  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         2 P.....    2 (6)
Q 2_Macaca          2 P.....    2 (6)
Q 15_Ictidomys      2 P.....    2 (6)
Q 11_Cavia          2 P.....    2 (6)
Q 17_Tupaia         2 P.....    2 (6)
Q 16_Equus          2 S.....    2 (6)
Q Consensus         2 p.....    2 (6)
                      |     
T Consensus        24 P.....   24 (73)
T signal           24 S.....   24 (73)
T 10               24 P.....   24 (82)
T 14               24 P.....   24 (55)
T 22               24 P.....   24 (37)
T 9                24 P.....   24 (67)
T 5                27 P.....   27 (74)
T 6                27 P.....   27 (68)
T 2                21 Salrpl   26 (84)
T 3                26 P.....   26 (84)
T 53                8 P.....    8 (40)
Confidence            2     


No 10 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=1.56  E-value=14  Score=7.37  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.3

Q 5_Mustela         5 Q    5 (6)
Q 2_Macaca          5 E    5 (6)
Q 15_Ictidomys      5 Q    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q 17_Tupaia         5 Q    5 (6)
Q 16_Equus          5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus         8 ~    8 (26)
T signal            8 L    8 (26)
T 44                8 F    8 (26)
T 43                8 L    8 (26)
T 40                9 L    9 (23)
T 41                9 F    9 (23)
T 3                 8 Q    8 (24)
T 5                 8 Q    8 (24)
T 10                8 Q    8 (24)
T 11                8 Q    8 (24)
Confidence            2


No 11 
>P13216_19_MM_ref_sig5_130
Probab=1.55  E-value=14  Score=7.30  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        11 ~   11 (24)
T signal           11 P   11 (24)
T 128               5 P    5 (18)
T 135              10 F   10 (23)
T 143              10 P   10 (23)
T 127               5 R    5 (18)
T 105               4 S    4 (17)
T 130               4 T    4 (17)
Confidence            3


No 12 
>P36517_14_Mito_ref_sig5_130
Probab=1.47  E-value=15  Score=6.90  Aligned_cols=3  Identities=67%  Similarity=1.331  Sum_probs=1.5

Q 5_Mustela         2 PLE    4 (6)
Q 2_Macaca          2 PLR    4 (6)
Q 15_Ictidomys      2 PLQ    4 (6)
Q 11_Cavia          2 PLR    4 (6)
Q 17_Tupaia         2 PFQ    4 (6)
Q 16_Equus          2 SLE    4 (6)
Q Consensus         2 pl~    4 (6)
                      ||+
T Consensus        12 plr   14 (19)
T signal           12 PLR   14 (19)
Confidence            554


No 13 
>P36526_16_Mito_ref_sig5_130
Probab=1.44  E-value=15  Score=6.85  Aligned_cols=2  Identities=100%  Similarity=2.045  Sum_probs=1.1

Q 5_Mustela         1 RP    2 (6)
Q 2_Macaca          1 RP    2 (6)
Q 15_Ictidomys      1 RP    2 (6)
Q 11_Cavia          1 RP    2 (6)
Q 17_Tupaia         1 RP    2 (6)
Q 16_Equus          1 RS    2 (6)
Q Consensus         1 Rp    2 (6)
                      ||
T Consensus        19 RP   20 (21)
T signal           19 RP   20 (21)
T 11               19 RP   20 (21)
T 13               19 RP   20 (21)
T 14               19 RP   20 (21)
T 18               18 RP   19 (20)
T 20               19 RP   20 (21)
T 5                19 RP   20 (21)
T 7                19 RP   20 (21)
T 8                19 RP   20 (21)
T 9                15 RP   16 (17)
Confidence            55


No 14 
>P12074_24_IM_ref_sig5_130
Probab=1.39  E-value=16  Score=7.42  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.2

Q 5_Mustela         5 Q    5 (6)
Q 2_Macaca          5 E    5 (6)
Q 15_Ictidomys      5 Q    5 (6)
Q 11_Cavia          5 Q    5 (6)
Q 17_Tupaia         5 Q    5 (6)
Q 16_Equus          5 Q    5 (6)
Q Consensus         5 q    5 (6)
                      |
T Consensus        19 q   19 (29)
T signal           19 Q   19 (29)
T 32               19 G   19 (29)
T 37               21 Q   21 (31)
T 43               19 P   19 (29)
T 54               19 P   19 (29)
T 44               19 R   19 (29)
T 51               19 Q   19 (29)
T 59               19 R   19 (29)
T 60               19 Q   19 (29)
T 7                17 Q   17 (27)
Confidence            2


No 15 
>P05626_35_Mito_IM_ref_sig5_130
Probab=1.36  E-value=17  Score=7.87  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        18 p   18 (40)
T signal           18 T   18 (40)
T 47               14 P   14 (34)
T 43               14 P   14 (32)
T 45               14 P   14 (32)
T 48               14 P   14 (32)
T 49               14 P   14 (32)
Confidence            3


No 16 
>P29147_46_MM_ref_sig5_130
Probab=1.34  E-value=17  Score=5.14  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.0

Q 5_Mustela         1 R    1 (6)
Q 2_Macaca          1 R    1 (6)
Q 15_Ictidomys      1 R    1 (6)
Q 11_Cavia          1 R    1 (6)
Q 17_Tupaia         1 R    1 (6)
Q 16_Equus          1 R    1 (6)
Q Consensus         1 R    1 (6)
                      -
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 77_Aplysia        1 M    1 (50)
T 61_Papio          1 V    1 (50)
T 19_Tupaia         1 M    1 (50)
T 80_Tribolium      1 M    1 (50)
T 79_Apis           1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 54_Canis          1 M    1 (50)
T 39_Nomascus       1 M    1 (50)
Confidence            0


No 17 
>P09624_21_MM_ref_sig5_130
Probab=1.34  E-value=17  Score=7.10  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 L.    3 (6)
Q 2_Macaca          3 L.    3 (6)
Q 15_Ictidomys      3 L.    3 (6)
Q 11_Cavia          3 L.    3 (6)
Q 17_Tupaia         3 F.    3 (6)
Q 16_Equus          3 L.    3 (6)
Q Consensus         3 l.    3 (6)
                      | 
T Consensus        14 f.   14 (26)
T signal           14 F.   14 (26)
T 188              10 F.   10 (22)
T 183              14 L.   14 (26)
T 186              14 F.   14 (25)
T 191              14 Fl   15 (27)
T 187              11 F.   11 (21)
Confidence            3 


No 18 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=1.33  E-value=17  Score=4.06  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 19 
>P26267_25_MM_ref_sig5_130
Probab=1.32  E-value=17  Score=7.44  Aligned_cols=3  Identities=33%  Similarity=0.490  Sum_probs=1.1

Q 5_Mustela         2 PLE    4 (6)
Q 2_Macaca          2 PLR    4 (6)
Q 15_Ictidomys      2 PLQ    4 (6)
Q 11_Cavia          2 PLR    4 (6)
Q 17_Tupaia         2 PFQ    4 (6)
Q 16_Equus          2 SLE    4 (6)
Q Consensus         2 pl~    4 (6)
                      |.+
T Consensus        12 p~~   14 (30)
T signal           12 PTV   14 (30)
T 14               15 PIQ   17 (33)
T 9                15 PVQ   17 (33)
Confidence            333


No 20 
>P34899_31_Mito_ref_sig5_130
Probab=1.32  E-value=17  Score=7.66  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q 17_Tupaia         2 P    2 (6)
Q 16_Equus          2 S    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        20 p   20 (36)
T signal           20 P   20 (36)
T 145              20 P   20 (36)
T 135              19 P   19 (35)
T 141              20 P   20 (36)
T 140              20 P   20 (35)
T 134              16 P   16 (34)
T 138              18 P   18 (35)
T 139              18 H   18 (33)
T 143              18 S   18 (34)
T 144              18 S   18 (34)
Confidence            3


Done!
