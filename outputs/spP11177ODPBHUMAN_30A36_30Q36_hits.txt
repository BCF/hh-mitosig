Query         5_Mustela
Match_columns 6
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:08:29 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_30A36_30Q36_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9NPH0_32_Mito_ref_sig5_130      0.8      30   0.068    3.9   0.0    1    1-1       1-1   (37)
  2 P16622_31_IM_ref_sig5_130        0.8      31   0.072    7.0   0.0    1    3-3      18-18  (36)
  3 O00142_33_Mito_ref_sig5_130      0.8      32   0.074    4.0   0.0    1    1-1       1-1   (38)
  4 P21801_20_IM_ref_sig5_130        0.7      34   0.078    6.4   0.0    1    4-4      15-15  (25)
  5 P39112_41_MM_ref_sig5_130        0.7      36   0.083    7.2   0.0    1    5-5       3-3   (46)
  6 P07257_16_IM_ref_sig5_130        0.7      38   0.087    5.9   0.0    1    2-2      21-21  (21)
  7 Q04728_8_MM_ref_sig5_130         0.6      39    0.09    5.4   0.0    2    1-2       8-9   (13)
  8 Q02372_28_IM_ref_sig5_130        0.6      43   0.099    3.1   0.0    1    2-2      33-33  (33)
  9 Q16595_41_Cy_Mito_ref_sig5_130   0.6      44     0.1    6.9   0.0    1    2-2      46-46  (46)
 10 O14561_68_Mito_ref_sig5_130      0.6      44     0.1    7.5   0.0    1    5-5      10-10  (73)
 11 P10355_8_IM_ref_sig5_130         0.6      45     0.1    5.3   0.0    1    4-4       5-5   (13)
 12 P17505_17_MM_ref_sig5_130        0.5      46    0.11    5.7   0.0    1    4-4      21-21  (22)
 13 P07756_38_Mito_Nu_ref_sig5_130   0.5      48    0.11    4.4   0.0    1    1-1       1-1   (43)
 14 P53163_33_Mito_ref_sig5_130      0.5      48    0.11    6.6   0.0    1    1-1       1-1   (38)
 15 P0CS90_23_MM_Nu_ref_sig5_130     0.5      48    0.11    6.0   0.0    1    2-2      28-28  (28)
 16 P12687_27_Mito_ref_sig5_130      0.5      49    0.11    6.3   0.0    1    2-2      24-24  (32)
 17 P51998_26_Mito_ref_sig5_130      0.5      49    0.11    6.3   0.0    1    4-4       2-2   (31)
 18 P26269_27_MM_ref_sig5_130        0.5      51    0.12    6.3   0.0    1    5-5      24-24  (32)
 19 Q96CB9_25_Mito_ref_sig5_130      0.5      53    0.12    2.8   0.0    1    1-1       1-1   (30)
 20 P11024_43_IM_ref_sig5_130        0.5      54    0.12    3.0   0.0    1    1-1       1-1   (48)

No 1  
>Q9NPH0_32_Mito_ref_sig5_130
Probab=0.82  E-value=30  Score=3.91  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 92_Coccidioide    1 M    1 (36)
T 77_Strongyloce    1 M    1 (36)
T 76_Ciona          1 M    1 (36)
T 74_Latimeria      1 V    1 (36)
T 72_Branchiosto    1 F    1 (36)
T 60_Ficedula       1 M    1 (36)
T 49_Condylura      1 M    1 (36)
Confidence            0


No 2  
>P16622_31_IM_ref_sig5_130
Probab=0.78  E-value=31  Score=7.01  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.3

Q 5_Mustela         3 V    3 (6)
Q 15_Ictidomys      3 M    3 (6)
Q Consensus         3 ~    3 (6)
                      .
T Consensus        18 l   18 (36)
T signal           18 L   18 (36)
Confidence            2


No 3  
>O00142_33_Mito_ref_sig5_130
Probab=0.76  E-value=32  Score=3.99  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 92_Pediculus      1 M    1 (34)
T 83_Nematostell    1 M    1 (34)
T 79_Bombus         1 M    1 (34)
T 75_Bombyx         1 M    1 (34)
T 57_Xiphophorus    1 M    1 (34)
T 45_Anolis         1 M    1 (34)
T 40_Monodelphis    1 M    1 (34)
Confidence            0


No 4  
>P21801_20_IM_ref_sig5_130
Probab=0.72  E-value=34  Score=6.42  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.2

Q 5_Mustela         4 T..    4 (6)
Q 15_Ictidomys      4 T..    4 (6)
Q Consensus         4 t..    4 (6)
                      |  
T Consensus        15 t..   15 (25)
T signal           15 T..   15 (25)
T 192              11 Tig   13 (23)
T 191              11 A..   11 (21)
Confidence            2  


No 5  
>P39112_41_MM_ref_sig5_130
Probab=0.69  E-value=36  Score=7.17  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q Consensus         5 v    5 (6)
                      |
T Consensus         3 v    3 (46)
T signal            3 V    3 (46)
Confidence            1


No 6  
>P07257_16_IM_ref_sig5_130
Probab=0.65  E-value=38  Score=5.93  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 Q    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q Consensus         2 q    2 (6)
                      -
T Consensus        21 a   21 (21)
T signal           21 A   21 (21)
T 20               18 A   18 (18)
T 34               18 S   18 (18)
T 14               19 T   19 (19)
T 15               20 A   20 (20)
T 23               22 A   22 (22)
T 19               20 A   20 (20)
T 40               22 A   22 (22)
T 42               20 A   20 (20)
Confidence            0


No 7  
>Q04728_8_MM_ref_sig5_130
Probab=0.64  E-value=39  Score=5.39  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.9

Q 5_Mustela         1 LQ    2 (6)
Q 15_Ictidomys      1 LQ    2 (6)
Q Consensus         1 lq    2 (6)
                      ||
T Consensus         8 lq    9 (13)
T signal            8 LQ    9 (13)
T 90                8 LQ    9 (13)
T 88                8 LQ    9 (13)
Confidence            44


No 8  
>Q02372_28_IM_ref_sig5_130
Probab=0.58  E-value=43  Score=3.11  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         2 Q    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q Consensus         2 q    2 (6)
                      -
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 9  
>Q16595_41_Cy_Mito_ref_sig5_130
Probab=0.58  E-value=44  Score=6.87  Aligned_cols=1  Identities=0%  Similarity=-0.629  Sum_probs=0.0

Q 5_Mustela         2 Q    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q Consensus         2 q    2 (6)
                      -
T Consensus        46 ~   46 (46)
T signal           46 I   46 (46)
T 159              49 I   49 (49)
T 157              47 -   46 (46)
T 156              47 -   46 (46)
T 153              44 -   43 (43)
T 145              47 -   46 (46)
T 142              45 -   44 (44)
T 147              45 -   44 (44)
T 141              39 -   38 (38)
Confidence            0


No 10 
>O14561_68_Mito_ref_sig5_130
Probab=0.58  E-value=44  Score=7.45  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q Consensus         5 v    5 (6)
                      |
T Consensus        10 V   10 (73)
T signal           10 V   10 (73)
T 82               10 V   10 (72)
T 80               10 V   10 (76)
T 78               10 V   10 (66)
T 79               10 V   10 (65)
T 76               10 V   10 (67)
T 73               10 V   10 (70)
T 65               10 V   10 (73)
T 77               13 L   13 (94)
T 72               10 V   10 (73)
Confidence            2


No 11 
>P10355_8_IM_ref_sig5_130
Probab=0.56  E-value=45  Score=5.25  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (6)
Q 15_Ictidomys      4 T    4 (6)
Q Consensus         4 t    4 (6)
                      |
T Consensus         5 t    5 (13)
T signal            5 T    5 (13)
Confidence            2


No 12 
>P17505_17_MM_ref_sig5_130
Probab=0.55  E-value=46  Score=5.69  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (6)
Q 15_Ictidomys      4 T    4 (6)
Q Consensus         4 t    4 (6)
                      +
T Consensus        21 a   21 (22)
T signal           21 T   21 (22)
T 148              21 A   21 (22)
T 156              21 S   21 (22)
T 157              16 A   16 (17)
T 115              19 A   19 (20)
T 55               28 T   28 (29)
T 10               27 A   27 (28)
T 26               16 A   16 (17)
T 120              19 T   19 (20)
T 125              19 S   19 (20)
Confidence            2


No 13 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.53  E-value=48  Score=4.45  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (43)
T signal            1 M    1 (43)
T 79_Emiliania      1 M    1 (42)
T 77_Thalassiosi    1 M    1 (42)
T 60_Anolis         1 M    1 (42)
T 48_Alligator      1 M    1 (42)
T 44_Papio          1 M    1 (42)
T 28_Ovis           1 M    1 (42)
T 18_Pongo          1 M    1 (42)
T cl|HABBABABA|1    1 M    1 (52)
T 72_Branchiosto    1 M    1 (41)
Confidence            0


No 14 
>P53163_33_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.58  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 169               1 M    1 (32)
T 170               1 M    1 (37)
Confidence            0


No 15 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=0.53  E-value=48  Score=5.97  Aligned_cols=1  Identities=0%  Similarity=-0.495  Sum_probs=0.0

Q 5_Mustela         2 Q    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q Consensus         2 q    2 (6)
                      -
T Consensus        28 V   28 (28)
T signal           28 V   28 (28)
T 55               29 I   29 (29)
T 65               29 V   29 (29)
T 54               28 V   28 (28)
T 57               26 V   26 (26)
T 59               26 V   26 (26)
T 62               24 V   24 (24)
T 56               24 V   24 (24)
T 60               23 V   23 (23)
Confidence            0


No 16 
>P12687_27_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=6.31  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         2 Q    2 (6)
Q 15_Ictidomys      2 Q    2 (6)
Q Consensus         2 q    2 (6)
                      |
T Consensus        24 q   24 (32)
T signal           24 Q   24 (32)
T 93               23 H   23 (31)
T 84               17 Q   17 (25)
Confidence            3


No 17 
>P51998_26_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=6.26  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.2

Q 5_Mustela         4 T    4 (6)
Q 15_Ictidomys      4 T    4 (6)
Q Consensus         4 t    4 (6)
                      |
T Consensus         2 t    2 (31)
T signal            2 T    2 (31)
Confidence            2


No 18 
>P26269_27_MM_ref_sig5_130
Probab=0.50  E-value=51  Score=6.27  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         5 V    5 (6)
Q 15_Ictidomys      5 V    5 (6)
Q Consensus         5 v    5 (6)
                      |
T Consensus        24 v   24 (32)
T signal           24 V   24 (32)
Confidence            2


No 19 
>Q96CB9_25_Mito_ref_sig5_130
Probab=0.48  E-value=53  Score=2.77  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 100_Pediculus     1 M    1 (30)
T 96_Nematostell    1 M    1 (30)
T 80_Bombus         1 M    1 (30)
T 78_Aedes          1 M    1 (30)
T 63_Columba        1 M    1 (30)
Confidence            0


No 20 
>P11024_43_IM_ref_sig5_130
Probab=0.48  E-value=54  Score=3.03  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (6)
Q 15_Ictidomys      1 L    1 (6)
Q Consensus         1 l    1 (6)
                      .
T Consensus         1 M    1 (48)
T signal            1 M    1 (48)
T 144_Moniliopht    1 M    1 (47)
T 141_Cryptospor    1 M    1 (47)
T 108_Ichthyopht    1 M    1 (47)
T 99_Aspergillus    1 M    1 (47)
T 97_Volvox         1 M    1 (47)
T 96_Schizophyll    1 M    1 (47)
Confidence            0


Done!
