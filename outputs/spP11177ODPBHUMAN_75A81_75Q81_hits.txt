Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:38 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_75A81_75Q81_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P07806_47_Cy_Mito_ref_sig5_130   0.5      54    0.12    6.8   0.0    1    2-2      52-52  (52)
  2 P34942_23_MM_ref_sig5_130        0.5      54    0.12    3.1   0.0    1    2-2      28-28  (28)
  3 P40360_13_Mito_ref_sig5_130      0.5      54    0.12    5.4   0.0    2    1-2      11-12  (18)
  4 P38071_9_MM_ref_sig5_130         0.5      55    0.13    5.1   0.0    2    5-6       6-7   (14)
  5 P53219_38_Mito_ref_sig5_130      0.5      55    0.13    6.5   0.0    2    2-3      13-14  (43)
  6 Q02372_28_IM_ref_sig5_130        0.4      60    0.14    2.8   0.0    1    2-2      33-33  (33)
  7 P09624_21_MM_ref_sig5_130        0.4      62    0.14    5.6   0.0    1    5-5      11-11  (26)
  8 Q9HCC0_22_MM_ref_sig5_130        0.4      63    0.15    5.8   0.0    2    3-4      25-26  (27)
  9 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   0.4      66    0.15    5.7   0.0    2    3-4      24-25  (28)
 10 P01098_23_Mito_ref_sig5_130      0.4      69    0.16    5.7   0.0    3    2-4      23-25  (28)
 11 P10355_8_IM_ref_sig5_130         0.4      72    0.16    4.8   0.0    2    5-6       6-7   (13)
 12 P05165_52_MM_ref_sig5_130        0.4      72    0.16    3.1   0.0    1    2-2      57-57  (57)
 13 P10507_20_MM_ref_sig5_130        0.4      72    0.17    5.6   0.0    1    2-2       9-9   (25)
 14 P83484_51_Mito_IM_ref_sig5_130   0.4      72    0.17    5.1   0.0    1    3-3      42-42  (56)
 15 P07919_13_IM_ref_sig5_130        0.4      74    0.17    5.0   0.0    2    3-4      14-15  (18)
 16 Q00711_28_IM_ref_sig5_130        0.3      77    0.18    5.7   0.0    1    3-3      32-32  (33)
 17 P21642_33_Mito_ref_sig5_130      0.3      77    0.18    6.0   0.0    1    4-4       8-8   (38)
 18 P36516_59_Mito_ref_sig5_130      0.3      79    0.18    6.5   0.0    1    2-2      64-64  (64)
 19 P17764_30_Mito_ref_sig5_130      0.3      82    0.19    5.7   0.0    1    2-2      30-30  (35)
 20 P36519_19_Mito_ref_sig5_130      0.3      85     0.2    5.2   0.0    1    5-5       9-9   (24)

No 1  
>P07806_47_Cy_Mito_ref_sig5_130
Probab=0.48  E-value=54  Score=6.83  Aligned_cols=1  Identities=0%  Similarity=-0.462  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      -
T Consensus        52 N   52 (52)
T signal           52 N   52 (52)
T 176              29 N   29 (29)
Confidence            0


No 2  
>P34942_23_MM_ref_sig5_130
Probab=0.48  E-value=54  Score=3.09  Aligned_cols=1  Identities=0%  Similarity=-1.028  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 3  
>P40360_13_Mito_ref_sig5_130
Probab=0.47  E-value=54  Score=5.38  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.9

Q 5_Mustela         1 KY    2 (6)
Q Consensus         1 ky    2 (6)
                      ||
T Consensus        11 ky   12 (18)
T signal           11 KY   12 (18)
T 54               11 RY   12 (18)
T 53               11 KY   12 (18)
T 52               11 GY   12 (17)
T 50               11 KY   12 (17)
T 51               11 KV   12 (18)
T 49               12 KL   13 (19)
Confidence            44


No 4  
>P38071_9_MM_ref_sig5_130
Probab=0.47  E-value=55  Score=5.14  Aligned_cols=2  Identities=100%  Similarity=1.315  Sum_probs=0.8

Q 5_Mustela         5 KR    6 (6)
Q Consensus         5 kr    6 (6)
                      ||
T Consensus         6 KR    7 (14)
T signal            6 KR    7 (14)
T 173               6 KR    7 (12)
T 174               6 KR    7 (12)
Confidence            33


No 5  
>P53219_38_Mito_ref_sig5_130
Probab=0.46  E-value=55  Score=6.54  Aligned_cols=2  Identities=100%  Similarity=2.394  Sum_probs=0.8

Q 5_Mustela         2 YG    3 (6)
Q Consensus         2 yg    3 (6)
                      ||
T Consensus        13 yg   14 (43)
T signal           13 YG   14 (43)
Confidence            33


No 6  
>Q02372_28_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=2.79  Aligned_cols=1  Identities=0%  Similarity=-0.629  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      -
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 7  
>P09624_21_MM_ref_sig5_130
Probab=0.42  E-value=62  Score=5.64  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         5 K    5 (6)
Q Consensus         5 k    5 (6)
                      |
T Consensus        11 k   11 (26)
T signal           11 K   11 (26)
T 188               7 T    7 (22)
T 183              11 K   11 (26)
T 186              11 R   11 (25)
T 191              11 S   11 (27)
T 187               8 K    8 (21)
Confidence            3


No 8  
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.41  E-value=63  Score=5.77  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=1.0

Q 5_Mustela         3 GD    4 (6)
Q Consensus         3 gd    4 (6)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 9  
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=0.39  E-value=66  Score=5.73  Aligned_cols=2  Identities=50%  Similarity=1.464  Sum_probs=1.1

Q 5_Mustela         3 GD    4 (6)
Q Consensus         3 gd    4 (6)
                      ||
T Consensus        24 GD   25 (28)
T signal           24 GN   25 (28)
T 102              22 GD   23 (26)
T 105              22 GD   23 (26)
T 110              22 GD   23 (26)
T 111              22 GD   23 (26)
T 108              22 GD   23 (26)
T 113              22 GD   23 (26)
T 76               22 GD   23 (26)
Confidence            55


No 10 
>P01098_23_Mito_ref_sig5_130
Probab=0.38  E-value=69  Score=5.73  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=1.2

Q 5_Mustela         2 YGD    4 (6)
Q Consensus         2 ygd    4 (6)
                      |.|
T Consensus        23 ysd   25 (28)
T signal           23 YSD   25 (28)
Confidence            433


No 11 
>P10355_8_IM_ref_sig5_130
Probab=0.36  E-value=72  Score=4.76  Aligned_cols=2  Identities=100%  Similarity=1.315  Sum_probs=0.8

Q 5_Mustela         5 KR    6 (6)
Q Consensus         5 kr    6 (6)
                      ||
T Consensus         6 kr    7 (13)
T signal            6 KR    7 (13)
Confidence            44


No 12 
>P05165_52_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=3.08  Aligned_cols=1  Identities=0%  Similarity=-0.928  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      =
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 13 
>P10507_20_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.56  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.2

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      |
T Consensus         9 f    9 (25)
T signal            9 F    9 (25)
T 191               6 -    5 (19)
Confidence            2


No 14 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.09  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        42 ~   42 (56)
T signal           42 S   42 (56)
T 34_Volvox        39 -   38 (38)
T 134_Guillardia   39 -   38 (38)
T 152_Trichinell   39 -   38 (38)
T 159_Ixodes       39 -   38 (38)
T 41_Galdieria     39 -   38 (38)
T 112_Tribolium    39 -   38 (38)
T 117_Ceratitis    39 -   38 (38)
T 111_Musca        39 -   38 (38)
T 39_Selaginella   39 -   38 (38)
Confidence            2


No 15 
>P07919_13_IM_ref_sig5_130
Probab=0.35  E-value=74  Score=5.01  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.9

Q 5_Mustela         3 GD    4 (6)
Q Consensus         3 gd    4 (6)
                      ||
T Consensus        14 gD   15 (18)
T signal           14 GD   15 (18)
T 41               14 GD   15 (18)
T 13               14 RD   15 (18)
T 23               14 GD   15 (18)
T 58               13 ED   14 (17)
T 61               14 GD   15 (18)
T 18               14 GE   15 (18)
T 22               14 GD   15 (18)
T 59               14 GD   15 (18)
T 79               11 GE   12 (15)
Confidence            44


No 16 
>Q00711_28_IM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.67  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        32 G   32 (33)
T signal           32 G   32 (33)
T 13               28 -   27 (27)
T 46               32 G   32 (33)
T 71               32 G   32 (33)
T 14               33 G   33 (34)
T 18               31 G   31 (32)
Confidence            3


No 17 
>P21642_33_Mito_ref_sig5_130
Probab=0.34  E-value=77  Score=5.95  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.2

Q 5_Mustela         4 D    4 (6)
Q Consensus         4 d    4 (6)
                      |
T Consensus         8 ~    8 (38)
T signal            8 A    8 (38)
T cl|CABBABABA|1    8 D    8 (38)
Confidence            2


No 18 
>P36516_59_Mito_ref_sig5_130
Probab=0.33  E-value=79  Score=6.55  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.0

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      |
T Consensus        64 f   64 (64)
T signal           64 F   64 (64)
Confidence            0


No 19 
>P17764_30_Mito_ref_sig5_130
Probab=0.32  E-value=82  Score=5.70  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         2 Y    2 (6)
Q Consensus         2 y    2 (6)
                      |
T Consensus        30 Y   30 (35)
T signal           30 Y   30 (35)
T 144              33 Y   33 (38)
T 149              33 Y   33 (38)
T 152              38 Y   38 (43)
T 154              33 Y   33 (38)
T 157              21 Y   21 (26)
T 161              33 Y   33 (38)
T 153              21 Y   21 (26)
T 134              33 Y   33 (38)
T 159              33 Y   33 (38)
Confidence            3


No 20 
>P36519_19_Mito_ref_sig5_130
Probab=0.31  E-value=85  Score=5.21  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.2

Q 5_Mustela         5 K    5 (6)
Q Consensus         5 k    5 (6)
                      .
T Consensus         9 ~    9 (24)
T signal            9 H    9 (24)
T 131              10 I   10 (25)
T 136               8 I    8 (23)
T 137               8 K    8 (23)
T 127               5 V    5 (20)
T 130               2 R    2 (17)
T 135               2 I    2 (17)
T 129               3 R    3 (18)
T 132               3 C    3 (18)
T 133               3 T    3 (18)
Confidence            2


Done!
