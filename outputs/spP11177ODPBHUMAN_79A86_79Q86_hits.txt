Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:45 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_79A86_79Q86_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P10355_8_IM_ref_sig5_130         4.3     4.2  0.0096    8.0   0.0    6    1-6       6-11  (13)
  2 P19783_22_IM_ref_sig5_130        3.8     4.9   0.011    9.0   0.0    6    1-6      12-17  (27)
  3 P14066_25_IM_ref_sig5_130        0.6      42   0.096    6.7   0.0    1    4-4      26-26  (30)
  4 P10507_20_MM_ref_sig5_130        0.6      43   0.098    6.4   0.0    1    3-3      15-15  (25)
  5 P32387_45_Mito_ref_sig5_130      0.6      45     0.1    7.2   0.0    3    3-5      44-46  (50)
  6 P40360_13_Mito_ref_sig5_130      0.6      45     0.1    5.8   0.0    2    2-3       4-5   (18)
  7 P26360_45_Mito_IM_ref_sig5_130   0.6      46    0.11    7.2   0.0    1    3-3      13-13  (50)
  8 P28834_11_Mito_ref_sig5_130      0.5      47    0.11    5.7   0.0    1    3-3      16-16  (16)
  9 P21642_33_Mito_ref_sig5_130      0.5      48    0.11    6.8   0.0    3    3-5      14-16  (38)
 10 P33198_8_Mito_ref_sig5_130       0.5      50    0.11    5.3   0.0    1    2-2      12-12  (13)
 11 P00431_67_MM_ref_sig5_130        0.5      50    0.12    5.1   0.0    1    1-1       1-1   (72)
 12 P07756_38_Mito_Nu_ref_sig5_130   0.5      52    0.12    4.5   0.0    1    1-1       1-1   (43)
 13 P13184_23_IM_ref_sig5_130        0.5      53    0.12    6.2   0.0    1    4-4      16-16  (28)
 14 P07246_27_MM_ref_sig5_130        0.5      53    0.12    5.1   0.0    1    3-3      32-32  (32)
 15 P54898_44_Mito_ref_sig5_130      0.5      56    0.13    6.9   0.0    1    4-4      37-37  (49)
 16 P25284_26_MM_ref_sig5_130        0.5      57    0.13    6.2   0.0    1    5-5      29-29  (31)
 17 P11325_9_MM_ref_sig5_130         0.4      59    0.14    5.2   0.0    4    2-5       8-11  (14)
 18 P36516_59_Mito_ref_sig5_130      0.4      61    0.14    7.2   0.0    1    2-2       7-7   (64)
 19 P18155_35_Mito_ref_sig5_130      0.4      65    0.15    3.1   0.0    1    3-3      40-40  (40)
 20 P36520_57_Mito_ref_sig5_130      0.4      67    0.15    7.0   0.0    1    4-4      22-22  (62)

No 1  
>P10355_8_IM_ref_sig5_130
Probab=4.27  E-value=4.2  Score=7.98  Aligned_cols=6  Identities=50%  Similarity=0.905  Sum_probs=3.0

Q 5_Mustela         1 KRIIDT    6 (7)
Q Consensus         1 kriidt    6 (7)
                      ||++-|
T Consensus         6 krlvtt   11 (13)
T signal            6 KRLVTT   11 (13)
Confidence            465544


No 2  
>P19783_22_IM_ref_sig5_130
Probab=3.77  E-value=4.9  Score=9.04  Aligned_cols=6  Identities=67%  Similarity=0.783  Sum_probs=3.1

Q 5_Mustela         1 KRIIDT    6 (7)
Q Consensus         1 kriidt    6 (7)
                      ||-|-|
T Consensus        12 kRaiST   17 (27)
T signal           12 KRAIST   17 (27)
T 57               12 KRVIST   17 (27)
T 82               12 RRALST   17 (27)
T 50               12 RRAIST   17 (27)
T 56               12 KRSIST   17 (27)
T 80               12 SRAIST   17 (27)
T 35               12 KRAIST   17 (26)
T 25               12 KRALST   17 (27)
T 36               12 RRALST   17 (27)
Confidence            455554


No 3  
>P14066_25_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.66  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        26 i   26 (30)
T signal           26 I   26 (30)
Confidence            2


No 4  
>P10507_20_MM_ref_sig5_130
Probab=0.59  E-value=43  Score=6.40  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         3 I    3 (7)
Q Consensus         3 i    3 (7)
                      +
T Consensus        15 l   15 (25)
T signal           15 L   15 (25)
T 191               9 L    9 (19)
Confidence            3


No 5  
>P32387_45_Mito_ref_sig5_130
Probab=0.56  E-value=45  Score=7.24  Aligned_cols=3  Identities=67%  Similarity=1.276  Sum_probs=1.3

Q 5_Mustela         3 IID    5 (7)
Q Consensus         3 iid    5 (7)
                      |+|
T Consensus        44 ild   46 (50)
T signal           44 ILD   46 (50)
Confidence            444


No 6  
>P40360_13_Mito_ref_sig5_130
Probab=0.56  E-value=45  Score=5.78  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.7

Q 5_Mustela         2 RI    3 (7)
Q Consensus         2 ri    3 (7)
                      +|
T Consensus         4 ~l    5 (18)
T signal            4 RI    5 (18)
T 54                4 HI    5 (18)
T 53                4 QA    5 (18)
T 52                4 RL    5 (17)
T 50                4 HL    5 (17)
T 51                4 HL    5 (18)
T 49                4 KL    5 (19)
Confidence            33


No 7  
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.55  E-value=46  Score=7.21  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         3 I    3 (7)
Q Consensus         3 i    3 (7)
                      .
T Consensus        13 l   13 (50)
T signal           13 L   13 (50)
T 181              13 F   13 (45)
T 183              13 F   13 (48)
T 170              13 L   13 (51)
T 171              13 V   13 (49)
T 182              16 P   16 (50)
T 174              13 L   13 (47)
T 178              14 L   14 (44)
T 177              14 L   14 (50)
T 168              13 L   13 (52)
Confidence            1


No 8  
>P28834_11_Mito_ref_sig5_130
Probab=0.54  E-value=47  Score=5.69  Aligned_cols=1  Identities=0%  Similarity=-0.629  Sum_probs=0.0

Q 5_Mustela         3 I    3 (7)
Q Consensus         3 i    3 (7)
                      -
T Consensus        16 q   16 (16)
T signal           16 Q   16 (16)
Confidence            0


No 9  
>P21642_33_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.80  Aligned_cols=3  Identities=0%  Similarity=-0.795  Sum_probs=1.2

Q 5_Mustela         3 IID    5 (7)
Q Consensus         3 iid    5 (7)
                      |||
T Consensus        14 ~~~   16 (38)
T signal           14 GET   16 (38)
T cl|CABBABABA|1   14 IID   16 (38)
Confidence            343


No 10 
>P33198_8_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=5.34  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         2 R    2 (7)
Q Consensus         2 r    2 (7)
                      |
T Consensus        12 r   12 (13)
T signal           12 R   12 (13)
Confidence            3


No 11 
>P00431_67_MM_ref_sig5_130
Probab=0.51  E-value=50  Score=5.08  Aligned_cols=1  Identities=0%  Similarity=-0.463  Sum_probs=0.0

Q 5_Mustela         1 K    1 (7)
Q Consensus         1 k    1 (7)
                      -
T Consensus         1 M    1 (72)
T signal            1 M    1 (72)
T cl|DABBABABA|1    1 M    1 (61)
T cl|ZABBABABA|1    1 P    1 (61)
T cl|LEBBABABA|1    1 M    1 (61)
T cl|MEBBABABA|1    1 Q    1 (61)
T cl|TEBBABABA|1    1 M    1 (61)
T cl|ZEBBABABA|1    1 M    1 (61)
T cl|PIBBABABA|1    1 M    1 (61)
T cl|DUBBABABA|1    1 M    1 (61)
Confidence            0


No 12 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.49  E-value=52  Score=4.52  Aligned_cols=1  Identities=0%  Similarity=-0.463  Sum_probs=0.0

Q 5_Mustela         1 K    1 (7)
Q Consensus         1 k    1 (7)
                      -
T Consensus         1 M    1 (43)
T signal            1 M    1 (43)
T 79_Emiliania      1 M    1 (42)
T 77_Thalassiosi    1 M    1 (42)
T 60_Anolis         1 M    1 (42)
T 48_Alligator      1 M    1 (42)
T 44_Papio          1 M    1 (42)
T 28_Ovis           1 M    1 (42)
T 18_Pongo          1 M    1 (42)
T cl|HABBABABA|1    1 M    1 (52)
T 72_Branchiosto    1 M    1 (41)
Confidence            0


No 13 
>P13184_23_IM_ref_sig5_130
Probab=0.49  E-value=53  Score=6.20  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        16 I   16 (28)
T signal           16 I   16 (28)
T 63               16 F   16 (28)
T 28               16 I   16 (28)
T 15               16 I   16 (28)
T 25               16 T   16 (28)
T 16               16 I   16 (28)
T 19               16 I   16 (28)
T 12               16 I   16 (28)
T 14               16 I   16 (28)
Confidence            3


No 14 
>P07246_27_MM_ref_sig5_130
Probab=0.48  E-value=53  Score=5.15  Aligned_cols=1  Identities=0%  Similarity=-0.696  Sum_probs=0.0

Q 5_Mustela         3 I    3 (7)
Q Consensus         3 i    3 (7)
                      -
T Consensus        32 ~   32 (32)
T signal           32 K   32 (32)
T 3                34 K   34 (34)
T 12               29 Q   29 (29)
T 14               30 A   30 (30)
T 60_Zymoseptori   32 P   32 (32)
T 43_Pyrenophora   32 N   32 (32)
T 29_Talaromyces   32 R   32 (32)
T 12_Zygosacchar   32 V   32 (32)
T 11_Vanderwalto   32 A   32 (32)
T 18               32 N   32 (32)
Confidence            0


No 15 
>P54898_44_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=6.93  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        37 ~   37 (49)
T signal           37 I   37 (49)
T 154              39 F   39 (51)
T 151              38 F   38 (50)
T 104              37 I   37 (43)
T 137              44 F   44 (49)
Confidence            3


No 16 
>P25284_26_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.24  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 D    5 (7)
Q Consensus         5 d    5 (7)
                      |
T Consensus        29 D   29 (31)
T signal           29 D   29 (31)
T 188              29 D   29 (31)
T 184              28 D   28 (30)
T 187              27 D   27 (29)
T 171              26 D   26 (27)
T 182              26 D   26 (27)
T 164              25 D   25 (27)
T 183              26 D   26 (28)
Confidence            2


No 17 
>P11325_9_MM_ref_sig5_130
Probab=0.43  E-value=59  Score=5.24  Aligned_cols=4  Identities=25%  Similarity=0.750  Sum_probs=1.6

Q 5_Mustela         2 RIID    5 (7)
Q Consensus         2 riid    5 (7)
                      |.+.
T Consensus         8 rfls   11 (14)
T signal            8 RFLS   11 (14)
Confidence            3443


No 18 
>P36516_59_Mito_ref_sig5_130
Probab=0.42  E-value=61  Score=7.15  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         2 R    2 (7)
Q Consensus         2 r    2 (7)
                      |
T Consensus         7 r    7 (64)
T signal            7 R    7 (64)
Confidence            2


No 19 
>P18155_35_Mito_ref_sig5_130
Probab=0.40  E-value=65  Score=3.09  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         3 I    3 (7)
Q Consensus         3 i    3 (7)
                      |
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 95_Emiliania     40 -   39 (39)
T 93_Volvox        40 -   39 (39)
T 89_Ornithorhyn   40 -   39 (39)
T 77_Pediculus     40 -   39 (39)
T 75_Trichoplax    40 -   39 (39)
T 71_Saccoglossu   40 -   39 (39)
T 67_Monodelphis   40 -   39 (39)
T 41_Capra         40 -   39 (39)
Confidence            0


No 20 
>P36520_57_Mito_ref_sig5_130
Probab=0.39  E-value=67  Score=6.98  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         4 I    4 (7)
Q Consensus         4 i    4 (7)
                      |
T Consensus        22 i   22 (62)
T signal           22 I   22 (62)
Confidence            2


Done!
