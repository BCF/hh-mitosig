Query         5_Mustela
Match_columns 7
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:08:55 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_46A53_46Q53_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P26360_45_Mito_IM_ref_sig5_130   0.3      85     0.2    6.4   0.0    1    3-3       8-8   (50)
  2 P17764_30_Mito_ref_sig5_130      0.3      96    0.22    5.7   0.0    1    3-3      28-28  (35)
  3 P00366_57_MM_ref_sig5_130        0.3      98    0.22    4.2   0.0    1    3-3      62-62  (62)
  4 P56522_34_MM_ref_sig5_130        0.3   1E+02    0.23    2.3   0.0    2    4-5      37-38  (39)
  5 P43165_34_Mito_ref_sig5_130      0.3   1E+02    0.24    5.7   0.0    4    3-6      28-31  (39)
  6 P11024_43_IM_ref_sig5_130        0.3   1E+02    0.24    2.5   0.0    3    5-7      46-48  (48)
  7 P07471_12_IM_ref_sig5_130        0.3   1E+02    0.24    4.8   0.0    3    1-3       8-10  (17)
  8 Q5A8K2_8_Mito_Cy_ref_sig5_130    0.3   1E+02    0.24    4.5   0.0    3    1-3       6-8   (13)
  9 P06576_47_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.25    6.0   0.0    2    3-4      44-45  (52)
 10 Q6UB35_31_Mito_ref_sig5_130      0.2 1.1E+02    0.25    2.8   0.0    7    1-7      12-18  (36)
 11 P01098_23_Mito_ref_sig5_130      0.2 1.1E+02    0.25    5.3   0.0    7    1-7       6-12  (28)
 12 P10719_46_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.25    6.0   0.0    2    3-4      44-45  (51)
 13 P07926_68_MitoM_ref_sig5_130     0.2 1.1E+02    0.25    6.4   0.0    2    3-4      68-69  (73)
 14 P11325_9_MM_ref_sig5_130         0.2 1.1E+02    0.26    4.5   0.0    3    1-3       2-4   (14)
 15 P00829_48_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.26    6.0   0.0    2    3-4      44-45  (53)
 16 P51557_62_Mito_ref_sig5_130      0.2 1.1E+02    0.26    4.5   0.0    7    1-7      35-41  (67)
 17 P28834_11_Mito_ref_sig5_130      0.2 1.1E+02    0.26    4.6   0.0    3    1-3       2-4   (16)
 18 P83484_51_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.26    4.7   0.0    3    1-3      45-47  (56)
 19 P36526_16_Mito_ref_sig5_130      0.2 1.2E+02    0.27    4.7   0.0    3    1-3      17-19  (21)
 20 P00447_26_MM_ref_sig5_130        0.2 1.2E+02    0.28    5.3   0.0    5    3-7      24-28  (31)

No 1  
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.31  E-value=85  Score=6.38  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 R    3 (7)
Q 11_Cavia          3 R    3 (7)
Q Consensus         3 r    3 (7)
                      |
T Consensus         8 R    8 (50)
T signal            8 R    8 (50)
T 181               8 R    8 (45)
T 183               8 R    8 (48)
T 170               8 R    8 (51)
T 171               8 R    8 (49)
T 182               8 R    8 (50)
T 174               8 R    8 (47)
T 178               9 R    9 (44)
T 177               9 R    9 (50)
T 168               8 R    8 (52)
Confidence            1


No 2  
>P17764_30_Mito_ref_sig5_130
Probab=0.27  E-value=96  Score=5.70  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         3 R    3 (7)
Q 11_Cavia          3 R    3 (7)
Q Consensus         3 r    3 (7)
                      |
T Consensus        28 R   28 (35)
T signal           28 R   28 (35)
T 144              31 R   31 (38)
T 149              31 R   31 (38)
T 152              36 R   36 (43)
T 154              31 R   31 (38)
T 157              19 R   19 (26)
T 161              31 R   31 (38)
T 153              19 R   19 (26)
T 134              31 R   31 (38)
T 159              31 R   31 (38)
Confidence            3


No 3  
>P00366_57_MM_ref_sig5_130
Probab=0.27  E-value=98  Score=4.22  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         3 R    3 (7)
Q 11_Cavia          3 R    3 (7)
Q Consensus         3 r    3 (7)
                      -
T Consensus        62 ~   62 (62)
T signal           62 D   62 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            0


No 4  
>P56522_34_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=2.26  Aligned_cols=2  Identities=50%  Similarity=0.750  Sum_probs=0.0

Q 5_Mustela         4 DE    5 (7)
Q 11_Cavia          4 DE    5 (7)
Q Consensus         4 de    5 (7)
                      .|
T Consensus        37 ~~   38 (39)
T signal           37 QE   38 (39)
T 138_Tuber        37 G-   37 (37)
T 98_Oryza         37 L-   37 (37)
T 88_Apis          37 Q-   37 (37)
T 81_Amphimedon    37 S-   37 (37)
T 79_Trichoplax    37 T-   37 (37)
T 61_Meleagris     37 N-   37 (37)


No 5  
>P43165_34_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.66  Aligned_cols=4  Identities=25%  Similarity=0.526  Sum_probs=0.0

Q 5_Mustela         3 RDEK    6 (7)
Q 11_Cavia          3 RDER    6 (7)
Q Consensus         3 rde~    6 (7)
                      |-|+
T Consensus        28 Rper   31 (39)
T signal           28 RPQH   31 (39)
T 16               28 RPGR   31 (39)
T 35               29 RSEQ   32 (40)
T 23               34 RPAR   37 (45)
T 28               34 RPMR   37 (44)
T 6                34 RPER   37 (45)
T 15               34 RPER   37 (45)
T 24               34 RPEQ   37 (45)
T 26               34 TLER   37 (45)
T 29               34 RLER   37 (45)


No 6  
>P11024_43_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=2.53  Aligned_cols=3  Identities=33%  Similarity=0.313  Sum_probs=0.0

Q 5_Mustela         5 EKV    7 (7)
Q 11_Cavia          5 ERV    7 (7)
Q Consensus         5 e~v    7 (7)
                      .+|
T Consensus        46 ~~~   48 (48)
T signal           46 APV   48 (48)
T 144_Moniliopht   46 LA-   47 (47)
T 141_Cryptospor   46 LR-   47 (47)
T 108_Ichthyopht   46 EK-   47 (47)
T 99_Aspergillus   46 TR-   47 (47)
T 97_Volvox        46 VR-   47 (47)
T 96_Schizophyll   46 HH-   47 (47)


No 7  
>P07471_12_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=4.82  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |.|
T Consensus         8 LsR   10 (17)
T signal            8 LSR   10 (17)
T 8                 8 LNR   10 (17)
T 15                8 LSR   10 (17)
T 17                8 LGR   10 (17)
T 18                8 LSR   10 (17)
T 42                8 LNR   10 (17)
T 3                 8 LSR   10 (17)


No 8  
>Q5A8K2_8_Mito_Cy_ref_sig5_130
Probab=0.25  E-value=1e+02  Score=4.49  Aligned_cols=3  Identities=67%  Similarity=1.010  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |.|
T Consensus         6 lrr    8 (13)
T signal            6 LRR    8 (13)


No 9  
>P06576_47_Mito_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=6.03  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         3 RD    4 (7)
Q 11_Cavia          3 RD    4 (7)
Q Consensus         3 rd    4 (7)
                      ||
T Consensus        44 Rd   45 (52)
T signal           44 RD   45 (52)
T 130              44 GD   45 (52)
T 148              46 RD   47 (53)
T 147              51 RD   52 (59)
T 112              37 RS   38 (44)
T 131              37 RG   38 (44)
T 135              36 RD   37 (43)
T 151              41 RD   42 (48)
T 143              49 RD   50 (57)
T 144              44 RN   45 (52)


No 10 
>Q6UB35_31_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=2.75  Aligned_cols=7  Identities=29%  Similarity=0.363  Sum_probs=0.0

Q 5_Mustela         1 LERDEKV    7 (7)
Q 11_Cavia          1 LERDERV    7 (7)
Q Consensus         1 lerde~v    7 (7)
                      +.++...
T Consensus        12 ~~~~~~~   18 (36)
T signal           12 LRRPPQP   18 (36)
T 44_Alligator     12 GSYDLVQ   18 (35)
T 38_Ictidomys     12 DPCDGYS   18 (35)
T 25_Myotis        12 YEKGYKR   18 (35)
T 19_Tupaia        12 VLSLLQE   18 (35)
T 39_Condylura     12 KEVLTLL   18 (35)


No 11 
>P01098_23_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.33  Aligned_cols=7  Identities=14%  Similarity=0.681  Sum_probs=0.0

Q 5_Mustela         1 LERDEKV    7 (7)
Q 11_Cavia          1 LERDERV    7 (7)
Q Consensus         1 lerde~v    7 (7)
                      +.|..|.
T Consensus         6 isrntrl   12 (28)
T signal            6 ISRNTRL   12 (28)


No 12 
>P10719_46_Mito_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.97  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         3 RD    4 (7)
Q 11_Cavia          3 RD    4 (7)
Q Consensus         3 rd    4 (7)
                      ||
T Consensus        44 Rd   45 (51)
T signal           44 RD   45 (51)
T 130              44 GD   45 (50)
T 148              46 RD   47 (52)
T 110              37 RS   38 (44)
T 131              36 RD   37 (43)
T 135              37 RG   38 (44)
T 151              41 RD   42 (48)
T 149              51 RD   52 (58)
T 144              49 RD   50 (56)
T 145              44 RN   45 (51)


No 13 
>P07926_68_MitoM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=6.39  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         3 RD    4 (7)
Q 11_Cavia          3 RD    4 (7)
Q Consensus         3 rd    4 (7)
                      ||
T Consensus        68 RD   69 (73)
T signal           68 RD   69 (73)
T 10               77 RD   78 (82)
T 14               56 --   55 (55)
T 22               38 --   37 (37)
T 9                62 CD   63 (67)
T 5                69 RD   70 (74)
T 6                63 RD   64 (68)
T 2                79 RD   80 (84)
T 3                79 RD   80 (84)
T 53               35 KD   36 (40)


No 14 
>P11325_9_MM_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.47  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |.|
T Consensus         2 lsr    4 (14)
T signal            2 LSR    4 (14)


No 15 
>P00829_48_Mito_IM_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=5.97  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         3 RD    4 (7)
Q 11_Cavia          3 RD    4 (7)
Q Consensus         3 rd    4 (7)
                      ||
T Consensus        44 Rd   45 (53)
T signal           44 RD   45 (53)
T 125              44 GD   45 (52)
T 111              37 RS   38 (45)
T 131              37 RG   38 (45)
T 136              36 RD   37 (44)
T 152              41 RD   42 (49)
T 149              46 RD   47 (54)
T 142              49 RD   50 (58)
T 151              51 RD   52 (60)
T 147              44 RN   45 (53)


No 16 
>P51557_62_Mito_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.51  Aligned_cols=7  Identities=14%  Similarity=-0.111  Sum_probs=0.0

Q 5_Mustela         1 LERDEKV    7 (7)
Q 11_Cavia          1 LERDERV    7 (7)
Q Consensus         1 lerde~v    7 (7)
                      +.++.++
T Consensus        35 ~~~~~~~   41 (67)
T signal           35 LNWRALG   41 (67)
T 64_Maylandia     35 KLKMLPA   41 (67)
T 49_Columba       35 SQELSRL   41 (67)
T 77_Phytophthor   35 EDAAART   41 (65)
T 76_Amphimedon    35 GTRSNQS   41 (65)
T cl|CABBABABA|1   34 LYEAEET   40 (60)
T cl|QABBABABA|1   34 RLEVVVE   40 (60)
T 44_Ictidomys     34 DKVLSKV   40 (59)


No 17 
>P28834_11_Mito_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.62  Aligned_cols=3  Identities=67%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |.|
T Consensus         2 lnr    4 (16)
T signal            2 LNR    4 (16)


No 18 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.72  Aligned_cols=3  Identities=67%  Similarity=0.877  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |.|
T Consensus        45 ~~~   47 (56)
T signal           45 LGR   47 (56)
T 34_Volvox        39 ---   38 (38)
T 134_Guillardia   39 ---   38 (38)
T 152_Trichinell   39 ---   38 (38)
T 159_Ixodes       39 ---   38 (38)
T 41_Galdieria     39 ---   38 (38)
T 112_Tribolium    39 ---   38 (38)
T 117_Ceratitis    39 ---   38 (38)
T 111_Musca        39 ---   38 (38)
T 39_Selaginella   39 ---   38 (38)


No 19 
>P36526_16_Mito_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=4.72  Aligned_cols=3  Identities=67%  Similarity=0.955  Sum_probs=0.0

Q 5_Mustela         1 LER    3 (7)
Q 11_Cavia          1 LER    3 (7)
Q Consensus         1 ler    3 (7)
                      |-|
T Consensus        17 LtR   19 (21)
T signal           17 LTR   19 (21)
T 11               17 LTR   19 (21)
T 13               17 LTR   19 (21)
T 14               17 LTR   19 (21)
T 18               16 LTR   18 (20)
T 20               17 LLR   19 (21)
T 5                17 LRR   19 (21)
T 7                17 LRR   19 (21)
T 8                17 LRR   19 (21)
T 9                13 LKR   15 (17)


No 20 
>P00447_26_MM_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=5.27  Aligned_cols=5  Identities=60%  Similarity=0.727  Sum_probs=0.0

Q 5_Mustela         3 RDEKV    7 (7)
Q 11_Cavia          3 RDERV    7 (7)
Q Consensus         3 rde~v    7 (7)
                      |.-||
T Consensus        24 rRTKV   28 (31)
T signal           24 RRTKV   28 (31)
T 183              19 KRTKV   23 (26)
T 185              22 KRTKV   26 (29)
T 184              25 RRTKV   29 (32)
T 176              18 KRTKV   22 (25)
T 181              19 RRTKV   23 (26)


Done!
