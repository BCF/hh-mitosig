Query         5_Mustela
Match_columns 6
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:08:12 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_18A24_18Q24_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       92.1 0.00027 6.2E-07   20.2   0.0    6    1-6      19-24  (35)
  2 Q9H300_52_IM_Nu_ref_sig5_130    20.7    0.47  0.0011   12.8   0.0    6    1-6      39-44  (57)
  3 P53077_37_Mito_ref_sig5_130      6.3     2.6  0.0059   10.2   0.0    5    1-5      11-15  (42)
  4 Q99757_59_Mito_ref_sig5_130      5.5       3   0.007   10.6   0.0    3    3-5       8-10  (64)
  5 P20069_32_MM_ref_sig5_130        5.2     3.3  0.0075    9.6   0.0    3    3-5      30-32  (37)
  6 Q9YHT1_44_IM_ref_sig5_130        5.1     3.3  0.0076    9.9   0.0    4    3-6      32-35  (49)
  7 P32445_17_Mito_ref_sig5_130      5.1     3.3  0.0077    8.8   0.0    4    3-6       8-11  (22)
  8 Q95108_59_Mito_ref_sig5_130      5.0     3.5   0.008   10.4   0.0    3    3-5       8-10  (64)
  9 Q64591_34_Mito_ref_sig5_130      4.9     3.6  0.0082    9.6   0.0    3    3-5      22-24  (39)
 10 Q93170_22_Mito_ref_sig5_130      4.5       4  0.0091    9.0   0.0    5    1-5      13-17  (27)
 11 Q0QF01_42_IM_ref_sig5_130        4.2     4.3  0.0098    9.6   0.0    4    3-6      31-34  (47)
 12 P07257_16_IM_ref_sig5_130        4.2     4.3  0.0098    8.3   0.0    3    3-5      15-17  (21)
 13 P04182_35_MM_ref_sig5_130        4.0     4.5    0.01    9.2   0.0    1    3-3      16-16  (40)
 14 P12686_37_Mito_ref_sig5_130      4.0     4.5    0.01    9.6   0.0    3    3-5      35-37  (42)
 15 P00366_57_MM_ref_sig5_130        4.0     4.5    0.01    7.3   0.0    1    3-3      51-51  (62)
 16 Q40089_21_Mito_IM_ref_sig5_130   4.0     4.6   0.011    8.7   0.0    4    2-5      18-21  (26)
 17 Q9ZP06_22_MM_ref_sig5_130        3.8     4.8   0.011    8.6   0.0    2    2-3      19-20  (27)
 18 P22068_44_Mito_IM_ref_sig5_130   3.5     5.4   0.012    9.6   0.0    3    3-5      11-13  (49)
 19 P25711_33_IM_ref_sig5_130        3.3     5.7   0.013    9.1   0.0    3    3-5      30-32  (38)
 20 P36428_49_Mito_Cy_ref_sig5_130   3.3     5.8   0.013    9.7   0.0    2    2-3      27-28  (54)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=92.10  E-value=0.00027  Score=20.24  Aligned_cols=6  Identities=83%  Similarity=1.614  Sum_probs=5.1

Q 5_Mustela         1 LRRRFH    6 (6)
Q 2_Macaca          1 LKRRFH    6 (6)
Q Consensus         1 l~rrfh    6 (6)
                      +||+||
T Consensus        19 lrR~FH   24 (35)
T signal           19 LKRRFH   24 (35)
T 143              23 PRRGFH   28 (39)
T 144              19 LKRSFH   24 (35)
T 145              19 LQREFH   24 (35)
T 147              19 LRREFH   24 (35)
T 135              28 LRSGIH   33 (44)
T 141              33 QRRGFR   38 (49)
T 142              18 GRRRFH   23 (34)
T 149              18 QRREFH   23 (34)
T 150              18 RRRHFH   23 (34)
Confidence            589998


No 2  
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=20.68  E-value=0.47  Score=12.84  Aligned_cols=6  Identities=67%  Similarity=1.143  Sum_probs=3.3

Q 5_Mustela         1 LRRRFH    6 (6)
Q 2_Macaca          1 LKRRFH    6 (6)
Q Consensus         1 l~rrfh    6 (6)
                      |-|||+
T Consensus        39 lgrRFn   44 (57)
T signal           39 LGRRFN   44 (57)
T 109              37 LGRRLN   42 (55)
T 119              39 SGRRFN   44 (57)
T 120              37 LGRRFN   42 (55)
T 129              35 LGRRFD   40 (53)
T 130              39 FGRRFT   44 (57)
T 136              39 FPCRFN   44 (57)
T 117              38 LSPRFN   43 (56)
T 116              14 SLPRFH   19 (32)
Confidence            346764


No 3  
>P53077_37_Mito_ref_sig5_130
Probab=6.31  E-value=2.6  Score=10.25  Aligned_cols=5  Identities=60%  Similarity=1.212  Sum_probs=2.3

Q 5_Mustela         1 LRRRF    5 (6)
Q 2_Macaca          1 LKRRF    5 (6)
Q Consensus         1 l~rrf    5 (6)
                      |||-|
T Consensus        11 lkrsf   15 (42)
T signal           11 LKRSF   15 (42)
Confidence            34544


No 4  
>Q99757_59_Mito_ref_sig5_130
Probab=5.52  E-value=3  Score=10.58  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 80                8 RRF   10 (62)
T 82                8 RRC   10 (64)
T 81                8 RRL   10 (65)
T 88                8 RRF   10 (64)
T 78                8 RRF   10 (66)
T 61                8 RRL   10 (47)
T 65                8 RRL   10 (43)
T 77                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            555


No 5  
>P20069_32_MM_ref_sig5_130
Probab=5.19  E-value=3.3  Score=9.65  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |+|
T Consensus        30 RqF   32 (37)
T signal           30 RRF   32 (37)
T 143              30 RQF   32 (37)
T 58               31 RRF   33 (37)
T 118              34 RQF   36 (40)
T 119              32 RRF   34 (38)
T 123              31 RQF   33 (37)
T 127              32 RGF   34 (38)
T 129              32 RQF   34 (38)
T 134              31 RQF   33 (37)
T 121              21 RRF   23 (27)
Confidence            455


No 6  
>Q9YHT1_44_IM_ref_sig5_130
Probab=5.13  E-value=3.3  Score=9.93  Aligned_cols=4  Identities=75%  Similarity=1.497  Sum_probs=1.7

Q 5_Mustela         3 RRFH    6 (6)
Q 2_Macaca          3 RRFH    6 (6)
Q Consensus         3 rrfh    6 (6)
                      |-||
T Consensus        32 R~FH   35 (49)
T signal           32 RNFH   35 (49)
T 127              26 RDFH   29 (43)
T 141              30 RGFD   33 (47)
T 160              32 RGFH   35 (49)
T 126              30 RDFH   33 (47)
T 129              29 RQLH   32 (45)
T 133              31 RNFH   34 (47)
T 140              34 RNFH   37 (51)
T 163              31 RNFH   34 (48)
T 176              27 RNFY   30 (44)
Confidence            3444


No 7  
>P32445_17_Mito_ref_sig5_130
Probab=5.12  E-value=3.3  Score=8.76  Aligned_cols=4  Identities=75%  Similarity=1.206  Sum_probs=1.7

Q 5_Mustela         3 RRFH    6 (6)
Q 2_Macaca          3 RRFH    6 (6)
Q Consensus         3 rrfh    6 (6)
                      |-||
T Consensus         8 R~fh   11 (22)
T signal            8 RFFH   11 (22)
T 7                 8 RSLH   11 (22)
T 8                 8 RFFS   11 (22)
T 12                8 RGFH   11 (22)
T 15                8 RSFH   11 (22)
T 16                7 RSFH   10 (21)
T 17                7 RSLH   10 (21)
T 11                4 RQFH    7 (18)
T 14                4 RQFH    7 (18)
T 5                 8 RAFS   11 (22)
Confidence            3344


No 8  
>Q95108_59_Mito_ref_sig5_130
Probab=4.97  E-value=3.5  Score=10.39  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 82                8 RRL   10 (65)
T 80                8 RRF   10 (62)
T 79                8 RRF   10 (66)
T 58                8 RRL   10 (47)
T 77                8 KRV   10 (67)
T 78                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            454


No 9  
>Q64591_34_Mito_ref_sig5_130
Probab=4.87  E-value=3.6  Score=9.58  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus        22 rRF   24 (39)
T signal           22 QRF   24 (39)
T 76               22 RRF   24 (39)
T 78               22 RRF   24 (39)
T 52               16 RRF   18 (32)
T 55               20 HKF   22 (36)
T 80               22 RRF   24 (38)
T 31               11 ARF   13 (28)
T 34                9 -EF   10 (25)
T 46               11 RRF   13 (28)
T 85               12 GRF   14 (29)
Confidence            455


No 10 
>Q93170_22_Mito_ref_sig5_130
Probab=4.45  E-value=4  Score=8.99  Aligned_cols=5  Identities=60%  Similarity=1.065  Sum_probs=2.2

Q 5_Mustela         1 LRRRF    5 (6)
Q 2_Macaca          1 LKRRF    5 (6)
Q Consensus         1 l~rrf    5 (6)
                      |||-|
T Consensus        13 lkrif   17 (27)
T signal           13 LKRIF   17 (27)
Confidence            34543


No 11 
>Q0QF01_42_IM_ref_sig5_130
Probab=4.22  E-value=4.3  Score=9.62  Aligned_cols=4  Identities=75%  Similarity=1.456  Sum_probs=1.6

Q 5_Mustela         3 RRFH    6 (6)
Q 2_Macaca          3 RRFH    6 (6)
Q Consensus         3 rrfh    6 (6)
                      |-||
T Consensus        31 R~FH   34 (47)
T signal           31 RSFH   34 (47)
T 124              31 RDFH   34 (47)
T 126              29 RKLH   32 (43)
T 136              34 RNFH   37 (50)
T 137              31 RNFH   34 (46)
T 144              32 RNLH   35 (48)
T 145              31 RNFH   34 (47)
T 171              28 CGFH   31 (44)
T 154              14 RKFH   17 (30)
Confidence            3344


No 12 
>P07257_16_IM_ref_sig5_130
Probab=4.22  E-value=4.3  Score=8.31  Aligned_cols=3  Identities=67%  Similarity=1.265  Sum_probs=1.4

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      ||+
T Consensus        15 R~~   17 (21)
T signal           15 RRL   17 (21)
T 20               12 RRY   14 (18)
T 34               12 RRY   14 (18)
T 14               13 RHY   15 (19)
T 15               14 RKF   16 (20)
T 23               16 RRL   18 (22)
T 19               14 RHL   16 (20)
T 40               16 RHY   18 (22)
T 42               14 RKL   16 (20)
Confidence            444


No 13 
>P04182_35_MM_ref_sig5_130
Probab=4.02  E-value=4.5  Score=9.22  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         3 R    3 (6)
Q 2_Macaca          3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        16 r   16 (40)
T signal           16 R   16 (40)
T 138              16 G   16 (40)
T 140              16 R   16 (40)
T 134              18 R   18 (42)
T 141              16 R   16 (40)
T 153              16 R   16 (40)
T 139              16 R   16 (40)
T 127              15 C   15 (39)
T 128              18 R   18 (42)
T 132              17 Q   17 (41)
Confidence            3


No 14 
>P12686_37_Mito_ref_sig5_130
Probab=4.01  E-value=4.5  Score=9.56  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.7

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus        35 rrf   37 (42)
T signal           35 RRF   37 (42)
Confidence            555


No 15 
>P00366_57_MM_ref_sig5_130
Probab=4.00  E-value=4.5  Score=7.30  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 R    3 (6)
Q 2_Macaca          3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        51 ~   51 (62)
T signal           51 R   51 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            2


No 16 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=3.98  E-value=4.6  Score=8.66  Aligned_cols=4  Identities=75%  Similarity=1.290  Sum_probs=1.8

Q 5_Mustela         2 RRRF    5 (6)
Q 2_Macaca          2 KRRF    5 (6)
Q Consensus         2 ~rrf    5 (6)
                      +|+|
T Consensus        18 rR~f   21 (26)
T signal           18 RRPF   21 (26)
T 44               18 RRPF   21 (26)
T 43               18 TRSF   21 (26)
T 40               16 TRRF   19 (23)
T 41               16 TRRF   19 (23)
T 3                18 RRSY   21 (24)
T 5                18 RRTY   21 (24)
T 10               18 RRTY   21 (24)
T 11               18 RRGY   21 (24)
Confidence            3444


No 17 
>Q9ZP06_22_MM_ref_sig5_130
Probab=3.83  E-value=4.8  Score=8.63  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.8

Q 5_Mustela         2 RR    3 (6)
Q 2_Macaca          2 KR    3 (6)
Q Consensus         2 ~r    3 (6)
                      ||
T Consensus        19 RR   20 (27)
T signal           19 RR   20 (27)
T 93               19 RR   20 (27)
T 95               19 RR   20 (27)
T 97               20 TR   21 (28)
T 91               23 RR   24 (31)
T 94               18 RR   19 (26)
T 98               18 RR   19 (26)
Confidence            34


No 18 
>P22068_44_Mito_IM_ref_sig5_130
Probab=3.48  E-value=5.4  Score=9.60  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus        11 rrf   13 (49)
T signal           11 RRF   13 (49)
Confidence            555


No 19 
>P25711_33_IM_ref_sig5_130
Probab=3.32  E-value=5.7  Score=9.06  Aligned_cols=3  Identities=33%  Similarity=0.335  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (6)
Q 2_Macaca          3 RRF    5 (6)
Q Consensus         3 rrf    5 (6)
                      |||
T Consensus        30 RR~   32 (38)
T signal           30 QRR   32 (38)
T 153              28 RRA   30 (36)
T 150              29 RRY   31 (36)
T 151              29 RRY   31 (36)
T 152              28 RRF   30 (35)
T 146              30 RRH   32 (35)
T 149              32 RRF   34 (38)
T 147              28 RRF   30 (34)
T 148              28 RRF   30 (34)
Confidence            455


No 20 
>P36428_49_Mito_Cy_ref_sig5_130
Probab=3.30  E-value=5.8  Score=9.68  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.8

Q 5_Mustela         2 RR    3 (6)
Q 2_Macaca          2 KR    3 (6)
Q Consensus         2 ~r    3 (6)
                      ||
T Consensus        27 rr   28 (54)
T signal           27 RR   28 (54)
T 193              29 RR   30 (54)
Confidence            33


Done!
