Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:41 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_76A83_76Q83_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q04467_39_Mito_ref_sig5_130     91.0 0.00048 1.1E-06   14.2   0.0    6    1-6      39-44  (44)
  2 P33198_8_Mito_ref_sig5_130      47.0   0.085  0.0002   12.1   0.0    6    1-6       8-13  (13)
  3 P38071_9_MM_ref_sig5_130         0.5      47    0.11    5.5   0.0    3    4-6       6-8   (14)
  4 P07756_38_Mito_Nu_ref_sig5_130   0.5      52    0.12    4.5   0.0    1    6-6       4-4   (43)
  5 P43265_42_IM_ref_sig5_130        0.5      53    0.12    7.0   0.0    1    3-3      34-34  (47)
  6 P28350_44_Mito_Cy_ref_sig5_130   0.5      53    0.12    7.0   0.0    1    4-4      40-40  (49)
  7 P08067_30_IM_ref_sig5_130        0.5      53    0.12    6.5   0.0    1    5-5      21-21  (35)
  8 P18155_35_Mito_ref_sig5_130      0.5      56    0.13    3.2   0.0    1    3-3      40-40  (40)
  9 P07246_27_MM_ref_sig5_130        0.5      57    0.13    5.1   0.0    1    1-1       1-1   (32)
 10 P36521_31_Mito_ref_sig5_130      0.5      57    0.13    6.5   0.0    1    2-2      18-18  (36)
 11 Q27607_9_Mito_ref_sig5_130       0.4      57    0.13    5.3   0.0    1    3-3      11-11  (14)
 12 P25284_26_MM_ref_sig5_130        0.4      59    0.13    6.2   0.0    1    5-5      25-25  (31)
 13 P36516_59_Mito_ref_sig5_130      0.4      60    0.14    7.2   0.0    1    5-5       7-7   (64)
 14 Q12349_32_Mito_IM_ref_sig5_130   0.4      60    0.14    6.5   0.0    1    5-5       8-8   (37)
 15 P07806_47_Cy_Mito_ref_sig5_130   0.4      60    0.14    6.9   0.0    1    4-4      42-42  (52)
 16 Q6SZW1_27_Cy_Mito_ref_sig5_130   0.4      61    0.14    3.2   0.0    1    3-3      32-32  (32)
 17 P53163_33_Mito_ref_sig5_130      0.4      73    0.17    6.3   0.0    1    5-5       4-4   (38)
 18 Q9HCC0_22_MM_ref_sig5_130        0.4      75    0.17    5.8   0.0    2    2-3      25-26  (27)
 19 Q96CB9_25_Mito_ref_sig5_130      0.3      75    0.17    2.5   0.0    1    3-3      30-30  (30)
 20 P01098_23_Mito_ref_sig5_130      0.3      76    0.17    5.8   0.0    3    1-3      23-25  (28)

No 1  
>Q04467_39_Mito_ref_sig5_130
Probab=90.97  E-value=0.00048  Score=14.22  Aligned_cols=6  Identities=83%  Similarity=1.381  Sum_probs=3.8

Q 5_Mustela         1 YGDKRI    6 (7)
Q Consensus         1 ygdkri    6 (7)
                      +.||||
T Consensus        39 ~~~~~~   44 (44)
T signal           39 YADKRI   44 (44)
T 92_Chlorella     39 LT----   40 (40)
T 86_Trichoplax    39 VE----   40 (40)
T 76_Bombyx        39 DG----   40 (40)
T 69_Meleagris     39 WT----   40 (40)
T 65_Latimeria     39 II----   40 (40)
T 38_Sarcophilus   39 TA----   40 (40)
Confidence            357776


No 2  
>P33198_8_Mito_ref_sig5_130
Probab=47.03  E-value=0.085  Score=12.06  Aligned_cols=6  Identities=67%  Similarity=1.287  Sum_probs=4.4

Q 5_Mustela         1 YGDKRI    6 (7)
Q Consensus         1 ygdkri    6 (7)
                      |.|.||
T Consensus         8 yadqri   13 (13)
T signal            8 YADQRI   13 (13)
Confidence            678876


No 3  
>P38071_9_MM_ref_sig5_130
Probab=0.54  E-value=47  Score=5.50  Aligned_cols=3  Identities=67%  Similarity=0.800  Sum_probs=1.4

Q 5_Mustela         4 KRI    6 (7)
Q Consensus         4 kri    6 (7)
                      ||.
T Consensus         6 KR~    8 (14)
T signal            6 KRY    8 (14)
T 173               6 KRM    8 (12)
T 174               6 KRL    8 (12)
Confidence            553


No 4  
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.49  E-value=52  Score=4.52  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         6 I    6 (7)
Q Consensus         6 i    6 (7)
                      |
T Consensus         4 i    4 (43)
T signal            4 I    4 (43)
T 79_Emiliania      4 E    4 (42)
T 77_Thalassiosi    4 Q    4 (42)
T 60_Anolis         4 L    4 (42)
T 48_Alligator      4 T    4 (42)
T 44_Papio          4 L    4 (42)
T 28_Ovis           4 L    4 (42)
T 18_Pongo          4 I    4 (42)
T cl|HABBABABA|1    4 L    4 (52)
T 72_Branchiosto    4 L    4 (41)
Confidence            2


No 5  
>P43265_42_IM_ref_sig5_130
Probab=0.49  E-value=53  Score=6.96  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 D    3 (7)
Q Consensus         3 d    3 (7)
                      |
T Consensus        34 d   34 (47)
T signal           34 D   34 (47)
Confidence            2


No 6  
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.48  E-value=53  Score=6.99  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.2

Q 5_Mustela         4 K    4 (7)
Q Consensus         4 k    4 (7)
                      |
T Consensus        40 k   40 (49)
T signal           40 K   40 (49)
Confidence            1


No 7  
>P08067_30_IM_ref_sig5_130
Probab=0.48  E-value=53  Score=6.52  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        21 R   21 (35)
T signal           21 R   21 (35)
T 190              21 R   21 (35)
T 193              20 R   20 (34)
T 189              18 R   18 (32)
T 191              18 R   18 (32)
T 192              20 R   20 (34)
T 195              19 R   19 (33)
T 194              19 R   19 (33)
Confidence            3


No 8  
>P18155_35_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=3.25  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         3 D    3 (7)
Q Consensus         3 d    3 (7)
                      -
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 95_Emiliania     40 -   39 (39)
T 93_Volvox        40 -   39 (39)
T 89_Ornithorhyn   40 -   39 (39)
T 77_Pediculus     40 -   39 (39)
T 75_Trichoplax    40 -   39 (39)
T 71_Saccoglossu   40 -   39 (39)
T 67_Monodelphis   40 -   39 (39)
T 41_Capra         40 -   39 (39)
Confidence            0


No 9  
>P07246_27_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=5.07  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         1 Y    1 (7)
Q Consensus         1 y    1 (7)
                      -
T Consensus         1 M    1 (32)
T signal            1 M    1 (32)
T 3                 1 M    1 (34)
T 12                1 M    1 (29)
T 14                1 M    1 (30)
T 60_Zymoseptori    1 M    1 (32)
T 43_Pyrenophora    1 M    1 (32)
T 29_Talaromyces    1 M    1 (32)
T 12_Zygosacchar    1 M    1 (32)
T 11_Vanderwalto    1 M    1 (32)
T 18                1 M    1 (32)
Confidence            0


No 10 
>P36521_31_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.50  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         2 G    2 (7)
Q Consensus         2 g    2 (7)
                      |
T Consensus        18 g   18 (36)
T signal           18 G   18 (36)
Confidence            2


No 11 
>Q27607_9_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=5.28  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         3 D    3 (7)
Q Consensus         3 d    3 (7)
                      .
T Consensus        11 s   11 (14)
T signal           11 S   11 (14)
Confidence            3


No 12 
>P25284_26_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=6.21  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        25 r   25 (31)
T signal           25 R   25 (31)
T 188              25 R   25 (31)
T 184              24 A   24 (30)
T 187              23 R   23 (29)
T 171              22 R   22 (27)
T 182              22 Q   22 (27)
T 164              21 R   21 (27)
T 183              21 R   21 (28)
Confidence            2


No 13 
>P36516_59_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=7.18  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus         7 r    7 (64)
T signal            7 R    7 (64)
Confidence            2


No 14 
>Q12349_32_Mito_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=6.47  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus         8 r    8 (37)
T signal            8 R    8 (37)
Confidence            3


No 15 
>P07806_47_Cy_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=6.92  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.3

Q 5_Mustela         4 K    4 (7)
Q Consensus         4 k    4 (7)
                      +
T Consensus        42 ~   42 (52)
T signal           42 V   42 (52)
T 176              19 K   19 (29)
Confidence            3


No 16 
>Q6SZW1_27_Cy_Mito_ref_sig5_130
Probab=0.43  E-value=61  Score=3.19  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 D    3 (7)
Q Consensus         3 d    3 (7)
                      .
T Consensus        32 ~   32 (32)
T signal           32 G   32 (32)
T 93_Amphimedon    32 I   32 (32)
T 87_Branchiosto   32 V   32 (32)
T 83_Aplysia       32 N   32 (32)
T 81_Aedes         32 G   32 (32)
T 61_Chrysemys     32 L   32 (32)
T 57_Ficedula      32 I   32 (32)
T 52_Falco         32 V   32 (32)
Confidence            0


No 17 
>P53163_33_Mito_ref_sig5_130
Probab=0.36  E-value=73  Score=6.26  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus         4 R    4 (38)
T signal            4 R    4 (38)
T 169               4 R    4 (32)
T 170               4 R    4 (37)
Confidence            3


No 18 
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=5.77  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=1.0

Q 5_Mustela         2 GD    3 (7)
Q Consensus         2 gd    3 (7)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 19 
>Q96CB9_25_Mito_ref_sig5_130
Probab=0.35  E-value=75  Score=2.55  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         3 D    3 (7)
Q Consensus         3 d    3 (7)
                      .
T Consensus        30 ~   30 (30)
T signal           30 K   30 (30)
T 100_Pediculus    30 F   30 (30)
T 96_Nematostell   30 V   30 (30)
T 80_Bombus        30 L   30 (30)
T 78_Aedes         30 T   30 (30)
T 63_Columba       30 H   30 (30)
Confidence            0


No 20 
>P01098_23_Mito_ref_sig5_130
Probab=0.35  E-value=76  Score=5.82  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=1.3

Q 5_Mustela         1 YGD    3 (7)
Q Consensus         1 ygd    3 (7)
                      |.|
T Consensus        23 ysd   25 (28)
T signal           23 YSD   25 (28)
Confidence            444


Done!
