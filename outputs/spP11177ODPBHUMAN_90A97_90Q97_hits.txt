Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:02 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_90A97_90Q97_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P37841_53_IM_ref_sig5_130        0.9      26   0.061    8.0   0.0    3    1-3      52-54  (58)
  2 P23709_38_IM_ref_sig5_130        0.9      28   0.064    7.7   0.0    1    4-4      18-18  (43)
  3 P08559_29_MM_ref_sig5_130        0.8      30    0.07    3.8   0.0    1    3-3      34-34  (34)
  4 P80433_25_IM_ref_sig5_130        0.8      31   0.071    7.0   0.0    1    4-4      14-14  (30)
  5 P17783_27_MM_ref_sig5_130        0.8      31   0.071    7.1   0.0    3    1-3      26-28  (32)
  6 Q9M7T0_30_MM_ref_sig5_130        0.8      32   0.074    7.2   0.0    1    2-2      30-30  (35)
  7 P01096_25_Mito_ref_sig5_130      0.8      33   0.075    6.9   0.0    2    1-2      24-25  (30)
  8 Q6UPE0_34_IM_ref_sig5_130        0.8      33   0.075    3.7   0.0    1    3-3      39-39  (39)
  9 Q09544_18_Mito_IM_ref_sig5_130   0.7      33   0.076    6.6   0.0    1    3-3      18-18  (23)
 10 Q9UII2_25_Mito_ref_sig5_130      0.7      36   0.083    6.8   0.0    2    1-2      24-25  (30)
 11 Q91YT0_20_IM_ref_sig5_130        0.6      39   0.089    6.5   0.0    3    2-4      20-22  (25)
 12 P10176_25_IM_ref_sig5_130        0.6      39   0.089    6.7   0.0    1    4-4      14-14  (30)
 13 P35571_42_Mito_ref_sig5_130      0.6      39    0.09    5.0   0.0    1    3-3      47-47  (47)
 14 O75489_36_IM_ref_sig5_130        0.6      41   0.094    7.1   0.0    1    4-4      16-16  (41)
 15 P36542_25_Mito_IM_ref_sig5_130   0.6      45     0.1    6.5   0.0    1    4-4       9-9   (30)
 16 Q99797_35_MM_ref_sig5_130        0.5      50    0.11    6.8   0.0    1    4-4      30-30  (40)
 17 O46419_25_Mito_ref_sig5_130      0.5      51    0.12    6.3   0.0    1    2-2      24-24  (30)
 18 P05626_35_Mito_IM_ref_sig5_130   0.5      53    0.12    6.7   0.0    1    4-4       9-9   (40)
 19 Q9Z0J5_36_Mito_ref_sig5_130      0.4      57    0.13    3.9   0.0    1    3-3      41-41  (41)
 20 P22354_18_Mito_ref_sig5_130      0.4      58    0.13    5.9   0.0    1    4-4      15-15  (23)

No 1  
>P37841_53_IM_ref_sig5_130
Probab=0.91  E-value=26  Score=8.02  Aligned_cols=3  Identities=67%  Similarity=1.630  Sum_probs=1.5

Q 5_Mustela         1 GFA    3 (7)
Q Consensus         1 gfa    3 (7)
                      ||+
T Consensus        52 GFs   54 (58)
T signal           52 GFS   54 (58)
T 193              59 GFS   61 (65)
T 194              59 GFS   61 (64)
T 190              66 GFS   68 (72)
T 181              59 GFS   61 (63)
T 188              61 GFS   63 (65)
T 180              39 ---   38 (38)
T 184              60 GFA   62 (66)
T 182              61 GFA   63 (67)
Confidence            454


No 2  
>P23709_38_IM_ref_sig5_130
Probab=0.86  E-value=28  Score=7.67  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        18 G   18 (43)
T signal           18 G   18 (43)
T 202              15 G   15 (40)
T 205              15 G   15 (40)
T 217              15 R   15 (40)
T 220              15 G   15 (40)
T 192              15 G   15 (40)
T 196              15 G   15 (40)
T 197              15 G   15 (39)
T 225              15 G   15 (40)
T 191              16 G   16 (41)
Confidence            2


No 3  
>P08559_29_MM_ref_sig5_130
Probab=0.80  E-value=30  Score=3.84  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      |
T Consensus        34 ~   34 (34)
T signal           34 A   34 (34)
T 85_Trichinella   34 -   33 (33)
T 81_Apis          34 -   33 (33)
T 76_Trichoplax    34 -   33 (33)
T 55_Pelodiscus    34 -   33 (33)
T 44_Capra         34 -   33 (33)
T 39_Ailuropoda    34 -   33 (33)
Confidence            0


No 4  
>P80433_25_IM_ref_sig5_130
Probab=0.78  E-value=31  Score=7.03  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        14 g   14 (30)
T signal           14 G   14 (30)
T 3                14 G   14 (30)
T 20               14 A   14 (30)
T 22               14 G   14 (30)
T 31               14 G   14 (30)
T 36               14 G   14 (30)
T 5                14 G   14 (30)
T 8                14 G   14 (30)
T 19               14 A   14 (30)
Confidence            3


No 5  
>P17783_27_MM_ref_sig5_130
Probab=0.78  E-value=31  Score=7.07  Aligned_cols=3  Identities=67%  Similarity=1.088  Sum_probs=1.4

Q 5_Mustela         1 GFA    3 (7)
Q Consensus         1 gfa    3 (7)
                      |||
T Consensus        26 ~fa   28 (32)
T signal           26 SFA   28 (32)
T 120              29 SYC   31 (35)
T 121              21 GYA   23 (27)
T 123              21 SFS   23 (27)
T 118              21 GFA   23 (27)
T 122              24 GYA   26 (30)
T 125              25 GYA   27 (31)
Confidence            354


No 6  
>Q9M7T0_30_MM_ref_sig5_130
Probab=0.76  E-value=32  Score=7.20  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.3

Q 5_Mustela         2 F    2 (7)
Q Consensus         2 f    2 (7)
                      |
T Consensus        30 f   30 (35)
T signal           30 F   30 (35)
Confidence            2


No 7  
>P01096_25_Mito_ref_sig5_130
Probab=0.75  E-value=33  Score=6.90  Aligned_cols=2  Identities=100%  Similarity=2.261  Sum_probs=0.8

Q 5_Mustela         1 GF    2 (7)
Q Consensus         1 gf    2 (7)
                      ||
T Consensus        24 GF   25 (30)
T signal           24 GF   25 (30)
T 26               24 GF   25 (30)
T 48               24 GF   25 (30)
T 60               24 GF   25 (30)
T 69               24 GF   25 (30)
T 38               24 GF   25 (30)
T 42               24 GF   25 (30)
T 40               24 GF   25 (30)
T 49               24 GF   25 (30)
T 23               24 GF   25 (29)
Confidence            34


No 8  
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.75  E-value=33  Score=3.73  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        39 ~   39 (39)
T signal           39 A   39 (39)
T 99_Culex         38 -   37 (37)
T 93_Megachile     38 -   37 (37)
T 68_Ciona         38 -   37 (37)
T 66_Xiphophorus   38 -   37 (37)
T 61_Oryzias       38 -   37 (37)
Confidence            0


No 9  
>Q09544_18_Mito_IM_ref_sig5_130
Probab=0.74  E-value=33  Score=6.57  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      |
T Consensus        18 a   18 (23)
T signal           18 A   18 (23)
Confidence            2


No 10 
>Q9UII2_25_Mito_ref_sig5_130
Probab=0.69  E-value=36  Score=6.78  Aligned_cols=2  Identities=100%  Similarity=2.261  Sum_probs=0.8

Q 5_Mustela         1 GF    2 (7)
Q Consensus         1 gf    2 (7)
                      ||
T Consensus        24 GF   25 (30)
T signal           24 GF   25 (30)
T 61               24 GF   25 (29)
T 41               24 GF   25 (30)
T 43               24 GF   25 (30)
T 48               24 GF   25 (30)
T 24               24 GF   25 (29)
T 57               21 GF   22 (26)
T 22               24 GF   25 (30)
T 72               24 GI   25 (30)
Confidence            34


No 11 
>Q91YT0_20_IM_ref_sig5_130
Probab=0.64  E-value=39  Score=6.48  Aligned_cols=3  Identities=33%  Similarity=0.944  Sum_probs=1.1

Q 5_Mustela         2 FAG    4 (7)
Q Consensus         2 fag    4 (7)
                      |+|
T Consensus        20 ~sg   22 (25)
T signal           20 FSS   22 (25)
T 132              22 AAG   24 (29)
T 93               20 FSG   22 (25)
Confidence            333


No 12 
>P10176_25_IM_ref_sig5_130
Probab=0.64  E-value=39  Score=6.74  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        14 g   14 (30)
T signal           14 G   14 (30)
T 6                14 A   14 (30)
T 18               14 G   14 (30)
T 23               14 G   14 (30)
T 29               14 G   14 (30)
T 38               14 G   14 (30)
T 2                14 R   14 (31)
T 3                14 G   14 (30)
T 5                14 G   14 (30)
T 27               14 A   14 (30)
Confidence            3


No 13 
>P35571_42_Mito_ref_sig5_130
Probab=0.64  E-value=39  Score=5.03  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


No 14 
>O75489_36_IM_ref_sig5_130
Probab=0.61  E-value=41  Score=7.10  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        16 G   16 (41)
T signal           16 G   16 (41)
T 206              14 G   14 (39)
T 214              14 R   14 (39)
T 221              14 G   14 (39)
T 192              14 G   14 (39)
T 194              14 G   14 (39)
T 201              14 G   14 (39)
T 203              14 G   14 (39)
T 193              14 G   14 (39)
Confidence            2


No 15 
>P36542_25_Mito_IM_ref_sig5_130
Probab=0.56  E-value=45  Score=6.51  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus         9 ~    9 (30)
T signal            9 G    9 (30)
T 131              11 G   11 (32)
T 115               5 -    4 (25)
T 129               5 -    4 (27)
T 130               6 -    5 (26)
T 125               5 -    4 (25)
T 78                9 A    9 (30)
T 108               6 -    5 (23)
T 66                8 V    8 (27)
Confidence            2


No 16 
>Q99797_35_MM_ref_sig5_130
Probab=0.51  E-value=50  Score=6.78  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        30 g   30 (40)
T signal           30 G   30 (40)
T 102              30 G   30 (40)
T 164              30 D   30 (40)
T 180              30 G   30 (40)
T 183              30 G   30 (40)
T 122              29 G   29 (39)
T 158              30 -   29 (37)
T 168              30 R   30 (40)
T 174              30 R   30 (40)
T 155              33 -   32 (41)
Confidence            2


No 17 
>O46419_25_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=6.27  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.3

Q 5_Mustela         2 F    2 (7)
Q Consensus         2 f    2 (7)
                      |
T Consensus        24 F   24 (30)
T signal           24 F   24 (30)
T 118              24 F   24 (29)
T 119              24 F   24 (29)
T 120              24 F   24 (29)
T 122              24 F   24 (29)
T 123              24 F   24 (29)
T 131              24 F   24 (29)
T 133              24 F   24 (29)
T 107              24 F   24 (30)
T 117              23 F   23 (28)
Confidence            2


No 18 
>P05626_35_Mito_IM_ref_sig5_130
Probab=0.48  E-value=53  Score=6.71  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      +
T Consensus         9 ~    9 (40)
T signal            9 G    9 (40)
T 47                5 S    5 (34)
T 43                5 A    5 (32)
T 45                5 A    5 (32)
T 48                5 S    5 (32)
T 49                5 A    5 (32)
Confidence            1


No 19 
>Q9Z0J5_36_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=3.91  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        41 ~   41 (41)
T signal           41 N   41 (41)
T 85_Bombus        35 -   34 (34)
T 58_Orcinus       35 -   34 (34)
T 44_Chrysemys     35 -   34 (34)
T 30_Bos           35 -   34 (34)
Confidence            0


No 20 
>P22354_18_Mito_ref_sig5_130
Probab=0.45  E-value=58  Score=5.91  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        15 g   15 (23)
T signal           15 G   15 (23)
Confidence            3


Done!
