Query         5_Mustela
Match_columns 7
No_of_seqs    4 out of 20
Neff          1.3 
Searched_HMMs 436
Date          Wed Sep  6 16:08:10 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_16A23_16Q23_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       80.0  0.0061 1.4E-05   17.2   0.0    7    1-7      17-23  (35)
  2 Q9H300_52_IM_Nu_ref_sig5_130    40.8    0.12 0.00028   15.0   0.0    6    2-7      38-43  (57)
  3 Q99757_59_Mito_ref_sig5_130     27.8    0.28 0.00064   14.0   0.0    6    2-7       5-10  (64)
  4 Q95108_59_Mito_ref_sig5_130     26.2    0.31 0.00071   13.8   0.0    6    2-7       5-10  (64)
  5 Q02380_46_IM_ref_sig5_130       22.7     0.4 0.00092   13.2   0.0    7    1-7      31-37  (51)
  6 P40360_13_Mito_ref_sig5_130      8.0     1.9  0.0043    9.3   0.0    6    2-7       5-10  (18)
  7 Q9ZP06_22_MM_ref_sig5_130        6.2     2.6   0.006    9.6   0.0    3    3-5      18-20  (27)
  8 P52903_26_MM_ref_sig5_130        5.9     2.8  0.0064    9.8   0.0    4    3-6      22-25  (31)
  9 P43265_42_IM_ref_sig5_130        5.8     2.9  0.0066   10.6   0.0    5    3-7      13-17  (47)
 10 Q0QF01_42_IM_ref_sig5_130        5.7     2.9  0.0066   10.4   0.0    4    2-5      10-13  (47)
 11 P12063_32_MM_ref_sig5_130        5.4     3.1  0.0072   10.1   0.0    1    5-5      35-35  (37)
 12 P17783_27_MM_ref_sig5_130        5.3     3.2  0.0073    9.8   0.0    4    2-5      22-25  (32)
 13 P35434_22_Mito_IM_ref_sig5_130   5.0     3.5  0.0079    9.4   0.0    4    2-5       6-9   (27)
 14 P12007_30_MM_ref_sig5_130        4.9     3.5   0.008    9.8   0.0    5    2-6       7-11  (35)
 15 P53077_37_Mito_ref_sig5_130      4.9     3.6  0.0082   10.2   0.0    5    3-7      11-15  (42)
 16 P17695_35_Cy_Mito_ref_sig5_130   4.6     3.8  0.0087   10.0   0.0    4    4-7      26-29  (40)
 17 Q91YT0_20_IM_ref_sig5_130        4.3     4.1  0.0095    9.1   0.0    3    4-6       4-6   (25)
 18 P21801_20_IM_ref_sig5_130        4.2     4.3  0.0098    9.0   0.0    3    3-5       6-8   (25)
 19 Q64591_34_Mito_ref_sig5_130      4.2     4.3  0.0099    9.7   0.0    3    5-7      22-24  (39)
 20 Q42777_22_MM_ref_sig5_130        4.2     4.3  0.0099    9.2   0.0    4    2-5       6-9   (27)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=80.03  E-value=0.0061  Score=17.23  Aligned_cols=7  Identities=86%  Similarity=1.602  Sum_probs=5.1

Q 5_Mustela         1 GLLRRRF    7 (7)
Q 2_Macaca          1 GLLKRRF    7 (7)
Q 18_Ceratotheri    1 GFLRRRF    7 (7)
Q 4_Pongo           1 RLLKRRF    7 (7)
Q Consensus         1 gll~rrf    7 (7)
                      +++||+|
T Consensus        17 ~~lrR~F   23 (35)
T signal           17 GLLKRRF   23 (35)
T 143              21 GLPRRGF   27 (39)
T 144              17 VFLKRSF   23 (35)
T 145              17 VVLQREF   23 (35)
T 147              17 VVLRREF   23 (35)
T 135              26 LQLRSGI   32 (44)
T 141              31 LQQRRGF   37 (49)
T 142              16 VLGRRRF   22 (34)
T 149              16 ALQRREF   22 (34)
T 150              16 AVRRRHF   22 (34)
Confidence            4678887


No 2  
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=40.78  E-value=0.12  Score=14.96  Aligned_cols=6  Identities=83%  Similarity=1.298  Sum_probs=4.2

Q 5_Mustela         2 LLRRRF    7 (7)
Q 2_Macaca          2 LLKRRF    7 (7)
Q 18_Ceratotheri    2 FLRRRF    7 (7)
Q 4_Pongo           2 LLKRRF    7 (7)
Q Consensus         2 ll~rrf    7 (7)
                      ||-|||
T Consensus        38 llgrRF   43 (57)
T signal           38 LLGRRF   43 (57)
T 109              36 LLGRRL   41 (55)
T 119              38 LSGRRF   43 (57)
T 120              36 LLGRRF   41 (55)
T 129              34 LLGRRF   39 (53)
T 130              38 LFGRRF   43 (57)
T 136              38 IFPCRF   43 (57)
T 117              37 LLSPRF   42 (56)
T 116              13 FSLPRF   18 (32)
Confidence            567776


No 3  
>Q99757_59_Mito_ref_sig5_130
Probab=27.79  E-value=0.28  Score=13.99  Aligned_cols=6  Identities=83%  Similarity=1.231  Sum_probs=3.4

Q 5_Mustela         2 LLRRRF    7 (7)
Q 2_Macaca          2 LLKRRF    7 (7)
Q 18_Ceratotheri    2 FLRRRF    7 (7)
Q 4_Pongo           2 LLKRRF    7 (7)
Q Consensus         2 ll~rrf    7 (7)
                      ||-|||
T Consensus         5 LlLRRf   10 (64)
T signal            5 LLLRRF   10 (64)
T 80                5 MVLRRF   10 (62)
T 82                5 LLLRRC   10 (64)
T 81                5 LFLRRL   10 (65)
T 88                5 LLLRRF   10 (64)
T 78                5 LVLRRF   10 (66)
T 61                5 LALRRL   10 (47)
T 65                5 LALRRL   10 (43)
T 77                5 LALRRL   10 (70)
T 76                5 L-LRPF    9 (68)
Confidence            455665


No 4  
>Q95108_59_Mito_ref_sig5_130
Probab=26.22  E-value=0.31  Score=13.82  Aligned_cols=6  Identities=83%  Similarity=1.231  Sum_probs=3.3

Q 5_Mustela         2 LLRRRF    7 (7)
Q 2_Macaca          2 LLKRRF    7 (7)
Q 18_Ceratotheri    2 FLRRRF    7 (7)
Q 4_Pongo           2 LLKRRF    7 (7)
Q Consensus         2 ll~rrf    7 (7)
                      ||-|||
T Consensus         5 LlLRRf   10 (64)
T signal            5 LLLRRF   10 (64)
T 82                5 LFLRRL   10 (65)
T 80                5 LVLRRF   10 (62)
T 79                5 LVLRRF   10 (66)
T 58                5 LALRRL   10 (47)
T 77                5 LFLKRV   10 (67)
T 78                5 LALRRL   10 (70)
T 76                5 L-LRPF    9 (68)
Confidence            455665


No 5  
>Q02380_46_IM_ref_sig5_130
Probab=22.70  E-value=0.4  Score=13.19  Aligned_cols=7  Identities=57%  Similarity=1.132  Sum_probs=3.2

Q 5_Mustela         1 G...LLRRRF..    7 (7)
Q 2_Macaca          1 G...LLKRRF..    7 (7)
Q 18_Ceratotheri    1 G...FLRRRF..    7 (7)
Q 4_Pongo           1 R...LLKRRF..    7 (7)
Q Consensus         1 g...ll~rrf..    7 (7)
                      |   ||.|+|  
T Consensus        31 g...fl~r~~..   37 (51)
T signal           31 G...FLTRDF..   37 (51)
T 55               31 G...FLTRSF..   37 (51)
T 59               31 G...LLARGL..   37 (51)
T 51               35 G...LLTRGI..   41 (57)
T 94               31 G...LLRRSV..   37 (42)
T 49               34 G...--ASRL..   38 (52)
T 46               27 GrvsLLGRNVvs   38 (51)
T 42               41 G...I-APRL..   46 (60)
T 24               28 R...FPLLGF..   34 (44)
T 40               28 C...VLRKSL..   34 (48)
Confidence            3   455543  


No 6  
>P40360_13_Mito_ref_sig5_130
Probab=7.95  E-value=1.9  Score=9.26  Aligned_cols=6  Identities=0%  Similarity=0.401  Sum_probs=3.1

Q 5_Mustela         2 LLR.RRF    7 (7)
Q 2_Macaca          2 LLK.RRF    7 (7)
Q 18_Ceratotheri    2 FLR.RRF    7 (7)
Q 4_Pongo           2 LLK.RRF    7 (7)
Q Consensus         2 ll~.rrf    7 (7)
                      +|. +||
T Consensus         5 ll~.~~~   10 (18)
T signal            5 IFA.HEL   10 (18)
T 54                5 IFS.NGL   10 (18)
T 53                5 ALN.RGF   10 (18)
T 52                5 LLG.QRL   10 (17)
T 50                5 LFK.HGF   10 (17)
T 51                5 LLS.GGL   10 (18)
T 49                5 LLSgARF   11 (19)
Confidence            455 554


No 7  
>Q9ZP06_22_MM_ref_sig5_130
Probab=6.17  E-value=2.6  Score=9.62  Aligned_cols=3  Identities=67%  Similarity=1.354  Sum_probs=1.4

Q 5_Mustela         3 LRR    5 (7)
Q 2_Macaca          3 LKR    5 (7)
Q 18_Ceratotheri    3 LRR    5 (7)
Q 4_Pongo           3 LKR    5 (7)
Q Consensus         3 l~r    5 (7)
                      |||
T Consensus        18 lRR   20 (27)
T signal           18 IRR   20 (27)
T 93               18 SRR   20 (27)
T 95               18 LRR   20 (27)
T 97               19 LTR   21 (28)
T 91               22 LRR   24 (31)
T 94               18 -RR   19 (26)
T 98               17 LRR   19 (26)
Confidence            444


No 8  
>P52903_26_MM_ref_sig5_130
Probab=5.93  E-value=2.8  Score=9.82  Aligned_cols=4  Identities=50%  Similarity=0.667  Sum_probs=1.6

Q 5_Mustela         3 LRRR    6 (7)
Q 2_Macaca          3 LKRR    6 (7)
Q 18_Ceratotheri    3 LRRR    6 (7)
Q 4_Pongo           3 LKRR    6 (7)
Q Consensus         3 l~rr    6 (7)
                      |+|+
T Consensus        22 lrR~   25 (31)
T signal           22 ATRR   25 (31)
T 105              25 LRRQ   28 (33)
T 113              26 RHRS   29 (35)
T 114              28 LNRP   31 (37)
T 117              26 LRRR   29 (35)
T 104              25 LRRS   28 (34)
T 111              31 LRRP   34 (38)
T 108              28 LRRS   31 (36)
T 116              27 LRSP   30 (35)
T 109              23 --RL   24 (29)
Confidence            3443


No 9  
>P43265_42_IM_ref_sig5_130
Probab=5.77  E-value=2.9  Score=10.64  Aligned_cols=5  Identities=80%  Similarity=1.145  Sum_probs=2.4

Q 5_Mustela         3 LRRRF    7 (7)
Q 2_Macaca          3 LKRRF    7 (7)
Q 18_Ceratotheri    3 LRRRF    7 (7)
Q 4_Pongo           3 LKRRF    7 (7)
Q Consensus         3 l~rrf    7 (7)
                      |-|||
T Consensus        13 lfrrf   17 (47)
T signal           13 LFRRF   17 (47)
Confidence            34555


No 10 
>Q0QF01_42_IM_ref_sig5_130
Probab=5.74  E-value=2.9  Score=10.43  Aligned_cols=4  Identities=75%  Similarity=1.007  Sum_probs=1.6

Q 5_Mustela         2 LLRR    5 (7)
Q 2_Macaca          2 LLKR    5 (7)
Q 18_Ceratotheri    2 FLRR    5 (7)
Q 4_Pongo           2 LLKR    5 (7)
Q Consensus         2 ll~r    5 (7)
                      +|++
T Consensus        10 lL~~   13 (47)
T signal           10 LLRA   13 (47)
T 124              10 VVAK   13 (47)
T 126              10 VLTK   13 (43)
T 136              11 LLSR   14 (50)
T 137              10 LLSK   13 (46)
T 144              10 GLAR   13 (48)
T 145               7 VVCR   10 (47)
T 171               1 ----    0 (44)
T 154               1 ----    0 (30)
Confidence            3443


No 11 
>P12063_32_MM_ref_sig5_130
Probab=5.37  E-value=3.1  Score=10.12  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 2_Macaca          5 R    5 (7)
Q 18_Ceratotheri    5 R    5 (7)
Q 4_Pongo           5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        35 r   35 (37)
T signal           35 R   35 (37)
T 193              35 R   35 (37)
Confidence            3


No 12 
>P17783_27_MM_ref_sig5_130
Probab=5.33  E-value=3.2  Score=9.77  Aligned_cols=4  Identities=75%  Similarity=1.041  Sum_probs=1.7

Q 5_Mustela         2 LLRR    5 (7)
Q 2_Macaca          2 LLKR    5 (7)
Q 18_Ceratotheri    2 FLRR    5 (7)
Q 4_Pongo           2 LLKR    5 (7)
Q Consensus         2 ll~r    5 (7)
                      +|||
T Consensus        22 llrR   25 (32)
T signal           22 LLSR   25 (32)
T 120              25 LLRR   28 (35)
T 121              17 ILRR   20 (27)
T 123              17 VIRR   20 (27)
T 118              17 GRRR   20 (27)
T 122              20 LVRR   23 (30)
T 125              21 LLRR   24 (31)
Confidence            3444


No 13 
>P35434_22_Mito_IM_ref_sig5_130
Probab=4.98  E-value=3.5  Score=9.36  Aligned_cols=4  Identities=75%  Similarity=1.107  Sum_probs=1.6

Q 5_Mustela         2 LLRR    5 (7)
Q 2_Macaca          2 LLKR    5 (7)
Q 18_Ceratotheri    2 FLRR    5 (7)
Q 4_Pongo           2 LLKR    5 (7)
Q Consensus         2 ll~r    5 (7)
                      +|+|
T Consensus         6 ~L~r    9 (27)
T signal            6 LLRH    9 (27)
T 160               6 LLRR    9 (27)
T 152               6 VFRR    9 (27)
T 140               5 VLRR    8 (25)
T 143               5 ALLR    8 (33)
T 132               6 LLLA    9 (34)
T 137               6 LLAR    9 (29)
T 121               6 FLLR    9 (25)
T 127               6 FL-R    8 (24)
T 120               6 ALFA    9 (26)
Confidence            3443


No 14 
>P12007_30_MM_ref_sig5_130
Probab=4.94  E-value=3.5  Score=9.84  Aligned_cols=5  Identities=80%  Similarity=1.092  Sum_probs=2.1

Q 5_Mustela         2 LLRRR    6 (7)
Q 2_Macaca          2 LLKRR    6 (7)
Q 18_Ceratotheri    2 FLRRR    6 (7)
Q 4_Pongo           2 LLKRR    6 (7)
Q Consensus         2 ll~rr    6 (7)
                      +|-||
T Consensus         7 llg~r   11 (35)
T signal            7 LLGRR   11 (35)
T 106               7 ILGRR   11 (37)
T 129               6 ---WR    7 (34)
T 134               7 LLGWR   11 (34)
T 136               7 RLGSS   11 (37)
T 141               7 LLGRR   11 (37)
T 122               7 LVCAR   11 (25)
Confidence            34444


No 15 
>P53077_37_Mito_ref_sig5_130
Probab=4.87  E-value=3.6  Score=10.18  Aligned_cols=5  Identities=60%  Similarity=1.212  Sum_probs=2.4

Q 5_Mustela         3 LRRRF    7 (7)
Q 2_Macaca          3 LKRRF    7 (7)
Q 18_Ceratotheri    3 LRRRF    7 (7)
Q 4_Pongo           3 LKRRF    7 (7)
Q Consensus         3 l~rrf    7 (7)
                      |||-|
T Consensus        11 lkrsf   15 (42)
T signal           11 LKRSF   15 (42)
Confidence            45544


No 16 
>P17695_35_Cy_Mito_ref_sig5_130
Probab=4.62  E-value=3.8  Score=10.01  Aligned_cols=4  Identities=50%  Similarity=1.148  Sum_probs=1.8

Q 5_Mustela         4 RRRF    7 (7)
Q 2_Macaca          4 KRRF    7 (7)
Q 18_Ceratotheri    4 RRRF    7 (7)
Q 4_Pongo           4 KRRF    7 (7)
Q Consensus         4 ~rrf    7 (7)
                      -+||
T Consensus        26 skRf   29 (40)
T signal           26 AKRF   29 (40)
T 42               17 SQKF   20 (33)
T 41               15 SKRF   18 (31)
T 40               15 TRRL   18 (33)
Confidence            3444


No 17 
>Q91YT0_20_IM_ref_sig5_130
Probab=4.33  E-value=4.1  Score=9.07  Aligned_cols=3  Identities=33%  Similarity=0.523  Sum_probs=1.1

Q 5_Mustela         4 RRR    6 (7)
Q 2_Macaca          4 KRR    6 (7)
Q 18_Ceratotheri    4 RRR    6 (7)
Q 4_Pongo           4 KRR    6 (7)
Q Consensus         4 ~rr    6 (7)
                      -|+
T Consensus         4 AR~    6 (25)
T signal            4 ARH    6 (25)
T 132               4 ARP    6 (29)
T 93                4 ARR    6 (25)
Confidence            343


No 18 
>P21801_20_IM_ref_sig5_130
Probab=4.21  E-value=4.3  Score=9.04  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.3

Q 5_Mustela         3 LRR    5 (7)
Q 2_Macaca          3 LKR    5 (7)
Q 18_Ceratotheri    3 LRR    5 (7)
Q 4_Pongo           3 LKR    5 (7)
Q Consensus         3 l~r    5 (7)
                      |+|
T Consensus         6 lrR    8 (25)
T signal            6 LRR    8 (25)
T 192               2 LRR    4 (23)
T 191               2 IQR    4 (21)
Confidence            444


No 19 
>Q64591_34_Mito_ref_sig5_130
Probab=4.17  E-value=4.3  Score=9.68  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.3

Q 5_Mustela         5 RRF    7 (7)
Q 2_Macaca          5 RRF    7 (7)
Q 18_Ceratotheri    5 RRF    7 (7)
Q 4_Pongo           5 RRF    7 (7)
Q Consensus         5 rrf    7 (7)
                      |||
T Consensus        22 rRF   24 (39)
T signal           22 QRF   24 (39)
T 76               22 RRF   24 (39)
T 78               22 RRF   24 (39)
T 52               16 RRF   18 (32)
T 55               20 HKF   22 (36)
T 80               22 RRF   24 (38)
T 31               11 ARF   13 (28)
T 34                9 -EF   10 (25)
T 46               11 RRF   13 (28)
T 85               12 GRF   14 (29)
Confidence            344


No 20 
>Q42777_22_MM_ref_sig5_130
Probab=4.17  E-value=4.3  Score=9.19  Aligned_cols=4  Identities=100%  Similarity=1.448  Sum_probs=1.7

Q 5_Mustela         2 LLRR......    5 (7)
Q 2_Macaca          2 LLKR......    5 (7)
Q 18_Ceratotheri    2 FLRR......    5 (7)
Q 4_Pongo           2 LLKR......    5 (7)
Q Consensus         2 ll~r......    5 (7)
                      ||||      
T Consensus         6 llrr......    9 (27)
T signal            6 LLRR......    9 (27)
T 8                 6 LLRRkliitr   15 (33)
Confidence            3444      


Done!
