Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:42 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_77A84_77Q84_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36532_24_Mito_ref_sig5_130      0.5      57    0.13    6.2   0.0    2    4-5      15-16  (29)
  2 Q12349_32_Mito_IM_ref_sig5_130   0.4      61    0.14    6.5   0.0    1    4-4       8-8   (37)
  3 P28350_44_Mito_Cy_ref_sig5_130   0.4      61    0.14    6.8   0.0    1    4-4      41-41  (49)
  4 P07246_27_MM_ref_sig5_130        0.4      61    0.14    5.0   0.0    1    3-3      32-32  (32)
  5 Q06645_61_MitoM_ref_sig5_130     0.4      64    0.15    6.4   0.0    1    1-1       1-1   (66)
  6 P53163_33_Mito_ref_sig5_130      0.4      66    0.15    6.4   0.0    1    5-5       5-5   (38)
  7 P92507_25_IM_ref_sig5_130        0.4      66    0.15    6.1   0.0    2    4-5      14-15  (30)
  8 P25284_26_MM_ref_sig5_130        0.4      68    0.16    6.0   0.0    1    4-4      25-25  (31)
  9 P36516_59_Mito_ref_sig5_130      0.4      71    0.16    6.9   0.0    1    4-4       7-7   (64)
 10 P21642_33_Mito_ref_sig5_130      0.4      71    0.16    6.3   0.0    1    6-6      15-15  (38)
 11 Q02253_32_Mito_ref_sig5_130      0.4      73    0.17    6.2   0.0    3    4-6      14-16  (37)
 12 Q9HCC0_22_MM_ref_sig5_130        0.4      74    0.17    5.8   0.0    2    1-2      25-26  (27)
 13 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   0.3      77    0.18    5.8   0.0    2    1-2      24-25  (28)
 14 P26267_25_MM_ref_sig5_130        0.3      80    0.18    5.8   0.0    1    5-5       8-8   (30)
 15 P07919_13_IM_ref_sig5_130        0.3      84    0.19    5.0   0.0    2    1-2      14-15  (18)
 16 Q8K2C6_36_Mito_ref_sig5_130      0.3      86     0.2    3.2   0.0    1    3-3      41-41  (41)
 17 P99028_13_IM_ref_sig5_130        0.3      96    0.22    5.0   0.0    2    1-2      14-15  (18)
 18 P38077_33_Mito_IM_ref_sig5_130   0.3      98    0.22    5.8   0.0    2    4-5       4-5   (38)
 19 P36531_14_Mito_ref_sig5_130      0.3      99    0.23    3.9   0.0    1    4-4       3-3   (19)
 20 P09624_21_MM_ref_sig5_130        0.3   1E+02    0.23    5.2   0.0    4    1-4       9-12  (26)

No 1  
>P36532_24_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.22  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.8

Q 5_Mustela         4 RI    5 (7)
Q Consensus         4 ri    5 (7)
                      ||
T Consensus        15 ri   16 (29)
T signal           15 RI   16 (29)
Confidence            33


No 2  
>Q12349_32_Mito_IM_ref_sig5_130
Probab=0.43  E-value=61  Score=6.46  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         4 R    4 (7)
Q Consensus         4 r    4 (7)
                      |
T Consensus         8 r    8 (37)
T signal            8 R    8 (37)
Confidence            3


No 3  
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.42  E-value=61  Score=6.81  Aligned_cols=1  Identities=0%  Similarity=0.899  Sum_probs=0.2

Q 5_Mustela         4 R    4 (7)
Q Consensus         4 r    4 (7)
                      .
T Consensus        41 k   41 (49)
T signal           41 K   41 (49)
Confidence            2


No 4  
>P07246_27_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=4.99  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      +
T Consensus        32 ~   32 (32)
T signal           32 K   32 (32)
T 3                34 K   34 (34)
T 12               29 Q   29 (29)
T 14               30 A   30 (30)
T 60_Zymoseptori   32 P   32 (32)
T 43_Pyrenophora   32 N   32 (32)
T 29_Talaromyces   32 R   32 (32)
T 12_Zygosacchar   32 V   32 (32)
T 11_Vanderwalto   32 A   32 (32)
T 18               32 N   32 (32)
Confidence            0


No 5  
>Q06645_61_MitoM_ref_sig5_130
Probab=0.41  E-value=64  Score=6.35  Aligned_cols=1  Identities=0%  Similarity=-1.161  Sum_probs=0.0

Q 5_Mustela         1 G    1 (7)
Q Consensus         1 g    1 (7)
                      =
T Consensus         1 M    1 (66)
T signal            1 M    1 (66)
T 59_Zonotrichia    1 M    1 (66)
T 54_Latimeria      1 M    1 (66)
T 52_Anolis         1 M    1 (66)
T 39_Heterocepha    1 M    1 (66)
T cl|GABBABABA|1    1 M    1 (66)
T cl|KABBABABA|1    1 M    1 (66)
T cl|LABBABABA|3    1 M    1 (66)
T cl|RABBABABA|7    1 M    1 (66)
T 1                 1 M    1 (68)
Confidence            0


No 6  
>P53163_33_Mito_ref_sig5_130
Probab=0.40  E-value=66  Score=6.40  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         5 I    5 (7)
Q Consensus         5 i    5 (7)
                      |
T Consensus         5 l    5 (38)
T signal            5 I    5 (38)
T 169               5 L    5 (32)
T 170               5 L    5 (37)
Confidence            3


No 7  
>P92507_25_IM_ref_sig5_130
Probab=0.39  E-value=66  Score=6.07  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.8

Q 5_Mustela         4 RI    5 (7)
Q Consensus         4 ri    5 (7)
                      ||
T Consensus        14 ri   15 (30)
T signal           14 RI   15 (30)
Confidence            34


No 8  
>P25284_26_MM_ref_sig5_130
Probab=0.38  E-value=68  Score=6.01  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         4 R    4 (7)
Q Consensus         4 r    4 (7)
                      |
T Consensus        25 r   25 (31)
T signal           25 R   25 (31)
T 188              25 R   25 (31)
T 184              24 A   24 (30)
T 187              23 R   23 (29)
T 171              22 R   22 (27)
T 182              22 Q   22 (27)
T 164              21 R   21 (27)
T 183              21 R   21 (28)
Confidence            3


No 9  
>P36516_59_Mito_ref_sig5_130
Probab=0.37  E-value=71  Score=6.93  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         4 R    4 (7)
Q Consensus         4 r    4 (7)
                      |
T Consensus         7 r    7 (64)
T signal            7 R    7 (64)
Confidence            2


No 10 
>P21642_33_Mito_ref_sig5_130
Probab=0.37  E-value=71  Score=6.27  Aligned_cols=1  Identities=0%  Similarity=-0.894  Sum_probs=0.3

Q 5_Mustela         6 I    6 (7)
Q Consensus         6 i    6 (7)
                      |
T Consensus        15 ~   15 (38)
T signal           15 E   15 (38)
T cl|CABBABABA|1   15 I   15 (38)
Confidence            3


No 11 
>Q02253_32_Mito_ref_sig5_130
Probab=0.36  E-value=73  Score=6.23  Aligned_cols=3  Identities=67%  Similarity=1.276  Sum_probs=1.2

Q 5_Mustela         4 RII    6 (7)
Q Consensus         4 rii    6 (7)
                      ||+
T Consensus        14 riL   16 (37)
T signal           14 RIL   16 (37)
T 23               14 RIL   16 (37)
T 135              13 RIL   15 (35)
T 138              14 RIL   16 (37)
T 134              11 QML   13 (33)
Confidence            443


No 12 
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.36  E-value=74  Score=5.79  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=1.0

Q 5_Mustela         1 GD    2 (7)
Q Consensus         1 gd    2 (7)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 13 
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=0.34  E-value=77  Score=5.75  Aligned_cols=2  Identities=50%  Similarity=1.464  Sum_probs=1.0

Q 5_Mustela         1 GD    2 (7)
Q Consensus         1 gd    2 (7)
                      ||
T Consensus        24 GD   25 (28)
T signal           24 GN   25 (28)
T 102              22 GD   23 (26)
T 105              22 GD   23 (26)
T 110              22 GD   23 (26)
T 111              22 GD   23 (26)
T 108              22 GD   23 (26)
T 113              22 GD   23 (26)
T 76               22 GD   23 (26)
Confidence            45


No 14 
>P26267_25_MM_ref_sig5_130
Probab=0.33  E-value=80  Score=5.85  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         5 I    5 (7)
Q Consensus         5 i    5 (7)
                      |
T Consensus         8 i    8 (30)
T signal            8 I    8 (30)
T 14                8 I    8 (33)
T 9                 8 V    8 (33)
Confidence            3


No 15 
>P07919_13_IM_ref_sig5_130
Probab=0.31  E-value=84  Score=5.04  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.9

Q 5_Mustela         1 GD    2 (7)
Q Consensus         1 gd    2 (7)
                      ||
T Consensus        14 gD   15 (18)
T signal           14 GD   15 (18)
T 41               14 GD   15 (18)
T 13               14 RD   15 (18)
T 23               14 GD   15 (18)
T 58               13 ED   14 (17)
T 61               14 GD   15 (18)
T 18               14 GE   15 (18)
T 22               14 GD   15 (18)
T 59               14 GD   15 (18)
T 79               11 GE   12 (15)
Confidence            34


No 16 
>Q8K2C6_36_Mito_ref_sig5_130
Probab=0.30  E-value=86  Score=3.16  Aligned_cols=1  Identities=0%  Similarity=-0.130  Sum_probs=0.0

Q 5_Mustela         3 K    3 (7)
Q Consensus         3 k    3 (7)
                      .
T Consensus        41 ~   41 (41)
T signal           41 A   41 (41)
T 107_Tribolium    41 G   41 (41)
T 92_Nasonia       41 F   41 (41)
T 132_Schizophyl   41 G   41 (41)
T 145_Zymoseptor   41 R   41 (41)
T 112_Nannochlor   41 W   41 (41)
T 102_Metaseiulu   41 S   41 (41)
T 155_Leishmania   41 S   41 (41)
Confidence            0


No 17 
>P99028_13_IM_ref_sig5_130
Probab=0.27  E-value=96  Score=4.95  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.8

Q 5_Mustela         1 GD    2 (7)
Q Consensus         1 gd    2 (7)
                      ||
T Consensus        14 gD   15 (18)
T signal           14 GD   15 (18)
T 23               14 GD   15 (18)
T 42               14 GD   15 (18)
T 3                14 GD   15 (18)
T 14               14 RD   15 (18)
T 24               14 GD   15 (18)
T 59               13 ED   14 (17)
T 62               14 GD   15 (18)
Confidence            34


No 18 
>P38077_33_Mito_IM_ref_sig5_130
Probab=0.27  E-value=98  Score=5.84  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.8

Q 5_Mustela         4 RI    5 (7)
Q Consensus         4 ri    5 (7)
                      ||
T Consensus         4 ri    5 (38)
T signal            4 RI    5 (38)
Confidence            44


No 19 
>P36531_14_Mito_ref_sig5_130
Probab=0.27  E-value=99  Score=3.94  Aligned_cols=1  Identities=0%  Similarity=0.899  Sum_probs=0.2

Q 5_Mustela         4 R    4 (7)
Q Consensus         4 r    4 (7)
                      |
T Consensus         3 r    3 (19)
T signal            3 K    3 (19)
T cl|DABBABABA|1    3 S    3 (18)
T cl|GABBABABA|1    3 N    3 (18)
T cl|KABBABABA|1    3 T    3 (18)
T 9_Kazachstania    3 S    3 (18)
T 5_Vanderwaltoz    3 R    3 (18)
T 3_Naumovozyma     3 N    3 (18)
T 18_Scheffersom    3 R    3 (18)
T 16_Meyerozyma     3 R    3 (18)
T 14_Komagataell    3 K    3 (18)
Confidence            2


No 20 
>P09624_21_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.22  Aligned_cols=4  Identities=50%  Similarity=0.875  Sum_probs=0.0

Q 5_Mustela         1 GDKR    4 (7)
Q Consensus         1 gdkr    4 (7)
                      +.||
T Consensus         9 ~skR   12 (26)
T signal            9 NNKR   12 (26)
T 188               5 NSTR    8 (22)
T 183               9 ASKR   12 (26)
T 186               9 AMRR   12 (25)
T 191               9 GASR   12 (27)
T 187               6 SSKR    9 (21)


Done!
