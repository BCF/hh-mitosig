Query         5_Mustela
Match_columns 6
No_of_seqs    7 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:07:47 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_2A8_2Q8_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36527_26_Mito_ref_sig5_130      1.6      14   0.031    7.7   0.0    3    2-4      17-19  (31)
  2 Q9Z0J5_36_Mito_ref_sig5_130      1.5      15   0.035    5.1   0.0    1    2-2      41-41  (41)
  3 P55954_29_IM_ref_sig5_130        1.3      17   0.039    7.6   0.0    2    3-4      29-30  (34)
  4 P35914_27_MM_Pe_ref_sig5_130     1.2      19   0.043    7.4   0.0    1    2-2      21-21  (32)
  5 Q5BJX1_13_Mito_ref_sig5_130      1.2      19   0.043    6.5   0.0    1    4-4      10-10  (18)
  6 Q02376_27_IM_ref_sig5_130        1.1      20   0.046    7.3   0.0    1    3-3       4-4   (32)
  7 P80269_41_IM_ref_sig5_130        1.1      20   0.047    7.8   0.0    1    3-3      21-21  (46)
  8 P41563_27_Mito_ref_sig5_130      1.1      21   0.047    7.1   0.0    1    3-3      11-11  (32)
  9 P26269_27_MM_ref_sig5_130        1.1      21   0.048    7.3   0.0    1    3-3      29-29  (32)
 10 P31039_44_IM_ref_sig5_130        1.1      21   0.048    4.8   0.0    1    2-2      49-49  (49)
 11 Q25423_17_IM_ref_sig5_130        1.0      24   0.054    6.6   0.0    1    3-3      21-21  (22)
 12 P14063_12_Mito_ref_sig5_130      1.0      24   0.055    6.2   0.0    2    4-5      14-15  (17)
 13 P14066_25_IM_ref_sig5_130        1.0      24   0.055    7.0   0.0    2    3-4      15-16  (30)
 14 P49411_43_Mito_ref_sig5_130      1.0      24   0.056    7.7   0.0    2    3-4      14-15  (48)
 15 Q96251_36_Mito_IM_ref_sig5_130   0.9      27   0.061    7.3   0.0    2    3-4       7-8   (41)
 16 P14604_29_MM_ref_sig5_130        0.9      27   0.062    7.0   0.0    2    3-4      29-30  (34)
 17 Q0QF01_42_IM_ref_sig5_130        0.8      30   0.068    7.3   0.0    1    3-3       8-8   (47)
 18 P25712_34_IM_ref_sig5_130        0.8      30   0.069    7.0   0.0    1    2-2      39-39  (39)
 19 Q05932_42_IM_MM_Cy_ref_sig5_13   0.8      32   0.074    7.3   0.0    1    2-2      17-17  (47)
 20 P30084_27_MM_ref_sig5_130        0.7      34   0.077    6.6   0.0    2    3-4      29-30  (32)

No 1  
>P36527_26_Mito_ref_sig5_130
Probab=1.61  E-value=14  Score=7.74  Aligned_cols=3  Identities=100%  Similarity=1.354  Sum_probs=1.6

Q 5_Mustela         2 VSG    4 (6)
Q 8_Pteropus        2 VSG    4 (6)
Q 12_Sorex          2 VSG    4 (6)
Q 9_Ailuropoda      2 ASG    4 (6)
Q 15_Ictidomys      2 VSG    4 (6)
Q 11_Cavia          2 VSA    4 (6)
Q 19_Trichechus     2 ASG    4 (6)
Q Consensus         2 vsg    4 (6)
                      |||
T Consensus        17 vsg   19 (31)
T signal           17 VSG   19 (31)
Confidence            455


No 2  
>Q9Z0J5_36_Mito_ref_sig5_130
Probab=1.46  E-value=15  Score=5.13  Aligned_cols=1  Identities=0%  Similarity=-0.729  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 8_Pteropus        2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 15_Ictidomys      2 V    2 (6)
Q 11_Cavia          2 V    2 (6)
Q 19_Trichechus     2 A    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        41 ~   41 (41)
T signal           41 N   41 (41)
T 85_Bombus        35 -   34 (34)
T 58_Orcinus       35 -   34 (34)
T 44_Chrysemys     35 -   34 (34)
T 30_Bos           35 -   34 (34)
Confidence            0


No 3  
>P55954_29_IM_ref_sig5_130
Probab=1.34  E-value=17  Score=7.62  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus        29 sg   30 (34)
T signal           29 SG   30 (34)
Confidence            43


No 4  
>P35914_27_MM_Pe_ref_sig5_130
Probab=1.22  E-value=19  Score=7.36  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         2 V    2 (6)
Q 8_Pteropus        2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 15_Ictidomys      2 V    2 (6)
Q 11_Cavia          2 V    2 (6)
Q 19_Trichechus     2 A    2 (6)
Q Consensus         2 v    2 (6)
                      |
T Consensus        21 v   21 (32)
T signal           21 V   21 (32)
T 49               22 I   22 (33)
T 68               21 V   21 (32)
T 51               18 V   18 (26)
T 52               17 V   17 (25)
T 75               21 V   21 (32)
T 76               21 V   21 (32)
T 77               21 F   21 (32)
T 78               21 V   21 (32)
T 59               18 I   18 (29)
Confidence            3


No 5  
>Q5BJX1_13_Mito_ref_sig5_130
Probab=1.21  E-value=19  Score=6.48  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G........    4 (6)
Q 8_Pteropus        4 G........    4 (6)
Q 12_Sorex          4 G........    4 (6)
Q 9_Ailuropoda      4 G........    4 (6)
Q 15_Ictidomys      4 G........    4 (6)
Q 11_Cavia          4 A........    4 (6)
Q 19_Trichechus     4 G........    4 (6)
Q Consensus         4 g........    4 (6)
                      |        
T Consensus        10 g........   10 (18)
T signal           10 G........   10 (18)
T 12               10 G........   10 (18)
T 22               10 G........   10 (18)
T 17               10 A........   10 (18)
T 26               10 S........   10 (19)
T 15               11 Grrnselqe   19 (27)
T 61               10 G........   10 (17)
T 62               10 G........   10 (17)
Confidence            3        


No 6  
>Q02376_27_IM_ref_sig5_130
Probab=1.15  E-value=20  Score=7.29  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus         4 s    4 (32)
T signal            4 S    4 (32)
T 10                4 S    4 (33)
T 11                4 P    4 (32)
T 17                4 P    4 (32)
T 30                4 S    4 (32)
T 13                4 S    4 (32)
T 12                4 S    4 (32)
T 8                 4 S    4 (33)
Confidence            2


No 7  
>P80269_41_IM_ref_sig5_130
Probab=1.13  E-value=20  Score=7.83  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.3

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      +
T Consensus        21 ~   21 (46)
T signal           21 A   21 (46)
T 127              21 A   21 (32)
T 128              21 P   21 (32)
T 129              23 P   23 (34)
T 130              21 S   21 (32)
T 131              21 S   21 (32)
T 124              21 L   21 (37)
T 126              21 L   21 (37)
T 123              21 L   21 (38)
Confidence            2


No 8  
>P41563_27_Mito_ref_sig5_130
Probab=1.13  E-value=21  Score=7.10  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus        11 S   11 (32)
T signal           11 S   11 (32)
T 20               11 S   11 (30)
T 42               11 S   11 (32)
T 45               11 S   11 (31)
T 46               10 S   10 (30)
T 61               11 S   11 (31)
T 76               11 R   11 (30)
T 84               11 S   11 (31)
T 94               11 S   11 (31)
T 114              11 S   11 (31)
Confidence            2


No 9  
>P26269_27_MM_ref_sig5_130
Probab=1.11  E-value=21  Score=7.28  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus        29 s   29 (32)
T signal           29 S   29 (32)
Confidence            2


No 10 
>P31039_44_IM_ref_sig5_130
Probab=1.11  E-value=21  Score=4.78  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 8_Pteropus        2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 15_Ictidomys      2 V    2 (6)
Q 11_Cavia          2 V    2 (6)
Q 19_Trichechus     2 A    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            0


No 11 
>Q25423_17_IM_ref_sig5_130
Probab=1.00  E-value=24  Score=6.64  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus        21 ~   21 (22)
T signal           21 G   21 (22)
T cl|CABBABABA|1   24 S   24 (25)
Confidence            2


No 12 
>P14063_12_Mito_ref_sig5_130
Probab=0.98  E-value=24  Score=6.15  Aligned_cols=2  Identities=100%  Similarity=1.763  Sum_probs=0.8

Q 5_Mustela         4 GL    5 (6)
Q 8_Pteropus        4 GL    5 (6)
Q 12_Sorex          4 GL    5 (6)
Q 9_Ailuropoda      4 GL    5 (6)
Q 15_Ictidomys      4 GL    5 (6)
Q 11_Cavia          4 AL    5 (6)
Q 19_Trichechus     4 GL    5 (6)
Q Consensus         4 gl    5 (6)
                      ||
T Consensus        14 Gl   15 (17)
T signal           14 GL   15 (17)
T 2                14 GL   15 (17)
T 33               14 GL   15 (17)
T 7                14 GL   15 (17)
T 35               10 GL   11 (13)
T 20               14 GY   15 (17)
T 19               14 GY   15 (17)
T 13                9 GL   10 (12)
Confidence            43


No 13 
>P14066_25_IM_ref_sig5_130
Probab=0.98  E-value=24  Score=7.03  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=1.0

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus        15 sg   16 (30)
T signal           15 SG   16 (30)
Confidence            55


No 14 
>P49411_43_Mito_ref_sig5_130
Probab=0.97  E-value=24  Score=7.68  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus        14 sG   15 (48)
T signal           14 SG   15 (48)
T 163              14 CG   15 (48)
T 166               9 SG   10 (43)
T 167              14 SG   15 (48)
T 170              11 SG   12 (45)
T 171              14 SG   15 (48)
T 177              14 GG   15 (48)
T 157              14 SG   15 (62)
Confidence            34


No 15 
>Q96251_36_Mito_IM_ref_sig5_130
Probab=0.90  E-value=27  Score=7.33  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus         7 SG    8 (41)
T signal            7 SG    8 (41)
T 185               7 SG    8 (45)
T 183               7 SG    8 (43)
T 184               7 SG    8 (43)
T 172               7 SG    8 (49)
Confidence            33


No 16 
>P14604_29_MM_ref_sig5_130
Probab=0.88  E-value=27  Score=6.97  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus        29 SG   30 (34)
T signal           29 SG   30 (34)
T 122              29 AG   30 (34)
T 133              29 SG   30 (34)
T 142              29 SS   30 (34)
T 145              29 SG   30 (34)
T 146              29 SG   30 (34)
T 152              26 SG   27 (31)
T 139              29 SG   30 (34)
T 129              29 SG   30 (34)
T 120              29 AG   30 (34)
Confidence            43


No 17 
>Q0QF01_42_IM_ref_sig5_130
Probab=0.82  E-value=30  Score=7.28  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         3 S    3 (6)
Q 8_Pteropus        3 S    3 (6)
Q 12_Sorex          3 S    3 (6)
Q 9_Ailuropoda      3 S    3 (6)
Q 15_Ictidomys      3 S    3 (6)
Q 11_Cavia          3 S    3 (6)
Q 19_Trichechus     3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus         8 S    8 (47)
T signal            8 S    8 (47)
T 124               8 S    8 (47)
T 126               8 S    8 (43)
T 136               9 S    9 (50)
T 137               8 S    8 (46)
T 144               8 S    8 (48)
T 145               5 A    5 (47)
T 171               1 -    0 (44)
T 154               1 -    0 (30)
Confidence            2


No 18 
>P25712_34_IM_ref_sig5_130
Probab=0.80  E-value=30  Score=7.00  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q 8_Pteropus        2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 15_Ictidomys      2 V    2 (6)
Q 11_Cavia          2 V    2 (6)
Q 19_Trichechus     2 A    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        39 g   39 (39)
T signal           39 G   39 (39)
T 33               39 G   39 (39)
T 60               32 G   32 (32)
T 63               28 G   28 (28)
T 59               40 E   40 (40)
T 35               39 G   39 (39)
T 5                39 G   39 (39)
T 1                38 -   37 (37)
T 62               38 -   37 (37)
T 10               36 -   35 (35)
Confidence            0


No 19 
>Q05932_42_IM_MM_Cy_ref_sig5_130
Probab=0.75  E-value=32  Score=7.30  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.3

Q 5_Mustela         2 V    2 (6)
Q 8_Pteropus        2 V    2 (6)
Q 12_Sorex          2 V    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 15_Ictidomys      2 V    2 (6)
Q 11_Cavia          2 V    2 (6)
Q 19_Trichechus     2 A    2 (6)
Q Consensus         2 v    2 (6)
                      |
T Consensus        17 v   17 (47)
T signal           17 A   17 (47)
T 141              17 V   17 (47)
T 143              17 V   17 (47)
T 153              17 V   17 (47)
T 156              17 V   17 (47)
T 132              17 I   17 (47)
T 124              11 A   11 (31)
T 108               7 L    7 (27)
Confidence            2


No 20 
>P30084_27_MM_ref_sig5_130
Probab=0.73  E-value=34  Score=6.64  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         3 SG    4 (6)
Q 8_Pteropus        3 SG    4 (6)
Q 12_Sorex          3 SG    4 (6)
Q 9_Ailuropoda      3 SG    4 (6)
Q 15_Ictidomys      3 SG    4 (6)
Q 11_Cavia          3 SA    4 (6)
Q 19_Trichechus     3 SG    4 (6)
Q Consensus         3 sg    4 (6)
                      ||
T Consensus        29 SG   30 (32)
T signal           29 SG   30 (32)
T 128              29 AG   30 (32)
T 131              29 SS   30 (32)
T 140              29 SG   30 (32)
T 136              29 SG   30 (31)
T 135              26 SG   27 (29)
T 120              29 AG   30 (32)
Confidence            33


Done!
