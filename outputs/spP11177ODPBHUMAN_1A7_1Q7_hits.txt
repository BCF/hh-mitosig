Query         5_Mustela
Match_columns 6
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:07:45 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_1A7_1Q7_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P12234_49_IM_ref_sig5_130        1.9      11   0.025    6.1   0.0    1    2-2      54-54  (54)
  2 Q02375_42_IM_ref_sig5_130        1.9      11   0.026    8.6   0.0    1    3-3      25-25  (47)
  3 P31937_36_Mito_ref_sig5_130      1.8      12   0.028    8.3   0.0    1    4-4      12-12  (41)
  4 P10719_46_Mito_IM_ref_sig5_130   1.7      13    0.03    8.5   0.0    1    2-2      51-51  (51)
  5 P14604_29_MM_ref_sig5_130        1.6      14   0.031    7.8   0.0    1    4-4      29-29  (34)
  6 Q02374_36_IM_ref_sig5_130        1.6      14   0.032    8.1   0.0    1    4-4       5-5   (41)
  7 P82919_34_Mito_ref_sig5_130      1.5      15   0.033    8.0   0.0    1    2-2      39-39  (39)
  8 P30084_27_MM_ref_sig5_130        1.5      15   0.035    7.6   0.0    1    2-2      32-32  (32)
  9 P06576_47_Mito_IM_ref_sig5_130   1.5      15   0.035    8.3   0.0    1    2-2      52-52  (52)
 10 P05166_28_MM_ref_sig5_130        1.5      15   0.035    7.7   0.0    1    2-2      33-33  (33)
 11 P36542_25_Mito_IM_ref_sig5_130   1.4      16   0.037    7.5   0.0    1    2-2      30-30  (30)
 12 P35571_42_Mito_ref_sig5_130      1.3      18   0.041    5.7   0.0    1    2-2      47-47  (47)
 13 O13401_34_MM_ref_sig5_130        1.2      19   0.043    7.7   0.0    1    3-3      27-27  (39)
 14 P14066_25_IM_ref_sig5_130        1.2      19   0.043    7.4   0.0    2    4-5      15-16  (30)
 15 Q02253_32_Mito_ref_sig5_130      1.2      19   0.043    7.7   0.0    1    2-2      37-37  (37)
 16 P00829_48_Mito_IM_ref_sig5_130   1.2      19   0.044    8.1   0.0    1    4-4      14-14  (53)
 17 Q9NUB1_37_MM_ref_sig5_130        1.1      21   0.047    7.7   0.0    1    4-4      39-39  (42)
 18 P42026_37_Mito_ref_sig5_130      1.1      21   0.048    7.6   0.0    1    5-5       8-8   (42)
 19 Q3T0L3_8_Mito_ref_sig5_130       1.1      21   0.048    6.0   0.0    1    2-2      13-13  (13)
 20 Q9SVM8_34_Mito_ref_sig5_130      1.1      22    0.05    7.5   0.0    1    2-2      39-39  (39)

No 1  
>P12234_49_IM_ref_sig5_130
Probab=1.95  E-value=11  Score=6.08  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        54 ~   54 (54)
T signal           54 Q   54 (54)
T 124_Salpingoec   42 -   41 (41)
T 115_Sorghum      42 -   41 (41)
T 97_Branchiosto   42 -   41 (41)
T 80_Trichoplax    42 -   41 (41)
T 66_Ciona         42 -   41 (41)
T 62_Geospiza      42 -   41 (41)
T 58_Anas          42 -   41 (41)
T 57_Falco         42 -   41 (41)
T 54_Ficedula      42 -   41 (41)
Confidence            0


No 2  
>Q02375_42_IM_ref_sig5_130
Probab=1.87  E-value=11  Score=8.58  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         3 V    3 (6)
Q 9_Ailuropoda      3 A    3 (6)
Q 11_Cavia          3 V    3 (6)
Q Consensus         3 ~    3 (6)
                      +
T Consensus        25 ~   25 (47)
T signal           25 V   25 (47)
T 35               22 -   21 (43)
T 57               25 V   25 (47)
T 96               25 I   25 (47)
T 163              25 L   25 (47)
T 16               21 -   20 (41)
T 31               19 -   18 (39)
T 90               21 -   20 (41)
T 115              21 -   20 (41)
T 148              21 -   20 (45)
Confidence            2


No 3  
>P31937_36_Mito_ref_sig5_130
Probab=1.77  E-value=12  Score=8.31  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         4 S    4 (6)
Q 9_Ailuropoda      4 S    4 (6)
Q 11_Cavia          4 S    4 (6)
Q Consensus         4 s    4 (6)
                      +
T Consensus        12 ~   12 (41)
T signal           12 S   12 (41)
T 43                5 S    5 (34)
T 5                12 G   12 (32)
T 33               11 G   11 (34)
T 52               12 S   12 (38)
T 98               12 R   12 (38)
T 55               12 R   12 (38)
Confidence            2


No 4  
>P10719_46_Mito_IM_ref_sig5_130
Probab=1.66  E-value=13  Score=8.46  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      +
T Consensus        51 s   51 (51)
T signal           51 S   51 (51)
T 130              51 -   50 (50)
T 148              53 -   52 (52)
T 110              44 A   44 (44)
T 131              43 A   43 (43)
T 135              44 A   44 (44)
T 151              48 S   48 (48)
T 149              58 S   58 (58)
T 144              56 A   56 (56)
T 145              51 A   51 (51)
Confidence            0


No 5  
>P14604_29_MM_ref_sig5_130
Probab=1.61  E-value=14  Score=7.80  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         4 S    4 (6)
Q 9_Ailuropoda      4 S    4 (6)
Q 11_Cavia          4 S    4 (6)
Q Consensus         4 s    4 (6)
                      |
T Consensus        29 S   29 (34)
T signal           29 S   29 (34)
T 122              29 A   29 (34)
T 133              29 S   29 (34)
T 142              29 S   29 (34)
T 145              29 S   29 (34)
T 146              29 S   29 (34)
T 152              26 S   26 (31)
T 139              29 S   29 (34)
T 129              29 S   29 (34)
T 120              29 A   29 (34)
Confidence            1


No 6  
>Q02374_36_IM_ref_sig5_130
Probab=1.56  E-value=14  Score=8.09  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         4 S    4 (6)
Q 9_Ailuropoda      4 S    4 (6)
Q 11_Cavia          4 S    4 (6)
Q Consensus         4 s    4 (6)
                      |
T Consensus         5 S    5 (41)
T signal            5 S    5 (41)
T 4                 2 S    2 (38)
T 34                2 S    2 (38)
T 42                2 S    2 (38)
T 58                2 A    2 (33)
T 5                 2 S    2 (41)
T 13                2 S    2 (38)
T 64                1 -    0 (28)
T 67                1 -    0 (28)
T 45                2 S    2 (35)
Confidence            2


No 7  
>P82919_34_Mito_ref_sig5_130
Probab=1.52  E-value=15  Score=7.97  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        39 V   39 (39)
T signal           39 V   39 (39)
T 45               39 V   39 (39)
T 49               39 V   39 (39)
T 52               38 V   38 (38)
T 55               39 V   39 (39)
T 64               39 V   39 (39)
T 32               37 F   37 (37)
T 33               35 V   35 (35)
T 40               35 V   35 (35)
T 39               36 V   36 (36)
Confidence            0


No 8  
>P30084_27_MM_ref_sig5_130
Probab=1.48  E-value=15  Score=7.59  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        32 n   32 (32)
T signal           32 N   32 (32)
T 128              32 N   32 (32)
T 131              32 H   32 (32)
T 140              32 N   32 (32)
T 136              32 -   31 (31)
T 135              29 N   29 (29)
T 120              32 N   32 (32)
Confidence            0


No 9  
>P06576_47_Mito_IM_ref_sig5_130
Probab=1.46  E-value=15  Score=8.31  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        52 p   52 (52)
T signal           52 P   52 (52)
T 130              52 P   52 (52)
T 148              54 -   53 (53)
T 147              59 P   59 (59)
T 112              45 -   44 (44)
T 131              45 -   44 (44)
T 135              44 -   43 (43)
T 151              49 -   48 (48)
T 143              57 P   57 (57)
T 144              52 P   52 (52)
Confidence            0


No 10 
>P05166_28_MM_ref_sig5_130
Probab=1.45  E-value=15  Score=7.65  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        33 v   33 (33)
T signal           33 T   33 (33)
T 86               33 -   32 (32)
T 49               34 A   34 (34)
T 62               36 V   36 (36)
T 64               33 V   33 (33)
T 67               35 V   35 (35)
T 70               39 V   39 (39)
T 74               39 V   39 (39)
T 89               33 V   33 (33)
T 65               38 -   37 (37)
Confidence            0


No 11 
>P36542_25_Mito_IM_ref_sig5_130
Probab=1.39  E-value=16  Score=7.48  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        30 d   30 (30)
T signal           30 D   30 (30)
T 131              32 D   32 (32)
T 115              25 D   25 (25)
T 129              27 D   27 (27)
T 130              26 D   26 (26)
T 125              25 E   25 (25)
T 78               30 E   30 (30)
T 108              23 E   23 (23)
T 66               27 E   27 (27)
Confidence            0


No 12 
>P35571_42_Mito_ref_sig5_130
Probab=1.27  E-value=18  Score=5.68  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


No 13 
>O13401_34_MM_ref_sig5_130
Probab=1.23  E-value=19  Score=7.75  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.3

Q 5_Mustela         3 V    3 (6)
Q 9_Ailuropoda      3 A    3 (6)
Q 11_Cavia          3 V    3 (6)
Q Consensus         3 ~    3 (6)
                      +
T Consensus        27 a   27 (39)
T signal           27 A   27 (39)
Confidence            2


No 14 
>P14066_25_IM_ref_sig5_130
Probab=1.22  E-value=19  Score=7.36  Aligned_cols=2  Identities=100%  Similarity=1.464  Sum_probs=0.8

Q 5_Mustela         4 SG    5 (6)
Q 9_Ailuropoda      4 SG    5 (6)
Q 11_Cavia          4 SA    5 (6)
Q Consensus         4 s~    5 (6)
                      ||
T Consensus        15 sg   16 (30)
T signal           15 SG   16 (30)
Confidence            44


No 15 
>Q02253_32_Mito_ref_sig5_130
Probab=1.21  E-value=19  Score=7.66  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        37 v   37 (37)
T signal           37 V   37 (37)
T 23               37 V   37 (37)
T 135              35 S   35 (35)
T 138              37 V   37 (37)
T 134              33 V   33 (33)
Confidence            0


No 16 
>P00829_48_Mito_IM_ref_sig5_130
Probab=1.21  E-value=19  Score=8.07  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         4 S    4 (6)
Q 9_Ailuropoda      4 S    4 (6)
Q 11_Cavia          4 S    4 (6)
Q Consensus         4 s    4 (6)
                      |
T Consensus        14 s   14 (53)
T signal           14 S   14 (53)
T 125              14 S   14 (52)
T 111              10 S   10 (45)
T 131              10 I   10 (45)
T 136              10 T   10 (44)
T 152              10 A   10 (49)
T 149              14 S   14 (54)
T 142              14 S   14 (58)
T 151              16 T   16 (60)
T 147              14 A   14 (53)
Confidence            1


No 17 
>Q9NUB1_37_MM_ref_sig5_130
Probab=1.12  E-value=21  Score=7.68  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         4 S    4 (6)
Q 9_Ailuropoda      4 S    4 (6)
Q 11_Cavia          4 S    4 (6)
Q Consensus         4 s    4 (6)
                      |
T Consensus        39 s   39 (42)
T signal           39 S   39 (42)
T 89               39 S   39 (42)
T 91               39 S   39 (42)
T 95               33 -   32 (33)
T 98               34 A   34 (37)
T 103              38 S   38 (41)
T 107              36 T   36 (39)
T 110              39 S   39 (42)
T 93               30 -   29 (29)
T 105              39 -   38 (38)
Confidence            2


No 18 
>P42026_37_Mito_ref_sig5_130
Probab=1.11  E-value=21  Score=7.56  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.2

Q 5_Mustela         5 G    5 (6)
Q 9_Ailuropoda      5 G    5 (6)
Q 11_Cavia          5 A    5 (6)
Q Consensus         5 ~    5 (6)
                      |
T Consensus         8 g    8 (42)
T signal            8 R    8 (42)
T 163               8 G    8 (43)
T 166               8 G    8 (43)
T 160               8 G    8 (43)
T 150               4 G    4 (39)
T 144               8 G    8 (37)
T 133               1 -    0 (33)
T 136               1 -    0 (42)
T 138               1 -    0 (30)
T 143               1 -    0 (29)
Confidence            2


No 19 
>Q3T0L3_8_Mito_ref_sig5_130
Probab=1.10  E-value=21  Score=5.99  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        13 r   13 (13)
T signal           13 R   13 (13)
T 81               13 R   13 (13)
T 95               13 R   13 (13)
T 83               13 R   13 (13)
T 85               13 R   13 (13)
T 88               13 R   13 (13)
T 89               13 R   13 (13)
T 76               13 K   13 (13)
Confidence            0


No 20 
>Q9SVM8_34_Mito_ref_sig5_130
Probab=1.06  E-value=22  Score=7.52  Aligned_cols=1  Identities=0%  Similarity=-0.263  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q 9_Ailuropoda      2 A    2 (6)
Q 11_Cavia          2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        39 I   39 (39)
T signal           39 I   39 (39)
T 10               40 I   40 (40)
T 8                40 I   40 (40)
T 9                40 I   40 (40)
T 11               40 I   40 (40)
T 4                44 I   44 (44)
T 5                41 V   41 (41)
T 6                41 V   41 (41)
T 7                41 I   41 (41)
T 3                49 I   49 (49)
Confidence            0


Done!
