Query         5_Mustela
Match_columns 7
No_of_seqs    7 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:07:57 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_8A15_8Q15_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P28042_16_Mito_ref_sig5_130     68.5    0.02 4.6E-05   14.5   0.0    7    1-7       3-9   (21)
  2 P09380_17_Mito_MM_Nu_ref_sig5_  30.7    0.23 0.00053   12.0   0.0    7    1-7       3-9   (22)
  3 P36527_26_Mito_ref_sig5_130      3.7     4.9   0.011    9.2   0.0    4    4-7      14-17  (31)
  4 P13621_23_Mito_IM_ref_sig5_130   3.3     5.7   0.013    8.8   0.0    3    2-4      22-24  (28)
  5 P13184_23_IM_ref_sig5_130        3.3     5.7   0.013    8.8   0.0    3    4-6       8-10  (28)
  6 Q9FLX7_11_IM_ref_sig5_130        3.1     6.3   0.015    7.8   0.0    3    2-4       8-10  (16)
  7 P35171_23_IM_ref_sig5_130        2.6     7.8   0.018    8.4   0.0    3    4-6       8-10  (28)
  8 Q02337_46_MM_ref_sig5_130        2.6     7.8   0.018    7.0   0.0    1    3-3       9-9   (51)
  9 Q01205_68_Mito_ref_sig5_130      2.3       9   0.021    9.8   0.0    1    3-3      30-30  (73)
 10 P30038_24_MM_ref_sig5_130        2.2     9.5   0.022    8.3   0.0    1    2-2      10-10  (29)
 11 P05630_22_Mito_IM_ref_sig5_130   2.1      10   0.023    8.2   0.0    2    2-3       9-10  (27)
 12 Q07021_73_MM_Nu_Cm_Cy_ref_sig5   2.1      10   0.023    9.7   0.0    1    5-5      29-29  (78)
 13 P23786_25_IM_ref_sig5_130        2.0      11   0.024    8.3   0.0    4    2-5      23-26  (30)
 14 P09440_34_Mito_ref_sig5_130      1.9      11   0.025    8.6   0.0    4    3-6      13-16  (39)
 15 Q02376_27_IM_ref_sig5_130        1.8      12   0.027    8.2   0.0    3    2-4       8-10  (32)
 16 P09624_21_MM_ref_sig5_130        1.8      12   0.027    7.8   0.0    3    2-4      12-14  (26)
 17 P0CS90_23_MM_Nu_ref_sig5_130     1.8      12   0.028    7.8   0.0    2    3-4      16-17  (28)
 18 P32799_9_IM_ref_sig5_130         1.7      12   0.028    6.8   0.0    3    4-6       2-4   (14)
 19 P54898_44_Mito_ref_sig5_130      1.7      13   0.029    8.8   0.0    3    2-4      35-37  (49)
 20 P10817_9_IM_ref_sig5_130         1.7      13   0.029    6.8   0.0    1    4-4       2-2   (14)

No 1  
>P28042_16_Mito_ref_sig5_130
Probab=68.50  E-value=0.02  Score=14.45  Aligned_cols=7  Identities=71%  Similarity=1.051  Sum_probs=5.0

Q 5_Mustela         1 RRPLEQV    7 (7)
Q 2_Macaca          1 RRPLREV    7 (7)
Q 15_Ictidomys      1 RRPLQQV    7 (7)
Q 11_Cavia          1 QRPLRQV    7 (7)
Q 17_Tupaia         1 RRPFQQV    7 (7)
Q 19_Trichechus     1 RRPLRQV    7 (7)
Q 16_Equus          1 RRSLEQV    7 (7)
Q Consensus         1 rRpl~qV    7 (7)
                      |||.-||
T Consensus         3 Rrpv~Qv    9 (21)
T signal            3 RRPVLQV    9 (21)
T 10                3 RRPAWQV    9 (22)
T 28                3 RYASAQI    9 (20)
T 31                3 RRRVAQV    9 (22)
T 49                3 RSASAQI    9 (20)
T 62                3 QRPVLQV    9 (21)
T 64                3 RRPIIQV    9 (21)
T 67                3 RKPVFQV    9 (21)
T 81                3 RRPIIQV    9 (21)
T 11                3 XRTVLKM    9 (19)
Confidence            6887765


No 2  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=30.74  E-value=0.23  Score=11.96  Aligned_cols=7  Identities=57%  Similarity=0.714  Sum_probs=4.0

Q 5_Mustela         1 RRPLEQV    7 (7)
Q 2_Macaca          1 RRPLREV    7 (7)
Q 15_Ictidomys      1 RRPLQQV    7 (7)
Q 11_Cavia          1 QRPLRQV    7 (7)
Q 17_Tupaia         1 RRPFQQV    7 (7)
Q 19_Trichechus     1 RRPLRQV    7 (7)
Q 16_Equus          1 RRSLEQV    7 (7)
Q Consensus         1 rRpl~qV    7 (7)
                      |||--||
T Consensus         3 rr~~~Qv    9 (22)
T signal            3 HRPALQV    9 (22)
T 26                3 RRPVLQV    9 (20)
T 18                3 RYASAQI    9 (20)
T 21                3 RSTSAQL    9 (20)
T 22                3 RSASLQI    9 (20)
T 23                3 RRPAWQV    9 (21)
T 27                3 RRPAVQV    9 (21)
T 30                3 RKPVFQV    9 (21)
T 24                3 RRRVAQV    9 (20)
T 17                2 QRSVLQV    8 (18)
Confidence            5665554


No 3  
>P36527_26_Mito_ref_sig5_130
Probab=3.74  E-value=4.9  Score=9.22  Aligned_cols=4  Identities=100%  Similarity=1.140  Sum_probs=1.9

Q 5_Mustela         4 LEQV    7 (7)
Q 2_Macaca          4 LREV    7 (7)
Q 15_Ictidomys      4 LQQV    7 (7)
Q 11_Cavia          4 LRQV    7 (7)
Q 17_Tupaia         4 FQQV    7 (7)
Q 19_Trichechus     4 LRQV    7 (7)
Q 16_Equus          4 LEQV    7 (7)
Q Consensus         4 l~qV    7 (7)
                      |+||
T Consensus        14 leqv   17 (31)
T signal           14 LEQV   17 (31)
Confidence            4554


No 4  
>P13621_23_Mito_IM_ref_sig5_130
Probab=3.35  E-value=5.7  Score=8.83  Aligned_cols=3  Identities=67%  Similarity=1.586  Sum_probs=1.4

Q 5_Mustela         2 RPL    4 (7)
Q 2_Macaca          2 RPL    4 (7)
Q 15_Ictidomys      2 RPL    4 (7)
Q 11_Cavia          2 RPL    4 (7)
Q 17_Tupaia         2 RPF    4 (7)
Q 19_Trichechus     2 RPL    4 (7)
Q 16_Equus          2 RSL    4 (7)
Q Consensus         2 Rpl    4 (7)
                      ||+
T Consensus        22 RP~   24 (28)
T signal           22 RPF   24 (28)
T 148              22 RPF   24 (28)
T 131              18 RPA   20 (24)
T 123              18 RPV   20 (24)
T 137              15 RPI   17 (21)
T 130              18 RPA   20 (24)
T 144              18 RPA   20 (24)
T 134              18 RPV   20 (24)
Confidence            454


No 5  
>P13184_23_IM_ref_sig5_130
Probab=3.31  E-value=5.7  Score=8.78  Aligned_cols=3  Identities=67%  Similarity=0.789  Sum_probs=1.3

Q 5_Mustela         4 LEQ    6 (7)
Q 2_Macaca          4 LRE    6 (7)
Q 15_Ictidomys      4 LQQ    6 (7)
Q 11_Cavia          4 LRQ    6 (7)
Q 17_Tupaia         4 FQQ    6 (7)
Q 19_Trichechus     4 LRQ    6 (7)
Q 16_Equus          4 LEQ    6 (7)
Q Consensus         4 l~q    6 (7)
                      |+|
T Consensus         8 lrq   10 (28)
T signal            8 LRQ   10 (28)
T 63                8 LRH   10 (28)
T 28                8 LQQ   10 (28)
T 15                8 LCR   10 (28)
T 25                8 LRQ   10 (28)
T 16                8 LQQ   10 (28)
T 19                8 VQQ   10 (28)
T 12                8 LQQ   10 (28)
T 14                8 FSE   10 (28)
Confidence            444


No 6  
>Q9FLX7_11_IM_ref_sig5_130
Probab=3.06  E-value=6.3  Score=7.82  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.7

Q 5_Mustela         2 RPL    4 (7)
Q 2_Macaca          2 RPL    4 (7)
Q 15_Ictidomys      2 RPL    4 (7)
Q 11_Cavia          2 RPL    4 (7)
Q 17_Tupaia         2 RPF    4 (7)
Q 19_Trichechus     2 RPL    4 (7)
Q 16_Equus          2 RSL    4 (7)
Q Consensus         2 Rpl    4 (7)
                      |||
T Consensus         8 ~pl   10 (16)
T signal            8 RPL   10 (16)
T 12                8 RPL   10 (16)
T 11                8 GPL   10 (16)
T 9                 8 RPL   10 (16)
T 17               11 GGL   13 (24)
Confidence            565


No 7  
>P35171_23_IM_ref_sig5_130
Probab=2.58  E-value=7.8  Score=8.40  Aligned_cols=3  Identities=67%  Similarity=0.789  Sum_probs=1.2

Q 5_Mustela         4 LEQ    6 (7)
Q 2_Macaca          4 LRE    6 (7)
Q 15_Ictidomys      4 LQQ    6 (7)
Q 11_Cavia          4 LRQ    6 (7)
Q 17_Tupaia         4 FQQ    6 (7)
Q 19_Trichechus     4 LRQ    6 (7)
Q 16_Equus          4 LEQ    6 (7)
Q Consensus         4 l~q    6 (7)
                      |+|
T Consensus         8 lrq   10 (28)
T signal            8 LRQ   10 (28)
T 73                8 LRH   10 (28)
T 29                8 IRQ   10 (28)
T 18                8 LQQ   10 (28)
T 21                8 FSE   10 (28)
T 20                8 LQQ   10 (28)
T 3                 8 LHQ   10 (28)
T 22                4 IRS    6 (27)
T 71                5 LPR    7 (24)
Confidence            344


No 8  
>Q02337_46_MM_ref_sig5_130
Probab=2.58  E-value=7.8  Score=7.00  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.2

Q 5_Mustela         3 P    3 (7)
Q 2_Macaca          3 P    3 (7)
Q 15_Ictidomys      3 P    3 (7)
Q 11_Cavia          3 P    3 (7)
Q 17_Tupaia         3 P    3 (7)
Q 19_Trichechus     3 P    3 (7)
Q 16_Equus          3 S    3 (7)
Q Consensus         3 p    3 (7)
                      |
T Consensus         9 ~    9 (51)
T signal            9 P    9 (51)
T 76_Pediculus      9 M    9 (50)
T 74_Branchiosto    9 V    9 (50)
T 69_Papio          9 G    9 (50)
T 67_Maylandia      9 A    9 (50)
T 60_Sus            9 G    9 (50)
T 72_Ciona          9 D    9 (50)
T 59_Anolis         9 T    9 (50)
T 51_Canis          9 G    9 (50)
T 38_Nomascus       9 N    9 (50)
Confidence            1


No 9  
>Q01205_68_Mito_ref_sig5_130
Probab=2.29  E-value=9  Score=9.77  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         3 P    3 (7)
Q 2_Macaca          3 P    3 (7)
Q 15_Ictidomys      3 P    3 (7)
Q 11_Cavia          3 P    3 (7)
Q 17_Tupaia         3 P    3 (7)
Q 19_Trichechus     3 P    3 (7)
Q 16_Equus          3 S    3 (7)
Q Consensus         3 p    3 (7)
                      +
T Consensus        30 s   30 (73)
T signal           30 S   30 (73)
T 126              29 S   29 (72)
T 144              31 S   31 (74)
T 148              19 S   19 (62)
T 128              30 A   30 (72)
T 129              30 A   30 (72)
T 131              30 S   30 (72)
T 142              26 A   26 (70)
T 141              26 P   26 (69)
Confidence            2


No 10 
>P30038_24_MM_ref_sig5_130
Probab=2.19  E-value=9.5  Score=8.28  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         2 R    2 (7)
Q 2_Macaca          2 R    2 (7)
Q 15_Ictidomys      2 R    2 (7)
Q 11_Cavia          2 R    2 (7)
Q 17_Tupaia         2 R    2 (7)
Q 19_Trichechus     2 R    2 (7)
Q 16_Equus          2 R    2 (7)
Q Consensus         2 R    2 (7)
                      |
T Consensus        10 r   10 (29)
T signal           10 R   10 (29)
T 77               10 R   10 (29)
T 102              12 A   12 (26)
T 126              10 R   10 (29)
T 127              11 R   11 (30)
T 128              10 R   10 (29)
T 129              10 R   10 (29)
T 141               8 C    8 (27)
T 145               9 R    9 (28)
T 121              10 R   10 (29)
Confidence            2


No 11 
>P05630_22_Mito_IM_ref_sig5_130
Probab=2.10  E-value=10  Score=8.17  Aligned_cols=2  Identities=100%  Similarity=2.045  Sum_probs=0.8

Q 5_Mustela         2 RP    3 (7)
Q 2_Macaca          2 RP    3 (7)
Q 15_Ictidomys      2 RP    3 (7)
Q 11_Cavia          2 RP    3 (7)
Q 17_Tupaia         2 RP    3 (7)
Q 19_Trichechus     2 RP    3 (7)
Q 16_Equus          2 RS    3 (7)
Q Consensus         2 Rp    3 (7)
                      ||
T Consensus         9 r~   10 (27)
T signal            9 RP   10 (27)
T 133               9 RP   10 (27)
T 148               9 HP   10 (27)
T 167               9 RS   10 (27)
T 155               9 RP   10 (27)
T 147               9 RP   10 (27)
T 140               9 RL   10 (26)
T 142               9 RC   10 (32)
T 121               9 AL   10 (34)
T 134               9 RP   10 (29)
Confidence            44


No 12 
>Q07021_73_MM_Nu_Cm_Cy_ref_sig5_130
Probab=2.06  E-value=10  Score=9.74  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.4

Q 5_Mustela         5 E    5 (7)
Q 2_Macaca          5 R    5 (7)
Q 15_Ictidomys      5 Q    5 (7)
Q 11_Cavia          5 R    5 (7)
Q 17_Tupaia         5 Q    5 (7)
Q 19_Trichechus     5 R    5 (7)
Q 16_Equus          5 E    5 (7)
Q Consensus         5 ~    5 (7)
                      +
T Consensus        29 r   29 (78)
T signal           29 R   29 (78)
T 52               22 S   22 (78)
T 36                1 -    0 (31)
T 26               24 R   24 (85)
T 48               27 R   27 (80)
T 30               18 Q   18 (68)
T 19                1 -    0 (49)
T 16                1 -    0 (49)
T 21                1 -    0 (45)
T 13                1 -    0 (47)
Confidence            3


No 13 
>P23786_25_IM_ref_sig5_130
Probab=2.00  E-value=11  Score=8.25  Aligned_cols=4  Identities=75%  Similarity=1.373  Sum_probs=2.1

Q 5_Mustela         2 RPLE    5 (7)
Q 2_Macaca          2 RPLR    5 (7)
Q 15_Ictidomys      2 RPLQ    5 (7)
Q 11_Cavia          2 RPLR    5 (7)
Q 17_Tupaia         2 RPFQ    5 (7)
Q 19_Trichechus     2 RPLR    5 (7)
Q 16_Equus          2 RSLE    5 (7)
Q Consensus         2 Rpl~    5 (7)
                      |||.
T Consensus        23 RpLS   26 (30)
T signal           23 RPLS   26 (30)
T 101              23 RPLS   26 (30)
T 104              23 RPLS   26 (30)
T 108              23 RPLS   26 (30)
T 109              24 RSLS   27 (31)
T 117              23 RSLS   26 (30)
T 119              23 RPLS   26 (30)
T 59               28 RSLS   31 (35)
Confidence            5653


No 14 
>P09440_34_Mito_ref_sig5_130
Probab=1.94  E-value=11  Score=8.64  Aligned_cols=4  Identities=25%  Similarity=0.559  Sum_probs=1.7

Q 5_Mustela         3 PLEQ    6 (7)
Q 2_Macaca          3 PLRE    6 (7)
Q 15_Ictidomys      3 PLQQ    6 (7)
Q 11_Cavia          3 PLRQ    6 (7)
Q 17_Tupaia         3 PFQQ    6 (7)
Q 19_Trichechus     3 PLRQ    6 (7)
Q 16_Equus          3 SLEQ    6 (7)
Q Consensus         3 pl~q    6 (7)
                      .++|
T Consensus        13 afqq   16 (39)
T signal           13 AFQQ   16 (39)
Confidence            3444


No 15 
>Q02376_27_IM_ref_sig5_130
Probab=1.83  E-value=12  Score=8.21  Aligned_cols=3  Identities=67%  Similarity=1.586  Sum_probs=1.3

Q 5_Mustela         2 RPL    4 (7)
Q 2_Macaca          2 RPL    4 (7)
Q 15_Ictidomys      2 RPL    4 (7)
Q 11_Cavia          2 RPL    4 (7)
Q 17_Tupaia         2 RPF    4 (7)
Q 19_Trichechus     2 RPL    4 (7)
Q 16_Equus          2 RSL    4 (7)
Q Consensus         2 Rpl    4 (7)
                      |||
T Consensus         8 rp~   10 (32)
T signal            8 RPF   10 (32)
T 10                8 RPV   10 (33)
T 11                8 RSL   10 (32)
T 17                8 RSF   10 (32)
T 30                8 RSF   10 (32)
T 13                8 RPL   10 (32)
T 12                8 HPL   10 (32)
T 8                 9 SQL   11 (33)
Confidence            444


No 16 
>P09624_21_MM_ref_sig5_130
Probab=1.82  E-value=12  Score=7.77  Aligned_cols=3  Identities=33%  Similarity=0.778  Sum_probs=1.3

Q 5_Mustela         2 RPL.    4 (7)
Q 2_Macaca          2 RPL.    4 (7)
Q 15_Ictidomys      2 RPL.    4 (7)
Q 11_Cavia          2 RPL.    4 (7)
Q 17_Tupaia         2 RPF.    4 (7)
Q 19_Trichechus     2 RPL.    4 (7)
Q 16_Equus          2 RSL.    4 (7)
Q Consensus         2 Rpl.    4 (7)
                      |.| 
T Consensus        12 Rsf.   14 (26)
T signal           12 RAF.   14 (26)
T 188               8 RSF.   10 (22)
T 183              12 RSL.   14 (26)
T 186              12 RSF.   14 (25)
T 191              12 RRFl   15 (27)
T 187               9 RSF.   11 (21)
Confidence            444 


No 17 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=1.78  E-value=12  Score=7.80  Aligned_cols=2  Identities=0%  Similarity=0.401  Sum_probs=0.8

Q 5_Mustela         3 PL    4 (7)
Q 2_Macaca          3 PL    4 (7)
Q 15_Ictidomys      3 PL    4 (7)
Q 11_Cavia          3 PL    4 (7)
Q 17_Tupaia         3 PF    4 (7)
Q 19_Trichechus     3 PL    4 (7)
Q 16_Equus          3 SL    4 (7)
Q Consensus         3 pl    4 (7)
                      |+
T Consensus        16 ~~   17 (28)
T signal           16 SF   17 (28)
T 55               17 AL   18 (29)
T 65               17 VV   18 (29)
T 54               17 -P   17 (28)
T 57               14 PI   15 (26)
T 59               14 PL   15 (26)
T 62               13 -N   13 (24)
T 56               12 PL   13 (24)
T 60               11 NV   12 (23)
Confidence            44


No 18 
>P32799_9_IM_ref_sig5_130
Probab=1.74  E-value=12  Score=6.79  Aligned_cols=3  Identities=33%  Similarity=0.567  Sum_probs=1.2

Q 5_Mustela         4 LEQ    6 (7)
Q 2_Macaca          4 LRE    6 (7)
Q 15_Ictidomys      4 LQQ    6 (7)
Q 11_Cavia          4 LRQ    6 (7)
Q 17_Tupaia         4 FQQ    6 (7)
Q 19_Trichechus     4 LRQ    6 (7)
Q 16_Equus          4 LEQ    6 (7)
Q Consensus         4 l~q    6 (7)
                      |+|
T Consensus         2 ~rq    4 (14)
T signal            2 FRQ    4 (14)
T 25                2 LAR    4 (12)
T 27                2 LRT    4 (12)
T 33                2 FRQ    4 (14)
T 34                2 FRQ    4 (13)
T 35                2 YRQ    4 (13)
T 40                2 FKQ    4 (13)
T 24                2 LRQ    4 (13)
T 43                2 LRQ    4 (13)
Confidence            344


No 19 
>P54898_44_Mito_ref_sig5_130
Probab=1.71  E-value=13  Score=8.81  Aligned_cols=3  Identities=33%  Similarity=0.811  Sum_probs=1.2

Q 5_Mustela         2 RPL    4 (7)
Q 2_Macaca          2 RPL    4 (7)
Q 15_Ictidomys      2 RPL    4 (7)
Q 11_Cavia          2 RPL    4 (7)
Q 17_Tupaia         2 RPF    4 (7)
Q 19_Trichechus     2 RPL    4 (7)
Q 16_Equus          2 RSL    4 (7)
Q Consensus         2 Rpl    4 (7)
                      ||+
T Consensus        35 R~~   37 (49)
T signal           35 RQI   37 (49)
T 154              37 RPF   39 (51)
T 151              36 RPF   38 (50)
T 104              35 RSI   37 (43)
T 137              42 RSF   44 (49)
Confidence            443


No 20 
>P10817_9_IM_ref_sig5_130
Probab=1.69  E-value=13  Score=6.83  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         4 L    4 (7)
Q 2_Macaca          4 L    4 (7)
Q 15_Ictidomys      4 L    4 (7)
Q 11_Cavia          4 L    4 (7)
Q 17_Tupaia         4 F    4 (7)
Q 19_Trichechus     4 L    4 (7)
Q 16_Equus          4 L    4 (7)
Q Consensus         4 l    4 (7)
                      |
T Consensus         2 l    2 (14)
T signal            2 L    2 (14)
T 14                2 L    2 (14)
T 18                2 L    2 (14)
T 22                2 L    2 (14)
T 23                2 R    2 (14)
T 46                2 L    2 (14)
T 4                 2 L    2 (14)
Confidence            3


Done!
