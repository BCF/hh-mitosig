Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:37 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_74A80_74Q80_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P49364_30_Mito_ref_sig5_130      0.5      57    0.13    6.3   0.0    1    5-5      21-21  (35)
  2 P38071_9_MM_ref_sig5_130         0.4      59    0.13    5.1   0.0    1    3-3       8-8   (14)
  3 Q96CB9_25_Mito_ref_sig5_130      0.4      60    0.14    2.6   0.0    1    2-2      30-30  (30)
  4 Q9HCC0_22_MM_ref_sig5_130        0.4      62    0.14    5.8   0.0    2    4-5      25-26  (27)
  5 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   0.4      65    0.15    5.8   0.0    2    4-5      24-25  (28)
  6 P01098_23_Mito_ref_sig5_130      0.4      66    0.15    5.8   0.0    2    3-4      23-24  (28)
  7 P83484_51_Mito_IM_ref_sig5_130   0.4      69    0.16    5.2   0.0    1    4-4      42-42  (56)
  8 P07919_13_IM_ref_sig5_130        0.4      72    0.17    5.0   0.0    2    4-5      14-15  (18)
  9 Q00711_28_IM_ref_sig5_130        0.4      74    0.17    5.7   0.0    1    4-4      32-32  (33)
 10 P17764_30_Mito_ref_sig5_130      0.3      78    0.18    5.8   0.0    1    3-3      30-30  (35)
 11 P36967_16_Mito_ref_sig5_130      0.3      79    0.18    5.2   0.0    2    3-4      11-12  (21)
 12 P22142_42_IM_ref_sig5_130        0.3      83    0.19    4.4   0.0    1    3-3      42-42  (47)
 13 P36516_59_Mito_ref_sig5_130      0.3      83    0.19    6.5   0.0    1    3-3      51-51  (64)
 14 P99028_13_IM_ref_sig5_130        0.3      83    0.19    4.9   0.0    1    5-5      15-15  (18)
 15 P12063_32_MM_ref_sig5_130        0.3      88     0.2    5.8   0.0    2    2-3      17-18  (37)
 16 P14063_12_Mito_ref_sig5_130      0.3      95    0.22    4.7   0.0    2    3-4       2-3   (17)
 17 Q9SVM8_34_Mito_ref_sig5_130      0.3      97    0.22    5.7   0.0    1    4-4       8-8   (39)
 18 P23965_28_MM_ref_sig5_130        0.3      98    0.22    5.4   0.0    1    4-4      30-30  (33)
 19 P0C2C4_29_Mito_ref_sig5_130      0.3   1E+02    0.23    5.5   0.0    1    4-4      30-30  (34)
 20 O13401_34_MM_ref_sig5_130        0.3   1E+02    0.23    5.6   0.0    2    2-3      35-36  (39)

No 1  
>P49364_30_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.27  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 D    5 (6)
Q Consensus         5 d    5 (6)
                      |
T Consensus        21 d   21 (35)
T signal           21 D   21 (35)
T 163              21 D   21 (35)
T 164              20 D   20 (34)
T 159              21 D   21 (35)
Confidence            2


No 2  
>P38071_9_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=5.06  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         3 Y    3 (6)
Q Consensus         3 y    3 (6)
                      |
T Consensus         8 ~    8 (14)
T signal            8 Y    8 (14)
T 173               8 M    8 (12)
T 174               8 L    8 (12)
Confidence            3


No 3  
>Q96CB9_25_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=2.65  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         2 K    2 (6)
Q Consensus         2 k    2 (6)
                      +
T Consensus        30 ~   30 (30)
T signal           30 K   30 (30)
T 100_Pediculus    30 F   30 (30)
T 96_Nematostell   30 V   30 (30)
T 80_Bombus        30 L   30 (30)
T 78_Aedes         30 T   30 (30)
T 63_Columba       30 H   30 (30)
Confidence            0


No 4  
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.42  E-value=62  Score=5.80  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.9

Q 5_Mustela         4 GD    5 (6)
Q Consensus         4 gd    5 (6)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 5  
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=0.40  E-value=65  Score=5.77  Aligned_cols=2  Identities=50%  Similarity=1.464  Sum_probs=1.0

Q 5_Mustela         4 GD    5 (6)
Q Consensus         4 gd    5 (6)
                      ||
T Consensus        24 GD   25 (28)
T signal           24 GN   25 (28)
T 102              22 GD   23 (26)
T 105              22 GD   23 (26)
T 110              22 GD   23 (26)
T 111              22 GD   23 (26)
T 108              22 GD   23 (26)
T 113              22 GD   23 (26)
T 76               22 GD   23 (26)
Confidence            44


No 6  
>P01098_23_Mito_ref_sig5_130
Probab=0.40  E-value=66  Score=5.80  Aligned_cols=2  Identities=50%  Similarity=1.364  Sum_probs=0.8

Q 5_Mustela         3 YG    4 (6)
Q Consensus         3 yg    4 (6)
                      |.
T Consensus        23 ys   24 (28)
T signal           23 YS   24 (28)
Confidence            33


No 7  
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.38  E-value=69  Score=5.16  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        42 ~   42 (56)
T signal           42 S   42 (56)
T 34_Volvox        39 -   38 (38)
T 134_Guillardia   39 -   38 (38)
T 152_Trichinell   39 -   38 (38)
T 159_Ixodes       39 -   38 (38)
T 41_Galdieria     39 -   38 (38)
T 112_Tribolium    39 -   38 (38)
T 117_Ceratitis    39 -   38 (38)
T 111_Musca        39 -   38 (38)
T 39_Selaginella   39 -   38 (38)
Confidence            2


No 8  
>P07919_13_IM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.04  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.8

Q 5_Mustela         4 GD    5 (6)
Q Consensus         4 gd    5 (6)
                      ||
T Consensus        14 gD   15 (18)
T signal           14 GD   15 (18)
T 41               14 GD   15 (18)
T 13               14 RD   15 (18)
T 23               14 GD   15 (18)
T 58               13 ED   14 (17)
T 61               14 GD   15 (18)
T 18               14 GE   15 (18)
T 22               14 GD   15 (18)
T 59               14 GD   15 (18)
T 79               11 GE   12 (15)
Confidence            33


No 9  
>Q00711_28_IM_ref_sig5_130
Probab=0.35  E-value=74  Score=5.71  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        32 G   32 (33)
T signal           32 G   32 (33)
T 13               28 -   27 (27)
T 46               32 G   32 (33)
T 71               32 G   32 (33)
T 14               33 G   33 (34)
T 18               31 G   31 (32)
Confidence            2


No 10 
>P17764_30_Mito_ref_sig5_130
Probab=0.34  E-value=78  Score=5.78  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         3 Y    3 (6)
Q Consensus         3 y    3 (6)
                      |
T Consensus        30 Y   30 (35)
T signal           30 Y   30 (35)
T 144              33 Y   33 (38)
T 149              33 Y   33 (38)
T 152              38 Y   38 (43)
T 154              33 Y   33 (38)
T 157              21 Y   21 (26)
T 161              33 Y   33 (38)
T 153              21 Y   21 (26)
T 134              33 Y   33 (38)
T 159              33 Y   33 (38)
Confidence            3


No 11 
>P36967_16_Mito_ref_sig5_130
Probab=0.33  E-value=79  Score=5.22  Aligned_cols=2  Identities=50%  Similarity=1.946  Sum_probs=0.8

Q 5_Mustela         3 YG    4 (6)
Q Consensus         3 yg    4 (6)
                      ||
T Consensus        11 fg   12 (21)
T signal           11 FG   12 (21)
Confidence            33


No 12 
>P22142_42_IM_ref_sig5_130
Probab=0.32  E-value=83  Score=4.37  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         3 Y    3 (6)
Q Consensus         3 y    3 (6)
                      |
T Consensus        42 ~   42 (47)
T signal           42 Y   42 (47)
T 180_Trichinell   28 -   27 (27)
T 177_Loa          28 -   27 (27)
T 164_Beta         28 -   27 (27)
T 135_Galdieria    28 -   27 (27)
Confidence            2


No 13 
>P36516_59_Mito_ref_sig5_130
Probab=0.32  E-value=83  Score=6.48  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.2

Q 5_Mustela         3 Y    3 (6)
Q Consensus         3 y    3 (6)
                      |
T Consensus        51 y   51 (64)
T signal           51 Y   51 (64)
Confidence            2


No 14 
>P99028_13_IM_ref_sig5_130
Probab=0.31  E-value=83  Score=4.94  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         5 D    5 (6)
Q Consensus         5 d    5 (6)
                      |
T Consensus        15 D   15 (18)
T signal           15 D   15 (18)
T 23               15 D   15 (18)
T 42               15 D   15 (18)
T 3                15 D   15 (18)
T 14               15 D   15 (18)
T 24               15 D   15 (18)
T 59               14 D   14 (17)
T 62               15 D   15 (18)
Confidence            3


No 15 
>P12063_32_MM_ref_sig5_130
Probab=0.30  E-value=88  Score=5.76  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.9

Q 5_Mustela         2 KY    3 (6)
Q Consensus         2 ky    3 (6)
                      ||
T Consensus        17 ky   18 (37)
T signal           17 KY   18 (37)
T 193              17 KY   18 (37)
Confidence            44


No 16 
>P14063_12_Mito_ref_sig5_130
Probab=0.28  E-value=95  Score=4.66  Aligned_cols=2  Identities=50%  Similarity=1.946  Sum_probs=0.8

Q 5_Mustela         3 YG    4 (6)
Q Consensus         3 yg    4 (6)
                      ||
T Consensus         2 FG    3 (17)
T signal            2 FG    3 (17)
T 2                 2 FG    3 (17)
T 33                2 FG    3 (17)
T 7                 2 FG    3 (17)
T 35                1 --    0 (13)
T 20                2 FG    3 (17)
T 19                2 FG    3 (17)
T 13                1 --    0 (12)
Confidence            34


No 17 
>Q9SVM8_34_Mito_ref_sig5_130
Probab=0.27  E-value=97  Score=5.67  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus         8 G    8 (39)
T signal            8 G    8 (39)
T 10                8 G    8 (40)
T 8                 8 G    8 (40)
T 9                 8 G    8 (40)
T 11                8 G    8 (40)
T 4                 8 G    8 (44)
T 5                 8 G    8 (41)
T 6                 8 G    8 (41)
T 7                 8 G    8 (41)
T 3                 8 G    8 (49)
Confidence            3


No 18 
>P23965_28_MM_ref_sig5_130
Probab=0.27  E-value=98  Score=5.41  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        30 ~   30 (33)
T signal           30 S   30 (33)
T 94               40 V   40 (43)
T 93               43 A   43 (46)
T 72               46 G   46 (49)
T 79               43 G   43 (46)
T 63               31 A   31 (34)
T 78               34 G   34 (37)
T 39               41 G   41 (44)
Confidence            3


No 19 
>P0C2C4_29_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.49  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         4 G    4 (6)
Q Consensus         4 g    4 (6)
                      |
T Consensus        30 g   30 (34)
T signal           30 G   30 (34)
Confidence            2


No 20 
>O13401_34_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.62  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.0

Q 5_Mustela         2 KY    3 (6)
Q Consensus         2 ky    3 (6)
                      ||
T Consensus        35 ky   36 (39)
T signal           35 KY   36 (39)


Done!
