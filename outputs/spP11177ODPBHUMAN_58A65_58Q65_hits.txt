Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:13 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_58A65_58Q65_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9Z0J5_36_Mito_ref_sig5_130      0.3      87     0.2    3.5   0.0    1    3-3      41-41  (41)
  2 P00366_57_MM_ref_sig5_130        0.3      90    0.21    4.3   0.0    1    3-3      62-62  (62)
  3 P22315_53_IM_ref_sig5_130        0.3      91    0.21    6.4   0.0    1    4-4      50-50  (58)
  4 P09380_17_Mito_MM_Nu_ref_sig5_   0.3      93    0.21    5.2   0.0    1    4-4       8-8   (22)
  5 Q9W6G7_44_Mito_ref_sig5_130      0.3      98    0.23    6.1   0.0    1    5-5      26-26  (49)
  6 P11796_24_MM_ref_sig5_130        0.3      99    0.23    5.4   0.0    1    4-4      21-21  (29)
  7 P17614_54_Mito_IM_ref_sig5_130   0.3   1E+02    0.23    4.9   0.0    6    1-6      50-55  (59)
  8 P17694_33_IM_ref_sig5_130        0.3   1E+02    0.23    2.5   0.0    1    4-4      38-38  (38)
  9 Q25417_9_Mito_ref_sig5_130       0.3   1E+02    0.24    4.6   0.0    3    1-3      10-12  (14)
 10 Q6UPE0_34_IM_ref_sig5_130        0.2 1.1E+02    0.24    2.6   0.0    7    1-7      33-39  (39)
 11 P22570_32_MM_ref_sig5_130        0.2 1.1E+02    0.24    2.9   0.0    2    4-5      31-32  (37)
 12 P01098_23_Mito_ref_sig5_130      0.2 1.1E+02    0.25    5.3   0.0    2    6-7      25-26  (28)
 13 P82928_72_Mito_ref_sig5_130      0.2 1.1E+02    0.25    6.5   0.0    7    1-7      67-73  (77)
 14 P17695_35_Cy_Mito_ref_sig5_130   0.2 1.1E+02    0.25    5.8   0.0    3    2-4      36-38  (40)
 15 P36527_26_Mito_ref_sig5_130      0.2 1.1E+02    0.26    5.4   0.0    3    2-4       2-4   (31)
 16 O75879_30_Mito_ref_sig5_130      0.2 1.1E+02    0.26    5.5   0.0    6    2-7      17-22  (35)
 17 P28834_11_Mito_ref_sig5_130      0.2 1.1E+02    0.26    4.6   0.0    2    3-4      15-16  (16)
 18 P30048_62_Mito_ref_sig5_130      0.2 1.1E+02    0.26    3.8   0.0    3    2-4      65-67  (67)
 19 P81140_13_MM_ref_sig5_130        0.2 1.2E+02    0.27    4.7   0.0    3    4-6      16-18  (18)
 20 P34897_29_Mito_MM_Nu_IM_ref_si   0.2 1.2E+02    0.27    3.2   0.0    4    1-4      30-33  (34)

No 1  
>Q9Z0J5_36_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=3.46  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        41 ~   41 (41)
T signal           41 N   41 (41)
T 85_Bombus        35 -   34 (34)
T 58_Orcinus       35 -   34 (34)
T 44_Chrysemys     35 -   34 (34)
T 30_Bos           35 -   34 (34)
Confidence            0


No 2  
>P00366_57_MM_ref_sig5_130
Probab=0.29  E-value=90  Score=4.30  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        62 ~   62 (62)
T signal           62 D   62 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            0


No 3  
>P22315_53_IM_ref_sig5_130
Probab=0.29  E-value=91  Score=6.42  Aligned_cols=1  Identities=0%  Similarity=0.401  Sum_probs=0.2

Q 5_Mustela         4 Q.    4 (7)
Q Consensus         4 q.    4 (7)
                      | 
T Consensus        50 q.   50 (58)
T signal           50 H.   50 (58)
T 170              51 Q.   51 (59)
T 162              24 Q.   24 (32)
T 165              28 Q.   28 (36)
T 155              32 R.   32 (40)
T 141              46 P.   46 (54)
T 142              45 -.   44 (44)
T 148              30 Qp   31 (39)
T 137              41 K.   41 (41)
Confidence            2 


No 4  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=0.28  E-value=93  Score=5.20  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      |
T Consensus         8 Q    8 (22)
T signal            8 Q    8 (22)
T 26                8 Q    8 (20)
T 18                8 Q    8 (20)
T 21                8 Q    8 (20)
T 22                8 Q    8 (20)
T 23                8 Q    8 (21)
T 27                8 Q    8 (21)
T 30                8 Q    8 (21)
T 24                8 Q    8 (20)
T 17                7 Q    7 (18)
Confidence            2


No 5  
>Q9W6G7_44_Mito_ref_sig5_130
Probab=0.27  E-value=98  Score=6.14  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         5 Y    5 (7)
Q Consensus         5 y    5 (7)
                      |
T Consensus        26 y   26 (49)
T signal           26 Y   26 (49)
Confidence            3


No 6  
>P11796_24_MM_ref_sig5_130
Probab=0.27  E-value=99  Score=5.41  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      |
T Consensus        21 ~   21 (29)
T signal           21 Q   21 (29)
T 188              21 S   21 (29)
T 181              24 G   24 (32)
T 183              22 G   22 (30)
T 184              25 Q   25 (33)
T 175              26 S   26 (34)
T 180              23 H   23 (31)
T 186              34 H   34 (42)
T 187              32 Q   32 (40)
Confidence            2


No 7  
>P17614_54_Mito_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=4.94  Aligned_cols=6  Identities=33%  Similarity=0.601  Sum_probs=0.0

Q 5_Mustela         1 EVAQYD    6 (7)
Q Consensus         1 evaqyd    6 (7)
                      ..++|.
T Consensus        50 ~~a~~a   55 (59)
T signal           50 RAVQYA   55 (59)
T 74_Ficedula      51 ETRLVL   56 (58)
T cl|SABBABABA|1   51 PTKAET   56 (59)
T cl|MIBBABABA|1   51 GDFVTD   56 (59)
T cl|NOBBABABA|1   51 MQTRGF   56 (59)
T cl|SOBBABABA|1   51 GKVVAV   56 (59)


No 8  
>P17694_33_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=2.52  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.0

Q 5_Mustela         4 Q    4 (7)
Q Consensus         4 q    4 (7)
                      |
T Consensus        38 ~   38 (38)
T signal           38 Q   38 (38)
T 203_Boea         38 -   37 (37)
T 189_Naegleria    38 -   37 (37)
T 167_Loa          38 -   37 (37)
T 125_Talaromyce   38 -   37 (37)
T 84_Andalucia     38 -   37 (37)


No 9  
>Q25417_9_Mito_ref_sig5_130
Probab=0.25  E-value=1e+02  Score=4.58  Aligned_cols=3  Identities=67%  Similarity=0.844  Sum_probs=0.0

Q 5_Mustela         1 EVA    3 (7)
Q Consensus         1 eva    3 (7)
                      |.|
T Consensus        10 ema   12 (14)
T signal           10 EMA   12 (14)


No 10 
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=2.55  Aligned_cols=7  Identities=0%  Similarity=-0.221  Sum_probs=0.0

Q 5_Mustela         1 EVAQYDG    7 (7)
Q Consensus         1 evaqydg    7 (7)
                      +...+-|
T Consensus        33 ~~~~~~~   39 (39)
T signal           33 CAVASAA   39 (39)
T 99_Culex         33 ASSTG--   37 (37)
T 93_Megachile     33 YFNYD--   37 (37)
T 68_Ciona         33 YTHVI--   37 (37)
T 66_Xiphophorus   33 SASHQ--   37 (37)
T 61_Oryzias       33 TCSPK--   37 (37)


No 11 
>P22570_32_MM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=2.87  Aligned_cols=2  Identities=0%  Similarity=1.049  Sum_probs=0.0

Q 5_Mustela         4 QY    5 (7)
Q Consensus         4 qy    5 (7)
                      |+
T Consensus        31 ~~   32 (37)
T signal           31 HF   32 (37)
T 90_Drosophila    30 --   29 (29)
T 84_Megachile     30 --   29 (29)
T 73_Nematostell   30 --   29 (29)
T 49_Columba       30 --   29 (29)
T 38_Myotis        30 --   29 (29)


No 12 
>P01098_23_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.35  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.0

Q 5_Mustela         6 DG    7 (7)
Q Consensus         6 dg    7 (7)
                      ||
T Consensus        25 dg   26 (28)
T signal           25 DG   26 (28)


No 13 
>P82928_72_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=6.54  Aligned_cols=7  Identities=43%  Similarity=0.653  Sum_probs=0.0

Q 5_Mustela         1 EVAQYDG    7 (7)
Q Consensus         1 evaqydg    7 (7)
                      |..|+.|
T Consensus        67 El~~~~~   73 (77)
T signal           67 ELARTRG   73 (77)
T 46               67 ERGQ-KG   72 (76)
T 56               67 EPLQ-SN   72 (76)
T 52               67 RPMQY-G   72 (76)
T 54               67 ELRL--E   71 (75)
T 61               67 ELGEKPA   73 (77)
T 45               67 EPQQ--D   71 (75)
T 26               28 ETS----   30 (30)
T 24               26 -------   25 (25)


No 14 
>P17695_35_Cy_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.75  Aligned_cols=3  Identities=67%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         2 VAQ    4 (7)
Q Consensus         2 vaq    4 (7)
                      |.|
T Consensus        36 VSQ   38 (40)
T signal           36 VSQ   38 (40)
T 42               29 VSQ   31 (33)
T 41               27 VSQ   29 (31)
T 40               29 VSQ   31 (33)


No 15 
>P36527_26_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.42  Aligned_cols=3  Identities=67%  Similarity=0.767  Sum_probs=0.0

Q 5_Mustela         2 VAQ    4 (7)
Q Consensus         2 vaq    4 (7)
                      +||
T Consensus         2 laq    4 (31)
T signal            2 LAQ    4 (31)


No 16 
>O75879_30_Mito_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=5.49  Aligned_cols=6  Identities=50%  Similarity=0.789  Sum_probs=0.0

Q 5_Mustela         2 VAQYDG    7 (7)
Q Consensus         2 vaqydg    7 (7)
                      .|-.||
T Consensus        17 lA~vdg   22 (35)
T signal           17 FARVDG   22 (35)
T 147              17 LVWVYG   22 (35)
T 149              17 LAWIDD   22 (35)
T 152              17 FALVDR   22 (31)
T 165              17 LVWIHG   22 (35)
T 172              17 LANIDR   22 (35)
T 144              17 SALLAS   22 (35)
T 146              17 LAWVDS   22 (35)
T 160              17 LACVPR   22 (35)


No 17 
>P28834_11_Mito_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.64  Aligned_cols=2  Identities=100%  Similarity=0.850  Sum_probs=0.0

Q 5_Mustela         3 AQ    4 (7)
Q Consensus         3 aq    4 (7)
                      ||
T Consensus        15 aq   16 (16)
T signal           15 AQ   16 (16)


No 18 
>P30048_62_Mito_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=3.84  Aligned_cols=3  Identities=67%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         2 VAQ    4 (7)
Q Consensus         2 vaq    4 (7)
                      +.|
T Consensus        65 ~~~   67 (67)
T signal           65 VTQ   67 (67)
T 79_Drosophila    66 FR-   67 (67)
T 76_Meleagris     66 SD-   67 (67)
T 70_Strongyloce   66 TS-   67 (67)
T 69_Metaseiulus   66 GA-   67 (67)
T 63_Columba       66 VS-   67 (67)
T 57_Falco         66 KV-   67 (67)
T 44_Camelus       66 LD-   67 (67)


No 19 
>P81140_13_MM_ref_sig5_130
Probab=0.23  E-value=1.2e+02  Score=4.68  Aligned_cols=3  Identities=33%  Similarity=1.276  Sum_probs=0.0

Q 5_Mustela         4 QYD    6 (7)
Q Consensus         4 qyd    6 (7)
                      .+|
T Consensus        16 eFD   18 (18)
T signal           16 EFD   18 (18)
T 122              16 KFD   18 (18)
T 123              16 DFD   18 (18)
T 127              16 EFD   18 (18)
T 129              17 AFE   19 (19)
T 130              16 EFD   18 (18)
T 145              16 EFD   18 (18)
T 131              14 VFD   16 (16)
T 113              12 EFD   14 (14)


No 20 
>P34897_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=3.18  Aligned_cols=4  Identities=50%  Similarity=0.509  Sum_probs=0.0

Q 5_Mustela         1 EVAQ    4 (7)
Q Consensus         1 evaq    4 (7)
                      +.++
T Consensus        30 ~~~~   33 (34)
T signal           30 NAAQ   33 (34)
T 86_Glycine       30 SSLS   33 (33)
T 87_Trichoplax    30 KKRQ   33 (33)
T 76_Aplysia       30 QEGI   33 (33)
T 74_Ciona         30 GRES   33 (33)
T 60_Maylandia     30 FDRT   33 (33)
T 58_Pseudopodoc   30 AAAA   33 (33)


Done!
