Query         5_Mustela
Match_columns 7
No_of_seqs    6 out of 20
Neff          1.5 
Searched_HMMs 436
Date          Wed Sep  6 16:07:51 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_4A11_4Q11_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P05630_22_Mito_IM_ref_sig5_130  45.2   0.096 0.00022   13.5   0.0    6    2-7       5-10  (27)
  2 P36526_16_Mito_ref_sig5_130     28.9    0.26 0.00059   11.6   0.0    6    2-7      15-20  (21)
  3 P10818_26_IM_ref_sig5_130       24.4    0.35 0.00081   12.2   0.0    6    1-6      12-17  (31)
  4 P12074_24_IM_ref_sig5_130       10.1     1.4  0.0031   10.5   0.0    6    1-6      10-15  (29)
  5 P11178_55_MM_ref_sig5_130        4.6     3.8  0.0087   10.7   0.0    1    5-5      34-34  (60)
  6 P42026_37_Mito_ref_sig5_130      4.1     4.4    0.01    9.7   0.0    2    2-3       8-9   (42)
  7 P0C2C1_34_Mito_ref_sig5_130      3.7       5   0.011    9.5   0.0    4    4-7       9-12  (39)
  8 P36428_49_Mito_Cy_ref_sig5_130   3.7       5   0.012   10.1   0.0    4    4-7      26-29  (54)
  9 Q9ZP06_22_MM_ref_sig5_130        3.6     5.2   0.012    8.8   0.0    4    3-6      17-20  (27)
 10 Q9SVM8_34_Mito_ref_sig5_130      3.3     5.8   0.013    9.4   0.0    5    2-6       9-13  (39)
 11 P05091_17_MM_ref_sig5_130        3.1     6.2   0.014    8.3   0.0    2    5-6      14-15  (22)
 12 Q0P5L8_21_Mito_Nu_ref_sig5_130   2.9     6.7   0.015    5.1   0.0    1    3-3      26-26  (26)
 13 Q02376_27_IM_ref_sig5_130        2.8     7.1   0.016    8.8   0.0    1    3-3       6-6   (32)
 14 P80433_25_IM_ref_sig5_130        2.7     7.4   0.017    8.7   0.0    1    5-5      17-17  (30)
 15 Q04728_8_MM_ref_sig5_130         2.7     7.5   0.017    7.3   0.0    5    2-6       6-10  (13)
 16 P11325_9_MM_ref_sig5_130         2.6     7.5   0.017    7.4   0.0    3    5-7       3-5   (14)
 17 P30038_24_MM_ref_sig5_130        2.6     7.7   0.018    8.5   0.0    1    6-6      15-15  (29)
 18 P12695_28_MM_ref_sig5_130        2.5     7.9   0.018    8.7   0.0    1    3-3       4-4   (33)
 19 P17764_30_Mito_ref_sig5_130      2.3     8.8    0.02    8.6   0.0    1    5-5      17-17  (35)
 20 Q5JRX3_15_MM_ref_sig5_130        2.3     8.9    0.02    7.5   0.0    2    4-5      13-14  (20)

No 1  
>P05630_22_Mito_IM_ref_sig5_130
Probab=45.16  E-value=0.096  Score=13.49  Aligned_cols=6  Identities=67%  Similarity=1.348  Sum_probs=3.6

Q 5_Mustela         2 GLMRRP    7 (7)
Q 8_Pteropus        2 GLLRRP    7 (7)
Q 12_Sorex          2 GLARRP    7 (7)
Q 15_Ictidomys      2 GLVRRP    7 (7)
Q 11_Cavia          2 ALVQRP    7 (7)
Q 16_Equus          2 GLVRRS    7 (7)
Q Consensus         2 gl~rrp    7 (7)
                      .|+|||
T Consensus         5 allrr~   10 (27)
T signal            5 ALLRRP   10 (27)
T 133               5 ALLRRP   10 (27)
T 148               5 SLLRHP   10 (27)
T 167               5 TLLRRS   10 (27)
T 155               5 ALLRRP   10 (27)
T 147               5 AVFRRP   10 (27)
T 140               5 TLLRRL   10 (26)
T 142               5 ALLRRC   10 (32)
T 121               5 RLLLAL   10 (34)
T 134               5 RLLARP   10 (29)
Confidence            466665


No 2  
>P36526_16_Mito_ref_sig5_130
Probab=28.87  E-value=0.26  Score=11.58  Aligned_cols=6  Identities=33%  Similarity=0.783  Sum_probs=3.8

Q 5_Mustela         2 GLMRRP    7 (7)
Q 8_Pteropus        2 GLLRRP    7 (7)
Q 12_Sorex          2 GLARRP    7 (7)
Q 15_Ictidomys      2 GLVRRP    7 (7)
Q 11_Cavia          2 ALVQRP    7 (7)
Q 16_Equus          2 GLVRRS    7 (7)
Q Consensus         2 gl~rrp    7 (7)
                      .+++||
T Consensus        15 s~LtRP   20 (21)
T signal           15 NALTRP   20 (21)
T 11               15 SLLTRP   20 (21)
T 13               15 SQLTRP   20 (21)
T 14               15 TQLTRP   20 (21)
T 18               14 TLLTRP   19 (20)
T 20               15 TNLLRP   20 (21)
T 5                15 TNLRRP   20 (21)
T 7                15 ASLRRP   20 (21)
T 8                15 ANLRRP   20 (21)
T 9                11 SLLKRP   16 (17)
Confidence            356776


No 3  
>P10818_26_IM_ref_sig5_130
Probab=24.38  E-value=0.35  Score=12.23  Aligned_cols=6  Identities=67%  Similarity=1.071  Sum_probs=3.0

Q 5_Mustela         1 SGLMRR    6 (7)
Q 8_Pteropus        1 SGLLRR    6 (7)
Q 12_Sorex          1 SGLARR    6 (7)
Q 15_Ictidomys      1 SGLVRR    6 (7)
Q 11_Cavia          1 SALVQR    6 (7)
Q 16_Equus          1 SGLVRR    6 (7)
Q Consensus         1 sgl~rr    6 (7)
                      |||+.|
T Consensus        12 SgLLgR   17 (31)
T signal           12 SGLLGR   17 (31)
T 53                9 SGLLLR   14 (28)
T 56                9 FGLPVR   14 (28)
T 64                9 SRLLGR   14 (28)
T 36               11 SLLLGG   16 (30)
T 44                9 SRLLGR   14 (28)
T 55                9 SRLLNW   14 (28)
T 6                11 LRHLGR   16 (30)
T 7                10 SGLLGR   15 (29)
T 43                7 SGILRR   12 (25)
Confidence            455544


No 4  
>P12074_24_IM_ref_sig5_130
Probab=10.06  E-value=1.4  Score=10.54  Aligned_cols=6  Identities=50%  Similarity=0.650  Sum_probs=2.5

Q 5_Mustela         1 SGLMRR    6 (7)
Q 8_Pteropus        1 SGLLRR    6 (7)
Q 12_Sorex          1 SGLARR    6 (7)
Q 15_Ictidomys      1 SGLVRR    6 (7)
Q 11_Cavia          1 SALVQR    6 (7)
Q 16_Equus          1 SGLVRR    6 (7)
Q Consensus         1 sgl~rr    6 (7)
                      |+|+-|
T Consensus        10 s~lLgR   15 (29)
T signal           10 SRLLGR   15 (29)
T 32               10 SRPLGR   15 (29)
T 37               12 SGLLGR   17 (31)
T 43               10 SRLLGR   15 (29)
T 54               10 SRLLNW   15 (29)
T 44               10 SGLLSR   15 (29)
T 51               10 FGLPVR   15 (29)
T 59               10 SRLLGR   15 (29)
T 60               10 AGLLGR   15 (29)
T 7                 8 LRHLGR   13 (27)
Confidence            344433


No 5  
>P11178_55_MM_ref_sig5_130
Probab=4.62  E-value=3.8  Score=10.67  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R.    5 (7)
Q 8_Pteropus        5 R.    5 (7)
Q 12_Sorex          5 R.    5 (7)
Q 15_Ictidomys      5 R.    5 (7)
Q 11_Cavia          5 Q.    5 (7)
Q 16_Equus          5 R.    5 (7)
Q Consensus         5 r.    5 (7)
                      | 
T Consensus        34 r.   34 (60)
T signal           34 R.   34 (60)
T 110              26 Rg   27 (52)
T 149              23 R.   23 (48)
T 151              26 R.   26 (51)
T 153              26 Q.   26 (51)
T 164              23 R.   23 (47)
T 127              20 A.   20 (44)
T 132              21 R.   21 (46)
T 114               1 R.    1 (28)
T 126               1 M.    1 (29)
Confidence            3 


No 6  
>P42026_37_Mito_ref_sig5_130
Probab=4.09  E-value=4.4  Score=9.71  Aligned_cols=2  Identities=50%  Similarity=0.501  Sum_probs=0.7

Q 5_Mustela         2 GL    3 (7)
Q 8_Pteropus        2 GL    3 (7)
Q 12_Sorex          2 GL    3 (7)
Q 15_Ictidomys      2 GL    3 (7)
Q 11_Cavia          2 AL    3 (7)
Q 16_Equus          2 GL    3 (7)
Q Consensus         2 gl    3 (7)
                      ||
T Consensus         8 gl    9 (42)
T signal            8 RL    9 (42)
T 163               8 GL    9 (43)
T 166               8 GL    9 (43)
T 160               8 GP    9 (43)
T 150               4 GL    5 (39)
T 144               8 GL    9 (37)
T 133               1 --    0 (33)
T 136               1 --    0 (42)
T 138               1 --    0 (30)
T 143               1 --    0 (29)
Confidence            33


No 7  
>P0C2C1_34_Mito_ref_sig5_130
Probab=3.73  E-value=5  Score=9.49  Aligned_cols=4  Identities=50%  Similarity=1.282  Sum_probs=1.6

Q 5_Mustela         4 MRRP    7 (7)
Q 8_Pteropus        4 LRRP    7 (7)
Q 12_Sorex          4 ARRP    7 (7)
Q 15_Ictidomys      4 VRRP    7 (7)
Q 11_Cavia          4 VQRP    7 (7)
Q 16_Equus          4 VRRS    7 (7)
Q Consensus         4 ~rrp    7 (7)
                      ++||
T Consensus         9 ~q~~   12 (39)
T signal            9 FQRP   12 (39)
T 41                9 LQGP   12 (39)
T 50                9 VQGL   12 (39)
T 56                9 VQRP   12 (39)
T 17                8 AKRS   11 (39)
T 31                9 VQKN   12 (40)
T 30                5 WIGL    8 (40)
T 15                1 ----    0 (26)
T 35                1 ----    0 (26)
T 26                1 ----    0 (26)
Confidence            3443


No 8  
>P36428_49_Mito_Cy_ref_sig5_130
Probab=3.68  E-value=5  Score=10.14  Aligned_cols=4  Identities=75%  Similarity=1.647  Sum_probs=1.7

Q 5_Mustela         4 MRRP    7 (7)
Q 8_Pteropus        4 LRRP    7 (7)
Q 12_Sorex          4 ARRP    7 (7)
Q 15_Ictidomys      4 VRRP    7 (7)
Q 11_Cavia          4 VQRP    7 (7)
Q 16_Equus          4 VRRS    7 (7)
Q Consensus         4 ~rrp    7 (7)
                      +|||
T Consensus        26 lrrp   29 (54)
T signal           26 LRRP   29 (54)
T 193              28 LRRP   31 (54)
Confidence            3444


No 9  
>Q9ZP06_22_MM_ref_sig5_130
Probab=3.60  E-value=5.2  Score=8.81  Aligned_cols=4  Identities=50%  Similarity=1.140  Sum_probs=1.9

Q 5_Mustela         3 LMRR    6 (7)
Q 8_Pteropus        3 LLRR    6 (7)
Q 12_Sorex          3 LARR    6 (7)
Q 15_Ictidomys      3 LVRR    6 (7)
Q 11_Cavia          3 LVQR    6 (7)
Q 16_Equus          3 LVRR    6 (7)
Q Consensus         3 l~rr    6 (7)
                      ++||
T Consensus        17 vlRR   20 (27)
T signal           17 VIRR   20 (27)
T 93               17 VSRR   20 (27)
T 95               17 LLRR   20 (27)
T 97               18 LLTR   21 (28)
T 91               21 LLRR   24 (31)
T 94               17 V-RR   19 (26)
T 98               16 ILRR   19 (26)
Confidence            4455


No 10 
>Q9SVM8_34_Mito_ref_sig5_130
Probab=3.29  E-value=5.8  Score=9.39  Aligned_cols=5  Identities=60%  Similarity=1.305  Sum_probs=2.3

Q 5_Mustela         2 GLMRR    6 (7)
Q 8_Pteropus        2 GLLRR    6 (7)
Q 12_Sorex          2 GLARR    6 (7)
Q 15_Ictidomys      2 GLVRR    6 (7)
Q 11_Cavia          2 ALVQR    6 (7)
Q 16_Equus          2 GLVRR    6 (7)
Q Consensus         2 gl~rr    6 (7)
                      +|+|+
T Consensus         9 ~lLRq   13 (39)
T signal            9 GLLRQ   13 (39)
T 10                9 SLVRQ   13 (40)
T 8                 9 NLLRQ   13 (40)
T 9                 9 NVLRQ   13 (40)
T 11                9 NILRQ   13 (40)
T 4                 9 NLLRQ   13 (44)
T 5                 9 SLLRH   13 (41)
T 6                 9 SLLRH   13 (41)
T 7                 9 GLLRH   13 (41)
T 3                 9 GLLRR   13 (49)
Confidence            35554


No 11 
>P05091_17_MM_ref_sig5_130
Probab=3.13  E-value=6.2  Score=8.28  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.8

Q 5_Mustela         5 RR    6 (7)
Q 8_Pteropus        5 RR    6 (7)
Q 12_Sorex          5 RR    6 (7)
Q 15_Ictidomys      5 RR    6 (7)
Q 11_Cavia          5 QR    6 (7)
Q 16_Equus          5 RR    6 (7)
Q Consensus         5 rr    6 (7)
                      ||
T Consensus        14 rr   15 (22)
T signal           14 RR   15 (22)
T 58               11 CR   12 (19)
T 61               11 RR   12 (19)
T 73               10 -R   10 (17)
T 72               18 RR   19 (26)
T 45               17 RA   18 (25)
T 47               17 SR   18 (22)
T 51               17 RQ   18 (24)
Confidence            44


No 12 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=2.91  E-value=6.7  Score=5.09  Aligned_cols=1  Identities=0%  Similarity=-0.529  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q 8_Pteropus        3 L    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 15_Ictidomys      3 L    3 (7)
Q 11_Cavia          3 L    3 (7)
Q 16_Equus          3 L    3 (7)
Q Consensus         3 l    3 (7)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 13 
>Q02376_27_IM_ref_sig5_130
Probab=2.78  E-value=7.1  Score=8.80  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 L    3 (7)
Q 8_Pteropus        3 L    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 15_Ictidomys      3 L    3 (7)
Q 11_Cavia          3 L    3 (7)
Q 16_Equus          3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus         6 l    6 (32)
T signal            6 L    6 (32)
T 10                6 L    6 (33)
T 11                6 L    6 (32)
T 17                6 A    6 (32)
T 30                6 L    6 (32)
T 13                6 V    6 (32)
T 12                6 L    6 (32)
T 8                 7 Q    7 (33)
Confidence            2


No 14 
>P80433_25_IM_ref_sig5_130
Probab=2.69  E-value=7.4  Score=8.70  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 8_Pteropus        5 R    5 (7)
Q 12_Sorex          5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q 11_Cavia          5 Q    5 (7)
Q 16_Equus          5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        17 r   17 (30)
T signal           17 R   17 (30)
T 3                17 R   17 (30)
T 20               17 R   17 (30)
T 22               17 R   17 (30)
T 31               17 P   17 (30)
T 36               17 R   17 (30)
T 5                17 R   17 (30)
T 8                17 R   17 (30)
T 19               17 R   17 (30)
Confidence            3


No 15 
>Q04728_8_MM_ref_sig5_130
Probab=2.67  E-value=7.5  Score=7.29  Aligned_cols=5  Identities=40%  Similarity=0.793  Sum_probs=2.7

Q 5_Mustela         2 GLMRR    6 (7)
Q 8_Pteropus        2 GLLRR    6 (7)
Q 12_Sorex          2 GLARR    6 (7)
Q 15_Ictidomys      2 GLVRR    6 (7)
Q 11_Cavia          2 ALVQR    6 (7)
Q 16_Equus          2 GLVRR    6 (7)
Q Consensus         2 gl~rr    6 (7)
                      -|++|
T Consensus         6 tllq~   10 (13)
T signal            6 TLLQR   10 (13)
T 90                6 TLLQQ   10 (13)
T 88                6 TLLQH   10 (13)
Confidence            35665


No 16 
>P11325_9_MM_ref_sig5_130
Probab=2.65  E-value=7.5  Score=7.42  Aligned_cols=3  Identities=67%  Similarity=1.342  Sum_probs=1.2

Q 5_Mustela         5 RRP    7 (7)
Q 8_Pteropus        5 RRP    7 (7)
Q 12_Sorex          5 RRP    7 (7)
Q 15_Ictidomys      5 RRP    7 (7)
Q 11_Cavia          5 QRP    7 (7)
Q 16_Equus          5 RRS    7 (7)
Q Consensus         5 rrp    7 (7)
                      .||
T Consensus         3 srp    5 (14)
T signal            3 SRP    5 (14)
Confidence            344


No 17 
>P30038_24_MM_ref_sig5_130
Probab=2.60  E-value=7.7  Score=8.52  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         6 R    6 (7)
Q 8_Pteropus        6 R    6 (7)
Q 12_Sorex          6 R    6 (7)
Q 15_Ictidomys      6 R    6 (7)
Q 11_Cavia          6 R    6 (7)
Q 16_Equus          6 R    6 (7)
Q Consensus         6 r    6 (7)
                      |
T Consensus        15 r   15 (29)
T signal           15 R   15 (29)
T 77               15 R   15 (29)
T 102              17 A   17 (26)
T 126              15 R   15 (29)
T 127              16 R   16 (30)
T 128              15 R   15 (29)
T 129              15 R   15 (29)
T 141              13 R   13 (27)
T 145              14 Y   14 (28)
T 121              15 R   15 (29)
Confidence            2


No 18 
>P12695_28_MM_ref_sig5_130
Probab=2.55  E-value=7.9  Score=8.72  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 L    3 (7)
Q 8_Pteropus        3 L    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 15_Ictidomys      3 L    3 (7)
Q 11_Cavia          3 L    3 (7)
Q 16_Equus          3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus         4 ~    4 (33)
T signal            4 F    4 (33)
T 9                 4 F    4 (32)
T 61                4 F    4 (36)
T 72                4 L    4 (34)
T 81                4 L    4 (34)
T 38                4 L    4 (30)
T 84                4 F    4 (30)
T 87                4 L    4 (37)
Confidence            2


No 19 
>P17764_30_Mito_ref_sig5_130
Probab=2.32  E-value=8.8  Score=8.64  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 8_Pteropus        5 R    5 (7)
Q 12_Sorex          5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q 11_Cavia          5 Q    5 (7)
Q 16_Equus          5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        17 r   17 (35)
T signal           17 R   17 (35)
T 144              20 R   20 (38)
T 149              20 Q   20 (38)
T 152              25 R   25 (43)
T 154              20 R   20 (38)
T 157               8 I    8 (26)
T 161              20 R   20 (38)
T 153               9 R    9 (26)
T 134              20 R   20 (38)
T 159              20 C   20 (38)
Confidence            3


No 20 
>Q5JRX3_15_MM_ref_sig5_130
Probab=2.31  E-value=8.9  Score=7.45  Aligned_cols=2  Identities=50%  Similarity=1.248  Sum_probs=0.8

Q 5_Mustela         4 MR    5 (7)
Q 8_Pteropus        4 LR    5 (7)
Q 12_Sorex          4 AR    5 (7)
Q 15_Ictidomys      4 VR    5 (7)
Q 11_Cavia          4 VQ    5 (7)
Q 16_Equus          4 VR    5 (7)
Q Consensus         4 ~r    5 (7)
                      +|
T Consensus        13 lr   14 (20)
T signal           13 LR   14 (20)
T 140              15 SR   16 (19)
T 151              12 LW   13 (18)
T 159              13 LW   14 (19)
T 164              13 --   12 (17)
T 112              12 LR   13 (18)
T 143              17 TR   18 (24)
T 146              13 II   14 (19)
Confidence            33


Done!
