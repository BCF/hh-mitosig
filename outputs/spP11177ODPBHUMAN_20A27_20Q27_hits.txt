Query         5_Mustela
Match_columns 7
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:16 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_20A27_20Q27_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       94.6   3E-05 6.9E-08   23.6   0.0    7    1-7      21-27  (35)
  2 P09624_21_MM_ref_sig5_130       73.3   0.013   3E-05   15.5   0.0    7    1-7      12-18  (26)
  3 P32445_17_Mito_ref_sig5_130     60.5   0.037 8.4E-05   14.1   0.0    7    1-7       8-14  (22)
  4 P19234_31_IM_ref_sig5_130       41.8    0.12 0.00027   13.8   0.0    7    1-7      22-28  (36)
  5 P04394_32_IM_ref_sig5_130       36.4    0.16 0.00037   13.5   0.0    7    1-7      23-29  (37)
  6 P53193_10_MM_ref_sig5_130       21.2    0.45   0.001   10.6   0.0    6    1-6       8-13  (15)
  7 P37298_31_IM_ref_sig5_130       20.6    0.47  0.0011   12.3   0.0    7    1-7      22-28  (36)
  8 Q0QF01_42_IM_ref_sig5_130       14.5    0.81  0.0019   12.0   0.0    6    1-6      31-36  (47)
  9 Q9YHT1_44_IM_ref_sig5_130       14.4    0.82  0.0019   12.0   0.0    6    1-6      32-37  (49)
 10 Q02486_26_Mito_Nu_ref_sig5_130   6.3     2.6  0.0059    9.6   0.0    6    1-6       9-14  (31)
 11 P12686_37_Mito_ref_sig5_130      6.1     2.7  0.0061   10.5   0.0    4    1-4      35-38  (42)
 12 P07257_16_IM_ref_sig5_130        5.3     3.2  0.0073    8.9   0.0    4    1-4      15-18  (21)
 13 Q64591_34_Mito_ref_sig5_130      5.3     3.2  0.0074   10.0   0.0    3    1-3      22-24  (39)
 14 Q01205_68_Mito_ref_sig5_130      5.1     3.4  0.0078   11.1   0.0    5    2-6      59-63  (73)
 15 P21954_16_Mito_ref_sig5_130      4.8     3.6  0.0083    8.8   0.0    4    1-4       6-9   (21)
 16 Q99757_59_Mito_ref_sig5_130      4.6     3.8  0.0088   10.7   0.0    3    1-3       8-10  (64)
 17 P53163_33_Mito_ref_sig5_130      4.4       4  0.0092    9.9   0.0    5    2-6      32-36  (38)
 18 Q95108_59_Mito_ref_sig5_130      4.2     4.2  0.0097   10.5   0.0    3    1-3       8-10  (64)
 19 P43265_42_IM_ref_sig5_130        3.8     4.8   0.011   10.0   0.0    4    1-4      15-18  (47)
 20 Q06645_61_MitoM_ref_sig5_130     3.8     4.9   0.011    9.6   0.0    3    2-4      52-54  (66)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=94.60  E-value=3e-05  Score=23.56  Aligned_cols=7  Identities=86%  Similarity=1.222  Sum_probs=6.0

Q 5_Mustela         1 RRFHRTA    7 (7)
Q 2_Macaca          1 RRFHWTA    7 (7)
Q 15_Ictidomys      1 RRFHRTT    7 (7)
Q Consensus         1 rrfh~t~    7 (7)
                      |+||||+
T Consensus        21 R~FHrs~   27 (35)
T signal           21 RRFHWTA   27 (35)
T 143              25 RGFHRSP   31 (39)
T 144              21 RSFHRST   27 (35)
T 145              21 REFHRTA   27 (35)
T 147              21 REFHRTP   27 (35)
T 135              30 SGIHGSA   36 (44)
T 141              35 RGFRLSA   41 (49)
T 142              20 RRFHGSG   26 (34)
T 149              20 REFHRSV   26 (34)
T 150              20 RHFHRTV   26 (34)
Confidence            7899985


No 2  
>P09624_21_MM_ref_sig5_130
Probab=73.34  E-value=0.013  Score=15.53  Aligned_cols=7  Identities=43%  Similarity=0.634  Sum_probs=5.2

Q 5_Mustela         1 RRF.HRTA    7 (7)
Q 2_Macaca          1 RRF.HWTA    7 (7)
Q 15_Ictidomys      1 RRF.HRTT    7 (7)
Q Consensus         1 rrf.h~t~    7 (7)
                      |+| |.|+
T Consensus        12 Rsf.s~t~   18 (26)
T signal           12 RAF.SSTV   18 (26)
T 188               8 RSF.SSSA   14 (22)
T 183              12 RSL.HQST   18 (26)
T 186              12 RSF.HQTA   18 (25)
T 191              12 RRFlSGTS   19 (27)
T 187               9 RSF.SVAS   15 (21)
Confidence            789 8764


No 3  
>P32445_17_Mito_ref_sig5_130
Probab=60.49  E-value=0.037  Score=14.11  Aligned_cols=7  Identities=57%  Similarity=0.809  Sum_probs=4.8

Q 5_Mustela         1 RRFHRTA    7 (7)
Q 2_Macaca          1 RRFHWTA    7 (7)
Q 15_Ictidomys      1 RRFHRTT    7 (7)
Q Consensus         1 rrfh~t~    7 (7)
                      |-||-|+
T Consensus         8 R~fhATa   14 (22)
T signal            8 RFFHATT   14 (22)
T 7                 8 RSLHATA   14 (22)
T 8                 8 RFFSQTA   14 (22)
T 12                8 RGFHATS   14 (22)
T 15                8 RSFHATA   14 (22)
T 16                7 RSFHATA   13 (21)
T 17                7 RSLHATA   13 (21)
T 11                4 RQFHATA   10 (18)
T 14                4 RQFHASS   10 (18)
T 5                 8 RAFSATA   14 (22)
Confidence            5688774


No 4  
>P19234_31_IM_ref_sig5_130
Probab=41.76  E-value=0.12  Score=13.84  Aligned_cols=7  Identities=57%  Similarity=0.980  Sum_probs=4.6

Q 5_Mustela         1 R.RFHRTA    7 (7)
Q 2_Macaca          1 R.RFHWTA    7 (7)
Q 15_Ictidomys      1 R.RFHRTT    7 (7)
Q Consensus         1 r.rfh~t~    7 (7)
                      | .+|.|+
T Consensus        22 R.~LHkTa   28 (36)
T signal           22 R.NLHKTA   28 (36)
T 55               22 R.CLHKTA   28 (36)
T 99               17 R.NLHKTA   23 (31)
T 2                18 R.YLHKTP   24 (32)
T 13               18 R.GLHKTV   24 (32)
T 25               18 R.ALHRSA   24 (32)
T 102              23 R.YLHGTA   29 (36)
T 104              17 R.YLHETP   23 (31)
T 1                15 R.SLHQSA   21 (28)
T 33               15 RrYLHTTT   22 (29)
Confidence            4 578774


No 5  
>P04394_32_IM_ref_sig5_130
Probab=36.45  E-value=0.16  Score=13.53  Aligned_cols=7  Identities=57%  Similarity=0.980  Sum_probs=4.5

Q 5_Mustela         1 RRFHRTA    7 (7)
Q 2_Macaca          1 RRFHWTA    7 (7)
Q 15_Ictidomys      1 RRFHRTT    7 (7)
Q Consensus         1 rrfh~t~    7 (7)
                      |..|+|+
T Consensus        23 R~LHkTa   29 (37)
T signal           23 RNLHKTA   29 (37)
T 10               18 RNLHKTA   24 (32)
T 62               17 RYLHETP   23 (31)
T 75               29 RYLHKTV   35 (43)
T 7                18 RALHRSA   24 (32)
T 24               18 RYLHKTP   24 (32)
T 35               16 RYLHTTT   22 (30)
T 42               18 RGLHKTV   24 (32)
T 76               18 RYLHRTA   24 (32)
T 3                16 RSLHQSA   22 (29)
Confidence            4578774


No 6  
>P53193_10_MM_ref_sig5_130
Probab=21.18  E-value=0.45  Score=10.58  Aligned_cols=6  Identities=67%  Similarity=1.021  Sum_probs=3.8

Q 5_Mustela         1 RRFHRT    6 (7)
Q 2_Macaca          1 RRFHWT    6 (7)
Q 15_Ictidomys      1 RRFHRT    6 (7)
Q Consensus         1 rrfh~t    6 (7)
                      |||-.|
T Consensus         8 rrf~st   13 (15)
T signal            8 RRFTST   13 (15)
T 63                8 RRFLST   13 (15)
T 62                4 RRFITT    9 (11)
Confidence            788544


No 7  
>P37298_31_IM_ref_sig5_130
Probab=20.59  E-value=0.47  Score=12.33  Aligned_cols=7  Identities=57%  Similarity=0.809  Sum_probs=3.6

Q 5_Mustela         1 RRFHRTA    7 (7)
Q 2_Macaca          1 RRFHWTA    7 (7)
Q 15_Ictidomys      1 RRFHRTT    7 (7)
Q Consensus         1 rrfh~t~    7 (7)
                      |-||-|+
T Consensus        22 RafhsTa   28 (36)
T signal           22 RAFQSTA   28 (36)
T 12               11 RAFHTTS   17 (25)
T 15               13 RGFHSTA   19 (27)
T 11               11 RALHSTS   17 (25)
T 16               16 RCFQTSA   22 (30)
Confidence            3466553


No 8  
>Q0QF01_42_IM_ref_sig5_130
Probab=14.49  E-value=0.81  Score=12.00  Aligned_cols=6  Identities=67%  Similarity=0.933  Sum_probs=3.7

Q 5_Mustela         1 RRFHRT    6 (7)
Q 2_Macaca          1 RRFHWT    6 (7)
Q 15_Ictidomys      1 RRFHRT    6 (7)
Q Consensus         1 rrfh~t    6 (7)
                      |-||-|
T Consensus        31 R~FHFt   36 (47)
T signal           31 RSFHFT   36 (47)
T 124              31 RDFHFT   36 (47)
T 126              29 RKLHFS   34 (43)
T 136              34 RNFHFT   39 (50)
T 137              31 RNFHFS   36 (46)
T 144              32 RNLHFT   37 (48)
T 145              31 RNFHFT   36 (47)
T 171              28 CGFHFT   33 (44)
T 154              14 RKFHFT   19 (30)
Confidence            457765


No 9  
>Q9YHT1_44_IM_ref_sig5_130
Probab=14.41  E-value=0.82  Score=12.01  Aligned_cols=6  Identities=67%  Similarity=0.960  Sum_probs=3.7

Q 5_Mustela         1 RRFHRT    6 (7)
Q 2_Macaca          1 RRFHWT    6 (7)
Q 15_Ictidomys      1 RRFHRT    6 (7)
Q Consensus         1 rrfh~t    6 (7)
                      |-||-|
T Consensus        32 R~FHFt   37 (49)
T signal           32 RNFHFT   37 (49)
T 127              26 RDFHFT   31 (43)
T 141              30 RGFDFT   35 (47)
T 160              32 RGFHFT   37 (49)
T 126              30 RDFHFT   35 (47)
T 129              29 RQLHFS   34 (45)
T 133              31 RNFHFS   36 (47)
T 140              34 RNFHFS   39 (51)
T 163              31 RNFHFT   36 (48)
T 176              27 RNFYFT   32 (44)
Confidence            557755


No 10 
>Q02486_26_Mito_Nu_ref_sig5_130
Probab=6.28  E-value=2.6  Score=9.64  Aligned_cols=6  Identities=50%  Similarity=1.077  Sum_probs=3.2

Q 5_Mustela         1 RRFHRT    6 (7)
Q 2_Macaca          1 RRFHWT    6 (7)
Q 15_Ictidomys      1 RRFHRT    6 (7)
Q Consensus         1 rrfh~t    6 (7)
                      ++||.+
T Consensus         9 ~~~~k~   14 (31)
T signal            9 RSFHES   14 (31)
T cl|CABBABABA|1    9 GRISKL   14 (31)
T cl|FABBABABA|1    9 NVNGNT   14 (31)
T 2                 9 ADKPKR   14 (31)
Confidence            456654


No 11 
>P12686_37_Mito_ref_sig5_130
Probab=6.09  E-value=2.7  Score=10.55  Aligned_cols=4  Identities=75%  Similarity=1.298  Sum_probs=1.8

Q 5_Mustela         1 RRFH    4 (7)
Q 2_Macaca          1 RRFH    4 (7)
Q 15_Ictidomys      1 RRFH    4 (7)
Q Consensus         1 rrfh    4 (7)
                      |||-
T Consensus        35 rrfa   38 (42)
T signal           35 RRFA   38 (42)
Confidence            4553


No 12 
>P07257_16_IM_ref_sig5_130
Probab=5.29  E-value=3.2  Score=8.92  Aligned_cols=4  Identities=50%  Similarity=0.924  Sum_probs=1.9

Q 5_Mustela         1 RRFH    4 (7)
Q 2_Macaca          1 RRFH    4 (7)
Q 15_Ictidomys      1 RRFH    4 (7)
Q Consensus         1 rrfh    4 (7)
                      ||++
T Consensus        15 R~~s   18 (21)
T signal           15 RRLT   18 (21)
T 20               12 RRYT   15 (18)
T 34               12 RRYT   15 (18)
T 14               13 RHYS   16 (19)
T 15               14 RKFS   17 (20)
T 23               16 RRLS   19 (22)
T 19               14 RHLS   17 (20)
T 40               16 RHYH   19 (22)
T 42               14 RKLS   17 (20)
Confidence            4554


No 13 
>Q64591_34_Mito_ref_sig5_130
Probab=5.28  E-value=3.2  Score=10.05  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.7

Q 5_Mustela         1 RRF    3 (7)
Q 2_Macaca          1 RRF    3 (7)
Q 15_Ictidomys      1 RRF    3 (7)
Q Consensus         1 rrf    3 (7)
                      |||
T Consensus        22 rRF   24 (39)
T signal           22 QRF   24 (39)
T 76               22 RRF   24 (39)
T 78               22 RRF   24 (39)
T 52               16 RRF   18 (32)
T 55               20 HKF   22 (36)
T 80               22 RRF   24 (38)
T 31               11 ARF   13 (28)
T 34                9 -EF   10 (25)
T 46               11 RRF   13 (28)
T 85               12 GRF   14 (29)
Confidence            465


No 14 
>Q01205_68_Mito_ref_sig5_130
Probab=5.05  E-value=3.4  Score=11.06  Aligned_cols=5  Identities=60%  Similarity=1.039  Sum_probs=2.1

Q 5_Mustela         2 RFHRT    6 (7)
Q 2_Macaca          2 RFHWT    6 (7)
Q 15_Ictidomys      2 RFHRT    6 (7)
Q Consensus         2 rfh~t    6 (7)
                      ||-||
T Consensus        59 ryFRT   63 (73)
T signal           59 RFFQT   63 (73)
T 126              58 RYFRT   62 (72)
T 144              60 RYFRS   64 (74)
T 148              48 RHFRI   52 (62)
T 128              58 QYFRT   62 (72)
T 129              58 RYFKT   62 (72)
T 131              58 QYFRT   62 (72)
T 142              56 KHFRT   60 (70)
T 141              55 RFYRT   59 (69)
Confidence            34444


No 15 
>P21954_16_Mito_ref_sig5_130
Probab=4.80  E-value=3.6  Score=8.83  Aligned_cols=4  Identities=50%  Similarity=0.941  Sum_probs=2.1

Q 5_Mustela         1 RRFH    4 (7)
Q 2_Macaca          1 RRFH    4 (7)
Q 15_Ictidomys      1 RRFH    4 (7)
Q Consensus         1 rrfh    4 (7)
                      |||-
T Consensus         6 Rr~~    9 (21)
T signal            6 RRLF    9 (21)
T 141               7 RQFS   10 (21)
T 145               4 RRFF    7 (18)
T 136               5 RRLF    8 (18)
T 143               4 RRFL    7 (21)
T 144               4 RRCF    7 (17)
T 137               5 RRCF    8 (18)
T 138               4 RRYL    7 (19)
T 139               7 RQVL   10 (18)
Confidence            5654


No 16 
>Q99757_59_Mito_ref_sig5_130
Probab=4.58  E-value=3.8  Score=10.67  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.2

Q 5_Mustela         1 RRF    3 (7)
Q 2_Macaca          1 RRF    3 (7)
Q 15_Ictidomys      1 RRF    3 (7)
Q Consensus         1 rrf    3 (7)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 80                8 RRF   10 (62)
T 82                8 RRC   10 (64)
T 81                8 RRL   10 (65)
T 88                8 RRF   10 (64)
T 78                8 RRF   10 (66)
T 61                8 RRL   10 (47)
T 65                8 RRL   10 (43)
T 77                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            344


No 17 
>P53163_33_Mito_ref_sig5_130
Probab=4.42  E-value=4  Score=9.87  Aligned_cols=5  Identities=60%  Similarity=1.012  Sum_probs=2.2

Q 5_Mustela         2 RFHRT    6 (7)
Q 2_Macaca          2 RFHWT    6 (7)
Q 15_Ictidomys      2 RFHRT    6 (7)
Q Consensus         2 rfh~t    6 (7)
                      ||..|
T Consensus        32 RFNST   36 (38)
T signal           32 RFNST   36 (38)
T 169              27 RFNST   31 (32)
T 170              31 RFNST   35 (37)
Confidence            45443


No 18 
>Q95108_59_Mito_ref_sig5_130
Probab=4.24  E-value=4.2  Score=10.52  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.2

Q 5_Mustela         1 RRF    3 (7)
Q 2_Macaca          1 RRF    3 (7)
Q 15_Ictidomys      1 RRF    3 (7)
Q Consensus         1 rrf    3 (7)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 82                8 RRL   10 (65)
T 80                8 RRF   10 (62)
T 79                8 RRF   10 (66)
T 58                8 RRL   10 (47)
T 77                8 KRV   10 (67)
T 78                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            344


No 19 
>P43265_42_IM_ref_sig5_130
Probab=3.81  E-value=4.8  Score=10.00  Aligned_cols=4  Identities=75%  Similarity=1.348  Sum_probs=2.0

Q 5_Mustela         1 RRFH    4 (7)
Q 2_Macaca          1 RRFH    4 (7)
Q 15_Ictidomys      1 RRFH    4 (7)
Q Consensus         1 rrfh    4 (7)
                      |||.
T Consensus        15 rrfs   18 (47)
T signal           15 RRFS   18 (47)
Confidence            4553


No 20 
>Q06645_61_MitoM_ref_sig5_130
Probab=3.78  E-value=4.9  Score=9.58  Aligned_cols=3  Identities=33%  Similarity=0.080  Sum_probs=1.1

Q 5_Mustela         2 RF..H    4 (7)
Q 2_Macaca          2 RF..H    4 (7)
Q 15_Ictidomys      2 RF..H    4 (7)
Q Consensus         2 rf..h    4 (7)
                      |+  +
T Consensus        52 ~~..~   54 (66)
T signal           52 RE..F   54 (66)
T 59_Zonotrichia   52 KC..R   54 (66)
T 54_Latimeria     52 VV..R   54 (66)
T 52_Anolis        52 EF..Q   54 (66)
T 39_Heterocepha   52 AR..R   54 (66)
T cl|GABBABABA|1   52 AL..H   54 (66)
T cl|KABBABABA|1   52 LY..R   54 (66)
T cl|LABBABABA|3   52 LQ..V   54 (66)
T cl|RABBABABA|7   52 VF..S   54 (66)
T 1                52 QVrsF   56 (68)
Confidence            33  3


Done!
