Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:08:44 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_39A46_39Q46_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P18155_35_Mito_ref_sig5_130      0.7      37   0.085    3.6   0.0    1    1-1       1-1   (40)
  2 Q6UB35_31_Mito_ref_sig5_130      0.7      38   0.086    3.8   0.0    1    1-1       1-1   (36)
  3 P99028_13_IM_ref_sig5_130        0.7      38   0.086    6.1   0.0    1    4-4       9-9   (18)
  4 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.7      38   0.088    3.4   0.0    1    1-1       1-1   (26)
  5 P15650_30_MM_ref_sig5_130        0.6      38   0.088    3.6   0.0    1    1-1       1-1   (35)
  6 P41367_25_MM_ref_sig5_130        0.6      38   0.088    3.5   0.0    1    1-1       1-1   (30)
  7 Q05932_42_IM_MM_Cy_ref_sig5_13   0.6      39    0.09    7.3   0.0    2    3-4      42-43  (47)
  8 P08503_25_MM_ref_sig5_130        0.6      40   0.091    3.5   0.0    1    1-1       1-1   (30)
  9 Q3ZBF3_26_Mito_ref_sig5_130      0.6      42   0.097    3.5   0.0    1    1-1       1-1   (31)
 10 P34942_23_MM_ref_sig5_130        0.6      42   0.097    3.5   0.0    1    4-4       1-1   (28)
 11 P11310_25_MM_ref_sig5_130        0.6      43     0.1    3.5   0.0    1    1-1       1-1   (30)
 12 P08559_29_MM_ref_sig5_130        0.6      45     0.1    3.5   0.0    1    1-1       1-1   (34)
 13 Q9NPH0_32_Mito_ref_sig5_130      0.6      45     0.1    3.6   0.0    1    1-1       1-1   (37)
 14 O35854_27_Mito_ref_sig5_130      0.6      45     0.1    4.3   0.0    1    4-4       1-1   (32)
 15 P36967_16_Mito_ref_sig5_130      0.5      46    0.11    6.1   0.0    3    2-4       4-6   (21)
 16 Q9Y2D0_33_Mito_ref_sig5_130      0.5      47    0.11    4.6   0.0    1    4-4       1-1   (38)
 17 Q04467_39_Mito_ref_sig5_130      0.5      49    0.11    3.5   0.0    1    1-1       1-1   (44)
 18 P08680_49_MM_ref_sig5_130        0.5      50    0.12    4.0   0.0    1    1-1       1-1   (54)
 19 P22033_32_MM_ref_sig5_130        0.5      51    0.12    3.6   0.0    1    1-1       1-1   (37)
 20 P22557_49_MM_ref_sig5_130        0.5      51    0.12    4.1   0.0    1    1-1       1-1   (54)

No 1  
>P18155_35_Mito_ref_sig5_130
Probab=0.67  E-value=37  Score=3.65  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (40)
T signal            1 M    1 (40)
T 95_Emiliania      1 M    1 (39)
T 93_Volvox         1 M    1 (39)
T 89_Ornithorhyn    1 H    1 (39)
T 77_Pediculus      1 M    1 (39)
T 75_Trichoplax     1 M    1 (39)
T 71_Saccoglossu    1 M    1 (39)
T 67_Monodelphis    1 M    1 (39)
T 41_Capra          1 M    1 (39)
Confidence            0


No 2  
>Q6UB35_31_Mito_ref_sig5_130
Probab=0.66  E-value=38  Score=3.81  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (36)
T signal            1 M    1 (36)
T 44_Alligator      1 M    1 (35)
T 38_Ictidomys      1 M    1 (35)
T 25_Myotis         1 M    1 (35)
T 19_Tupaia         1 M    1 (35)
T 39_Condylura      1 M    1 (35)
Confidence            0


No 3  
>P99028_13_IM_ref_sig5_130
Probab=0.66  E-value=38  Score=6.06  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.4

Q 5_Mustela         4 M    4 (7)
Q Consensus         4 m    4 (7)
                      |
T Consensus         9 M    9 (18)
T signal            9 M    9 (18)
T 23                9 M    9 (18)
T 42                9 M    9 (18)
T 3                 9 M    9 (18)
T 14                9 M    9 (18)
T 24                9 M    9 (18)
T 59                8 M    8 (17)
T 62                9 M    9 (18)
Confidence            3


No 4  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.65  E-value=38  Score=3.40  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (26)
T signal            1 M    1 (26)
T 60                1 M    1 (26)
T 108_Galdieria     1 M    1 (23)
T 104_Cyanidiosc    1 M    1 (23)
T 102_Acanthamoe    1 M    1 (23)
T 78_Musca          1 M    1 (23)
T 76_Aplysia        1 M    1 (23)
T 74_Branchiosto    1 M    1 (23)
T 57_Loxodonta      1 M    1 (23)
T 6_Orcinus         1 M    1 (23)
Confidence            0


No 5  
>P15650_30_MM_ref_sig5_130
Probab=0.65  E-value=38  Score=3.57  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (35)
T signal            1 M    1 (35)
T 78_Saimiri        1 M    1 (34)
T 74_Tursiops       1 A    1 (34)
T 67_Xiphophorus    1 R    1 (34)
T 57_Meleagris      1 M    1 (34)
T 48_Anas           1 M    1 (34)
T 22_Ceratotheri    1 M    1 (34)
T 5_Cricetulus      1 M    1 (34)
Confidence            0


No 6  
>P41367_25_MM_ref_sig5_130
Probab=0.65  E-value=38  Score=3.49  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 102_Strongyloc    1 M    1 (28)
T 100_Leishmania    1 M    1 (28)
T 89_Caenorhabdi    1 M    1 (28)
T 88_Haplochromi    1 M    1 (28)
T 79_Cavia          1 M    1 (28)
T 76_Tribolium      1 M    1 (28)
T 71_Pediculus      1 M    1 (28)
T 64_Sarcophilus    1 T    1 (28)
T 41_Ficedula       1 M    1 (28)
Confidence            0


No 7  
>Q05932_42_IM_MM_Cy_ref_sig5_130
Probab=0.63  E-value=39  Score=7.34  Aligned_cols=2  Identities=50%  Similarity=0.783  Sum_probs=0.9

Q 5_Mustela         3 GM    4 (7)
Q Consensus         3 gm    4 (7)
                      ||
T Consensus        42 gM   43 (47)
T signal           42 SM   43 (47)
T 141              42 SM   43 (47)
T 143              42 GM   43 (47)
T 153              42 GM   43 (47)
T 156              42 GM   43 (47)
T 132              42 GM   43 (47)
T 124              26 RM   27 (31)
T 108              22 GM   23 (27)
Confidence            44


No 8  
>P08503_25_MM_ref_sig5_130
Probab=0.63  E-value=40  Score=3.50  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 96_Nematostell    1 M    1 (28)
T 51_Xenopus        1 M    1 (28)
T 47_Ornithorhyn    1 M    1 (28)
T 46_Geospiza       1 M    1 (28)
Confidence            0


No 9  
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=0.60  E-value=42  Score=3.54  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (31)
T signal            1 M    1 (31)
T 96_Brugia         1 M    1 (30)
T 87_Branchiosto    1 M    1 (30)
T 86_Oryzias        1 C    1 (30)
T 84_Pediculus      1 M    1 (30)
T 70_Anas           1 M    1 (30)
T 69_Strongyloce    1 S    1 (30)
T 63_Zonotrichia    1 M    1 (30)
T 55_Alligator      1 M    1 (30)
T 44_Myotis         1 M    1 (30)
Confidence            0


No 10 
>P34942_23_MM_ref_sig5_130
Probab=0.59  E-value=42  Score=3.46  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (7)
Q Consensus         4 m    4 (7)
                      |
T Consensus         1 M    1 (28)
T signal            1 M    1 (28)
T 78_Musca          1 M    1 (39)
T 74_Strongyloce    1 M    1 (39)
T 62_Alligator      1 M    1 (39)
T 60_Xenopus        1 M    1 (39)
T 56_Anas           1 M    1 (39)
T 52_Meleagris      1 M    1 (39)
T 40_Saimiri        1 M    1 (39)
Confidence            1


No 11 
>P11310_25_MM_ref_sig5_130
Probab=0.58  E-value=43  Score=3.50  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 81_Nematostell    1 M    1 (28)
T 80_Nasonia        1 M    1 (28)
T 70_Bombyx         1 M    1 (28)
T 64_Branchiosto    1 M    1 (28)
T 63_Aplysia        1 M    1 (28)
T 61_Saccoglossu    1 M    1 (28)
T 50_Columba        1 M    1 (28)
Confidence            0


No 12 
>P08559_29_MM_ref_sig5_130
Probab=0.57  E-value=45  Score=3.47  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 85_Trichinella    1 M    1 (33)
T 81_Apis           1 M    1 (33)
T 76_Trichoplax     1 M    1 (33)
T 55_Pelodiscus     1 M    1 (33)
T 44_Capra          1 V    1 (33)
T 39_Ailuropoda     1 M    1 (33)
Confidence            0


No 13 
>Q9NPH0_32_Mito_ref_sig5_130
Probab=0.57  E-value=45  Score=3.64  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 92_Coccidioide    1 M    1 (36)
T 77_Strongyloce    1 M    1 (36)
T 76_Ciona          1 M    1 (36)
T 74_Latimeria      1 V    1 (36)
T 72_Branchiosto    1 F    1 (36)
T 60_Ficedula       1 M    1 (36)
T 49_Condylura      1 M    1 (36)
Confidence            0


No 14 
>O35854_27_Mito_ref_sig5_130
Probab=0.56  E-value=45  Score=4.34  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (7)
Q Consensus         4 m    4 (7)
                      |
T Consensus         1 M    1 (32)
T signal            1 M    1 (32)
T 55_Tetrapisisp    1 M    1 (32)
T 51_Micromonas     1 M    1 (32)
T 50_Laccaria       1 M    1 (32)
T 47_Acanthamoeb    1 M    1 (32)
T 46_Dictyosteli    1 M    1 (32)
T 44_Xenopus        1 M    1 (32)
T 42_Alligator      1 M    1 (32)
T 41_Capra          1 M    1 (32)
T 38_Saimiri        1 M    1 (32)
Confidence            2


No 15 
>P36967_16_Mito_ref_sig5_130
Probab=0.55  E-value=46  Score=6.06  Aligned_cols=3  Identities=67%  Similarity=1.188  Sum_probs=1.5

Q 5_Mustela         2 QGM    4 (7)
Q Consensus         2 qgm    4 (7)
                      .||
T Consensus         4 agm    6 (21)
T signal            4 AGM    6 (21)
Confidence            355


No 16 
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.54  E-value=47  Score=4.59  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         4 M    4 (7)
Q Consensus         4 m    4 (7)
                      |
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 57_Xenopus        1 M    1 (38)
T 54_Chrysemys      1 M    1 (38)
T 49_Meleagris      1 M    1 (38)
T 42_Dasypus        1 M    1 (38)
T 41_Camelus        1 M    1 (38)
T 37_Ailuropoda     1 M    1 (38)
T 22_Myotis         1 M    1 (38)
T 14                1 L    1 (32)
Confidence            1


No 17 
>Q04467_39_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=3.48  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (44)
T signal            1 M    1 (44)
T 92_Chlorella      1 M    1 (40)
T 86_Trichoplax     1 M    1 (40)
T 76_Bombyx         1 M    1 (40)
T 69_Meleagris      1 F    1 (40)
T 65_Latimeria      1 A    1 (40)
T 38_Sarcophilus    1 M    1 (40)
Confidence            0


No 18 
>P08680_49_MM_ref_sig5_130
Probab=0.51  E-value=50  Score=4.02  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 113_Candida       1 M    1 (53)
T 98_Phytophthor    1 M    1 (53)
T 87_Puccinia       1 M    1 (53)
T 73_Nematostell    1 H    1 (53)
T 72_Bombus         1 M    1 (53)
T 71_Aplysia        1 M    1 (53)
T 68_Megachile      1 M    1 (53)
T 57_Saccoglossu    1 M    1 (53)
T 46_Xenopus        1 M    1 (53)
Confidence            0


No 19 
>P22033_32_MM_ref_sig5_130
Probab=0.51  E-value=51  Score=3.64  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 99_Phytophthor    1 M    1 (36)
T 97_Salpingoeca    1 M    1 (36)
T 83_Caenorhabdi    1 M    1 (36)
T 75_Branchiosto    1 L    1 (36)
T 65_Xenopus        1 M    1 (36)
Confidence            0


No 20 
>P22557_49_MM_ref_sig5_130
Probab=0.50  E-value=51  Score=4.05  Aligned_cols=1  Identities=0%  Similarity=-0.728  Sum_probs=0.0

Q 5_Mustela         1 N    1 (7)
Q Consensus         1 n    1 (7)
                      =
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 115_Kazachstan    1 M    1 (53)
T 106_Phytophtho    1 M    1 (53)
T 91_Dictyosteli    1 M    1 (53)
T 88_Puccinia       1 M    1 (53)
T 73_Aplysia        1 M    1 (53)
T 69_Bombus         1 M    1 (53)
T 68_Ceratitis      1 M    1 (53)
Confidence            0


Done!
