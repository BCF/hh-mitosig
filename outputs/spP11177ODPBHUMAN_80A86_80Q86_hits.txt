Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:46 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_80A86_80Q86_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P00431_67_MM_ref_sig5_130        0.6      43   0.098    5.1   0.0    1    1-1       1-1   (72)
  2 P54898_44_Mito_ref_sig5_130      0.6      43   0.099    7.0   0.0    1    4-4      38-38  (49)
  3 P53163_33_Mito_ref_sig5_130      0.5      48    0.11    6.6   0.0    1    2-2      38-38  (38)
  4 P26360_45_Mito_IM_ref_sig5_130   0.5      48    0.11    6.9   0.0    1    3-3      46-46  (50)
  5 P33198_8_Mito_ref_sig5_130       0.5      48    0.11    5.2   0.0    1    1-1      12-12  (13)
  6 P36531_14_Mito_ref_sig5_130      0.5      50    0.12    4.5   0.0    1    3-3       5-5   (19)
  7 P10507_20_MM_ref_sig5_130        0.5      52    0.12    6.0   0.0    1    2-2      15-15  (25)
  8 P13184_23_IM_ref_sig5_130        0.5      52    0.12    6.0   0.0    1    3-3      16-16  (28)
  9 Q64591_34_Mito_ref_sig5_130      0.5      57    0.13    6.3   0.0    1    4-4      37-37  (39)
 10 P08067_30_IM_ref_sig5_130        0.5      57    0.13    6.2   0.0    1    3-3      23-23  (35)
 11 P22557_49_MM_ref_sig5_130        0.4      57    0.13    3.8   0.0    1    2-2      54-54  (54)
 12 Q04728_8_MM_ref_sig5_130         0.4      58    0.13    5.0   0.0    1    2-2       3-3   (13)
 13 P18155_35_Mito_ref_sig5_130      0.4      60    0.14    3.0   0.0    1    2-2      40-40  (40)
 14 P31937_36_Mito_ref_sig5_130      0.4      61    0.14    6.4   0.0    1    5-5      40-40  (41)
 15 P05165_52_MM_ref_sig5_130        0.4      61    0.14    3.2   0.0    1    2-2      57-57  (57)
 16 P38077_33_Mito_IM_ref_sig5_130   0.4      63    0.15    6.2   0.0    1    2-2       5-5   (38)
 17 P35171_23_IM_ref_sig5_130        0.4      64    0.15    5.7   0.0    1    3-3      16-16  (28)
 18 P29147_46_MM_ref_sig5_130        0.4      64    0.15    3.7   0.0    1    2-2      51-51  (51)
 19 Q96252_26_Mito_IM_ref_sig5_130   0.4      65    0.15    5.9   0.0    1    2-2      31-31  (31)
 20 P16387_33_MM_ref_sig5_130        0.4      67    0.15    6.1   0.0    1    5-5      23-23  (38)

No 1  
>P00431_67_MM_ref_sig5_130
Probab=0.59  E-value=43  Score=5.06  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.0

Q 5_Mustela         1 R    1 (6)
Q Consensus         1 r    1 (6)
                      -
T Consensus         1 M    1 (72)
T signal            1 M    1 (72)
T cl|DABBABABA|1    1 M    1 (61)
T cl|ZABBABABA|1    1 P    1 (61)
T cl|LEBBABABA|1    1 M    1 (61)
T cl|MEBBABABA|1    1 Q    1 (61)
T cl|TEBBABABA|1    1 M    1 (61)
T cl|ZEBBABABA|1    1 M    1 (61)
T cl|PIBBABABA|1    1 M    1 (61)
T cl|DUBBABABA|1    1 M    1 (61)
Confidence            0


No 2  
>P54898_44_Mito_ref_sig5_130
Probab=0.58  E-value=43  Score=7.03  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         4 D    4 (6)
Q Consensus         4 d    4 (6)
                      |
T Consensus        38 s   38 (49)
T signal           38 S   38 (49)
T 154              40 S   40 (51)
T 151              39 S   39 (50)
T 104              38 D   38 (43)
T 137              45 S   45 (49)
Confidence            2


No 3  
>P53163_33_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.59  Aligned_cols=1  Identities=0%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      -
T Consensus        38 t   38 (38)
T signal           38 T   38 (38)
T 169              33 -   32 (32)
T 170              37 T   37 (37)
Confidence            0


No 4  
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.53  E-value=48  Score=6.91  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        46 i   46 (50)
T signal           46 I   46 (50)
T 181              41 I   41 (45)
T 183              44 I   44 (48)
T 170              47 V   47 (51)
T 171              45 V   45 (49)
T 182              46 I   46 (50)
T 174              43 I   43 (47)
T 178              40 I   40 (44)
T 177              46 I   46 (50)
T 168              48 A   48 (52)
Confidence            2


No 5  
>P33198_8_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=5.19  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         1 R    1 (6)
Q Consensus         1 r    1 (6)
                      |
T Consensus        12 r   12 (13)
T signal           12 R   12 (13)
Confidence            3


No 6  
>P36531_14_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=4.53  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      +
T Consensus         5 ~    5 (19)
T signal            5 I    5 (19)
T cl|DABBABABA|1    5 I    5 (18)
T cl|GABBABABA|1    5 S    5 (18)
T cl|KABBABABA|1    5 E    5 (18)
T 9_Kazachstania    5 I    5 (18)
T 5_Vanderwaltoz    5 L    5 (18)
T 3_Naumovozyma     5 M    5 (18)
T 18_Scheffersom    5 K    5 (18)
T 16_Meyerozyma     5 S    5 (18)
T 14_Komagataell    5 V    5 (18)
Confidence            2


No 7  
>P10507_20_MM_ref_sig5_130
Probab=0.50  E-value=52  Score=5.95  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      +
T Consensus        15 l   15 (25)
T signal           15 L   15 (25)
T 191               9 L    9 (19)
Confidence            2


No 8  
>P13184_23_IM_ref_sig5_130
Probab=0.49  E-value=52  Score=6.00  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        16 I   16 (28)
T signal           16 I   16 (28)
T 63               16 F   16 (28)
T 28               16 I   16 (28)
T 15               16 I   16 (28)
T 25               16 T   16 (28)
T 16               16 I   16 (28)
T 19               16 I   16 (28)
T 12               16 I   16 (28)
T 14               16 I   16 (28)
Confidence            3


No 9  
>Q64591_34_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.30  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         4 D    4 (6)
Q Consensus         4 d    4 (6)
                      |
T Consensus        37 ~   37 (39)
T signal           37 D   37 (39)
T 76               37 N   37 (39)
T 78               37 E   37 (39)
T 52               31 E   31 (32)
T 55               35 E   35 (36)
T 80               37 E   37 (38)
T 31               26 S   26 (28)
T 34               23 N   23 (25)
T 46               26 K   26 (28)
T 85               27 S   27 (29)
Confidence            2


No 10 
>P08067_30_IM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.21  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        23 ~   23 (35)
T signal           23 I   23 (35)
T 190              23 I   23 (35)
T 193              22 L   22 (34)
T 189              20 L   20 (32)
T 191              20 I   20 (32)
T 192              22 M   22 (34)
T 195              21 I   21 (33)
T 194              21 F   21 (33)
Confidence            3


No 11 
>P22557_49_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=3.77  Aligned_cols=1  Identities=0%  Similarity=-0.696  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      -
T Consensus        54 ~   54 (54)
T signal           54 K   54 (54)
T 115_Kazachstan   54 -   53 (53)
T 106_Phytophtho   54 -   53 (53)
T 91_Dictyosteli   54 -   53 (53)
T 88_Puccinia      54 -   53 (53)
T 73_Aplysia       54 -   53 (53)
T 69_Bombus        54 -   53 (53)
T 68_Ceratitis     54 -   53 (53)
Confidence            0


No 12 
>Q04728_8_MM_ref_sig5_130
Probab=0.44  E-value=58  Score=4.97  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus         3 i    3 (13)
T signal            3 I    3 (13)
T 90                3 I    3 (13)
T 88                3 I    3 (13)
Confidence            3


No 13 
>P18155_35_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=3.03  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 95_Emiliania     40 -   39 (39)
T 93_Volvox        40 -   39 (39)
T 89_Ornithorhyn   40 -   39 (39)
T 77_Pediculus     40 -   39 (39)
T 75_Trichoplax    40 -   39 (39)
T 71_Saccoglossu   40 -   39 (39)
T 67_Monodelphis   40 -   39 (39)
T 41_Capra         40 -   39 (39)
Confidence            0


No 14 
>P31937_36_Mito_ref_sig5_130
Probab=0.42  E-value=61  Score=6.35  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.2

Q 5_Mustela         5 T    5 (6)
Q Consensus         5 t    5 (6)
                      |
T Consensus        40 T   40 (41)
T signal           40 T   40 (41)
T 43               33 T   33 (34)
T 5                31 T   31 (32)
T 33               33 T   33 (34)
T 52               37 T   37 (38)
T 98               37 T   37 (38)
T 55               37 T   37 (38)
Confidence            2


No 15 
>P05165_52_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=3.24  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      =
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 16 
>P38077_33_Mito_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=6.22  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      |
T Consensus         5 i    5 (38)
T signal            5 I    5 (38)
Confidence            2


No 17 
>P35171_23_IM_ref_sig5_130
Probab=0.41  E-value=64  Score=5.72  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         3 I    3 (6)
Q Consensus         3 i    3 (6)
                      |
T Consensus        16 i   16 (28)
T signal           16 I   16 (28)
T 73               16 F   16 (28)
T 29               16 I   16 (28)
T 18               16 I   16 (28)
T 21               16 I   16 (28)
T 20               16 I   16 (28)
T 3                16 I   16 (28)
T 22               15 F   15 (27)
T 71               13 F   13 (24)
Confidence            3


No 18 
>P29147_46_MM_ref_sig5_130
Probab=0.40  E-value=64  Score=3.72  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      =
T Consensus        51 ~   51 (51)
T signal           51 D   51 (51)
T 77_Aplysia       51 -   50 (50)
T 61_Papio         51 -   50 (50)
T 19_Tupaia        51 -   50 (50)
T 80_Tribolium     51 -   50 (50)
T 79_Apis          51 -   50 (50)
T 74_Branchiosto   51 -   50 (50)
T 54_Canis         51 -   50 (50)
T 39_Nomascus      51 -   50 (50)
Confidence            0


No 19 
>Q96252_26_Mito_IM_ref_sig5_130
Probab=0.40  E-value=65  Score=5.89  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 I    2 (6)
Q Consensus         2 i    2 (6)
                      -
T Consensus        31 P   31 (31)
T signal           31 P   31 (31)
T 164              32 P   32 (32)
T 165              26 P   26 (26)
T 166              26 P   26 (26)
T 163              28 -   27 (27)
T 159              24 -   23 (23)
T 161              24 -   23 (23)
T 167              30 P   30 (30)
Confidence            0


No 20 
>P16387_33_MM_ref_sig5_130
Probab=0.39  E-value=67  Score=6.15  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         5 T    5 (6)
Q Consensus         5 t    5 (6)
                      |
T Consensus        23 t   23 (38)
T signal           23 T   23 (38)
Confidence            2


Done!
