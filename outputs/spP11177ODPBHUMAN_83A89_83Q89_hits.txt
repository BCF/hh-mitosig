Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:50 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_83A89_83Q89_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P00366_57_MM_ref_sig5_130        0.5      56    0.13    4.7   0.0    1    1-1       1-1   (62)
  2 P42847_59_Mito_ref_sig5_130      0.5      56    0.13    7.0   0.0    1    2-2      64-64  (64)
  3 P34899_31_Mito_ref_sig5_130      0.5      56    0.13    6.3   0.0    1    3-3      17-17  (36)
  4 P23786_25_IM_ref_sig5_130        0.5      57    0.13    6.0   0.0    1    4-4      25-25  (30)
  5 P01098_23_Mito_ref_sig5_130      0.4      60    0.14    5.9   0.0    1    3-3      27-27  (28)
  6 P12074_24_IM_ref_sig5_130        0.4      60    0.14    5.9   0.0    2    3-4      23-24  (29)
  7 P25284_26_MM_ref_sig5_130        0.4      62    0.14    5.9   0.0    1    5-5      28-28  (31)
  8 P22142_42_IM_ref_sig5_130        0.4      63    0.14    4.7   0.0    1    1-1       1-1   (47)
  9 P05091_17_MM_ref_sig5_130        0.4      65    0.15    5.4   0.0    3    3-5      16-18  (22)
 10 P10818_26_IM_ref_sig5_130        0.4      65    0.15    5.9   0.0    1    4-4      26-26  (31)
 11 P56522_34_MM_ref_sig5_130        0.4      66    0.15    2.5   0.0    1    1-1       1-1   (39)
 12 P0C2C4_29_Mito_ref_sig5_130      0.4      71    0.16    5.9   0.0    1    3-3       2-2   (34)
 13 P18886_25_IM_ref_sig5_130        0.4      71    0.16    5.8   0.0    1    4-4      25-25  (30)
 14 Q02376_27_IM_ref_sig5_130        0.4      72    0.16    5.8   0.0    1    4-4      10-10  (32)
 15 P13216_19_MM_ref_sig5_130        0.4      72    0.16    5.5   0.0    1    3-3      11-11  (24)
 16 Q9UGC7_26_Mito_ref_sig5_130      0.4      72    0.17    2.2   0.0    1    2-2      31-31  (31)
 17 P42026_37_Mito_ref_sig5_130      0.4      72    0.17    6.0   0.0    1    3-3      38-38  (42)
 18 P80433_25_IM_ref_sig5_130        0.4      73    0.17    5.8   0.0    1    3-3       6-6   (30)
 19 P22570_32_MM_ref_sig5_130        0.3      75    0.17    3.1   0.0    1    1-1       1-1   (37)
 20 Q3ZBF3_26_Mito_ref_sig5_130      0.3      75    0.17    2.8   0.0    1    2-2      31-31  (31)

No 1  
>P00366_57_MM_ref_sig5_130
Probab=0.46  E-value=56  Score=4.66  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      =
T Consensus         1 M    1 (62)
T signal            1 M    1 (62)
T 114_Galdieria     1 M    1 (43)
T 110_Naegleria     1 M    1 (43)
T 104_Ichthyopht    1 M    1 (43)
T 102_Guillardia    1 M    1 (43)
T 93_Amphimedon     1 M    1 (43)
T 92_Schistosoma    1 M    1 (43)
T 89_Culex          1 M    1 (43)
T 69_Gallus         1 M    1 (43)
T 24_Saimiri        1 V    1 (43)
Confidence            0


No 2  
>P42847_59_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=7.02  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.0

Q 5_Mustela         2 T    2 (6)
Q Consensus         2 t    2 (6)
                      .
T Consensus        64 s   64 (64)
T signal           64 S   64 (64)
Confidence            0


No 3  
>P34899_31_Mito_ref_sig5_130
Probab=0.46  E-value=56  Score=6.28  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         3 P.    3 (6)
Q Consensus         3 p.    3 (6)
                      | 
T Consensus        17 p.   17 (36)
T signal           17 S.   17 (36)
T 145              17 P.   17 (36)
T 135              17 G.   17 (35)
T 141              17 P.   17 (36)
T 140              17 P.   17 (35)
T 134              12 Pl   13 (34)
T 138              15 P.   15 (35)
T 139              15 P.   15 (33)
T 143              15 P.   15 (34)
T 144              15 P.   15 (34)
Confidence            2 


No 4  
>P23786_25_IM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.04  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      +
T Consensus        25 L   25 (30)
T signal           25 L   25 (30)
T 101              25 L   25 (30)
T 104              25 L   25 (30)
T 108              25 L   25 (30)
T 109              26 L   26 (31)
T 117              25 L   25 (30)
T 119              25 L   25 (30)
T 59               30 L   30 (35)
Confidence            3


No 5  
>P01098_23_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=5.90  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus        27 p   27 (28)
T signal           27 P   27 (28)
Confidence            2


No 6  
>P12074_24_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=5.91  Aligned_cols=2  Identities=50%  Similarity=1.680  Sum_probs=0.7

Q 5_Mustela         3 PI    4 (6)
Q Consensus         3 pi    4 (6)
                      |.
T Consensus        23 pM   24 (29)
T signal           23 PM   24 (29)
T 32               23 PM   24 (29)
T 37               25 PM   26 (31)
T 43               23 LM   24 (29)
T 54               23 PM   24 (29)
T 44               23 PM   24 (29)
T 51               23 PM   24 (29)
T 59               23 PM   24 (29)
T 60               23 PM   24 (29)
T 7                21 PM   22 (27)
Confidence            33


No 7  
>P25284_26_MM_ref_sig5_130
Probab=0.42  E-value=62  Score=5.92  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.2

Q 5_Mustela         5 S    5 (6)
Q Consensus         5 s    5 (6)
                      |
T Consensus        28 S   28 (31)
T signal           28 S   28 (31)
T 188              28 S   28 (31)
T 184              27 S   27 (30)
T 187              26 S   26 (29)
T 171              25 S   25 (27)
T 182              25 S   25 (27)
T 164              24 S   24 (27)
T 183              25 S   25 (28)
Confidence            2


No 8  
>P22142_42_IM_ref_sig5_130
Probab=0.41  E-value=63  Score=4.69  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      =
T Consensus         1 M    1 (47)
T signal            1 M    1 (47)
T 180_Trichinell    1 M    1 (27)
T 177_Loa           1 M    1 (27)
T 164_Beta          1 M    1 (27)
T 135_Galdieria     1 M    1 (27)
Confidence            0


No 9  
>P05091_17_MM_ref_sig5_130
Probab=0.40  E-value=65  Score=5.43  Aligned_cols=3  Identities=33%  Similarity=0.301  Sum_probs=1.2

Q 5_Mustela         3 PIS    5 (6)
Q Consensus         3 pis    5 (6)
                      |+|
T Consensus        16 llS   18 (22)
T signal           16 LLS   18 (22)
T 58               13 LLS   15 (19)
T 61               13 LLS   15 (19)
T 73               11 LLS   13 (17)
T 72               20 LLS   22 (26)
T 45               19 PFS   21 (25)
T 47               19 PLS   21 (22)
T 51               19 -LS   20 (24)
Confidence            343


No 10 
>P10818_26_IM_ref_sig5_130
Probab=0.40  E-value=65  Score=5.89  Aligned_cols=1  Identities=0%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      .
T Consensus        26 M   26 (31)
T signal           26 M   26 (31)
T 53               23 M   23 (28)
T 56               23 M   23 (28)
T 64               23 M   23 (28)
T 36               25 M   25 (30)
T 44               23 M   23 (28)
T 55               23 M   23 (28)
T 6                25 M   25 (30)
T 7                24 M   24 (29)
T 43               21 F   21 (25)
Confidence            3


No 11 
>P56522_34_MM_ref_sig5_130
Probab=0.39  E-value=66  Score=2.53  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      =
T Consensus         1 M    1 (39)
T signal            1 M    1 (39)
T 138_Tuber         1 M    1 (37)
T 98_Oryza          1 M    1 (37)
T 88_Apis           1 M    1 (37)
T 81_Amphimedon     1 M    1 (37)
T 79_Trichoplax     1 M    1 (37)
T 61_Meleagris      1 M    1 (37)
Confidence            0


No 12 
>P0C2C4_29_Mito_ref_sig5_130
Probab=0.37  E-value=71  Score=5.94  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus         2 p    2 (34)
T signal            2 P    2 (34)
Confidence            2


No 13 
>P18886_25_IM_ref_sig5_130
Probab=0.37  E-value=71  Score=5.76  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      +
T Consensus        25 L   25 (30)
T signal           25 L   25 (30)
T 84               25 L   25 (30)
T 87               25 L   25 (30)
T 102              26 L   26 (31)
T 114              25 L   25 (30)
T 92               25 L   25 (30)
T 94               25 L   25 (30)
T 48               30 L   30 (35)
Confidence            3


No 14 
>Q02376_27_IM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.81  Aligned_cols=1  Identities=0%  Similarity=0.335  Sum_probs=0.3

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      .
T Consensus        10 ~   10 (32)
T signal           10 F   10 (32)
T 10               10 V   10 (33)
T 11               10 L   10 (32)
T 17               10 F   10 (32)
T 30               10 F   10 (32)
T 13               10 L   10 (32)
T 12               10 L   10 (32)
T 8                11 L   11 (33)
Confidence            2


No 15 
>P13216_19_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=5.49  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus        11 ~   11 (24)
T signal           11 P   11 (24)
T 128               5 P    5 (18)
T 135              10 F   10 (23)
T 143              10 P   10 (23)
T 127               5 R    5 (18)
T 105               4 S    4 (17)
T 130               4 T    4 (17)
Confidence            2


No 16 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.36  E-value=72  Score=2.18  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 T    2 (6)
Q Consensus         2 t    2 (6)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 119_Populus      31 -   30 (30)
T 107_Phytophtho   31 -   30 (30)
T 101_Setaria      31 -   30 (30)
T 82_Anopheles     31 -   30 (30)
T 69_Chrysemys     31 -   30 (30)
T 55_Geospiza      31 -   30 (30)
Confidence            0


No 17 
>P42026_37_Mito_ref_sig5_130
Probab=0.36  E-value=72  Score=6.05  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.2

Q 5_Mustela         3 P    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus        38 p   38 (42)
T signal           38 P   38 (42)
T 163              39 P   39 (43)
T 166              39 P   39 (43)
T 160              39 P   39 (43)
T 150              35 P   35 (39)
T 144              37 P   37 (37)
T 133              31 A   31 (33)
T 136              40 A   40 (42)
T 138              28 A   28 (30)
T 143              27 A   27 (29)
Confidence            2


No 18 
>P80433_25_IM_ref_sig5_130
Probab=0.36  E-value=73  Score=5.76  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         3 P    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus         6 p    6 (30)
T signal            6 P    6 (30)
T 3                 6 P    6 (30)
T 20                6 P    6 (30)
T 22                6 P    6 (30)
T 31                6 P    6 (30)
T 36                6 P    6 (30)
T 5                 6 S    6 (30)
T 8                 6 P    6 (30)
T 19                6 P    6 (30)
Confidence            2


No 19 
>P22570_32_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=3.11  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (6)
Q Consensus         1 d    1 (6)
                      =
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 90_Drosophila     1 M    1 (29)
T 84_Megachile      1 M    1 (29)
T 73_Nematostell    1 P    1 (29)
T 49_Columba        1 M    1 (29)
T 38_Myotis         1 M    1 (29)
Confidence            0


No 20 
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=0.35  E-value=75  Score=2.84  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 T    2 (6)
Q Consensus         2 t    2 (6)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


Done!
