Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:08:50 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_43A50_43Q50_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q04467_39_Mito_ref_sig5_130      0.2 1.4E+02    0.32    2.4   0.0    1    6-6      43-43  (44)
  2 P06576_47_Mito_IM_ref_sig5_130   0.2 1.4E+02    0.33    5.6   0.0    2    6-7      44-45  (52)
  3 P07926_68_MitoM_ref_sig5_130     0.2 1.4E+02    0.33    6.0   0.0    2    6-7      68-69  (73)
  4 P00430_16_IM_ref_sig5_130        0.2 1.5E+02    0.34    4.6   0.0    2    2-3      20-21  (21)
  5 P10719_46_Mito_IM_ref_sig5_130   0.2 1.5E+02    0.34    5.5   0.0    2    6-7      44-45  (51)
  6 P36526_16_Mito_ref_sig5_130      0.2 1.5E+02    0.34    4.4   0.0    3    4-6      17-19  (21)
  7 P07471_12_IM_ref_sig5_130        0.2 1.5E+02    0.34    4.3   0.0    3    4-6       8-10  (17)
  8 P00829_48_Mito_IM_ref_sig5_130   0.2 1.5E+02    0.35    5.5   0.0    2    6-7      44-45  (53)
  9 P29147_46_MM_ref_sig5_130        0.2 1.5E+02    0.35    2.9   0.0    6    2-7      46-51  (51)
 10 Q9UGC7_26_Mito_ref_sig5_130      0.2 1.5E+02    0.35    1.5   0.0    5    2-6      27-31  (31)
 11 P10612_39_MitoM_ref_sig5_130     0.2 1.5E+02    0.35    3.3   0.0    3    4-6       2-4   (44)
 12 P28834_11_Mito_ref_sig5_130      0.2 1.6E+02    0.36    4.2   0.0    3    4-6       2-4   (16)
 13 P11325_9_MM_ref_sig5_130         0.2 1.6E+02    0.37    4.0   0.0    3    4-6       2-4   (14)
 14 P83484_51_Mito_IM_ref_sig5_130   0.2 1.6E+02    0.37    4.2   0.0    3    4-6      45-47  (56)
 15 P99028_13_IM_ref_sig5_130        0.2 1.7E+02    0.38    4.2   0.0    4    3-6       4-7   (18)
 16 Q25417_9_Mito_ref_sig5_130       0.2 1.7E+02    0.38    4.0   0.0    2    4-5       9-10  (14)
 17 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.2 1.7E+02    0.39    1.9   0.0    1    4-4      24-24  (26)
 18 P26969_86_Mito_ref_sig5_130      0.2 1.7E+02    0.39    6.1   0.0    3    4-6       1-3   (91)
 19 P36528_19_Mito_ref_sig5_130      0.2 1.7E+02    0.39    4.5   0.0    3    4-6       7-9   (24)
 20 P08165_32_MM_ref_sig5_130        0.2 1.7E+02    0.39    2.4   0.0    2    3-4      36-37  (37)

No 1  
>Q04467_39_Mito_ref_sig5_130
Probab=0.19  E-value=1.4e+02  Score=2.40  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 R    6 (7)
Q Consensus         6 r    6 (7)
                      |
T Consensus        43 ~   43 (44)
T signal           43 R   43 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)


No 2  
>P06576_47_Mito_IM_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=5.60  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 RD    7 (7)
Q Consensus         6 rd    7 (7)
                      ||
T Consensus        44 Rd   45 (52)
T signal           44 RD   45 (52)
T 130              44 GD   45 (52)
T 148              46 RD   47 (53)
T 147              51 RD   52 (59)
T 112              37 RS   38 (44)
T 131              37 RG   38 (44)
T 135              36 RD   37 (43)
T 151              41 RD   42 (48)
T 143              49 RD   50 (57)
T 144              44 RN   45 (52)


No 3  
>P07926_68_MitoM_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=5.96  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 RD    7 (7)
Q Consensus         6 rd    7 (7)
                      ||
T Consensus        68 RD   69 (73)
T signal           68 RD   69 (73)
T 10               77 RD   78 (82)
T 14               56 --   55 (55)
T 22               38 --   37 (37)
T 9                62 CD   63 (67)
T 5                69 RD   70 (74)
T 6                63 RD   64 (68)
T 2                79 RD   80 (84)
T 3                79 RD   80 (84)
T 53               35 KD   36 (40)


No 4  
>P00430_16_IM_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=4.58  Aligned_cols=2  Identities=100%  Similarity=1.199  Sum_probs=0.0

Q 5_Mustela         2 EE    3 (7)
Q Consensus         2 ee    3 (7)
                      ||
T Consensus        20 ee   21 (21)
T signal           20 EE   21 (21)
T 13               20 EE   21 (21)
T 14               20 ED   21 (21)
T 15               20 ED   21 (21)
T 16               20 EE   21 (21)
T 24               20 EE   21 (21)
T 25               20 AE   21 (21)
T 35               20 EE   21 (21)
T 44               20 EE   21 (21)
T 6                17 --   16 (16)


No 5  
>P10719_46_Mito_IM_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=5.54  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 RD    7 (7)
Q Consensus         6 rd    7 (7)
                      ||
T Consensus        44 Rd   45 (51)
T signal           44 RD   45 (51)
T 130              44 GD   45 (50)
T 148              46 RD   47 (52)
T 110              37 RS   38 (44)
T 131              36 RD   37 (43)
T 135              37 RG   38 (44)
T 151              41 RD   42 (48)
T 149              51 RD   52 (58)
T 144              49 RD   50 (56)
T 145              44 RN   45 (51)


No 6  
>P36526_16_Mito_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=4.44  Aligned_cols=3  Identities=67%  Similarity=0.955  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |-|
T Consensus        17 LtR   19 (21)
T signal           17 LTR   19 (21)
T 11               17 LTR   19 (21)
T 13               17 LTR   19 (21)
T 14               17 LTR   19 (21)
T 18               16 LTR   18 (20)
T 20               17 LLR   19 (21)
T 5                17 LRR   19 (21)
T 7                17 LRR   19 (21)
T 8                17 LRR   19 (21)
T 9                13 LKR   15 (17)


No 7  
>P07471_12_IM_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=4.35  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.|
T Consensus         8 LsR   10 (17)
T signal            8 LSR   10 (17)
T 8                 8 LNR   10 (17)
T 15                8 LSR   10 (17)
T 17                8 LGR   10 (17)
T 18                8 LSR   10 (17)
T 42                8 LNR   10 (17)
T 3                 8 LSR   10 (17)


No 8  
>P00829_48_Mito_IM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=5.55  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 RD    7 (7)
Q Consensus         6 rd    7 (7)
                      ||
T Consensus        44 Rd   45 (53)
T signal           44 RD   45 (53)
T 125              44 GD   45 (52)
T 111              37 RS   38 (45)
T 131              37 RG   38 (45)
T 136              36 RD   37 (44)
T 152              41 RD   42 (49)
T 149              46 RD   47 (54)
T 142              49 RD   50 (58)
T 151              51 RD   52 (60)
T 147              44 RN   45 (53)


No 9  
>P29147_46_MM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=2.91  Aligned_cols=6  Identities=17%  Similarity=0.053  Sum_probs=0.0

Q 5_Mustela         2 EELERD    7 (7)
Q Consensus         2 eelerd    7 (7)
                      ...+.|
T Consensus        46 ~~~~~~   51 (51)
T signal           46 YTSQAD   51 (51)
T 77_Aplysia       46 VVVIR-   50 (50)
T 61_Papio         46 SEVTG-   50 (50)
T 19_Tupaia        46 ASEVD-   50 (50)
T 80_Tribolium     46 WDVLD-   50 (50)
T 79_Apis          46 SQTSS-   50 (50)
T 74_Branchiosto   46 CRFLP-   50 (50)
T 54_Canis         46 GSRRA-   50 (50)
T 39_Nomascus      46 PHRGG-   50 (50)


No 10 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=1.53  Aligned_cols=5  Identities=0%  Similarity=-0.309  Sum_probs=0.0

Q 5_Mustela         2 EELER    6 (7)
Q Consensus         2 eeler    6 (7)
                      +...+
T Consensus        27 ~~~~~   31 (31)
T signal           27 SSGSP   31 (31)
T 119_Populus      27 TPSR-   30 (30)
T 107_Phytophtho   27 DGSF-   30 (30)
T 101_Setaria      27 EKDM-   30 (30)
T 82_Anopheles     27 DDKI-   30 (30)
T 69_Chrysemys     27 AAYK-   30 (30)
T 55_Geospiza      27 QQIV-   30 (30)


No 11 
>P10612_39_MitoM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=3.28  Aligned_cols=3  Identities=67%  Similarity=0.966  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.+
T Consensus         2 l~~    4 (44)
T signal            2 LAR    4 (44)
T 73_Anas           2 RRG    4 (43)
T 71_Monodelphis    2 VPT    4 (43)
T 67_Ornithorhyn    2 LEG    4 (43)
T 65_Anolis         2 LEA    4 (43)
T 63_Danio          2 ARW    4 (43)
T 52_Xenopus        2 LLL    4 (43)
T 50_Tupaia         2 ALN    4 (43)
T 48_Melopsittac    2 PIP    4 (43)
T 47_Falco          2 LAR    4 (43)


No 12 
>P28834_11_Mito_ref_sig5_130
Probab=0.17  E-value=1.6e+02  Score=4.21  Aligned_cols=3  Identities=67%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.|
T Consensus         2 lnr    4 (16)
T signal            2 LNR    4 (16)


No 13 
>P11325_9_MM_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=4.04  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.|
T Consensus         2 lsr    4 (14)
T signal            2 LSR    4 (14)


No 14 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=4.24  Aligned_cols=3  Identities=67%  Similarity=0.877  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.|
T Consensus        45 ~~~   47 (56)
T signal           45 LGR   47 (56)
T 34_Volvox        39 ---   38 (38)
T 134_Guillardia   39 ---   38 (38)
T 152_Trichinell   39 ---   38 (38)
T 159_Ixodes       39 ---   38 (38)
T 41_Galdieria     39 ---   38 (38)
T 112_Tribolium    39 ---   38 (38)
T 117_Ceratitis    39 ---   38 (38)
T 111_Musca        39 ---   38 (38)
T 39_Selaginella   39 ---   38 (38)


No 15 
>P99028_13_IM_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=4.23  Aligned_cols=4  Identities=75%  Similarity=0.659  Sum_probs=0.0

Q 5_Mustela         3 ELER    6 (7)
Q Consensus         3 eler    6 (7)
                      |-||
T Consensus         4 ede~    7 (18)
T signal            4 EDER    7 (18)
T 23                4 EDEQ    7 (18)
T 42                4 EDER    7 (18)
T 3                 4 EDEQ    7 (18)
T 14                4 EDER    7 (18)
T 24                4 EDKL    7 (18)
T 59                4 MEEK    7 (17)
T 62                4 EEEQ    7 (18)


No 16 
>Q25417_9_Mito_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=3.98  Aligned_cols=2  Identities=100%  Similarity=1.265  Sum_probs=0.0

Q 5_Mustela         4 LE    5 (7)
Q Consensus         4 le    5 (7)
                      ||
T Consensus         9 le   10 (14)
T signal            9 LE   10 (14)


No 17 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=1.88  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         4 L    4 (7)
Q Consensus         4 l    4 (7)
                      |
T Consensus        24 ~   24 (26)
T signal           24 L   24 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)


No 18 
>P26969_86_Mito_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=6.06  Aligned_cols=3  Identities=67%  Similarity=1.232  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      .||
T Consensus         1 MER    3 (91)
T signal            1 MER    3 (91)
T 198               1 MER    3 (64)
T 196               1 MER    3 (92)
T 193               1 MER    3 (93)
T 195               1 MER    3 (83)
T 194               1 MER    3 (77)
T 191               1 MER    3 (69)
T 188               1 MER    3 (73)
T 189               1 MER    3 (79)
T 185               1 MER    3 (43)


No 19 
>P36528_19_Mito_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=4.54  Aligned_cols=3  Identities=67%  Similarity=1.099  Sum_probs=0.0

Q 5_Mustela         4 LER    6 (7)
Q Consensus         4 ler    6 (7)
                      |.|
T Consensus         7 lkr    9 (24)
T signal            7 LKR    9 (24)


No 20 
>P08165_32_MM_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=2.37  Aligned_cols=2  Identities=50%  Similarity=0.335  Sum_probs=0.0

Q 5_Mustela         3 EL    4 (7)
Q Consensus         3 el    4 (7)
                      |.
T Consensus        36 ~~   37 (37)
T signal           36 EQ   37 (37)
T 117_Pyrenophor   30 --   29 (29)
T 89_Acyrthosiph   30 --   29 (29)
T 86_Musca         30 --   29 (29)
T 72_Nematostell   30 --   29 (29)
T 37_Myotis        30 --   29 (29)
T 34_Pongo         30 --   29 (29)


Done!
