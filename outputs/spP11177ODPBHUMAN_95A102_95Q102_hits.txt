Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:09 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_95A102_95Q102_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P41563_27_Mito_ref_sig5_130      0.9      28   0.063    7.0   0.0    3    2-4      14-16  (32)
  2 P12787_37_IM_ref_sig5_130        0.8      29   0.067    7.5   0.0    1    3-3       3-3   (42)
  3 P26360_45_Mito_IM_ref_sig5_130   0.8      32   0.073    7.7   0.0    1    6-6       3-3   (50)
  4 P10817_9_IM_ref_sig5_130         0.7      33   0.076    5.9   0.0    1    3-3      14-14  (14)
  5 P14408_41_Mito_Cy_ref_sig5_130   0.7      34   0.079    5.8   0.0    1    1-1       1-1   (46)
  6 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.7      35    0.08    3.5   0.0    1    1-1       1-1   (26)
  7 P32387_45_Mito_ref_sig5_130      0.7      38   0.087    7.5   0.0    1    3-3      22-22  (50)
  8 P05166_28_MM_ref_sig5_130        0.6      41   0.094    6.7   0.0    1    5-5       4-4   (33)
  9 P23786_25_IM_ref_sig5_130        0.6      41   0.095    6.7   0.0    1    2-2      16-16  (30)
 10 P12234_49_IM_ref_sig5_130        0.6      42   0.097    4.8   0.0    1    1-1       1-1   (54)
 11 P18886_25_IM_ref_sig5_130        0.6      43   0.099    6.6   0.0    1    2-2      16-16  (30)
 12 P35914_27_MM_Pe_ref_sig5_130     0.6      46     0.1    6.6   0.0    1    3-3      14-14  (32)
 13 P35571_42_Mito_ref_sig5_130      0.5      47    0.11    4.8   0.0    1    3-3      47-47  (47)
 14 P11183_39_Mito_ref_sig5_130      0.5      48    0.11    6.9   0.0    1    3-3      44-44  (44)
 15 P09671_24_MM_ref_sig5_130        0.5      49    0.11    6.1   0.0    1    3-3      18-18  (29)
 16 P07471_12_IM_ref_sig5_130        0.5      52    0.12    5.6   0.0    1    3-3      17-17  (17)
 17 P36967_16_Mito_ref_sig5_130      0.5      55    0.13    5.8   0.0    1    4-4       4-4   (21)
 18 P04179_24_MM_ref_sig5_130        0.4      58    0.13    5.9   0.0    1    3-3      18-18  (29)
 19 Q9N2I8_21_Mito_ref_sig5_130      0.4      61    0.14    6.0   0.0    1    4-4      22-22  (26)
 20 Q01992_33_MM_ref_sig5_130        0.4      62    0.14    6.5   0.0    1    3-3      38-38  (38)

No 1  
>P41563_27_Mito_ref_sig5_130
Probab=0.87  E-value=28  Score=7.03  Aligned_cols=3  Identities=67%  Similarity=1.199  Sum_probs=1.3

Q 5_Mustela         2 VGA    4 (7)
Q Consensus         2 vga    4 (7)
                      +||
T Consensus        14 ~GA   16 (32)
T signal           14 LGA   16 (32)
T 20               14 LGA   16 (30)
T 42               14 VGG   16 (32)
T 45               14 VGA   16 (31)
T 46               13 MGA   15 (30)
T 61               14 AGA   16 (31)
T 76               14 MGG   16 (30)
T 84               14 VGA   16 (31)
T 94               14 IGA   16 (31)
T 114              14 LGA   16 (31)
Confidence            444


No 2  
>P12787_37_IM_ref_sig5_130
Probab=0.83  E-value=29  Score=7.48  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      |
T Consensus         3 g    3 (42)
T signal            3 A    3 (42)
T 122               3 G    3 (46)
T 99                3 G    3 (44)
T 100               3 G    3 (43)
T 101               3 A    3 (41)
T 103               3 A    3 (48)
T 108               1 -    0 (36)
T 129               3 G    3 (46)
T 130               3 G    3 (39)
T 78                1 -    0 (29)
Confidence            2


No 3  
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.77  E-value=32  Score=7.69  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         6 M.    6 (7)
Q Consensus         6 m.    6 (7)
                      | 
T Consensus         3 m.    3 (50)
T signal            3 M.    3 (50)
T 181               3 M.    3 (45)
T 183               3 M.    3 (48)
T 170               3 M.    3 (51)
T 171               3 M.    3 (49)
T 182               3 M.    3 (50)
T 174               3 M.    3 (47)
T 178               3 Ma    4 (44)
T 177               3 Ma    4 (50)
T 168               3 S.    3 (52)
Confidence            3 


No 4  
>P10817_9_IM_ref_sig5_130
Probab=0.74  E-value=33  Score=5.85  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      -
T Consensus        14 k   14 (14)
T signal           14 K   14 (14)
T 14               14 K   14 (14)
T 18               14 K   14 (14)
T 22               14 K   14 (14)
T 23               14 T   14 (14)
T 46               14 K   14 (14)
T 4                14 K   14 (14)
Confidence            0


No 5  
>P14408_41_Mito_Cy_ref_sig5_130
Probab=0.71  E-value=34  Score=5.78  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (7)
Q Consensus         1 a    1 (7)
                      -
T Consensus         1 M    1 (46)
T signal            1 M    1 (46)
T 132_Malassezia    1 M    1 (25)
T 76_Musca          1 M    1 (25)
T 49_Anas           1 M    1 (25)
T cl|CABBABABA|1    1 M    1 (25)
Confidence            0


No 6  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.71  E-value=35  Score=3.49  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (7)
Q Consensus         1 a    1 (7)
                      -
T Consensus         1 M    1 (26)
T signal            1 M    1 (26)
T 60                1 M    1 (26)
T 108_Galdieria     1 M    1 (23)
T 104_Cyanidiosc    1 M    1 (23)
T 102_Acanthamoe    1 M    1 (23)
T 78_Musca          1 M    1 (23)
T 76_Aplysia        1 M    1 (23)
T 74_Branchiosto    1 M    1 (23)
T 57_Loxodonta      1 M    1 (23)
T 6_Orcinus         1 M    1 (23)
Confidence            0


No 7  
>P32387_45_Mito_ref_sig5_130
Probab=0.66  E-value=38  Score=7.48  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      |
T Consensus        22 g   22 (50)
T signal           22 G   22 (50)
Confidence            3


No 8  
>P05166_28_MM_ref_sig5_130
Probab=0.61  E-value=41  Score=6.74  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         5 A    5 (7)
Q Consensus         5 a    5 (7)
                      |
T Consensus         4 A    4 (33)
T signal            4 A    4 (33)
T 86                4 A    4 (32)
T 49                4 A    4 (34)
T 62                4 A    4 (36)
T 64                4 A    4 (33)
T 67                4 A    4 (35)
T 70                4 A    4 (39)
T 74                4 A    4 (39)
T 89                4 A    4 (33)
T 65                4 A    4 (37)
Confidence            1


No 9  
>P23786_25_IM_ref_sig5_130
Probab=0.61  E-value=41  Score=6.65  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         2 V    2 (7)
Q Consensus         2 v    2 (7)
                      +
T Consensus        16 ~   16 (30)
T signal           16 V   16 (30)
T 101              16 V   16 (30)
T 104              16 L   16 (30)
T 108              16 F   16 (30)
T 109              17 I   17 (31)
T 117              16 V   16 (30)
T 119              16 V   16 (30)
T 59               20 V   20 (35)
Confidence            3


No 10 
>P12234_49_IM_ref_sig5_130
Probab=0.59  E-value=42  Score=4.84  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (7)
Q Consensus         1 a    1 (7)
                      -
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 124_Salpingoec    1 M    1 (41)
T 115_Sorghum       1 M    1 (41)
T 97_Branchiosto    1 L    1 (41)
T 80_Trichoplax     1 M    1 (41)
T 66_Ciona          1 M    1 (41)
T 62_Geospiza       1 M    1 (41)
T 58_Anas           1 K    1 (41)
T 57_Falco          1 M    1 (41)
T 54_Ficedula       1 P    1 (41)
Confidence            0


No 11 
>P18886_25_IM_ref_sig5_130
Probab=0.58  E-value=43  Score=6.60  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.3

Q 5_Mustela         2 V    2 (7)
Q Consensus         2 v    2 (7)
                      +
T Consensus        16 ~   16 (30)
T signal           16 L   16 (30)
T 84               16 L   16 (30)
T 87               16 A   16 (30)
T 102              17 I   17 (31)
T 114              16 V   16 (30)
T 92               16 F   16 (30)
T 94               16 V   16 (30)
T 48               20 V   20 (35)
Confidence            3


No 12 
>P35914_27_MM_Pe_ref_sig5_130
Probab=0.56  E-value=46  Score=6.58  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      |
T Consensus        14 g   14 (32)
T signal           14 G   14 (32)
T 49               15 G   15 (33)
T 68               14 G   14 (32)
T 51               14 -   13 (26)
T 52               13 -   12 (25)
T 75               14 G   14 (32)
T 76               14 G   14 (32)
T 77               14 G   14 (32)
T 78               14 G   14 (32)
T 59               14 -   13 (29)
Confidence            2


No 13 
>P35571_42_Mito_ref_sig5_130
Probab=0.54  E-value=47  Score=4.82  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      -
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


No 14 
>P11183_39_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.93  Aligned_cols=1  Identities=0%  Similarity=-1.726  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      =
T Consensus        44 F   44 (44)
T signal           44 F   44 (44)
T 150              49 Y   49 (49)
T 151              51 F   51 (51)
T 125              42 F   42 (42)
T 128              42 F   42 (42)
T 131              42 F   42 (42)
T 97               31 F   31 (31)
T 101              31 F   31 (31)
T 105              31 F   31 (31)
Confidence            0


No 15 
>P09671_24_MM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.14  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G..    3 (7)
Q Consensus         3 g..    3 (7)
                      |  
T Consensus        18 g..   18 (29)
T signal           18 G..   18 (29)
T 128              20 Swk   22 (33)
T 134              20 G..   20 (31)
T 138              20 T..   20 (31)
T 124              19 A..   19 (30)
T 145              18 G..   18 (29)
T 149              20 A..   20 (31)
T 156              18 G..   18 (29)
T 113              19 A..   19 (30)
T 114              18 G..   18 (29)
Confidence            2  


No 16 
>P07471_12_IM_ref_sig5_130
Probab=0.49  E-value=52  Score=5.63  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      -
T Consensus        17 k   17 (17)
T signal           17 K   17 (17)
T 8                17 K   17 (17)
T 15               17 K   17 (17)
T 17               17 T   17 (17)
T 18               17 K   17 (17)
T 42               17 K   17 (17)
T 3                17 K   17 (17)
Confidence            0


No 17 
>P36967_16_Mito_ref_sig5_130
Probab=0.46  E-value=55  Score=5.84  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (7)
Q Consensus         4 a    4 (7)
                      |
T Consensus         4 a    4 (21)
T signal            4 A    4 (21)
Confidence            3


No 18 
>P04179_24_MM_ref_sig5_130
Probab=0.45  E-value=58  Score=5.95  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G.    3 (7)
Q Consensus         3 g.    3 (7)
                      | 
T Consensus        18 g.   18 (29)
T signal           18 G.   18 (29)
T 162              18 G.   18 (29)
T 127              20 Rq   21 (32)
T 133              20 G.   20 (31)
T 135              20 A.   20 (31)
T 139              20 T.   20 (31)
T 118              11 P.   11 (22)
T 123              11 A.   11 (22)
T 113              19 A.   19 (30)
T 117              18 -.   17 (26)
Confidence            2 


No 19 
>Q9N2I8_21_Mito_ref_sig5_130
Probab=0.42  E-value=61  Score=5.98  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A.......    4 (7)
Q Consensus         4 a.......    4 (7)
                      |       
T Consensus        22 A.......   22 (26)
T signal           22 A.......   22 (26)
T 69               25 Aargaa..   30 (34)
T 78               25 Aanav...   29 (34)
T 5                25 A.......   25 (29)
T 34               32 Aarsaa..   37 (41)
T 60               25 Aagaasaa   32 (36)
T 25               25 Aagaaa..   30 (34)
T 88               25 Aaggaa..   30 (34)
T 46               25 A.......   25 (25)
Confidence            2       


No 20 
>Q01992_33_MM_ref_sig5_130
Probab=0.42  E-value=62  Score=6.47  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         3 G    3 (7)
Q Consensus         3 g    3 (7)
                      =
T Consensus        38 W   38 (38)
T signal           38 W   38 (38)
T 180              38 W   38 (38)
T 130              37 W   37 (37)
T 102              36 W   36 (36)
T 165              36 W   36 (36)
T 168              36 W   36 (36)
T 182              36 W   36 (36)
T 186              36 W   36 (36)
T 164              36 W   36 (36)
T 169              36 W   36 (36)
Confidence            0


Done!
