import os,subprocess
from Bio.Blast import NCBIStandalone,NCBIXML,NCBIWWW
from Bio import Entrez,SeqIO
Entrez.email = "taoqibao-2008@hotmail.com"
import time
import urllib.error
os.environ["http_proxy"]="http://proxy01.biochem.mpg.de:3128"
BLASTP="/export/bio-databases/blast/bin/blastp"
blastdbpath = '/export/bio-databases/blast/'
datapath = '/export2/susun/MitoSig/datapath/'
inputpath = '/export2/susun/MitoSig/morfsite_lite/inputs/'
outputpath = '/export2/susun/MitoSig/morfsite_lite/outputs/'


def getLinkID(gi):
    while True:
        try:
            handle=Entrez.elink(dbfrom="protein",db="taxonomy",id=gi)
            record=Entrez.read(handle,validate=False)
            taxoID=record[0]["LinkSetDb"][0]["Link"][0]["Id"]
            handle=Entrez.elink(dbfrom="protein",db="gene",id=gi)
            record=Entrez.read(handle,validate=False)
            geneID=record[0]["LinkSetDb"][0]["Link"][0]["Id"]
            break
        except urllib.error.HTTPError:
            print ('HTTPError')
            time.sleep(60)
        except RuntimeError as e:
            print (e)
            time.sleep(60)
        except IndexError:
            taxoID='NULL'
            geneID='NULL'
            break
    return taxoID,geneID
#---------------------------------------------------------------------------------
#input query fasta file
def getGIs_qblast(queryF,database="refseq_protein",evalue="1e-10",num_alignments="300",gilist="refEukarGi.txt"):    
    #hits={}
    gis=[]
    subprocess.call(BLASTP+' -db '+blastdbpath+database+' -query '+inputpath+queryF+' -evalue '+evalue+' -num_alignments '+num_alignments+' -outfmt 5 -out '+outputpath+'BlastOut -num_threads 8 -gilist '+datapath+gilist,shell=True)        
    print ("BlastOut of "+queryF+" was generated")
    record=open(outputpath+"BlastOut")
    result = NCBIXML.read(record)
    for i in result.descriptions:
        #score = str(i.score)
        #evalue = str(i.e)
        title = str(i.title)
        #acces = str(i.accession)
        gi = title.split("|")[1]
        gis.append(gi)
        #hits[gi]= [acces,score,evalue]
    return gis
    

#input id list 
#get the best hit for every species
def getBhits(gis):
    SPs = []
    BhitSps = {}
    Bhits = []
    for i in gis:
        print (i)
        try:
            handle=Entrez.elink(dbfrom="protein",db="taxonomy",id=i)
            record=Entrez.read(handle,validate=False)
            taxoID=record[0]["LinkSetDb"][0]["Link"][0]["Id"]
        except:
            continue
        if taxoID != 0:
            try:
                handle = Entrez.esummary(db = 'Taxonomy',id = taxoID)
                record = Entrez.read(handle,validate=False)
                sps = record[0]['ScientificName'].split()[0]
            except:
                continue
            if sps not in SPs:
                SPs.append(sps)
                Bhits.append(i)
                BhitSps[i] = sps            
        else:
            break
    return Bhits,BhitSps


#input Bhits
#backblast
def backBlast(Bhits,taxoID,geneID):
    #BBhits = 
    #index=Bhits.index('471402894')
    #for i in Bhits[index:]:
    BBhits = [] 
    for i in Bhits:
        if len(BBhits)<20:
            print (i)
            if i not in BBhits: 
                try: 
                    handle = Entrez.efetch(db = 'protein',id = i,rettype='fasta',retmode='text')
                    record = SeqIO.read(handle,'fasta')
                    seq = str(record.seq)
                except IndexError:
                    continue
                queryfile=open(inputpath+"queryF",'w')
                queryfile.write(">query\n"+seq+'\n')
                queryfile.close()
                print ("queryF is written")
                while True:
                    try:
                        subgis = getGIs_qblast("queryF",database="nr",evalue="10",num_alignments="2000",gilist="nrEukarGi.txt")
                        print ("backblast...")
                        break
                    except:
                        print ("try again")
                for j in subgis:   
                    subTaxo,subGene=getLinkID(j)
                    if subTaxo==taxoID:
                        if subGene==geneID:
                            BBhits.append(i)
                            print (BBhits)
                            break
                        else:
                            print ("nice try")
                            print (BBhits)
                            break
    return BBhits


#input BBhits
#get sequences and write fasta file---Orthologs of query seq
def writeFasta(BBhits,BhitSps,OUTfasta):
    ofile = open(OUTfasta,'w')
    n = 1
    if len(BBhits)==1:
        BBhits.append(BBhits[0])     #for mafft later       
    #from Bio import Entrez,SeqIO
    for i in BBhits:
        handle = Entrez.efetch(db='protein', id=i, rettype='fasta',retmode='text')
        record = SeqIO.read(handle,'fasta')
        handle.close()
        sID = record.id
        Seq = str(record.seq)
        sp = BhitSps[i]
        ofile.write('>'+str(n)+'_'+sp+' '+sID+'\n'+Seq+'\n')
        n = n+1
    ofile.close()



    

#input blastOUT or queryGI number
#checker file
def oneExe(queryF,ID):  
    print ('blasting...')
    queryGi = getGIs_qblast(queryF,database="nr",evalue="10",num_alignments="1",gilist="nrEukarGi.txt")
    OUTfasta = outputpath+ID+queryGi[0]+'_20.fasta'
    check = os.system('find '+OUTfasta)
    if check == 0:
        print ('The file '+OUTfasta+' already exist! haha')
    else:
        gis = getGIs_qblast(queryF)
        taxoID,geneID =  getLinkID(queryGi[0])
        print ('reading blast output...')
        Bhits,BhitSps = getBhits(gis)
        print ('Done with getting best hit for',len(Bhits),'species!')
        print ('back-blasting...')
        BBhits = backBlast(Bhits,taxoID,geneID) 
        print ('Done with back-blasting! There are',len(BBhits),'candidates for protein')
        print ('writing...')
        writeFasta(BBhits,BhitSps,OUTfasta)  
        print ('Done with searching orthologs of',gis[0],'! haha')
    return OUTfasta

