import os
import re
def parseFasta(infile, second = 'OFF'):
    inf = open(infile,'r')
    lines = inf.readlines()
    inf.close()
    ID = []
    site = []
    seqs = {}
    idSeqs = {}
    n = -1
    for i in lines:
        if '>' in i:
            if len(i.split(' '))>1:
                ID.append(i.split(' ')[0])
                if second == 'ON':
                    site.append(i.split(' ')[1].strip())
            else:
                ID.append(i.strip())
            n = n+1
            seqs[str(n)] = ''    
        else:
            seqs[str(n)] = seqs[str(n)]+i.strip()
    for j in range(n+1):
       idSeqs[ID[j]] = seqs[str(j)]
       if second == 'ON':
           pep = seqs[str(j)][:int(site[j])]
           nonSi = seqs[str(j)][int(site[j]):]
           idSeqs[ID[j]] = [seqs[str(j)],site[j],pep,nonSi]
           #idSeqs[ID[j]] = [seqs[str(j)],site[j]]
    return idSeqs

#input a multiple alignment, type-dictionary
def getStarter(a2m):
    for i in range(len(a2m)):
        n = 0
        for j in a2m.keys():
            if a2m[j][i] == '-':
                n = n +1
        rate = float(n)/float(len(a3m))
        if rate < 0.5:                    #less then 50% gaps in a column
            break
    return i



#read hhsearch out -o
#read hhalign out -o
def readOut(outfile,res='e'):   #res = 'n','s','l','e'
    file = open(outfile,'r')
    lines = file.readlines()
    file.close()
    hits = []
    for i in lines:
        if '>' in i:
            tmp = lines.index(i)
            hit = i+lines[tmp+1]
            hits.append(hit)
    eV = float(0)
    if len(hits)==0:
        eV = -1000
    else:
        for j in hits:
            e = float(j.split()[2].split('=')[1])
            eV = eV-e
            #sc = float(j.split()[3].split('=')[1])
            #score = score+sc
    result = eV
    return result



def corrCS(a2m,st,sto):
    for i in a2m.keys():
        if '>1_' in i:
            qID = i
    aSeq = a2m[qID]
    qSeq = aSeq.replace('.','').replace('-','')
    freg = aSeq[st:st+sto].replace('.','').replace('-','')
    if freg!='':
        L = len(freg)
        nst=st-aSeq[:st].count('.')-aSeq[:st].count('-')
        nsto=nst+L
    else:
        nst=st-aSeq[:st].count('.')-aSeq[:st].count('-')
        nsto = nst
    return nst, nsto


def corrCut(a2m,ST,STO):
    for i in a2m.keys():
        if '>1_' in i:
            qID = i
    aSeq = a2m[qID]
    qSeq = aSeq.replace('.','').replace('-','')
    regex = re.compile('[A-z]')
    n = -1
    count = -1
    if ST>STO:
        print ('ST should always smaller than STO! Sites exchanged.')
        tmp1 = ST
        tmp2 = STO
        ST = tmp2
        STO = tmp1
    if ST<0 or ST>=len(qSeq):
        print ('ST IS OUT OF RANGE!')
        nST = 0
    if STO<0 or STO>=len(qSeq):
        print ('STO IS OUT OF RANGE')
        nSTO = len(qSeq)
    if ST==STO:
        print ('Input is not a region! Fragmentation starts from 0.')
        ST = 0
    for j in aSeq:
        n = n+1
        if regex.search(j) is not None:
            count = count +1
            if count == ST:
                nST = n
            if count == STO:
                nSTO = n
                break
    return nST,nSTO

def mainSearch(infile,qPath,opath,db,ST,STO,iD,cri='e'):   #input multiple seqs a2m file for one query sequence      
    a2m = parseFasta(infile)
    #fileN = iD.split('/')[-1]
    l = len(a2m[list(a2m)[0]])
    score = {}                           #store the fragments and their sum-E score
    nST,nSTO = corrCut(a2m,ST,STO)
    for st in range (nST,nSTO):            #100-for mTS, the region of cutting and sliding, user defined
        #for sto in range(6,22):          #the length of the fragments, could be adapted
        for sto in range(6,8):
            file = open(qPath+'cut.a2m','w')
            for i in a2m.keys():
                file.write(i+'\n')
                file.write(a2m[i][st:st+sto]+'\n')
            file.close()
            nst,nsto = corrCS(a2m,st,sto)
            #os.system('addss.pl '+qPath+'cut.a2m')
            os.system('hhmake -i '+qPath+'cut.a2m -M 50')  #-M [0,100] use FASTA: columns with fewer than X% gaps are match states
            #os.system('hhsearch -i '+qPath+'cutN.hhm -d '+db+' -p 0 -e 100 -seq 10 -Z 20 -B 20 -o '+qPath+fileN+'_'+str(st)+'A'+str(st+sto)+'_'+str(nst)+'Q'+str(nsto)+'_hits.txt')
            os.system('hhsearch -i '+qPath+'cut.hhm -d '+db+' -p 0 -e 100 -seq 10 -Z 20 -B 20 -o '+opath+iD+'_'+str(st)+'A'+str(st+sto)+'_'+str(nst)+'Q'+str(nsto)+'_hits.txt')
            totalE = readOut(opath+iD+'_'+str(st)+'A'+str(st+sto)+'_'+str(nst)+'Q'+str(nsto)+'_hits.txt',res='e')
            score[str(st)+'A'+str(st+sto)+'_'+str(nst)+'Q'+str(nsto)] = -totalE   
    return score



#su sun2014
