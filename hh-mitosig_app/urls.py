from django.urls import path, re_path
from django.conf.urls import url

from . import views


app_name = 'hhmitosig_app'
urlpatterns = [
    path('', views.search, name='index'),
    path('search', views.search, name='search'),
    path('guide', views.guide, name='guide'),
    path('guidefirstInput', views.guideFirstInput, name='guideFirstInput'),
    path('guidesecondInput', views.guideSecondInput, name='guideSecondInput'),
    path('about', views.about, name='about'),
    path('contact', views.contact, name='contact'),
    path('tests', views.tests, name='tests'),
    path('running', views.running, name='running'),
    path('queued', views.queued, name='queueud'),
    path('file_not_found', views.file_not_found, name='file_not_found'),
    path('output_not_found', views.output_not_found, name='output_not_found')

#    path('', views.IndexView.as_view(), name='index'),
#    path('search', views.SearchView.as_view(), name='search'),
#    path('guide', views.GuideView.as_view(), name='guide'),
#    path('about', views.AboutView.as_view(), name='about'),
#    path('contact', views.ContactView.as_view(), name='contact'),
#    path('tests', views.TestsView.as_view(), name='tests'),
#    path('running', views.RunningView.as_view(), name='running'),
#    path('queued', views.QueuedView.as_view(), name='queueud'),
#    path('file_not_found', views.NotFoundView.as_view(), name='file_not_found'),
#    path('output_not_found', views.NotFoundView.as_view(), name='output_not_found')
]

handler404 = 'custom_404'
