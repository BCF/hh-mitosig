from django.apps import AppConfig


class Hhmitosig_appConfig(AppConfig):
    name = 'hhmitosig_app'
