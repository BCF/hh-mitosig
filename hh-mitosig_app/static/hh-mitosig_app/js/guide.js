$(document).ready(function () {
    var offset=parseInt($("#header").outerHeight())+parseInt($(".text-section").css("margin-top").replace(/px/g,''))+3;
    $("body").scrollspy({offset: offset, target: ".sidebar-left"});
    $("#guide_nav_menu_inner_div").html($("#guide_nav_normal").html());
    $(".guide-pseudo-button").on("click", function(e) {
        e.preventDefault();
        var href=$(this).attr("href");
        if (href!="#") {
            var offsetFinal=$(href).offset().top-offset+10;
            $(window).scrollTop(offsetFinal);
        }
    });
    $("#inner_router").trigger("click");
});
