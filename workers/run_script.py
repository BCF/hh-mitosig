from __future__ import absolute_import
import sys, os, time, pickle, shutil
from getpass import getpass
from subprocess import Popen, PIPE
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from morfsite import settings
from morfsite.config import SERVER_URL, ADMIN_EMAIL, PYTHON_EXE, HHSUITE_ROOT,\
NETSURFP_ROOT, CORES
##
from morfsite.custom_methods import connect_status_database, send_mail,\
send_mail_wrapped, generate_error_html, clear_record
##

MAIL_PASSWORD = getpass("Enter the mailbox password: ")

def finish_run(tag, success = True):
    """
    Method for clean finishing of the task
    """
    #if success:
        #shutil.rmtree(BASE_DIR + 'inputs/' + tag)
    cache_dir = BASE_DIR + 'caches/' + tag
    if os.path.isdir(cache_dir):
        shutil.rmtree(cache_dir)
    clear_record(tag, 'heavy')
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    if success:
        print('{}: Completed the task {} successfully'.format(curtime, tag))
    else:
        print('{}: Finished the task {} with an error'.format(curtime, tag))

def process_error(bytes_message, output_html, log_file, user_email, tag,
                  job_name_):
    """
    Method for processing error occured during a run
    """
    with open(log_file, 'ab') as log:
        log.write(bytes_message)
    message = bytes_message.decode('utf-8')
    if message.startswith('Task error:'):
        message = message.split(': ', 1)[1].split('-' * 10, 1)[0]
    else:
        text = 'A problem with the task {} occured.\n'.format(tag)
        text += message
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, text,
                          'Server Problem')
        message = 'Internal server error.\n\n\n' +\
        'The server administration has been notified.\n' +\
        'Necessary actions will be taken to fix the issue as soon as possible.'
        ##
    if user_email:
        subject = 'Your Job has Failed'
        text = 'Your job{} has failed. The following error occured:\n\n'
        text = text.format(job_name_)
        text += message
        send_mail_wrapped(log_file, MAIL_PASSWORD, user_email, text, subject)
    with open(output_html, 'w') as ofile:
        ofile.write(generate_error_html(message))
    finish_run(tag, success = False)

def run_script(args):
    """
    Method for running the main script
    """

    # Preparing for start
    starttime = time.strftime('%Y-%m-%d %H:%M:%S')
    mode, text_input, second_form_file, startsite, stopsite, tag = pickle.loads(args)
    ##
    print('{}: Received the task {}'.format(starttime, tag))
    job_name_= ' "{}"'.format(job_name) if job_name else ''

    # Setting paths
    input_fasta_dir = BASE_DIR + 'inputs/{}/input_fasta/'.format(tag)
    cache_dir = BASE_DIR + 'caches/{}/'.format(tag)
    raw_input_fasta_file = BASE_DIR + 'inputs/{}/input.fasta'.format(tag)
    script_name = '../core/'
    output_html_dir = BASE_DIR + 'outputs/{}/'.format(tag)
    output_html = output_html_dir + 'results.html'
    output_file = output_html_dir + 'results.txt'
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    if mode != 'SECOND_MODE':
        script_name += 'mitoSig_mTP.py'
    else:
        script_name += 'mitoSig_nmTP.py'

    # Setting environment
    os.environ['PATH'] += ':{}'.format(NETSURFP_ROOT)
    os.environ['HHLIB'] = HHSUITE_ROOT
    os.environ['hhalign'] = HHSUITE_ROOT + '/applications/hh-suite/bin/hhalign'
    
    # Updating waiting HTML
    template = BASE_DIR + 'morfeus_app/templates/morfeus_app/running.html'
    target = output_html_dir + 'results.html'
    shutil.copy(template, target)
    
    # Copying the static elements
    source = BASE_DIR + 'morfeus_app/static/morfeus_app/img/logo.svg'
    target = output_html_dir + 'logo.svg'
    shutil.copy(source, target)
    #suffix = '_pw' if mode == 'PROTEOME' else ''
    #source = BASE_DIR +\
    #'morfeus_app/static/morfeus_app/js/results{}.min.js'.format(suffix)
    ##
    #target = output_html_dir + 'results.min.js'
    #shutil.copy(source, target)
    #source = BASE_DIR +\
    #'morfeus_app/static/morfeus_app/css/results{}.min.css'.format(suffix)
    ##
    #target = output_html_dir + 'results.min.css'
    #shutil.copy(source, target)
        
    header += '<br />Job ID: ' + tag + '\n'
    header += '<br />Submitted: ' + submit_time + '\n'
    
    # Running main tool
    with open(log_file, 'a') as log:
        if mode != 'SECOND_MODE':
            c=Popen([script_name,'-q',raw_input_fasta_file,'-o',output_file])
            ##
        else:
            c = Popen([script_name,'-q',raw_input_fasta_file,'-o',output_file])
            ##
    stdout, stderr = c.communicate()
    if stderr:
        process_error(stderr, output_html, log_file, user_email, tag, job_name_)
        return
    else:
        with open(log_file, 'a') as log:
            Popen(['unix2dos', output_html_dir + 'results.txt'], stdout = log,\
            stderr = log).communicate()
            ##
        if user_email:
            subj = 'Your Job is Complete'
            text = 'Your job{} is complete.\n\n'.format(job_name_)
            text += 'Link to the result:\n'
            text += SERVER_URL + settings.MEDIA_URL + tag + '/results.html\n\n'
            text += 'Please note that your results are stored only for 7 days\n'
            send_mail_wrapped(log_file, MAIL_PASSWORD, user_email, text, subj)
    
    #Cleaninig up and finishing
    finish_run(tag)

curtime = time.strftime('%Y-%m-%d %H:%M:%S')
print('{}: Worker is ready'.format(curtime))
while True:
    result = None
    try:
        mysql_conn = connect_status_database()
        cmd = "SELECT args FROM heavy_queue ORDER BY submitted LIMIT 1"
        cursor = mysql_conn.cursor()
        cursor.execute(cmd)
        result = cursor.fetchall()
        mysql_conn.close()
    except KeyboardInterrupt:
        sys.exit()
    if result:
        args = result[0][0]
        run_script(args)
    else:
        time.sleep(1)
        continue
